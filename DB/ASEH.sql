USE [ASEH]
GO
/****** Object:  Table [dbo].[tbArticle]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbArticle](
	[AtcID] [bigint] IDENTITY(1,1) NOT NULL,
	[AtcName] [nvarchar](300) NULL,
	[AtcName_EN] [nvarchar](300) NULL,
	[AtcDesc] [ntext] NULL,
	[AtcDesc_EN] [ntext] NULL,
	[AtcAuthor] [nvarchar](50) NULL,
	[AtcAuthor_EN] [nvarchar](50) NULL,
	[AtcInvestor] [ntext] NULL,
	[AtcInvestor_EN] [ntext] NULL,
	[AtcStatus] [int] NULL,
	[AtcEnable] [int] NULL,
	[AtcIsSign] [int] NULL,
	[AtcPopup] [int] NULL,
	[AtcSort] [int] NULL,
	[AtcSetOD] [int] NULL,
	[AtcOpenDate] [datetime] NULL,
	[AtcCloseDate] [datetime] NULL,
	[AtcPicPath] [nvarchar](256) NULL,
	[AtcFBPicPath] [nvarchar](256) NULL,
	[AtcPicDeac] [nvarchar](50) NULL,
	[AtcSEOName] [nvarchar](300) NULL,
	[AtcSEODesc] [ntext] NULL,
	[CreateUser] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[LastUpdateUser] [nvarchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
	[LgeID] [varchar](10) NULL,
	[AttID] [varchar](50) NULL,
	[AtcSubID] [varchar](50) NULL,
	[AtcTop] [int] NULL,
	[AtcView] [int] NULL,
 CONSTRAINT [PK_tbArticle] PRIMARY KEY CLUSTERED 
(
	[AtcID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbArticleClass]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbArticleClass](
	[AtsID] [varchar](50) NOT NULL,
	[AtsName] [nvarchar](50) NULL,
	[AtsSort] [int] NULL,
 CONSTRAINT [PK_tbArticleClass] PRIMARY KEY CLUSTERED 
(
	[AtsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbArticleContent]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbArticleContent](
	[AtnID] [bigint] IDENTITY(1,1) NOT NULL,
	[AtsID] [varchar](50) NULL,
	[AtnSubject] [nvarchar](500) NULL,
	[AtnText] [ntext] NULL,
	[AtnSort] [int] NULL,
	[AtnPicPath] [nvarchar](100) NULL,
	[AtnVideoPath] [nvarchar](256) NULL,
	[AtnMp4Path] [nvarchar](256) NULL,
	[LastUpdateUser] [nvarchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
	[AtcID] [bigint] NULL,
	[LgeID] [varchar](10) NULL,
 CONSTRAINT [PK_tbArticleContent] PRIMARY KEY CLUSTERED 
(
	[AtnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbArticleRelation]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbArticleRelation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[AtcID] [bigint] NULL,
	[RelAtcID] [bigint] NULL,
	[Sort] [int] NULL,
 CONSTRAINT [PK_tbArticleRelation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbArticleSub]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbArticleSub](
	[SubID] [varchar](50) NOT NULL,
	[SubName] [nvarchar](50) NULL,
	[SubName_EN] [nvarchar](50) NULL,
	[SubDesc] [ntext] NULL,
	[SubEnable] [int] NULL,
	[SubSort] [int] NULL,
	[LastUpdateUser] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
	[LgeID] [varchar](10) NULL,
 CONSTRAINT [PK_tbArticleSub] PRIMARY KEY CLUSTERED 
(
	[SubID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbArticleType]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbArticleType](
	[AttID] [varchar](50) NOT NULL,
	[AttName] [nvarchar](50) NULL,
	[AttDesc] [ntext] NULL,
	[AttEnable] [int] NULL,
	[AttSort] [int] NULL,
	[LastUpdateUser] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
	[LgeID] [varchar](10) NULL,
 CONSTRAINT [PK_tbArticleType] PRIMARY KEY CLUSTERED 
(
	[AttID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbEventRecord]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbEventRecord](
	[EvrIndex] [bigint] IDENTITY(1,1) NOT NULL,
	[CreateTime] [datetime] NULL,
	[EvrTypeID] [varchar](25) NULL,
	[EvrMsg] [nvarchar](max) NULL,
	[UseID] [varchar](50) NULL,
 CONSTRAINT [PK_tbEventRecord] PRIMARY KEY CLUSTERED 
(
	[EvrIndex] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbEventType]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbEventType](
	[EvtTypeID] [varchar](25) NOT NULL,
	[EvtType] [nvarchar](25) NULL,
	[EvtDesc] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbEventType] PRIMARY KEY CLUSTERED 
(
	[EvtTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbExceptionRecord]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbExceptionRecord](
	[ExrIndex] [bigint] IDENTITY(1,1) NOT NULL,
	[CreateTime] [datetime] NULL,
	[ExrTypeID] [varchar](25) NULL,
	[ExrMsg] [nvarchar](max) NULL,
 CONSTRAINT [PK_tbExceptionRecord] PRIMARY KEY CLUSTERED 
(
	[ExrIndex] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbExceptionType]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbExceptionType](
	[ExtTypeID] [varchar](25) NOT NULL,
	[ExtType] [nvarchar](25) NULL,
	[ExtDesc] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbExceptionType] PRIMARY KEY CLUSTERED 
(
	[ExtTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbFile]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbFile](
	[FieID] [varchar](50) NOT NULL,
	[FitID] [varchar](50) NULL,
	[OpenDate] [datetime] NULL,
	[FieYear] [int] NULL,
	[FieMonth] [int] NULL,
	[FieDay] [int] NULL,
	[FieName] [nvarchar](50) NULL,
	[FieName_EN] [nvarchar](50) NULL,
	[FieFilePath] [nvarchar](500) NULL,
	[FieFilePath_EN] [nvarchar](500) NULL,
	[FiePicPath] [nvarchar](500) NULL,
	[FiePicPath_EN] [nvarchar](500) NULL,
	[FieDesc] [ntext] NULL,
	[FieDesc_EN] [ntext] NULL,
	[FieEnable] [int] NULL,
	[FieSort] [int] NULL,
	[LastUpdateUser] [nvarchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
	[CreateUser] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[FieView] [int] NULL,
	[FieDownload] [int] NULL,
 CONSTRAINT [PK_tbVideo] PRIMARY KEY CLUSTERED 
(
	[FieID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbFileLog]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbFileLog](
	[FigID] [bigint] IDENTITY(1,1) NOT NULL,
	[FIeID] [varchar](50) NULL,
	[FigDesc] [nvarchar](50) NULL,
	[CreateTime] [datetime] NULL,
 CONSTRAINT [PK_tbFileLog] PRIMARY KEY CLUSTERED 
(
	[FigID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbFileType]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbFileType](
	[FitID] [varchar](50) NOT NULL,
	[FitName] [nvarchar](50) NULL,
	[FitDesc] [ntext] NULL,
	[FitEnable] [int] NULL,
	[FitSort] [int] NULL,
	[FitGroup] [varchar](50) NULL,
	[LastUpdateUser] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_tbVideoType] PRIMARY KEY CLUSTERED 
(
	[FitID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbLanguage]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbLanguage](
	[LgeID] [varchar](10) NOT NULL,
	[LgeName] [nvarchar](30) NULL,
	[LgeSort] [int] NULL,
 CONSTRAINT [PK_tbLanguage] PRIMARY KEY CLUSTERED 
(
	[LgeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbMedia]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbMedia](
	[MdaID] [bigint] IDENTITY(1,1) NOT NULL,
	[MdaName] [nvarchar](50) NULL,
	[MdaFileName] [nvarchar](100) NULL,
	[MdaFilePath] [nvarchar](500) NULL,
	[MdaExtension] [varchar](30) NULL,
	[MdaSize] [varchar](50) NULL,
	[MdaDesc] [ntext] NULL,
	[MdaEnable] [int] NULL,
	[MdaSort] [int] NULL,
	[CreateUser] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[LastUpdateUser] [nvarchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
	[MdtID] [varchar](50) NULL,
	[UseID] [varchar](50) NULL,
 CONSTRAINT [PK_tbMedia] PRIMARY KEY CLUSTERED 
(
	[MdaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbMediaType]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbMediaType](
	[MdtID] [varchar](50) NOT NULL,
	[MdtName] [nvarchar](50) NULL,
	[MdtDesc] [ntext] NULL,
	[MdtEnable] [int] NULL,
	[MdtSort] [int] NULL,
	[MdtGroup] [varchar](50) NULL,
	[LastUpdateUser] [nvarchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_tbMediaType] PRIMARY KEY CLUSTERED 
(
	[MdtID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbMessage]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbMessage](
	[MsgID] [varchar](50) NOT NULL,
	[MsgUserName] [nvarchar](50) NULL,
	[MsgUserTel] [nvarchar](50) NULL,
	[MsgUserEmail] [nvarchar](250) NULL,
	[MsgUserCom] [nvarchar](50) NULL,
	[MsgUserTitle] [nvarchar](50) NULL,
	[ComCountryID] [nvarchar](100) NULL,
	[MsgSubject] [nvarchar](100) NULL,
	[MsgContent] [ntext] NULL,
	[MsgFeedback] [ntext] NULL,
	[MsgDesc] [ntext] NULL,
	[MsgStatus] [varchar](10) NULL,
	[MsgFilePath] [nvarchar](100) NULL,
	[CreateUser] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[LastUpdateUser] [nvarchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_tbMessage_1] PRIMARY KEY CLUSTERED 
(
	[MsgID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbPermission]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbPermission](
	[PerID] [varchar](50) NOT NULL,
	[PerName] [nvarchar](50) NULL,
	[PerPage] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbPermission] PRIMARY KEY CLUSTERED 
(
	[PerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbPermRole]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbPermRole](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PerID] [varchar](50) NOT NULL,
	[RolID] [varchar](50) NOT NULL,
	[RUPermission] [int] NULL,
 CONSTRAINT [PK_tbPermRole] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbPicRotate]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbPicRotate](
	[ProID] [bigint] IDENTITY(1,1) NOT NULL,
	[ProName] [nvarchar](50) NULL,
	[ProName_EN] [nvarchar](50) NULL,
	[ProDesc] [ntext] NULL,
	[ProDesc_EN] [ntext] NULL,
	[ProPicPath] [varchar](100) NULL,
	[ProPicPath_M] [varchar](100) NULL,
	[ProPicDesc] [nvarchar](50) NULL,
	[ProUrl] [nvarchar](300) NULL,
	[ProUrl_EN] [nvarchar](300) NULL,
	[ProEnable] [int] NULL,
	[ProType] [varchar](10) NULL,
	[ProSort] [int] NULL,
	[ProSetOD] [int] NULL,
	[ProStartDate] [datetime] NULL,
	[ProEndDate] [datetime] NULL,
	[CreateUser] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[LastUpdateUser] [nvarchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_tbPicRotate] PRIMARY KEY CLUSTERED 
(
	[ProID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbRecipient]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbRecipient](
	[RcpID] [varchar](50) NOT NULL,
	[RcpName] [nvarchar](50) NULL,
	[RcpEmail] [nvarchar](300) NULL,
	[RcpDesc] [ntext] NULL,
	[RcpEnable] [int] NULL,
	[RcpSort] [int] NULL,
	[CreateUser] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[LastUpdateUser] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
	[LgeID] [varchar](10) NULL,
	[RcpType] [varchar](10) NULL,
 CONSTRAINT [PK_tbRecipient] PRIMARY KEY CLUSTERED 
(
	[RcpID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbRoles]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbRoles](
	[RolID] [varchar](50) NOT NULL,
	[RolName] [nvarchar](50) NULL,
	[RolEnable] [int] NULL,
 CONSTRAINT [PK_tbRoles] PRIMARY KEY CLUSTERED 
(
	[RolID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbUsers]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbUsers](
	[UseID] [varchar](50) NOT NULL,
	[UseName] [nvarchar](50) NULL,
	[UsePassword] [varchar](128) NULL,
	[UseEnable] [int] NULL,
	[UseCountry] [nvarchar](50) NULL,
	[UseZipCode] [varchar](30) NULL,
	[UseAddr] [nvarchar](500) NULL,
	[UseCell] [varchar](30) NULL,
	[UseTel] [varchar](30) NULL,
	[UseFax] [varchar](30) NULL,
	[UseEmail] [varchar](250) NULL,
	[UseComName] [nvarchar](50) NULL,
	[UseComTel] [varchar](30) NULL,
	[UseComFax] [varchar](30) NULL,
	[UseRType] [int] NULL,
	[UseRTypeDesc] [nvarchar](100) NULL,
	[UseIsActivity] [int] NULL,
	[UseIsMatch] [int] NULL,
	[LastUpdateUser] [nvarchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
	[CreateUser] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[RolID] [varchar](50) NULL,
	[VerifyKey] [varchar](50) NULL,
 CONSTRAINT [PK_tbUsers] PRIMARY KEY CLUSTERED 
(
	[UseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbWebContent]    Script Date: 2019/6/27 下午 02:05:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbWebContent](
	[WctID] [varchar](50) NOT NULL,
	[WctName] [nvarchar](50) NULL,
	[WctDesc] [ntext] NULL,
	[WctEnable] [int] NULL,
	[WctSort] [int] NULL,
	[WctPicPath] [nvarchar](256) NULL,
	[LastUpdateUser] [nvarchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
	[CreateUser] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_tbWebContent] PRIMARY KEY CLUSTERED 
(
	[WctID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tbArticle] ON 

INSERT [dbo].[tbArticle] ([AtcID], [AtcName], [AtcName_EN], [AtcDesc], [AtcDesc_EN], [AtcAuthor], [AtcAuthor_EN], [AtcInvestor], [AtcInvestor_EN], [AtcStatus], [AtcEnable], [AtcIsSign], [AtcPopup], [AtcSort], [AtcSetOD], [AtcOpenDate], [AtcCloseDate], [AtcPicPath], [AtcFBPicPath], [AtcPicDeac], [AtcSEOName], [AtcSEODesc], [CreateUser], [CreateDate], [LastUpdateUser], [LastUpdateDate], [LgeID], [AttID], [AtcSubID], [AtcTop], [AtcView]) VALUES (171, N'量子電腦 邁向商業化', N'Quantum Computer Towards Commercialization', N'人工智慧將無所不在被應用在日常生活中，智慧型晶片市場將呈現指數型成長，晶片功能與運算速度需求也愈來愈高，電腦運算晶片的設計方法、替代材料和運算架構也必須不斷改變與升級，其中量子電腦是近年來最受到矚目的議題。工研院與美國史丹福大學在今年6月的年度研討會議中，有三位來自美國矽谷產學專家創業家，來台分享矽光子與量子電腦的研究方向和觀察。', N'', N'魏依玲', N'Yi-Ling Wei', NULL, NULL, 0, 1, 0, 0, 0, 1, CAST(0x0000A93800000000 AS DateTime), CAST(0x0000A9DA00000000 AS DateTime), N'', N'', N'', N'量子電腦 邁向商業化', N'人工智慧將無所不在被應用在日常生活中，智慧型晶片市場將呈現指數型成長，晶片功能與運算速度需求也愈來愈高，電腦運算晶片的設計方法、替代材料和運算架構也必須不斷改變與升級，其中量子電腦是近年來最受到矚目的議題。工研院與美國史丹福大學在今年6月的年度研討會議中，有三位來自美國矽谷產學專家創業家，來台分享矽光子與量子電腦的研究方向和觀察。', N'IEK', CAST(0x0000A9BB00A66473 AS DateTime), N'admin', CAST(0x0000AA1E0122FFFC AS DateTime), N'-1', N'AT001', N'0088f97f-fd09-4ed6-8879-bd42e41f86fd', 0, 5)
INSERT [dbo].[tbArticle] ([AtcID], [AtcName], [AtcName_EN], [AtcDesc], [AtcDesc_EN], [AtcAuthor], [AtcAuthor_EN], [AtcInvestor], [AtcInvestor_EN], [AtcStatus], [AtcEnable], [AtcIsSign], [AtcPopup], [AtcSort], [AtcSetOD], [AtcOpenDate], [AtcCloseDate], [AtcPicPath], [AtcFBPicPath], [AtcPicDeac], [AtcSEOName], [AtcSEODesc], [CreateUser], [CreateDate], [LastUpdateUser], [LastUpdateDate], [LgeID], [AttID], [AtcSubID], [AtcTop], [AtcView]) VALUES (172, N'Press room簽名檔', N'Press room簽名檔', N'', N'', N'', N'', NULL, NULL, 0, 1, 1, 0, 0, 1, CAST(0x0000AA2A00000000 AS DateTime), CAST(0x0000AA4800000000 AS DateTime), N'', N'', N'', N'', N'', N'admin', CAST(0x0000AA2A00FFFD68 AS DateTime), N'admin', CAST(0x0000AA2A01001306 AS DateTime), N'-1', N'BT001', N'93469f39-0c3f-4995-ac7c-0d0d2ff2bcb0', 0, 0)
SET IDENTITY_INSERT [dbo].[tbArticle] OFF
INSERT [dbo].[tbArticleClass] ([AtsID], [AtsName], [AtsSort]) VALUES (N'C02', N'文字區塊', 1)
INSERT [dbo].[tbArticleClass] ([AtsID], [AtsName], [AtsSort]) VALUES (N'C11', N'圖片區塊', 2)
INSERT [dbo].[tbArticleClass] ([AtsID], [AtsName], [AtsSort]) VALUES (N'C31', N'串流影片區塊(YouTube)', 3)
SET IDENTITY_INSERT [dbo].[tbArticleContent] ON 

INSERT [dbo].[tbArticleContent] ([AtnID], [AtsID], [AtnSubject], [AtnText], [AtnSort], [AtnPicPath], [AtnVideoPath], [AtnMp4Path], [LastUpdateUser], [LastUpdateDate], [AtcID], [LgeID]) VALUES (1, N'C02', N'', N'<h1>
	我是簽名檔</h1>
', 1, NULL, NULL, NULL, N'admin', CAST(0x0000AA2A00FFE28C AS DateTime), 172, N'CT')
INSERT [dbo].[tbArticleContent] ([AtnID], [AtsID], [AtnSubject], [AtnText], [AtnSort], [AtnPicPath], [AtnVideoPath], [AtnMp4Path], [LastUpdateUser], [LastUpdateDate], [AtcID], [LgeID]) VALUES (2, N'C02', N'', N'<h1>
	I am a signature file</h1>
', 1, NULL, NULL, NULL, N'admin', CAST(0x0000AA2A00FFF8CB AS DateTime), 172, N'EN')
SET IDENTITY_INSERT [dbo].[tbArticleContent] OFF
INSERT [dbo].[tbArticleSub] ([SubID], [SubName], [SubName_EN], [SubDesc], [SubEnable], [SubSort], [LastUpdateUser], [LastUpdateDate], [LgeID]) VALUES (N'0088f97f-fd09-4ed6-8879-bd42e41f86fd', N'Press room管理', N'', N'AT001', 1, 1, N'admin', CAST(0x0000A9BB00A8CF5B AS DateTime), NULL)
INSERT [dbo].[tbArticleSub] ([SubID], [SubName], [SubName_EN], [SubDesc], [SubEnable], [SubSort], [LastUpdateUser], [LastUpdateDate], [LgeID]) VALUES (N'93469f39-0c3f-4995-ac7c-0d0d2ff2bcb0', N'Press room簽名檔管理', N'', N'BT001', 1, 2, N'admin', CAST(0x0000AA2A00FACF86 AS DateTime), NULL)
INSERT [dbo].[tbArticleType] ([AttID], [AttName], [AttDesc], [AttEnable], [AttSort], [LastUpdateUser], [LastUpdateDate], [LgeID]) VALUES (N'AT001', N'Press room管理', N'', 1, 0, N'FOX', CAST(0x0000A85B00000000 AS DateTime), N'')
INSERT [dbo].[tbArticleType] ([AttID], [AttName], [AttDesc], [AttEnable], [AttSort], [LastUpdateUser], [LastUpdateDate], [LgeID]) VALUES (N'AT002', N'111', N'', 1, 0, N'FOX', CAST(0x0000A9F100000000 AS DateTime), N'')
INSERT [dbo].[tbArticleType] ([AttID], [AttName], [AttDesc], [AttEnable], [AttSort], [LastUpdateUser], [LastUpdateDate], [LgeID]) VALUES (N'AT003', N'222', N'', 1, 0, N'FOX', CAST(0x0000A9FB00000000 AS DateTime), N'')
INSERT [dbo].[tbArticleType] ([AttID], [AttName], [AttDesc], [AttEnable], [AttSort], [LastUpdateUser], [LastUpdateDate], [LgeID]) VALUES (N'AT004', N'333', N'', 1, 0, N'FOX', CAST(0x0000A9FB00000000 AS DateTime), N'')
SET IDENTITY_INSERT [dbo].[tbEventRecord] ON 

INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10551, CAST(0x0000A97C01027190 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10552, CAST(0x0000A97C01029EAD AS DateTime), N'EVT002', N'使用者［admin］後台登出', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10553, CAST(0x0000A97C0102ED09 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10554, CAST(0x0000A97C01094D18 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10555, CAST(0x0000A97E011D8E75 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10556, CAST(0x0000A97E011E7E27 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10557, CAST(0x0000A97E01298558 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10558, CAST(0x0000A97E012AF26C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10559, CAST(0x0000A97E012BFE6C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10560, CAST(0x0000A98100F95C5F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10561, CAST(0x0000A98300AA1719 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10562, CAST(0x0000A98300B75BB4 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10563, CAST(0x0000A98300BD2C80 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10564, CAST(0x0000A98300EF712D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10565, CAST(0x0000A98300F1B426 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10566, CAST(0x0000A98300F77EEB AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10567, CAST(0x0000A98300F7EA36 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10568, CAST(0x0000A98300F8F47A AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10569, CAST(0x0000A98300FB7E96 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10570, CAST(0x0000A98300FC5E9C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10571, CAST(0x0000A98300FE15CA AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10572, CAST(0x0000A98400A060AD AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10573, CAST(0x0000A98400A2E279 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10574, CAST(0x0000A98400A44519 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10575, CAST(0x0000A98400A68E66 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10576, CAST(0x0000A98400A6FDD2 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10577, CAST(0x0000A98400AB5C30 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10578, CAST(0x0000A98400ACD7A7 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10579, CAST(0x0000A98400B8A669 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10580, CAST(0x0000A98400BA4B78 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10581, CAST(0x0000A98400BB6EAA AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10582, CAST(0x0000A98400BBB68D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10583, CAST(0x0000A98400BC718E AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10584, CAST(0x0000A98500ECCF66 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10585, CAST(0x0000A985010D58CA AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10586, CAST(0x0000A985011831E4 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10587, CAST(0x0000A985011C99BD AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10588, CAST(0x0000A98800B6C891 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10589, CAST(0x0000A989010DF1D8 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10590, CAST(0x0000A989012F58D0 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10591, CAST(0x0000A9890132A09C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10592, CAST(0x0000A989013305FB AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10593, CAST(0x0000A98901339C2B AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10594, CAST(0x0000A989013500C0 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10595, CAST(0x0000A98901374AD5 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10596, CAST(0x0000A98901385977 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10597, CAST(0x0000A9890138DBAD AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10598, CAST(0x0000A98901392A76 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10599, CAST(0x0000A98A00A8C70D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10600, CAST(0x0000A98A00B45F87 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10601, CAST(0x0000A98A00B5DA70 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10602, CAST(0x0000A98A00B7E431 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10603, CAST(0x0000A98A00F74916 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10604, CAST(0x0000A98A00FBC049 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10605, CAST(0x0000A98A00FD25CA AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10606, CAST(0x0000A98A00FFFF32 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10607, CAST(0x0000A98A010BE86A AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10608, CAST(0x0000A98A011D186D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10609, CAST(0x0000A98A0121B204 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10610, CAST(0x0000A98A01263C5D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10611, CAST(0x0000A98A012654C3 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10612, CAST(0x0000A98B00B5577D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10613, CAST(0x0000A98B0102FDE3 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10614, CAST(0x0000A98B01049FDF AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10615, CAST(0x0000A98B0108DDCC AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10616, CAST(0x0000A98B010AE1F4 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10617, CAST(0x0000A98B010D332F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10618, CAST(0x0000A98B011184B6 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10619, CAST(0x0000A98B01126A20 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10620, CAST(0x0000A98B01147077 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10621, CAST(0x0000A98B0117D520 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10622, CAST(0x0000A98B011A1E04 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10623, CAST(0x0000A98B011A63E4 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10624, CAST(0x0000A98C00E7B9B1 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10625, CAST(0x0000A98C00EDEC23 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10626, CAST(0x0000A98C00FD8C21 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10627, CAST(0x0000A98C00FE5EFC AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10628, CAST(0x0000A98C00FEDD72 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10629, CAST(0x0000A98C00FEFEDC AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10630, CAST(0x0000A98C00FF1F4A AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10631, CAST(0x0000A98C00FF5944 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10632, CAST(0x0000A98C00FFE8FB AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10633, CAST(0x0000A98C010076DE AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10634, CAST(0x0000A98C0100E0E9 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10635, CAST(0x0000A98C010B8E06 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10636, CAST(0x0000A98C010C7FC2 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10637, CAST(0x0000A98C010CEAD5 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10638, CAST(0x0000A98C010D3104 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10639, CAST(0x0000A98C0110305D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10640, CAST(0x0000A98C011AF3AB AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10641, CAST(0x0000A98C011BEB73 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10642, CAST(0x0000A98C011D1F59 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10643, CAST(0x0000A98C011D9DA2 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10644, CAST(0x0000A98C011E328F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10645, CAST(0x0000A99100E48BD6 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10646, CAST(0x0000A9910103CFB6 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10647, CAST(0x0000A9910111CA6E AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10648, CAST(0x0000A991011222BA AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10649, CAST(0x0000A99101129A00 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
GO
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10650, CAST(0x0000A99101192B8D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10651, CAST(0x0000A991011FD35B AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10652, CAST(0x0000A99101298C9E AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10653, CAST(0x0000A991012AD00B AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10654, CAST(0x0000A991012C0EB1 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10655, CAST(0x0000A992011129BA AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10656, CAST(0x0000A99201116DC2 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10657, CAST(0x0000A9920111FB5A AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10658, CAST(0x0000A992011368CE AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10659, CAST(0x0000A9920116DDF5 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10660, CAST(0x0000A99201173BB9 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10661, CAST(0x0000A9920117A7F2 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10662, CAST(0x0000A992011A38B7 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10663, CAST(0x0000A992011B47DD AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10664, CAST(0x0000A99201204744 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10665, CAST(0x0000A99201207CA6 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10666, CAST(0x0000A99201213FDD AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10667, CAST(0x0000A9920121D456 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10668, CAST(0x0000A9920124EA64 AS DateTime), N'EVT001', N'使用者［boss001］後台登入', N'boss001')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10669, CAST(0x0000A9930101D6AC AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10670, CAST(0x0000A99301172330 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10671, CAST(0x0000A99301175CAD AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10672, CAST(0x0000A993011D36F4 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10673, CAST(0x0000A99301200FDF AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10674, CAST(0x0000A9930122FD81 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10675, CAST(0x0000A99301238879 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10676, CAST(0x0000A99600F69189 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10677, CAST(0x0000A99600F86BC4 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10678, CAST(0x0000A9960110EED3 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10679, CAST(0x0000A9960111D324 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10680, CAST(0x0000A9960111F63A AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10681, CAST(0x0000A99601124F43 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10682, CAST(0x0000A99601129670 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10683, CAST(0x0000A9960112BE80 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10684, CAST(0x0000A996011EA5F2 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10685, CAST(0x0000A9960123B726 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10686, CAST(0x0000A99700B344E0 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10687, CAST(0x0000A99700B42ED5 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10688, CAST(0x0000A99700B46193 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10689, CAST(0x0000A99700B9FAC6 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10690, CAST(0x0000A99700BAD469 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10691, CAST(0x0000A99700BEF9E9 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10692, CAST(0x0000A99700C2E3DD AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10693, CAST(0x0000A99700ECF6A7 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10694, CAST(0x0000A99700EEA8D9 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10695, CAST(0x0000A99700F2E421 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10696, CAST(0x0000A99700F36740 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10697, CAST(0x0000A99700F60C21 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10698, CAST(0x0000A99700F65327 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10699, CAST(0x0000A99700F858C2 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10700, CAST(0x0000A99700FCEA06 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10701, CAST(0x0000A9970105E0FD AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10702, CAST(0x0000A9970106BC39 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10703, CAST(0x0000A99701099272 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10704, CAST(0x0000A997010A48C9 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10705, CAST(0x0000A997010AC424 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10706, CAST(0x0000A997010C7AC7 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10707, CAST(0x0000A997010CCB74 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10708, CAST(0x0000A997010F6EA7 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10709, CAST(0x0000A997010FC908 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10710, CAST(0x0000A9970111E304 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10711, CAST(0x0000A997011660F8 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10712, CAST(0x0000A99800F378D9 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10713, CAST(0x0000A99800FB7C3E AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10714, CAST(0x0000A99800FD0173 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10715, CAST(0x0000A99800FD7593 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10716, CAST(0x0000A99800FDAD77 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10717, CAST(0x0000A99800FE8131 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10718, CAST(0x0000A9980107D25C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10719, CAST(0x0000A9980108F7D6 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10720, CAST(0x0000A99801093826 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10721, CAST(0x0000A998010AEB31 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10722, CAST(0x0000A998010C505F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10723, CAST(0x0000A99900E362C9 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10724, CAST(0x0000A99900EEB7DC AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10725, CAST(0x0000A99900F29E9D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10726, CAST(0x0000A99900F5E2C8 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10727, CAST(0x0000A99900F9FCCB AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10728, CAST(0x0000A999010481C8 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10729, CAST(0x0000A9990106F703 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10730, CAST(0x0000A9990107242F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10731, CAST(0x0000A99901127FE7 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10732, CAST(0x0000A9990112E40F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10733, CAST(0x0000A9990113CE5D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10734, CAST(0x0000A9990114BBB1 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10735, CAST(0x0000A99901155893 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10736, CAST(0x0000A99901261FC8 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10737, CAST(0x0000A9990127805B AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10738, CAST(0x0000A99A00E623EE AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10739, CAST(0x0000A99A00F44371 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10740, CAST(0x0000A99A00F499B2 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10741, CAST(0x0000A99E00E8AAFA AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10742, CAST(0x0000A99E00FA0683 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10743, CAST(0x0000A99F00B35BE8 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10744, CAST(0x0000A99F00F8BBFE AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10745, CAST(0x0000A99F01029842 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10746, CAST(0x0000A99F01039482 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10747, CAST(0x0000A99F01045068 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10748, CAST(0x0000A99F0105ABD1 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10749, CAST(0x0000A9A50139C9A7 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
GO
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10750, CAST(0x0000A9A600A8DFEC AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10751, CAST(0x0000A9A600ADF195 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10752, CAST(0x0000A9A601130394 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10753, CAST(0x0000A9A60127F4E9 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10754, CAST(0x0000A9A6012B57D0 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10755, CAST(0x0000A9A6012C0BCF AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10756, CAST(0x0000A9A601328A56 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10757, CAST(0x0000A9A60134426D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10758, CAST(0x0000A9A60134918B AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10759, CAST(0x0000A9A60134B1E4 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10760, CAST(0x0000A9A601371113 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10761, CAST(0x0000A9A60137378A AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10762, CAST(0x0000A9A700C28F53 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10763, CAST(0x0000A9A700C324FD AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10764, CAST(0x0000A9A700E64F6F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10765, CAST(0x0000A9A700E71181 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10766, CAST(0x0000A9A700E9779C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10767, CAST(0x0000A9A700EEC17E AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10768, CAST(0x0000A9A70101B1A0 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10769, CAST(0x0000A9A7010269C4 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10770, CAST(0x0000A9A7010303D3 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10771, CAST(0x0000A9A70103C914 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10772, CAST(0x0000A9A701043ECB AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10773, CAST(0x0000A9A701048313 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10774, CAST(0x0000A9A70111155C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10775, CAST(0x0000A9A701317769 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10776, CAST(0x0000A9A7013229A8 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10777, CAST(0x0000A9A70132B1A1 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10778, CAST(0x0000A9A70133363A AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10779, CAST(0x0000A9A70133984A AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10780, CAST(0x0000A9A800F556CD AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10781, CAST(0x0000A9A8012264CE AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10782, CAST(0x0000A9A80122BF0C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10783, CAST(0x0000A9A801236027 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10784, CAST(0x0000A9A801357EF8 AS DateTime), N'EVT011', N'使用者［user@gmail.com］前臺登入', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10785, CAST(0x0000A9AB00B8B54E AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10786, CAST(0x0000A9AB00B9A59D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10787, CAST(0x0000A9AB00BC5F8F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10788, CAST(0x0000A9AB00BD4783 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10789, CAST(0x0000A9AB00C03395 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10790, CAST(0x0000A9AB00E0C4E7 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10791, CAST(0x0000A9AB0103D289 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10792, CAST(0x0000A9AB012762DC AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10793, CAST(0x0000A9AB01280E2C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10794, CAST(0x0000A9AB01361547 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10795, CAST(0x0000A9AB0138EC30 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10796, CAST(0x0000A9AC00AC2852 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10797, CAST(0x0000A9AC00ADB9C8 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10798, CAST(0x0000A9AC00AE806F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10799, CAST(0x0000A9AC00B14AC6 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10800, CAST(0x0000A9AC00B6DB93 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10801, CAST(0x0000A9AC00BDF80F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10802, CAST(0x0000A9AC00DBF038 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10803, CAST(0x0000A9AC00E1F71D AS DateTime), N'EVT011', N'使用者［user@gmail.com］前臺登入', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10804, CAST(0x0000A9AC00E26E7E AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10805, CAST(0x0000A9AC00E4F198 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10806, CAST(0x0000A9AC00E51890 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10807, CAST(0x0000A9AC00E74B27 AS DateTime), N'EVT011', N'使用者［user@gmail.com］前臺登入', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10808, CAST(0x0000A9AC00E9AF06 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10809, CAST(0x0000A9AC00ED2FEE AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10810, CAST(0x0000A9AC00F474E5 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10811, CAST(0x0000A9AC00F48552 AS DateTime), N'EVT011', N'使用者［user@gmail.com］前臺登入', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10812, CAST(0x0000A9AC00F4CC92 AS DateTime), N'EVT012', N'使用者［user@gmail.com］前臺登出', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10813, CAST(0x0000A9AC00F7A8D6 AS DateTime), N'EVT011', N'使用者［user@gmail.com］前臺登入', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10814, CAST(0x0000A9AC00FB62EC AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10815, CAST(0x0000A9AC01143CC5 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10816, CAST(0x0000A9AC0118A876 AS DateTime), N'EVT011', N'使用者［user@gmail.com］前臺登入', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10817, CAST(0x0000A9AC011C7EFE AS DateTime), N'EVT012', N'使用者［user@gmail.com］前臺登出', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10818, CAST(0x0000A9AC011C95EC AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10819, CAST(0x0000A9AC012A07D6 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10820, CAST(0x0000A9AC012B1432 AS DateTime), N'EVT011', N'使用者［v1988314@gmail.com］前臺登入', N'v1988314@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10821, CAST(0x0000A9AC01321A91 AS DateTime), N'EVT011', N'使用者［user@gmail.com］前臺登入', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10822, CAST(0x0000A9AC0134A5D9 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10823, CAST(0x0000A9AD009BB8E6 AS DateTime), N'EVT011', N'使用者［user@gmail.com］前臺登入', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10824, CAST(0x0000A9AD009BB771 AS DateTime), N'EVT011', N'使用者［user@gmail.com］前臺登入', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10825, CAST(0x0000A9AD00A857CD AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10826, CAST(0x0000A9AD00A8604C AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10827, CAST(0x0000A9AD00B3A18F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10828, CAST(0x0000A9AD00B6EF31 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10829, CAST(0x0000A9AD00BA65FC AS DateTime), N'EVT012', N'使用者［boss@gmail.com］前臺登出', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10830, CAST(0x0000A9AD00BA7B65 AS DateTime), N'EVT011', N'使用者［user@gmail.com］前臺登入', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10831, CAST(0x0000A9AD00BD2CB3 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10832, CAST(0x0000A9AD00BFBE9D AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10833, CAST(0x0000A9AD00C34646 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10834, CAST(0x0000A9AD00C387A5 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10835, CAST(0x0000A9AD00F2118D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10836, CAST(0x0000A9AD00FA3AAD AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10837, CAST(0x0000A9AD00FAA99B AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10838, CAST(0x0000A9AD00FB5042 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10839, CAST(0x0000A9AD00FE3C20 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10840, CAST(0x0000A9AD010510AF AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10841, CAST(0x0000A9AD01058237 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10842, CAST(0x0000A9AD010E67B0 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10843, CAST(0x0000A9AD010FAFB8 AS DateTime), N'EVT012', N'使用者［yichuan@begonia-design.com.tw］前臺登出', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10844, CAST(0x0000A9AD011160E6 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10845, CAST(0x0000A9AD01221CEC AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10846, CAST(0x0000A9AD01224A75 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10847, CAST(0x0000A9AD0123A9DA AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10848, CAST(0x0000A9AD0126DC16 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10849, CAST(0x0000A9AD0128010F AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
GO
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10850, CAST(0x0000A9AD0129BE2C AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10851, CAST(0x0000A9AD0129EB31 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10852, CAST(0x0000A9AD0131B028 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10853, CAST(0x0000A9AD01322F71 AS DateTime), N'EVT002', N'使用者［admin］後台登出', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10854, CAST(0x0000A9AD01323CFE AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10855, CAST(0x0000A9AD0133110A AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10856, CAST(0x0000A9AD01331731 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10857, CAST(0x0000A9AD0155AA89 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10858, CAST(0x0000A9AD018105F6 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10859, CAST(0x0000A9AD0184BE05 AS DateTime), N'EVT011', N'使用者［user@gmail.com］前臺登入', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10860, CAST(0x0000A9AD01869330 AS DateTime), N'EVT012', N'使用者［user@gmail.com］前臺登出', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10861, CAST(0x0000A9AD0186A3E4 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10862, CAST(0x0000A9AE00883A31 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10863, CAST(0x0000A9AE0088FCEE AS DateTime), N'EVT011', N'使用者［user@gmail.com］前臺登入', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10864, CAST(0x0000A9AE00890016 AS DateTime), N'EVT011', N'使用者［user@gmail.com］前臺登入', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10865, CAST(0x0000A9AE00893855 AS DateTime), N'EVT012', N'使用者［user@gmail.com］前臺登出', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10866, CAST(0x0000A9AE009D5573 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10867, CAST(0x0000A9AE00A1F49B AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10868, CAST(0x0000A9AE00A22369 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10869, CAST(0x0000A9AE00A2E67F AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10870, CAST(0x0000A9AE00A47867 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10871, CAST(0x0000A9AE00A589E8 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10872, CAST(0x0000A9AE00AA8E32 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10873, CAST(0x0000A9AE00AAC596 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10874, CAST(0x0000A9AE00AE69EE AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10875, CAST(0x0000A9AE00AE6E35 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10876, CAST(0x0000A9AE00AE835B AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10877, CAST(0x0000A9AE00AF47C5 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10878, CAST(0x0000A9AE00B12F45 AS DateTime), N'EVT011', N'使用者［user@gmail.com］前臺登入', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10879, CAST(0x0000A9AE00B130F5 AS DateTime), N'EVT011', N'使用者［user@gmail.com］前臺登入', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10880, CAST(0x0000A9AE00B15B6D AS DateTime), N'EVT012', N'使用者［user@gmail.com］前臺登出', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10881, CAST(0x0000A9AE00B170C5 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10882, CAST(0x0000A9AE00B1E51F AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10883, CAST(0x0000A9AE00BFFFFF AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10884, CAST(0x0000A9AE00C703E6 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10885, CAST(0x0000A9AE00C7634F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10886, CAST(0x0000A9AE00C87190 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10887, CAST(0x0000A9AE00C89E97 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10888, CAST(0x0000A9AE00CA2831 AS DateTime), N'EVT011', N'使用者［user@gmail.com］前臺登入', N'user@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10889, CAST(0x0000A9AE00CCAA4C AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10890, CAST(0x0000A9AE00DC00B2 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10891, CAST(0x0000A9AE00E263E3 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10892, CAST(0x0000A9AE00E4A404 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10893, CAST(0x0000A9AE00E975C1 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10894, CAST(0x0000A9AE00ED7948 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10895, CAST(0x0000A9AE00EDC15B AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10896, CAST(0x0000A9AE00EE028D AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10897, CAST(0x0000A9AE00F15F9C AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10898, CAST(0x0000A9AE00F29EB9 AS DateTime), N'EVT012', N'使用者［yichuan@begonia-design.com.tw］前臺登出', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10899, CAST(0x0000A9AE00F362BE AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10900, CAST(0x0000A9AE00F3BE42 AS DateTime), N'EVT012', N'使用者［yichuan@begonia-design.com.tw］前臺登出', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10901, CAST(0x0000A9AE00F3D001 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10902, CAST(0x0000A9AE00F3D4B5 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10903, CAST(0x0000A9AE00F3F980 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10904, CAST(0x0000A9AE00F76E14 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10905, CAST(0x0000A9AE00F830AB AS DateTime), N'EVT012', N'使用者［robeccalu@itri.org.tw］前臺登出', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10906, CAST(0x0000A9AE00F84BF3 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10907, CAST(0x0000A9AE00FA2170 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10908, CAST(0x0000A9AE00FA3449 AS DateTime), N'EVT002', N'使用者［admin］後台登出', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10909, CAST(0x0000A9AE00FA3E05 AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10910, CAST(0x0000A9AE00FC2D56 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10911, CAST(0x0000A9AE01029B69 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10912, CAST(0x0000A9AE0105F784 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10913, CAST(0x0000A9AE0106250A AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10914, CAST(0x0000A9AE01096935 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10915, CAST(0x0000A9AE0109ECCB AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10916, CAST(0x0000A9AE010CF4B2 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10917, CAST(0x0000A9AE010DC1B9 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10918, CAST(0x0000A9AE010E23CA AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10919, CAST(0x0000A9AE010E4254 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10920, CAST(0x0000A9AE010E46A3 AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10921, CAST(0x0000A9AE010E7CBE AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10922, CAST(0x0000A9AE010F09CE AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10923, CAST(0x0000A9AE010F4373 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10924, CAST(0x0000A9AE01167EC7 AS DateTime), N'EVT012', N'使用者［boss@gmail.com］前臺登出', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10925, CAST(0x0000A9AE0117300E AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10926, CAST(0x0000A9AE012178A5 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10927, CAST(0x0000A9AE0122B02D AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10928, CAST(0x0000A9AE01269F61 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10929, CAST(0x0000A9AE012BC3D3 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10930, CAST(0x0000A9AE012D3022 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10931, CAST(0x0000A9AE01325F15 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10932, CAST(0x0000A9AE01431134 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10933, CAST(0x0000A9AE01455395 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10934, CAST(0x0000A9AE0146936A AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10935, CAST(0x0000A9AE01488F3B AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10936, CAST(0x0000A9AE014ADB7C AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10937, CAST(0x0000A9AE014C1E39 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10938, CAST(0x0000A9AE01672EDC AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10939, CAST(0x0000A9AE0167BAA0 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10940, CAST(0x0000A9AE01687285 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10941, CAST(0x0000A9AE016A594F AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10942, CAST(0x0000A9AE016E17FD AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10943, CAST(0x0000A9AE016E6FC8 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10944, CAST(0x0000A9AE017099B8 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10945, CAST(0x0000A9AE0178DBCF AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10946, CAST(0x0000A9AE0179F4F9 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10947, CAST(0x0000A9AE017AB9DA AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10948, CAST(0x0000A9AE017B4D23 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10949, CAST(0x0000A9AE017B72AA AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
GO
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10950, CAST(0x0000A9AE017CA7E0 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10951, CAST(0x0000A9AE017D6D95 AS DateTime), N'EVT012', N'使用者［boss@gmail.com］前臺登出', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10952, CAST(0x0000A9AF0019D55B AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10953, CAST(0x0000A9AF0019D86A AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10954, CAST(0x0000A9AF0019D900 AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10955, CAST(0x0000A9AF0019EE0A AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10956, CAST(0x0000A9AF008C8A2A AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10957, CAST(0x0000A9AF009F5791 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10958, CAST(0x0000A9AF00A1BF24 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10959, CAST(0x0000A9AF00A1C1AB AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10960, CAST(0x0000A9AF00AB82EE AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10961, CAST(0x0000A9AF00ACD668 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10962, CAST(0x0000A9AF00BF7F41 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10963, CAST(0x0000A9AF00EAFE76 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10964, CAST(0x0000A9AF00F1C335 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10965, CAST(0x0000A9AF00F2C530 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10966, CAST(0x0000A9AF010348DB AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10967, CAST(0x0000A9AF0103644A AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10968, CAST(0x0000A9AF01066B2B AS DateTime), N'EVT012', N'使用者［yichuan@begonia-design.com.tw］前臺登出', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10969, CAST(0x0000A9AF0106955F AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10970, CAST(0x0000A9AF010D96DE AS DateTime), N'EVT012', N'使用者［yichuan@begonia-design.com.tw］前臺登出', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10971, CAST(0x0000A9AF01175D97 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10972, CAST(0x0000A9AF011B056C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10973, CAST(0x0000A9AF011B506B AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10974, CAST(0x0000A9AF0127E2C3 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10975, CAST(0x0000A9AF01282C01 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10976, CAST(0x0000A9AF01292EA3 AS DateTime), N'EVT012', N'使用者［yichuan@begonia-design.com.tw］前臺登出', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10977, CAST(0x0000A9AF01293AC5 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10978, CAST(0x0000A9AF0134BB74 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10979, CAST(0x0000A9AF016085C8 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10980, CAST(0x0000A9AF016085C9 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10981, CAST(0x0000A9AF016085C0 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10982, CAST(0x0000A9B000C2B059 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10983, CAST(0x0000A9B000C59536 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10984, CAST(0x0000A9B000FCA51E AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10985, CAST(0x0000A9B00107C0DC AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10986, CAST(0x0000A9B001130EDA AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10987, CAST(0x0000A9B0014C947D AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10988, CAST(0x0000A9B0015F5805 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10989, CAST(0x0000A9B001722918 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10990, CAST(0x0000A9B00174C0E9 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10991, CAST(0x0000A9B0017D67B2 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10992, CAST(0x0000A9B0017F7545 AS DateTime), N'EVT012', N'使用者［boss@gmail.com］前臺登出', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10993, CAST(0x0000A9B100A3F179 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10994, CAST(0x0000A9B100A473B6 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10995, CAST(0x0000A9B100A62005 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10996, CAST(0x0000A9B100A873C9 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10997, CAST(0x0000A9B100AE07CB AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10998, CAST(0x0000A9B100B0B56D AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (10999, CAST(0x0000A9B100B70BDF AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11000, CAST(0x0000A9B100B77046 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11001, CAST(0x0000A9B100D33681 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11002, CAST(0x0000A9B100DBC61E AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11003, CAST(0x0000A9B100DBEB42 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11004, CAST(0x0000A9B100DC4817 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11005, CAST(0x0000A9B100DCBD42 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11006, CAST(0x0000A9B100DF9C86 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11007, CAST(0x0000A9B100DFFFEB AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11008, CAST(0x0000A9B100E466BF AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11009, CAST(0x0000A9B100E66F77 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11010, CAST(0x0000A9B100E7F811 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11011, CAST(0x0000A9B100EF2E1C AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11012, CAST(0x0000A9B10141CEA1 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11013, CAST(0x0000A9B10142E874 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11014, CAST(0x0000A9B10148CFDA AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11015, CAST(0x0000A9B10148E172 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11016, CAST(0x0000A9B101499C9F AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11017, CAST(0x0000A9B1015ECDFC AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11018, CAST(0x0000A9B2008A0E07 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11019, CAST(0x0000A9B20098B6D5 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11020, CAST(0x0000A9B200AA2BD0 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11021, CAST(0x0000A9B200AA9ECC AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11022, CAST(0x0000A9B200AAD03B AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11023, CAST(0x0000A9B200C66F8A AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11024, CAST(0x0000A9B200E566F8 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11025, CAST(0x0000A9B200E56A8C AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11026, CAST(0x0000A9B200E676FD AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11027, CAST(0x0000A9B200F70D93 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11028, CAST(0x0000A9B200FE6177 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11029, CAST(0x0000A9B200FEF914 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11030, CAST(0x0000A9B201044098 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11031, CAST(0x0000A9B20106F91F AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11032, CAST(0x0000A9B20109F5AD AS DateTime), N'EVT012', N'使用者［boss@gmail.com］前臺登出', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11033, CAST(0x0000A9B2010B265F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11034, CAST(0x0000A9B2010B5319 AS DateTime), N'EVT011', N'使用者［libraclark@gmail.com］前臺登入', N'libraclark@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11035, CAST(0x0000A9B2010B641B AS DateTime), N'EVT012', N'使用者［libraclark@gmail.com］前臺登出', N'libraclark@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11036, CAST(0x0000A9B2010E4ABE AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11037, CAST(0x0000A9B2010FB948 AS DateTime), N'EVT012', N'使用者［boss@gmail.com］前臺登出', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11038, CAST(0x0000A9B20114EFB9 AS DateTime), N'EVT012', N'使用者［yichuan@begonia-design.com.tw］前臺登出', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11039, CAST(0x0000A9B201155932 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11040, CAST(0x0000A9B20117D1CE AS DateTime), N'EVT012', N'使用者［yichuan@begonia-design.com.tw］前臺登出', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11041, CAST(0x0000A9B20117E87E AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11042, CAST(0x0000A9B20118ADC8 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11043, CAST(0x0000A9B201232C2E AS DateTime), N'EVT012', N'使用者［boss@gmail.com］前臺登出', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11044, CAST(0x0000A9B20124F769 AS DateTime), N'EVT011', N'使用者［libraclark@gmail.com］前臺登入', N'libraclark@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11045, CAST(0x0000A9B2012BC6DD AS DateTime), N'EVT012', N'使用者［libraclark@gmail.com］前臺登出', N'libraclark@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11046, CAST(0x0000A9B2012BD9C7 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11047, CAST(0x0000A9B2012D7F19 AS DateTime), N'EVT012', N'使用者［yichuan@begonia-design.com.tw］前臺登出', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11048, CAST(0x0000A9B2012DCDA2 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11049, CAST(0x0000A9B2012E0BCD AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
GO
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11050, CAST(0x0000A9B2012F92F1 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11051, CAST(0x0000A9B20130653F AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11052, CAST(0x0000A9B30087B3B7 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11053, CAST(0x0000A9B3008F699C AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11054, CAST(0x0000A9B3009FD634 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11055, CAST(0x0000A9B300A4D791 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11056, CAST(0x0000A9B300A8FE86 AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11057, CAST(0x0000A9B300AAE72C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11058, CAST(0x0000A9B300AD6A85 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11059, CAST(0x0000A9B300BF0671 AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11060, CAST(0x0000A9B300C3EE03 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11061, CAST(0x0000A9B300C8AE2F AS DateTime), N'EVT011', N'使用者［v1988314@hotmail.com］前臺登入', N'v1988314@hotmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11062, CAST(0x0000A9B300C8E784 AS DateTime), N'EVT012', N'使用者［v1988314@hotmail.com］前臺登出', N'v1988314@hotmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11063, CAST(0x0000A9B300C9E612 AS DateTime), N'EVT011', N'使用者［v1988314@gmail.com］前臺登入', N'v1988314@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11064, CAST(0x0000A9B300CA1625 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11065, CAST(0x0000A9B300CACBF0 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11066, CAST(0x0000A9B300DDD225 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11067, CAST(0x0000A9B300DFB994 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11068, CAST(0x0000A9B300E3E9E2 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11069, CAST(0x0000A9B300E431A3 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11070, CAST(0x0000A9B300E4592C AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11071, CAST(0x0000A9B300E737CD AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11072, CAST(0x0000A9B300E8C54D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11073, CAST(0x0000A9B300E97CBE AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11074, CAST(0x0000A9B300EA1C37 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11075, CAST(0x0000A9B300EE2FD8 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11076, CAST(0x0000A9B300F380A5 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11077, CAST(0x0000A9B300F399E5 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11078, CAST(0x0000A9B300F84261 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11079, CAST(0x0000A9B300FF200F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11080, CAST(0x0000A9B30103B513 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11081, CAST(0x0000A9B30103B559 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11082, CAST(0x0000A9B30105259D AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11083, CAST(0x0000A9B301054692 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11084, CAST(0x0000A9B30107E7E1 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11085, CAST(0x0000A9B301084642 AS DateTime), N'EVT012', N'使用者［robeccalu@itri.org.tw］前臺登出', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11086, CAST(0x0000A9B3010859C6 AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11087, CAST(0x0000A9B30109A75B AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11088, CAST(0x0000A9B3010C7050 AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11089, CAST(0x0000A9B3010DB352 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11090, CAST(0x0000A9B30110417F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11091, CAST(0x0000A9B301390D13 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11092, CAST(0x0000A9B4009F0973 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11093, CAST(0x0000A9B400A8C46E AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11094, CAST(0x0000A9B400AA30A3 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11095, CAST(0x0000A9B400AD6EEA AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11096, CAST(0x0000A9B400E7823C AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11097, CAST(0x0000A9B400E7F8A9 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11098, CAST(0x0000A9B400FAB2D5 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11099, CAST(0x0000A9B4010876E6 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11100, CAST(0x0000A9B4010CE891 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11101, CAST(0x0000A9B4010FDE28 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11102, CAST(0x0000A9B4010FF5C0 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11103, CAST(0x0000A9B401136010 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11104, CAST(0x0000A9B401141B8C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11105, CAST(0x0000A9B4011EBDE5 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11106, CAST(0x0000A9B40125B46F AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11107, CAST(0x0000A9B40131290A AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11108, CAST(0x0000A9B40136CCCC AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11109, CAST(0x0000A9B4013AC4BE AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11110, CAST(0x0000A9B40140CEFA AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11111, CAST(0x0000A9B500AEDDE8 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11112, CAST(0x0000A9B500B53622 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11113, CAST(0x0000A9B500B5CFB5 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11114, CAST(0x0000A9B500B81162 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11115, CAST(0x0000A9B500B9EFEA AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11116, CAST(0x0000A9B500C17DDD AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11117, CAST(0x0000A9B500C3ACF5 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11118, CAST(0x0000A9B500DE3397 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11119, CAST(0x0000A9B500EA164D AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11120, CAST(0x0000A9B500EE0CC1 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11121, CAST(0x0000A9B500F1AAED AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11122, CAST(0x0000A9B500FA77DE AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11123, CAST(0x0000A9B5011564CC AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11124, CAST(0x0000A9B600A8D626 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11125, CAST(0x0000A9B600A8DD81 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11126, CAST(0x0000A9B600C5C00D AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11127, CAST(0x0000A9B600C5FDDB AS DateTime), N'EVT012', N'使用者［yichuan@begonia-design.com.tw］前臺登出', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11128, CAST(0x0000A9B600C60233 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11129, CAST(0x0000A9B600C65234 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11130, CAST(0x0000A9B600E7FCF7 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11131, CAST(0x0000A9B600EB63B2 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11132, CAST(0x0000A9B600F3F4C7 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11133, CAST(0x0000A9B600F739A7 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11134, CAST(0x0000A9B601066D21 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11135, CAST(0x0000A9B6010B88A2 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11136, CAST(0x0000A9B6010B9F17 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11137, CAST(0x0000A9B601120FDB AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11138, CAST(0x0000A9B601143C7A AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11139, CAST(0x0000A9B6011EC7DB AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11140, CAST(0x0000A9B900AFEE24 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11141, CAST(0x0000A9B900B3AFCD AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11142, CAST(0x0000A9B900C1192E AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11143, CAST(0x0000A9B901101E45 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11144, CAST(0x0000A9B901101F15 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11145, CAST(0x0000A9B901205C03 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11146, CAST(0x0000A9B9012F2DD9 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11147, CAST(0x0000A9B9015D271B AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11148, CAST(0x0000A9B9015D26D7 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11149, CAST(0x0000A9BA011D013D AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
GO
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11150, CAST(0x0000A9BA011D07F6 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11151, CAST(0x0000A9BA011F5DE1 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11152, CAST(0x0000A9BA011F6F5B AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11153, CAST(0x0000A9BA0120EFD8 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11154, CAST(0x0000A9BA0122118F AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11155, CAST(0x0000A9BA01221A48 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11156, CAST(0x0000A9BA01291F03 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11157, CAST(0x0000A9BA01291FC7 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11158, CAST(0x0000A9BA01292176 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11159, CAST(0x0000A9BA013C7D38 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11160, CAST(0x0000A9BA014380E2 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11161, CAST(0x0000A9BA015E3DDE AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11162, CAST(0x0000A9BA015E3FCC AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11163, CAST(0x0000A9BA016D5DD2 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11164, CAST(0x0000A9BB009B72B2 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11165, CAST(0x0000A9BB009B764F AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11166, CAST(0x0000A9BB00A2C1FE AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11167, CAST(0x0000A9BB00A5C070 AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11168, CAST(0x0000A9BB00A5C82C AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11169, CAST(0x0000A9BB00AACD1E AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11170, CAST(0x0000A9BB00B0B6DB AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11171, CAST(0x0000A9BB00B18EE1 AS DateTime), N'EVT002', N'使用者［IEK］後台登出', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11172, CAST(0x0000A9BB00B60147 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11173, CAST(0x0000A9BB00BA9A5C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11174, CAST(0x0000A9BB00BB6856 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11175, CAST(0x0000A9BB00BB8A4C AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11176, CAST(0x0000A9BB00C2C90F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11177, CAST(0x0000A9BB00C359C4 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11178, CAST(0x0000A9BB00C5FA44 AS DateTime), N'EVT002', N'使用者［admin］後台登出', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11179, CAST(0x0000A9BB00CB7021 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11180, CAST(0x0000A9BB00E6A397 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11181, CAST(0x0000A9BB00E789C6 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11182, CAST(0x0000A9BB00EA37BC AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11183, CAST(0x0000A9BB00EAB30F AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11184, CAST(0x0000A9BB01088943 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11185, CAST(0x0000A9BB010C504F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11186, CAST(0x0000A9BB010F867C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11187, CAST(0x0000A9BB0113942D AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11188, CAST(0x0000A9BB01139D4B AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11189, CAST(0x0000A9BB0126B1E8 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11190, CAST(0x0000A9BB01318C8B AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11191, CAST(0x0000A9BB01673A80 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11192, CAST(0x0000A9BB016741C6 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11193, CAST(0x0000A9BB018A4F59 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11194, CAST(0x0000A9BC00115285 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11195, CAST(0x0000A9BC00822114 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11196, CAST(0x0000A9BC0085D3B2 AS DateTime), N'EVT012', N'使用者［robeccalu@itri.org.tw］前臺登出', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11197, CAST(0x0000A9BC008634EF AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11198, CAST(0x0000A9BC00884579 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11199, CAST(0x0000A9BC00886392 AS DateTime), N'EVT001', N'使用者［iek］後台登入', N'iek')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11200, CAST(0x0000A9BC0088EFDF AS DateTime), N'EVT001', N'使用者［iek］後台登入', N'iek')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11201, CAST(0x0000A9BC008D56A2 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11202, CAST(0x0000A9BC008D9353 AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11203, CAST(0x0000A9BC00A6FF4C AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11204, CAST(0x0000A9BC00A9DFC9 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11205, CAST(0x0000A9BC00AB59CD AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11206, CAST(0x0000A9BC00AB6AC2 AS DateTime), N'EVT012', N'使用者［yichuan@begonia-design.com.tw］前臺登出', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11207, CAST(0x0000A9BC00AEE7E3 AS DateTime), N'EVT001', N'使用者［iek］後台登入', N'iek')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11208, CAST(0x0000A9BC00B01910 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11209, CAST(0x0000A9BC00B1734D AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11210, CAST(0x0000A9BC00B1D635 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11211, CAST(0x0000A9BC00BA8B7B AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11212, CAST(0x0000A9BC00C3D926 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11213, CAST(0x0000A9BC00EBC7EA AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11214, CAST(0x0000A9BC00FB6584 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11215, CAST(0x0000A9BC00FD1B72 AS DateTime), N'EVT012', N'使用者［boss@gmail.com］前臺登出', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11216, CAST(0x0000A9BC00FF75DB AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11217, CAST(0x0000A9BC00FFE213 AS DateTime), N'EVT001', N'使用者［iek］後台登入', N'iek')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11218, CAST(0x0000A9BC00FFF2FC AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11219, CAST(0x0000A9BC010463E7 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11220, CAST(0x0000A9BC0110363B AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11221, CAST(0x0000A9BC0116E117 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11222, CAST(0x0000A9BC011BBD2E AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11223, CAST(0x0000A9BC011F99B1 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11224, CAST(0x0000A9BC012FBABF AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11225, CAST(0x0000A9BC013DC5DC AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11226, CAST(0x0000A9BC014DD9B2 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11227, CAST(0x0000A9BC0158590B AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11228, CAST(0x0000A9BC01598523 AS DateTime), N'EVT012', N'使用者［boss@gmail.com］前臺登出', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11229, CAST(0x0000A9BD009B0821 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11230, CAST(0x0000A9BD00A6BCB8 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11231, CAST(0x0000A9BD00A986C3 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11232, CAST(0x0000A9BD00A9BA96 AS DateTime), N'EVT001', N'使用者［iek］後台登入', N'iek')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11233, CAST(0x0000A9BD00AE048F AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11234, CAST(0x0000A9BD00B1F6FE AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11235, CAST(0x0000A9BD00B2F11F AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11236, CAST(0x0000A9BD00C375D4 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11237, CAST(0x0000A9BD00DE07C2 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11238, CAST(0x0000A9BE010C4A1C AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11239, CAST(0x0000A9C000BE5959 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11240, CAST(0x0000A9C000E846E0 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11241, CAST(0x0000A9C000F59E16 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11242, CAST(0x0000A9C000F59ED3 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11243, CAST(0x0000A9C000F59ED3 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11244, CAST(0x0000A9C001047C16 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11245, CAST(0x0000A9C001056056 AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11246, CAST(0x0000A9C00143B322 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11247, CAST(0x0000A9C200BCBF66 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11248, CAST(0x0000A9C200DC1C2E AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11249, CAST(0x0000A9C200F8519F AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
GO
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11250, CAST(0x0000A9C200F8CD95 AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11251, CAST(0x0000A9C200FCB3C4 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11252, CAST(0x0000A9C201089CDA AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11253, CAST(0x0000A9C201089F02 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11254, CAST(0x0000A9C2012B81DF AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11255, CAST(0x0000A9C400E47C85 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11256, CAST(0x0000A9C901080D7A AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11257, CAST(0x0000A9C901081727 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11258, CAST(0x0000A9C901156299 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11259, CAST(0x0000A9CA00DFFA9D AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11260, CAST(0x0000A9CB0119AEF6 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11261, CAST(0x0000A9CB011DDC1B AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11262, CAST(0x0000A9CB012FB109 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11263, CAST(0x0000A9CE00B4CA5F AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11264, CAST(0x0000A9CE00CE7AA8 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11265, CAST(0x0000A9CE00E2F987 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11266, CAST(0x0000A9CE00F405E4 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11267, CAST(0x0000A9CE00FAD79D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11268, CAST(0x0000A9CE00FAD9EC AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11269, CAST(0x0000A9CE00FFD2B3 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11270, CAST(0x0000A9CE012D809F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11271, CAST(0x0000A9CF00A37586 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11272, CAST(0x0000A9CF00A37A59 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11273, CAST(0x0000A9CF00E6A66E AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11274, CAST(0x0000A9CF010CBD33 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11275, CAST(0x0000A9CF011AE5C9 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11276, CAST(0x0000A9CF011AE51F AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11277, CAST(0x0000A9CF011AE5E0 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11278, CAST(0x0000A9D000B6D1E5 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11279, CAST(0x0000A9D000BAFED6 AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11280, CAST(0x0000A9D001010C2F AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11281, CAST(0x0000A9D001012211 AS DateTime), N'EVT012', N'使用者［yichuan@begonia-design.com.tw］前臺登出', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11282, CAST(0x0000A9D200EFE875 AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11283, CAST(0x0000A9D200F045F2 AS DateTime), N'EVT002', N'使用者［IEK］後台登出', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11284, CAST(0x0000A9D5013BFD01 AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11285, CAST(0x0000A9D5013CB607 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11286, CAST(0x0000A9D5013DFC47 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11287, CAST(0x0000A9D60134A7C6 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11288, CAST(0x0000A9D900EC9254 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11289, CAST(0x0000A9D900F01D0A AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11290, CAST(0x0000A9D90105DFA3 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11291, CAST(0x0000A9D9011324F4 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11292, CAST(0x0000A9DC00DE308C AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11293, CAST(0x0000A9DC00E0C26A AS DateTime), N'EVT012', N'使用者［robeccalu@itri.org.tw］前臺登出', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11294, CAST(0x0000A9DC00E6E26F AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11295, CAST(0x0000A9DC010ACF20 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11296, CAST(0x0000A9DD00B9D955 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11297, CAST(0x0000A9DD00FB88C0 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11298, CAST(0x0000A9DD010E7F4C AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11299, CAST(0x0000A9DD010E85EF AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11300, CAST(0x0000A9DE00C31A4E AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11301, CAST(0x0000A9DE00C327CD AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11302, CAST(0x0000A9DE00EBC992 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11303, CAST(0x0000A9DE01074F5F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11304, CAST(0x0000A9DE011CBB51 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11305, CAST(0x0000A9DE011ECAD9 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11306, CAST(0x0000A9DE0121A90A AS DateTime), N'EVT012', N'使用者［boss@gmail.com］前臺登出', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11307, CAST(0x0000A9DF00C3B668 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11308, CAST(0x0000A9DF00F805AF AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11309, CAST(0x0000A9DF00F95F3F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11310, CAST(0x0000A9DF010848C5 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11311, CAST(0x0000A9DF01092502 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11312, CAST(0x0000A9DF01113B48 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11313, CAST(0x0000A9DF01143568 AS DateTime), N'EVT011', N'使用者［v1988314@hotmail.com］前臺登入', N'v1988314@hotmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11314, CAST(0x0000A9DF0118A994 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11315, CAST(0x0000A9DF011C7200 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11316, CAST(0x0000A9DF011DE769 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11317, CAST(0x0000A9E0009E3B2A AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11318, CAST(0x0000A9E000A4F3C8 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11319, CAST(0x0000A9E000A4F5AB AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11320, CAST(0x0000A9E000A576C4 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11321, CAST(0x0000A9E000A9A324 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11322, CAST(0x0000A9E000BCCF78 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11323, CAST(0x0000A9E000EE21E6 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11324, CAST(0x0000A9E001222F88 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11325, CAST(0x0000A9E00124909D AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11326, CAST(0x0000A9E00125B028 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11327, CAST(0x0000A9E0012B5338 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11328, CAST(0x0000A9E0012B55D0 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11329, CAST(0x0000A9E0012B560A AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11330, CAST(0x0000A9E0012FAD50 AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11331, CAST(0x0000A9E0012FC5AA AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11332, CAST(0x0000A9E0012FED60 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11333, CAST(0x0000A9E300BA43A0 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11334, CAST(0x0000A9E300C09A7F AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11335, CAST(0x0000A9E300C9D097 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11336, CAST(0x0000A9E300CBC9D9 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11337, CAST(0x0000A9E300D79288 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11338, CAST(0x0000A9E300EB06A8 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11339, CAST(0x0000A9E400B6B258 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11340, CAST(0x0000A9E400B6DA93 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11341, CAST(0x0000A9E400BC96D9 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11342, CAST(0x0000A9E400BC9921 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11343, CAST(0x0000A9E4010DCBA3 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11344, CAST(0x0000A9E4010E3C2D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11345, CAST(0x0000A9E4010E897A AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11346, CAST(0x0000A9E40110B466 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11347, CAST(0x0000A9E401122A17 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11348, CAST(0x0000A9E40112AC59 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11349, CAST(0x0000A9E40114BC91 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
GO
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11350, CAST(0x0000A9E40119F85D AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11351, CAST(0x0000A9E4011B94A7 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11352, CAST(0x0000A9E4011C09E6 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11353, CAST(0x0000A9E4011C0C0A AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11354, CAST(0x0000A9E401283B05 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11355, CAST(0x0000A9E4012C260E AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11356, CAST(0x0000A9E4012E8B64 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11357, CAST(0x0000A9E4013F40DB AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11358, CAST(0x0000A9E500AAF95A AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11359, CAST(0x0000A9E500AD6CD4 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11360, CAST(0x0000A9E500C9CE66 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11361, CAST(0x0000A9E500DAF1AF AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11362, CAST(0x0000A9E500DE81EF AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11363, CAST(0x0000A9E500DE8971 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11364, CAST(0x0000A9E50105E47B AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11365, CAST(0x0000A9E501085249 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11366, CAST(0x0000A9E700C52D9E AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11367, CAST(0x0000A9F101033ADE AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11368, CAST(0x0000A9F1010E8819 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11369, CAST(0x0000A9F1012A7E25 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11370, CAST(0x0000A9F200FF254C AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11371, CAST(0x0000A9F201003E7F AS DateTime), N'EVT012', N'使用者［yichuan@begonia-design.com.tw］前臺登出', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11372, CAST(0x0000A9F201007FEF AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11373, CAST(0x0000A9F2010CDE51 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11374, CAST(0x0000A9F2011AF491 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11375, CAST(0x0000A9F201298144 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11376, CAST(0x0000A9F2012E9FFE AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11377, CAST(0x0000A9F300A9E96C AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11378, CAST(0x0000A9F300AAE03B AS DateTime), N'EVT012', N'使用者［yichuan@begonia-design.com.tw］前臺登出', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11379, CAST(0x0000A9F300AE21B9 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11380, CAST(0x0000A9F300B1E9BA AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11381, CAST(0x0000A9F300BFF5F1 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11382, CAST(0x0000A9F3012D5D81 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11383, CAST(0x0000A9F400AE2BD3 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11384, CAST(0x0000A9F400AE3122 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11385, CAST(0x0000A9F400AE3419 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11386, CAST(0x0000A9F400AE35FF AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11387, CAST(0x0000A9F400AE3732 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11388, CAST(0x0000A9F400AE38F6 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11389, CAST(0x0000A9F400B4BB4E AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11390, CAST(0x0000A9F400B80545 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11391, CAST(0x0000A9F400F6783B AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11392, CAST(0x0000A9F500BDD2EB AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11393, CAST(0x0000A9F800E23AA4 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11394, CAST(0x0000A9F801081ECE AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11395, CAST(0x0000A9F8010823F7 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11396, CAST(0x0000A9F900F1F703 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11397, CAST(0x0000A9F900F34EF1 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11398, CAST(0x0000A9FA00B04F85 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11399, CAST(0x0000A9FA011263BC AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11400, CAST(0x0000A9FB012A189B AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11401, CAST(0x0000A9FC009908E1 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11402, CAST(0x0000A9FC00B73077 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11403, CAST(0x0000A9FC00B7596F AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11404, CAST(0x0000A9FC00B7B5BE AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11405, CAST(0x0000A9FC00BF5E40 AS DateTime), N'EVT001', N'使用者［iek］後台登入', N'iek')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11406, CAST(0x0000A9FC00BF8A30 AS DateTime), N'EVT001', N'使用者［iek］後台登入', N'iek')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11407, CAST(0x0000A9FC00E89865 AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11408, CAST(0x0000A9FC00EA5855 AS DateTime), N'EVT002', N'使用者［IEK］後台登出', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11409, CAST(0x0000A9FC00EA9273 AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11410, CAST(0x0000A9FC00EC0985 AS DateTime), N'EVT011', N'使用者［cindy_lee@itri.org.tw］前臺登入', N'cindy_lee@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11411, CAST(0x0000A9FC00ECF42F AS DateTime), N'EVT001', N'使用者［ASVDA］後台登入', N'ASVDA')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11412, CAST(0x0000A9FC00F131A8 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11413, CAST(0x0000A9FC00F19F80 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11414, CAST(0x0000A9FC00F1FF35 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11415, CAST(0x0000A9FD00F0C5F6 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11416, CAST(0x0000A9FD0107F991 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11417, CAST(0x0000A9FF0097C7C9 AS DateTime), N'EVT001', N'使用者［iek］後台登入', N'iek')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11418, CAST(0x0000A9FF00EA9BB1 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11419, CAST(0x0000A9FF01844687 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11420, CAST(0x0000AA0000E132B4 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11421, CAST(0x0000AA0000F1EED3 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11422, CAST(0x0000AA0000FBF1E0 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11423, CAST(0x0000AA060003EE21 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11424, CAST(0x0000AA060110DA4F AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11425, CAST(0x0000AA06011131FB AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11426, CAST(0x0000AA060111B401 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11427, CAST(0x0000AA070122D680 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11428, CAST(0x0000AA08009E6319 AS DateTime), N'EVT001', N'使用者［ASVDA］後台登入', N'ASVDA')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11429, CAST(0x0000AA08009E66E3 AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11430, CAST(0x0000AA0800BB6ABE AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11431, CAST(0x0000AA0800BD7767 AS DateTime), N'EVT011', N'使用者［robeccalu@itri.org.tw］前臺登入', N'robeccalu@itri.org.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11432, CAST(0x0000AA0800BE0058 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11433, CAST(0x0000AA0800DC514E AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11434, CAST(0x0000AA0800DEAEE7 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11435, CAST(0x0000AA0800DEB12F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11436, CAST(0x0000AA0800DEB1B2 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11437, CAST(0x0000AA0800DEB1C4 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11438, CAST(0x0000AA0800E1EA94 AS DateTime), N'EVT011', N'使用者［yichuan@begonia-design.com.tw］前臺登入', N'yichuan@begonia-design.com.tw')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11439, CAST(0x0000AA0800E36933 AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11440, CAST(0x0000AA0800E52C7B AS DateTime), N'EVT001', N'使用者［ASVDA］後台登入', N'ASVDA')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11441, CAST(0x0000AA0800F18724 AS DateTime), N'EVT001', N'使用者［IEK］後台登入', N'IEK')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11442, CAST(0x0000AA0800F3AD81 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11443, CAST(0x0000AA0800F44D80 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11444, CAST(0x0000AA0800F58A4D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11445, CAST(0x0000AA080134BB53 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11446, CAST(0x0000AA080135B285 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11447, CAST(0x0000AA080136546A AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11448, CAST(0x0000AA08013718AB AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11449, CAST(0x0000AA0900B8B067 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
GO
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11450, CAST(0x0000AA0900B9EB5C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11451, CAST(0x0000AA0900E89D6C AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11452, CAST(0x0000AA0A00B71088 AS DateTime), N'EVT011', N'使用者［boss@gmail.com］前臺登入', N'boss@gmail.com')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11453, CAST(0x0000AA0A00E11F63 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11454, CAST(0x0000AA0A00E801AB AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11455, CAST(0x0000AA0A00E91699 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11456, CAST(0x0000AA0A00E967AA AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11457, CAST(0x0000AA0A00F95EF2 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11458, CAST(0x0000AA0A00FBEC18 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11459, CAST(0x0000AA0A01224701 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11460, CAST(0x0000AA0A01239D5C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11461, CAST(0x0000AA0A012613A9 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11462, CAST(0x0000AA0A0127160C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11463, CAST(0x0000AA0A012845B5 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11464, CAST(0x0000AA0D00E6068A AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11465, CAST(0x0000AA0D00E8F213 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11466, CAST(0x0000AA0D00ED2906 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11467, CAST(0x0000AA0D00EDC594 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11468, CAST(0x0000AA1000BC35EA AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11469, CAST(0x0000AA1000BDEC6C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11470, CAST(0x0000AA1000BED654 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11471, CAST(0x0000AA1000BF555D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11472, CAST(0x0000AA1000BFA231 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11473, CAST(0x0000AA1000C05A21 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11474, CAST(0x0000AA1000E5F662 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11475, CAST(0x0000AA1000E7F201 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11476, CAST(0x0000AA1000E7F201 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11477, CAST(0x0000AA1000E82B5F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11478, CAST(0x0000AA1000E85F23 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11479, CAST(0x0000AA11012EE130 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11480, CAST(0x0000AA1400B84354 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11481, CAST(0x0000AA1400BAE117 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11482, CAST(0x0000AA1400BCEB13 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11483, CAST(0x0000AA17011A15EE AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11484, CAST(0x0000AA1800B78E0F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11485, CAST(0x0000AA1E00E851F0 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11486, CAST(0x0000AA1E00E85761 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11487, CAST(0x0000AA1E00F3CE1C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11488, CAST(0x0000AA1E00F85292 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11489, CAST(0x0000AA1E00F85FF6 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11490, CAST(0x0000AA1E00F8E18D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11491, CAST(0x0000AA1E00F93053 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11492, CAST(0x0000AA1E00F93EA9 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11493, CAST(0x0000AA1E00FA00D6 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11494, CAST(0x0000AA1E00FA227C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11495, CAST(0x0000AA1E010B19D7 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11496, CAST(0x0000AA1E010C0FC4 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11497, CAST(0x0000AA1E010C1F8E AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11498, CAST(0x0000AA1E010DDDEC AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11499, CAST(0x0000AA1E010DE7E2 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11500, CAST(0x0000AA1E01144B7D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11501, CAST(0x0000AA1E0115E323 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11502, CAST(0x0000AA1E0115F9F7 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11503, CAST(0x0000AA1E01160727 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11504, CAST(0x0000AA1E01163B2C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11505, CAST(0x0000AA1E011DF6B1 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11506, CAST(0x0000AA1E011ED4EB AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11507, CAST(0x0000AA1E012046C0 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11508, CAST(0x0000AA1E0120F1B1 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11509, CAST(0x0000AA1E0122085B AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11510, CAST(0x0000AA1E0122BB39 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11511, CAST(0x0000AA2400FAFD7F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11512, CAST(0x0000AA290105A505 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11513, CAST(0x0000AA29010CC1A2 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11514, CAST(0x0000AA2901210EDD AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11515, CAST(0x0000AA2901275B93 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11516, CAST(0x0000AA29012A6E82 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11517, CAST(0x0000AA29012CCE84 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11518, CAST(0x0000AA2A00F1A1F2 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11519, CAST(0x0000AA2A00F9BA23 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11520, CAST(0x0000AA2A00FE1B4C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11521, CAST(0x0000AA2A00FFBECA AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11522, CAST(0x0000AA2A0103913F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11523, CAST(0x0000AA2A01040526 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11524, CAST(0x0000AA2A011E2FFC AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11525, CAST(0x0000AA3300EB45AB AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11526, CAST(0x0000AA3300F4E51C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11527, CAST(0x0000AA3300F83BD2 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11528, CAST(0x0000AA33011373A4 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11529, CAST(0x0000AA3301144BC6 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11530, CAST(0x0000AA33011707B7 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11531, CAST(0x0000AA330117422E AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11532, CAST(0x0000AA330117AA19 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11533, CAST(0x0000AA33011D7E36 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11534, CAST(0x0000AA33011E14E8 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11535, CAST(0x0000AA33011E62D0 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11536, CAST(0x0000AA330128A9B1 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11537, CAST(0x0000AA340121332D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11538, CAST(0x0000AA3401225700 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11539, CAST(0x0000AA340122BFDA AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11540, CAST(0x0000AA340124B4B3 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11541, CAST(0x0000AA4600FDAC6E AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11542, CAST(0x0000AA46010F01FB AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11543, CAST(0x0000AA460112C11D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11544, CAST(0x0000AA4601159A62 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11545, CAST(0x0000AA4601169DD4 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11546, CAST(0x0000AA460118A36D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11547, CAST(0x0000AA46011BFE9B AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11548, CAST(0x0000AA46011C53EE AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11549, CAST(0x0000AA46011D27AD AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
GO
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11550, CAST(0x0000AA53000D87BA AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11551, CAST(0x0000AA53000E4F1D AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11552, CAST(0x0000AA5300A4A4F1 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11553, CAST(0x0000AA5300A548C2 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11554, CAST(0x0000AA5300A7AAE0 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11555, CAST(0x0000AA5300A8CCD2 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11556, CAST(0x0000AA5300A92BA5 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11557, CAST(0x0000AA5300A98FE6 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11558, CAST(0x0000AA5301130AD1 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11559, CAST(0x0000AA5301231C13 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11560, CAST(0x0000AA530127EF6E AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11561, CAST(0x0000AA530129B345 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11562, CAST(0x0000AA53012BB5AB AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11563, CAST(0x0000AA53012BE7C9 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11564, CAST(0x0000AA53012C0954 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11565, CAST(0x0000AA53012CAB51 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11566, CAST(0x0000AA53012CED87 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11567, CAST(0x0000AA53012F02E1 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11568, CAST(0x0000AA53012F8881 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11569, CAST(0x0000AA530130147C AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11570, CAST(0x0000AA5301311F94 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11571, CAST(0x0000AA7800B82A6F AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11572, CAST(0x0000AA7800BB4DC6 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11573, CAST(0x0000AA78011DBB88 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11574, CAST(0x0000AA780129A530 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11575, CAST(0x0000AA78012A5DE1 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11576, CAST(0x0000AA78012D1129 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11577, CAST(0x0000AA78012D9068 AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
INSERT [dbo].[tbEventRecord] ([EvrIndex], [CreateTime], [EvrTypeID], [EvrMsg], [UseID]) VALUES (11578, CAST(0x0000AA78012F96EB AS DateTime), N'EVT001', N'使用者［admin］後台登入', N'admin')
SET IDENTITY_INSERT [dbo].[tbEventRecord] OFF
INSERT [dbo].[tbEventType] ([EvtTypeID], [EvtType], [EvtDesc]) VALUES (N'EVT001', N'登入', N'帳號登入')
INSERT [dbo].[tbEventType] ([EvtTypeID], [EvtType], [EvtDesc]) VALUES (N'EVT002', N'登出', N'帳號登出')
INSERT [dbo].[tbEventType] ([EvtTypeID], [EvtType], [EvtDesc]) VALUES (N'EVT003', N'帳號權限管理', N'修改密碼')
INSERT [dbo].[tbEventType] ([EvtTypeID], [EvtType], [EvtDesc]) VALUES (N'EVT004', N'帳號權限管理', N'帳號資料異動')
INSERT [dbo].[tbEventType] ([EvtTypeID], [EvtType], [EvtDesc]) VALUES (N'EVT005', N'帳號權限管理', N'群組資料異動')
INSERT [dbo].[tbEventType] ([EvtTypeID], [EvtType], [EvtDesc]) VALUES (N'EVT006', N'帳號權限管理', N'頁面權限資料異動')
INSERT [dbo].[tbEventType] ([EvtTypeID], [EvtType], [EvtDesc]) VALUES (N'EVT011', N'前台登入', N'前台會員登入')
INSERT [dbo].[tbEventType] ([EvtTypeID], [EvtType], [EvtDesc]) VALUES (N'EVT012', N'前台登出', N'前台會員登出')
SET IDENTITY_INSERT [dbo].[tbExceptionRecord] ON 

INSERT [dbo].[tbExceptionRecord] ([ExrIndex], [CreateTime], [ExrTypeID], [ExrMsg]) VALUES (12, CAST(0x0000A9AB00BC6DF5 AS DateTime), N'EXT001', N' SMTP 伺服器需要安全連接，或用戶端未經驗證。 伺服器回應為: 5.5.1 Authentication Required. Learn more at')
INSERT [dbo].[tbExceptionRecord] ([ExrIndex], [CreateTime], [ExrTypeID], [ExrMsg]) VALUES (13, CAST(0x0000A9AB00BC6F29 AS DateTime), N'EXT001', N' SMTP 伺服器需要安全連接，或用戶端未經驗證。 伺服器回應為: 5.5.1 Authentication Required. Learn more at')
INSERT [dbo].[tbExceptionRecord] ([ExrIndex], [CreateTime], [ExrTypeID], [ExrMsg]) VALUES (14, CAST(0x0000A9AB00BD592F AS DateTime), N'EXT001', N' 傳送郵件失敗。')
INSERT [dbo].[tbExceptionRecord] ([ExrIndex], [CreateTime], [ExrTypeID], [ExrMsg]) VALUES (15, CAST(0x0000A9AB00C03B0A AS DateTime), N'EXT001', N' SMTP 伺服器需要安全連接，或用戶端未經驗證。 伺服器回應為: 5.5.1 Authentication Required. Learn more at')
INSERT [dbo].[tbExceptionRecord] ([ExrIndex], [CreateTime], [ExrTypeID], [ExrMsg]) VALUES (16, CAST(0x0000A9AF01094954 AS DateTime), N'EXT001', N'System.FormatException: Input string was not in a correct format.
   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)
   at System.Number.ParseInt32(String s, NumberStyles style, NumberFormatInfo info)
   at STARTUP_CMS.Backend.Manage_CompanyList.btnEdit_Click(Object sender, EventArgs e) in c:\Users\Web-\Source\Repos\STARTUP\STARTUP\STARTUP_CMS\STARTUP_CMS\Backend\Manage_CompanyList.aspx.cs:line 389')
INSERT [dbo].[tbExceptionRecord] ([ExrIndex], [CreateTime], [ExrTypeID], [ExrMsg]) VALUES (17, CAST(0x0000A9AF0109B3CE AS DateTime), N'EXT001', N'System.FormatException: Input string was not in a correct format.
   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)
   at System.Number.ParseInt32(String s, NumberStyles style, NumberFormatInfo info)
   at STARTUP_CMS.Backend.Manage_CompanyList.btnEdit_Click(Object sender, EventArgs e) in c:\Users\Web-\Source\Repos\STARTUP\STARTUP\STARTUP_CMS\STARTUP_CMS\Backend\Manage_CompanyList.aspx.cs:line 389')
INSERT [dbo].[tbExceptionRecord] ([ExrIndex], [CreateTime], [ExrTypeID], [ExrMsg]) VALUES (18, CAST(0x0000A9AF0109D259 AS DateTime), N'EXT001', N'System.FormatException: Input string was not in a correct format.
   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)
   at System.Number.ParseInt32(String s, NumberStyles style, NumberFormatInfo info)
   at STARTUP_CMS.Backend.Manage_CompanyList.btnEdit_Click(Object sender, EventArgs e) in c:\Users\Web-\Source\Repos\STARTUP\STARTUP\STARTUP_CMS\STARTUP_CMS\Backend\Manage_CompanyList.aspx.cs:line 389')
INSERT [dbo].[tbExceptionRecord] ([ExrIndex], [CreateTime], [ExrTypeID], [ExrMsg]) VALUES (19, CAST(0x0000A9B500B345DE AS DateTime), N'EXT001', N'System.FormatException: Input string was not in a correct format.
   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)
   at System.Number.ParseInt32(String s, NumberStyles style, NumberFormatInfo info)
   at STARTUP_CMS.Backend.Manage_CompanyList.btnEdit_Click(Object sender, EventArgs e) in c:\Users\Web-\Source\Repos\STARTUP\STARTUP\STARTUP_CMS\STARTUP_CMS\Backend\Manage_CompanyList.aspx.cs:line 412')
INSERT [dbo].[tbExceptionRecord] ([ExrIndex], [CreateTime], [ExrTypeID], [ExrMsg]) VALUES (20, CAST(0x0000A9B500B550CF AS DateTime), N'EXT001', N'System.FormatException: Input string was not in a correct format.
   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)
   at System.Number.ParseInt32(String s, NumberStyles style, NumberFormatInfo info)
   at STARTUP_CMS.Backend.Manage_CompanyList.btnEdit_Click(Object sender, EventArgs e)')
INSERT [dbo].[tbExceptionRecord] ([ExrIndex], [CreateTime], [ExrTypeID], [ExrMsg]) VALUES (21, CAST(0x0000A9B500B5E80A AS DateTime), N'EXT001', N'System.FormatException: Input string was not in a correct format.
   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)
   at System.Number.ParseInt32(String s, NumberStyles style, NumberFormatInfo info)
   at STARTUP_CMS.Backend.Manage_CompanyList.btnEdit_Click(Object sender, EventArgs e)')
INSERT [dbo].[tbExceptionRecord] ([ExrIndex], [CreateTime], [ExrTypeID], [ExrMsg]) VALUES (22, CAST(0x0000A9B500B66A4E AS DateTime), N'EXT001', N'System.FormatException: Input string was not in a correct format.
   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)
   at System.Number.ParseInt32(String s, NumberStyles style, NumberFormatInfo info)
   at STARTUP_CMS.Backend.Manage_CompanyList.btnEdit_Click(Object sender, EventArgs e)')
INSERT [dbo].[tbExceptionRecord] ([ExrIndex], [CreateTime], [ExrTypeID], [ExrMsg]) VALUES (23, CAST(0x0000A9BB00C318C9 AS DateTime), N'EXT001', N'System.Threading.ThreadAbortException: Thread was being aborted.
   at System.Threading.Thread.AbortInternal()
   at System.Threading.Thread.Abort(Object stateInfo)
   at System.Web.HttpResponse.AbortCurrentThread()
   at STARTUP_CMS.Backend.Manage_NoticeUserList.btnOK_Click(Object sender, EventArgs e) in c:\Users\Web-\Source\Repos\STARTUP\STARTUP\STARTUP_CMS\STARTUP_CMS\Backend\Manage_NoticeUserList.aspx.cs:line 202')
INSERT [dbo].[tbExceptionRecord] ([ExrIndex], [CreateTime], [ExrTypeID], [ExrMsg]) VALUES (24, CAST(0x0000A9BB00C326B8 AS DateTime), N'EXT001', N'System.Threading.ThreadAbortException: Thread was being aborted.
   at System.Threading.Thread.AbortInternal()
   at System.Threading.Thread.Abort(Object stateInfo)
   at System.Web.HttpResponse.AbortCurrentThread()
   at STARTUP_CMS.Backend.Manage_NoticeUserList.btnOK_Click(Object sender, EventArgs e) in c:\Users\Web-\Source\Repos\STARTUP\STARTUP\STARTUP_CMS\STARTUP_CMS\Backend\Manage_NoticeUserList.aspx.cs:line 202')
INSERT [dbo].[tbExceptionRecord] ([ExrIndex], [CreateTime], [ExrTypeID], [ExrMsg]) VALUES (25, CAST(0x0000A9BC008E7A8D AS DateTime), N'EXT001', N'System.FormatException: Input string was not in a correct format.
   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)
   at System.Number.ParseInt32(String s, NumberStyles style, NumberFormatInfo info)
   at STARTUP_CMS.Backend.Manage_CompanyList.btnEdit_Click(Object sender, EventArgs e) in c:\Users\Web-\Source\Repos\STARTUP\STARTUP\STARTUP_CMS\STARTUP_CMS\Backend\Manage_CompanyList.aspx.cs:line 412')
INSERT [dbo].[tbExceptionRecord] ([ExrIndex], [CreateTime], [ExrTypeID], [ExrMsg]) VALUES (26, CAST(0x0000A9BC008EB128 AS DateTime), N'EXT001', N'System.FormatException: Input string was not in a correct format.
   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)
   at System.Number.ParseInt32(String s, NumberStyles style, NumberFormatInfo info)
   at STARTUP_CMS.Backend.Manage_CompanyList.btnEdit_Click(Object sender, EventArgs e) in c:\Users\Web-\Source\Repos\STARTUP\STARTUP\STARTUP_CMS\STARTUP_CMS\Backend\Manage_CompanyList.aspx.cs:line 412')
INSERT [dbo].[tbExceptionRecord] ([ExrIndex], [CreateTime], [ExrTypeID], [ExrMsg]) VALUES (27, CAST(0x0000A9E0009E7A4D AS DateTime), N'EXT001', N'System.FormatException: String was not recognized as a valid DateTime.
   at System.DateTime.Parse(String s, IFormatProvider provider)
   at System.Convert.ToDateTime(String value)
   at STARTUP_CMS.Backend.Manage_CompanyList.btnEdit_Click(Object sender, EventArgs e) in c:\Users\Web-\Source\Repos\STARTUP\STARTUP\STARTUP_CMS\STARTUP_CMS\Backend\Manage_CompanyList.aspx.cs:line 515')
INSERT [dbo].[tbExceptionRecord] ([ExrIndex], [CreateTime], [ExrTypeID], [ExrMsg]) VALUES (28, CAST(0x0000A9E0009E9666 AS DateTime), N'EXT001', N'System.FormatException: String was not recognized as a valid DateTime.
   at System.DateTime.Parse(String s, IFormatProvider provider)
   at System.Convert.ToDateTime(String value)
   at STARTUP_CMS.Backend.Manage_CompanyList.btnEdit_Click(Object sender, EventArgs e) in c:\Users\Web-\Source\Repos\STARTUP\STARTUP\STARTUP_CMS\STARTUP_CMS\Backend\Manage_CompanyList.aspx.cs:line 515')
INSERT [dbo].[tbExceptionRecord] ([ExrIndex], [CreateTime], [ExrTypeID], [ExrMsg]) VALUES (29, CAST(0x0000A9FC00E99675 AS DateTime), N'EXT001', N'System.FormatException: Input string was not in a correct format.
   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)
   at System.Number.ParseInt32(String s, NumberStyles style, NumberFormatInfo info)
   at STARTUP_CMS.Backend.Manage_CompanyList.btnOK_Click(Object sender, EventArgs e) in c:\Users\Web-\Source\Repos\STARTUP\STARTUP\STARTUP_CMS\STARTUP_CMS\Backend\Manage_CompanyList.aspx.cs:line 277')
INSERT [dbo].[tbExceptionRecord] ([ExrIndex], [CreateTime], [ExrTypeID], [ExrMsg]) VALUES (30, CAST(0x0000A9FC00E9DF18 AS DateTime), N'EXT001', N'System.FormatException: Input string was not in a correct format.
   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)
   at System.Number.ParseInt32(String s, NumberStyles style, NumberFormatInfo info)
   at STARTUP_CMS.Backend.Manage_CompanyList.btnOK_Click(Object sender, EventArgs e) in c:\Users\Web-\Source\Repos\STARTUP\STARTUP\STARTUP_CMS\STARTUP_CMS\Backend\Manage_CompanyList.aspx.cs:line 277')
SET IDENTITY_INSERT [dbo].[tbExceptionRecord] OFF
INSERT [dbo].[tbExceptionType] ([ExtTypeID], [ExtType], [ExtDesc]) VALUES (N'EXT001', N'DAL ERROR', N'資料存取層發現錯誤')
INSERT [dbo].[tbExceptionType] ([ExtTypeID], [ExtType], [ExtDesc]) VALUES (N'EXT002', N'BLL ERROR', N'資料邏輯層發現錯誤')
INSERT [dbo].[tbFile] ([FieID], [FitID], [OpenDate], [FieYear], [FieMonth], [FieDay], [FieName], [FieName_EN], [FieFilePath], [FieFilePath_EN], [FiePicPath], [FiePicPath_EN], [FieDesc], [FieDesc_EN], [FieEnable], [FieSort], [LastUpdateUser], [LastUpdateDate], [CreateUser], [CreateDate], [FieView], [FieDownload]) VALUES (N'6ce275f9-407f-46de-92b1-cd403566ad50', N'FIT001', CAST(0x0000AA5300000000 AS DateTime), 2019, 5, 20, N'WWW', N'WWW2', N'7-4.pdf', N'04_math_teacher.jpg', N'f15aaf5a-d44c-4e12-a934-803509abf0db.jpg', N'', N'', N'', 1, 2, N'admin', CAST(0x0000AA5301237196 AS DateTime), N'admin', CAST(0x0000AA5301237196 AS DateTime), 0, 0)
INSERT [dbo].[tbFile] ([FieID], [FitID], [OpenDate], [FieYear], [FieMonth], [FieDay], [FieName], [FieName_EN], [FieFilePath], [FieFilePath_EN], [FiePicPath], [FiePicPath_EN], [FieDesc], [FieDesc_EN], [FieEnable], [FieSort], [LastUpdateUser], [LastUpdateDate], [CreateUser], [CreateDate], [FieView], [FieDownload]) VALUES (N'794337e5-b3c7-449e-befe-933b7d5659ea', N'FIT001', CAST(0x0000AA2700000000 AS DateTime), 2019, 4, 6, N'試試', N'TRY', N'CH.pdf', N'EN.pdf', N'2b5cdf7a-b7e6-45e3-8faf-f137b35ea1fb.jpg', N'66a2aa74-7762-4c4d-8a71-33e70547cca3.jpg', N'', N'', 1, 1, N'admin', CAST(0x0000AA340124EFE9 AS DateTime), N'admin', CAST(0x0000AA3401236CA3 AS DateTime), 0, 0)
SET IDENTITY_INSERT [dbo].[tbFileLog] ON 

INSERT [dbo].[tbFileLog] ([FigID], [FIeID], [FigDesc], [CreateTime]) VALUES (1, N'794337e5-b3c7-449e-befe-933b7d5659ea', N'Download', CAST(0x0000AA5300000000 AS DateTime))
INSERT [dbo].[tbFileLog] ([FigID], [FIeID], [FigDesc], [CreateTime]) VALUES (2, N'794337e5-b3c7-449e-befe-933b7d5659ea', N'Download', CAST(0x0000AA5E00000000 AS DateTime))
INSERT [dbo].[tbFileLog] ([FigID], [FIeID], [FigDesc], [CreateTime]) VALUES (3, N'794337e5-b3c7-449e-befe-933b7d5659ea', N'Download', CAST(0x0000AA2C00000000 AS DateTime))
INSERT [dbo].[tbFileLog] ([FigID], [FIeID], [FigDesc], [CreateTime]) VALUES (4, N'794337e5-b3c7-449e-befe-933b7d5659ea', N'Questionnaire', CAST(0x0000AA2C00000000 AS DateTime))
INSERT [dbo].[tbFileLog] ([FigID], [FIeID], [FigDesc], [CreateTime]) VALUES (5, N'6ce275f9-407f-46de-92b1-cd403566ad50', N'Download', CAST(0x0000AA4A00000000 AS DateTime))
INSERT [dbo].[tbFileLog] ([FigID], [FIeID], [FigDesc], [CreateTime]) VALUES (6, N'6ce275f9-407f-46de-92b1-cd403566ad50', N'Download', CAST(0x0000AA4300000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[tbFileLog] OFF
INSERT [dbo].[tbFileType] ([FitID], [FitName], [FitDesc], [FitEnable], [FitSort], [FitGroup], [LastUpdateUser], [LastUpdateDate]) VALUES (N'FIT001', N'CSR報告', N'', 1, 1, N'A', N'FOX', CAST(0x0000AA3300000000 AS DateTime))
INSERT [dbo].[tbLanguage] ([LgeID], [LgeName], [LgeSort]) VALUES (N'CT', N'中文', 1)
INSERT [dbo].[tbLanguage] ([LgeID], [LgeName], [LgeSort]) VALUES (N'EN', N'英文', 2)
INSERT [dbo].[tbMediaType] ([MdtID], [MdtName], [MdtDesc], [MdtEnable], [MdtSort], [MdtGroup], [LastUpdateUser], [LastUpdateDate]) VALUES (N'MDT001', N'圖片', NULL, 1, 1, N'A', N'admin', CAST(0x0000A5CC00B54640 AS DateTime))
INSERT [dbo].[tbMediaType] ([MdtID], [MdtName], [MdtDesc], [MdtEnable], [MdtSort], [MdtGroup], [LastUpdateUser], [LastUpdateDate]) VALUES (N'MDT002', N'影片', NULL, 1, 2, N'A', N'admin', CAST(0x0000A5CC00B54640 AS DateTime))
INSERT [dbo].[tbMediaType] ([MdtID], [MdtName], [MdtDesc], [MdtEnable], [MdtSort], [MdtGroup], [LastUpdateUser], [LastUpdateDate]) VALUES (N'MDT003', N'檔案', NULL, 1, 3, N'A', N'admin', CAST(0x0000A5CC00B54640 AS DateTime))
INSERT [dbo].[tbPermission] ([PerID], [PerName], [PerPage]) VALUES (N'M11', N'[後台]檔案管理-檔案上下架管理', N'B')
INSERT [dbo].[tbPermission] ([PerID], [PerName], [PerPage]) VALUES (N'M21', N'[後台]首頁管理-Banner上下架管理', N'B')
INSERT [dbo].[tbPermission] ([PerID], [PerName], [PerPage]) VALUES (N'M31', N'[後台]最新消息管理-Press room管理', N'B')
INSERT [dbo].[tbPermission] ([PerID], [PerName], [PerPage]) VALUES (N'M32', N'[後台]最新消息管理-Press room簽名檔管理', N'B')
INSERT [dbo].[tbPermission] ([PerID], [PerName], [PerPage]) VALUES (N'M51', N'[後台]CSR 報告管理-CSR報告上下架管理', N'B')
INSERT [dbo].[tbPermission] ([PerID], [PerName], [PerPage]) VALUES (N'M52', N'[後台]CSR 報告管理-下載次數統計(總計)', N'B')
INSERT [dbo].[tbPermission] ([PerID], [PerName], [PerPage]) VALUES (N'M53', N'[後台]CSR 報告管理-下載次數統計(問卷人數)', N'B')
INSERT [dbo].[tbPermission] ([PerID], [PerName], [PerPage]) VALUES (N'S01', N'[後台]系統管理-管理員管理', N'B')
INSERT [dbo].[tbPermission] ([PerID], [PerName], [PerPage]) VALUES (N'S04', N'[後台]系統管理-群組權限管理', N'B')
SET IDENTITY_INSERT [dbo].[tbPermRole] ON 

INSERT [dbo].[tbPermRole] ([ID], [PerID], [RolID], [RUPermission]) VALUES (247, N'S01', N'RW001', 15)
INSERT [dbo].[tbPermRole] ([ID], [PerID], [RolID], [RUPermission]) VALUES (248, N'M11', N'RW001', 15)
INSERT [dbo].[tbPermRole] ([ID], [PerID], [RolID], [RUPermission]) VALUES (249, N'M21', N'RW001', 15)
INSERT [dbo].[tbPermRole] ([ID], [PerID], [RolID], [RUPermission]) VALUES (250, N'M31', N'RW001', 15)
INSERT [dbo].[tbPermRole] ([ID], [PerID], [RolID], [RUPermission]) VALUES (251, N'M32', N'RW001', 15)
INSERT [dbo].[tbPermRole] ([ID], [PerID], [RolID], [RUPermission]) VALUES (253, N'M51', N'RW001', 15)
INSERT [dbo].[tbPermRole] ([ID], [PerID], [RolID], [RUPermission]) VALUES (254, N'M52', N'RW001', 15)
INSERT [dbo].[tbPermRole] ([ID], [PerID], [RolID], [RUPermission]) VALUES (255, N'M53', N'RW001', 15)
INSERT [dbo].[tbPermRole] ([ID], [PerID], [RolID], [RUPermission]) VALUES (256, N'S01', N'RW001', 15)
INSERT [dbo].[tbPermRole] ([ID], [PerID], [RolID], [RUPermission]) VALUES (258, N'S04', N'RW001', 15)
SET IDENTITY_INSERT [dbo].[tbPermRole] OFF
SET IDENTITY_INSERT [dbo].[tbPicRotate] ON 

INSERT [dbo].[tbPicRotate] ([ProID], [ProName], [ProName_EN], [ProDesc], [ProDesc_EN], [ProPicPath], [ProPicPath_M], [ProPicDesc], [ProUrl], [ProUrl_EN], [ProEnable], [ProType], [ProSort], [ProSetOD], [ProStartDate], [ProEndDate], [CreateUser], [CreateDate], [LastUpdateUser], [LastUpdateDate]) VALUES (25, N'理念與願景', N'Mission', N'提供高效的的半導體一元化解決方案 - 封裝/測試及系統組裝。

提昇研發能力。

遵循最高的公司治理標準。

落實可持續的商業理念。', N'Providing efficient turnkey services – IC packaging, testing, and system assembly.

Enhancing research & development capability.

Following the highest corporate governance standards.

Implementing sustainable business philosophies.', N'7d527b7c-6edb-4338-a80a-f571309b9123.jpg', N'99c6e281-2835-47f5-bcf1-02e628137797.jpg', N'', N'http://www.aseglobal.com/ch/about.html', N'http://www.aseglobal.com/en/about.html', 1, NULL, 1, 1, CAST(0x0000AA1E00000000 AS DateTime), CAST(0x0000AA1E00000000 AS DateTime), N'admin', CAST(0x0000AA1E00000000 AS DateTime), N'admin', CAST(0x0000AA1E01145C6F AS DateTime))
SET IDENTITY_INSERT [dbo].[tbPicRotate] OFF
INSERT [dbo].[tbRecipient] ([RcpID], [RcpName], [RcpEmail], [RcpDesc], [RcpEnable], [RcpSort], [CreateUser], [CreateDate], [LastUpdateUser], [LastUpdateDate], [LgeID], [RcpType]) VALUES (N'3d13c62b-8786-4589-b798-0c8aecc72e5f', N'狐狸', N'darthfox2@gmail.com', N'1111', 1, 0, N'admin', CAST(0x0000AA0800F4A310 AS DateTime), N'admin', CAST(0x0000AA290127E706 AS DateTime), NULL, N'S')
INSERT [dbo].[tbRecipient] ([RcpID], [RcpName], [RcpEmail], [RcpDesc], [RcpEnable], [RcpSort], [CreateUser], [CreateDate], [LastUpdateUser], [LastUpdateDate], [LgeID], [RcpType]) VALUES (N'3d13c62b-8786-4589-b798-0c8aecc72e5k', N'狐狸2', N'darthfox2@gmail.com', N'2222', 1, 0, N'admin', CAST(0x0000AA0800F4A310 AS DateTime), N'admin', CAST(0x0000AA290127EC4E AS DateTime), NULL, N'C')
INSERT [dbo].[tbRoles] ([RolID], [RolName], [RolEnable]) VALUES (N'RW001', N'最高管理者', 1)
INSERT [dbo].[tbRoles] ([RolID], [RolName], [RolEnable]) VALUES (N'RW002', N'一般管理者', 1)
INSERT [dbo].[tbRoles] ([RolID], [RolName], [RolEnable]) VALUES (N'RW003', N'一般使用者', 1)
INSERT [dbo].[tbUsers] ([UseID], [UseName], [UsePassword], [UseEnable], [UseCountry], [UseZipCode], [UseAddr], [UseCell], [UseTel], [UseFax], [UseEmail], [UseComName], [UseComTel], [UseComFax], [UseRType], [UseRTypeDesc], [UseIsActivity], [UseIsMatch], [LastUpdateUser], [LastUpdateDate], [CreateUser], [CreateDate], [RolID], [VerifyKey]) VALUES (N'admin', N'管理者', N'B7462F6D2F9BCFF9', 1, NULL, NULL, NULL, NULL, NULL, NULL, N'darthfox2@gmail.com', NULL, NULL, NULL, NULL, NULL, 0, 0, N'IEK', CAST(0x0000A9AD013256FA AS DateTime), N'admin', CAST(0x0000A98400BD3916 AS DateTime), N'RW001', NULL)
INSERT [dbo].[tbUsers] ([UseID], [UseName], [UsePassword], [UseEnable], [UseCountry], [UseZipCode], [UseAddr], [UseCell], [UseTel], [UseFax], [UseEmail], [UseComName], [UseComTel], [UseComFax], [UseRType], [UseRTypeDesc], [UseIsActivity], [UseIsMatch], [LastUpdateUser], [LastUpdateDate], [CreateUser], [CreateDate], [RolID], [VerifyKey]) VALUES (N'bear', N'熊出沒', N'B7462F6D2F9BCFF9', 1, NULL, NULL, NULL, NULL, NULL, NULL, N'bear@gamil.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'admin', CAST(0x0000AA1E00F96D9E AS DateTime), N'admin', CAST(0x0000AA1E00F96D9E AS DateTime), N'RW001', NULL)
INSERT [dbo].[tbWebContent] ([WctID], [WctName], [WctDesc], [WctEnable], [WctSort], [WctPicPath], [LastUpdateUser], [LastUpdateDate], [CreateUser], [CreateDate]) VALUES (N'W001', N'網站瀏覽人數', NULL, 1, 1252, NULL, N'admin', CAST(0x0000A9E000000000 AS DateTime), N'admin', CAST(0x0000A9E000000000 AS DateTime))
ALTER TABLE [dbo].[tbArticleContent]  WITH CHECK ADD  CONSTRAINT [FK_tbArticleContent_tbArticle] FOREIGN KEY([AtcID])
REFERENCES [dbo].[tbArticle] ([AtcID])
GO
ALTER TABLE [dbo].[tbArticleContent] CHECK CONSTRAINT [FK_tbArticleContent_tbArticle]
GO
ALTER TABLE [dbo].[tbEventRecord]  WITH CHECK ADD  CONSTRAINT [FK_tbEventRecord_tbEventType] FOREIGN KEY([EvrTypeID])
REFERENCES [dbo].[tbEventType] ([EvtTypeID])
GO
ALTER TABLE [dbo].[tbEventRecord] CHECK CONSTRAINT [FK_tbEventRecord_tbEventType]
GO
ALTER TABLE [dbo].[tbExceptionRecord]  WITH CHECK ADD  CONSTRAINT [FK_tbExceptionRecord_tbExceptionType] FOREIGN KEY([ExrTypeID])
REFERENCES [dbo].[tbExceptionType] ([ExtTypeID])
GO
ALTER TABLE [dbo].[tbExceptionRecord] CHECK CONSTRAINT [FK_tbExceptionRecord_tbExceptionType]
GO
ALTER TABLE [dbo].[tbPermRole]  WITH CHECK ADD  CONSTRAINT [FK_tbPermRole_tbPermission] FOREIGN KEY([PerID])
REFERENCES [dbo].[tbPermission] ([PerID])
GO
ALTER TABLE [dbo].[tbPermRole] CHECK CONSTRAINT [FK_tbPermRole_tbPermission]
GO
ALTER TABLE [dbo].[tbPermRole]  WITH CHECK ADD  CONSTRAINT [FK_tbPermRole_tbRoles] FOREIGN KEY([RolID])
REFERENCES [dbo].[tbRoles] ([RolID])
GO
ALTER TABLE [dbo].[tbPermRole] CHECK CONSTRAINT [FK_tbPermRole_tbRoles]
GO
ALTER TABLE [dbo].[tbUsers]  WITH CHECK ADD  CONSTRAINT [FK_tbUsers_tbRoles] FOREIGN KEY([RolID])
REFERENCES [dbo].[tbRoles] ([RolID])
GO
ALTER TABLE [dbo].[tbUsers] CHECK CONSTRAINT [FK_tbUsers_tbRoles]
GO
