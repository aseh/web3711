﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models;
using Service;
using Service.Interface;
using Service.Misc;
using ASEH_CMS.App_Code;

namespace ASEH_CMS
{
    public partial class System_UserList : PageBase
    {
        private IExceptionRecordService _exceptionRecordService;
        private IEventRecordService _eventRecordService;
        private IUsersService _usersService;
        private IRolesService _rolesService;
        private CommonClass _common = new CommonClass();
        UserInfo User = new UserInfo();
        Security _security = new Security();

        new protected int iPageType = 1;
        new protected string strPageID = "S01";

        protected void Page_Load(object sender, EventArgs e)
        {
            this._exceptionRecordService = new ExceptionRecordService();
            this._eventRecordService = new EventRecordService();
            this._usersService = new UsersService();
            this._rolesService = new RolesService();

            CheckUser();
            CheckUserPermission(strPageID, iPageType);

            if (!this.Page.IsPostBack)
            {
                //取得登入者ID
                User = (UserInfo)Session["UserInfo"];
                hideLoginUseID.Value = User.UserID;

                //Role
                IEnumerable<tbRoles> listRole = _rolesService.GetAll().Where(x => x.RolEnable == 1);
                ListItem item = null;
                ddlSelRole.Items.Clear();
                foreach (tbRoles element in listRole)
                {
                    item = new ListItem(element.RolName, element.RolID);
                    ddlSelRole.Items.Add(item);
                }
            }
        }

        //新增按鈕的動作
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            lblPW.Visible = true;
            txbPW.Visible = true;
            rfvPW.Visible = true;

            this.txbUseID.Enabled = true;
            this.txbUseID.Text = "";
            this.txbName.Text = "";
            this.txbPW.Text = "";
            this.txbEmail.Text = "";

            //Role
            IEnumerable<tbRoles> listRole = _rolesService.GetAll();
            ListItem item = null;
            ddlRole.Items.Clear();
            foreach (tbRoles element in listRole)
            {
                item = new ListItem(element.RolName, element.RolID);
                ddlRole.Items.Add(item);
            }
            //定位角色選擇
            ddlRole.Items.FindByValue(ddlSelRole.SelectedValue).Selected = true;

            //是否啟用
            this.ckbIsEnable.Checked = true;

            this.btnOK.Visible = true;
            this.btnEdit.Visible = false;
            this.MultiView1.ActiveViewIndex = 1;
        }

        //查詢的動作
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            grid.DataBind();
        }

        //返回按鈕
        protected void btnBack_Click(object sender, EventArgs e)
        {
            this.MultiView1.ActiveViewIndex = 0;
        }

        //新增存檔
        protected void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new tbUsers();
                model.UseID = txbUseID.Text.Trim();
                model.UseName = txbName.Text;
                //密碼加密
                model.UsePassword = _security.Encrypt(txbPW.Text);
                model.UseEnable = _common.GetChecked(ckbIsEnable);
                model.CreateUser = hideLoginUseID.Value;
                model.CreateDate = DateTime.Now;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;
                model.RolID = ddlRole.SelectedValue;

                model.UseEmail = txbEmail.Text;

                _usersService.Create(model);

                //ShowMessage("儲存成功");
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }

            this.MultiView1.ActiveViewIndex = 0;
            this.grid.DataBind();
        }

        //修改存檔
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var model = _usersService.GetByID(hideUseID.Value);
                model.UseID = txbUseID.Text;
                model.UseName = txbName.Text;

                //密碼加密
                model.UsePassword = _security.Encrypt(txbPW.Text);

                model.UseEnable = _common.GetChecked(ckbIsEnable);
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;
                model.RolID = ddlRole.SelectedValue;

                model.UseEmail = txbEmail.Text;

                _usersService.Update(model);

                //ShowMessage("儲存成功");
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }

            this.MultiView1.ActiveViewIndex = 0;
            this.grid.DataBind();
        }

        #region GridView動作

        protected void grid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Modify":
                    {
                        //隱藏密碼
                        lblPW.Visible = true;
                        txbPW.Visible = true;
                        rfvPW.Visible = true;

                        string UseID = e.CommandArgument.ToString();

                        #region 最後登入時間

                        List<tbEventRecord> check = _eventRecordService.GetAll().Where(x => x.UseID == UseID).ToList();
                        if (check.Count != 0)
                        {
                            var modelE = _eventRecordService.GetAll().Where(x => x.UseID == UseID).OrderByDescending(o => o.CreateTime).First();
                            lblLoginDate2.Text = Convert.ToDateTime(modelE.CreateTime).ToString("yyyy/MM/dd HH:mm:ss");
                        }
                        else
                        {
                            lblLoginDate2.Text = "未登入";
                        }

                        #endregion

                        var model = _usersService.GetByID(UseID);

                        this.txbUseID.Text = model.UseID;
                        this.txbUseID.Enabled = false;//禁止修改帳號
                        this.hideUseID.Value = model.UseID;//暫存原始ID
                        this.txbName.Text = model.UseName;

                        this.txbPW.Text = _security.Decrypt(model.UsePassword);

                        this.txbEmail.Text = model.UseEmail;
                        
                        _common.SetChecked(ckbIsEnable, Convert.ToInt32(model.UseEnable));


                        //Role
                        IEnumerable<tbRoles> listRole = _rolesService.GetAll().Where(x => x.RolEnable == 1);
                        ListItem item = null;
                        ddlRole.Items.Clear();
                        foreach (tbRoles element in listRole)
                        {
                            item = new ListItem(element.RolName, element.RolID);
                            ddlRole.Items.Add(item);
                        }
                        //定位角色選擇
                        ddlRole.Items.FindByValue(model.RolID).Selected = true;

                        this.btnOK.Visible = false;
                        this.btnEdit.Visible = true;
                        this.MultiView1.ActiveViewIndex = 1;
                        break;
                    }
                case "Cancel":
                    {
                        string UseID = e.CommandArgument.ToString();

                        if (UseID == "admin")
                        {
                            ShowMessage("管理者帳號不允許刪除");
                        }
                        else
                        {
                            _usersService.Delete(UseID);
                            this.grid.DataBind();
                        }
                        break;
                    }
                default:
                    {
                        return;
                    }
            }
        }

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //最後登入時間
            Label newlblLoginDate = (Label)e.Row.FindControl("lblLoginDate");
            if (newlblLoginDate != null)
            {
                string UseID = newlblLoginDate.Text;

                List<tbEventRecord> check = _eventRecordService.GetAll().Where(x => x.UseID == UseID).ToList();
                if (check.Count != 0)
                {
                    var modelE = _eventRecordService.GetAll().Where(x => x.UseID == UseID).OrderByDescending(o => o.CreateTime).First();
                    newlblLoginDate.Text = Convert.ToDateTime(modelE.CreateTime).ToString("yyyy/MM/dd HH:mm:ss");
                }
                else
                {
                    newlblLoginDate.Text = "-";
                }
            }

            //是否啟用
            Label newLabel = (Label)e.Row.FindControl("lblIsEnable");
            if (newLabel != null)
            {
                if (int.Parse(newLabel.Text) == 1)
                {
                    Label lblEnable = _common.ChangeIsEnable(1);
                    newLabel.Text = lblEnable.Text;
                }
                else if (int.Parse(newLabel.Text) == 0)
                {
                    Label lblEnable = _common.ChangeIsEnable(0);
                    newLabel.Text = lblEnable.Text;
                    newLabel.ForeColor = lblEnable.ForeColor;
                }
            }

            //群組權限
            Label newlblRolName = (Label)e.Row.FindControl("lblRolName");
            if (newlblRolName != null)
            {
                string RolID = newlblRolName.Text;

                var model = _rolesService.GetByID(RolID);

                newlblRolName.Text = model.RolName;
            }
        }

        #endregion

    }
}