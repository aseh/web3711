﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models;
using Service;
using Service.Interface;
using Service.Misc;
using ASEH_CMS.App_Code;

namespace ASEH_CMS
{
    public partial class Manage_RecipientList : PageBase
    {
        private IExceptionRecordService _exceptionRecordService;
        private IEventRecordService _eventRecordService;
        private IRecipientService _recipientService;
        private CommonClass _common = new CommonClass();
        UserInfo User = new UserInfo();
        Security _security = new Security();

        new protected int iPageType = 1;
        new protected string strPageID = "S02";

        protected void Page_Load(object sender, EventArgs e)
        {
            _exceptionRecordService = new ExceptionRecordService();
            _eventRecordService = new EventRecordService();
            _recipientService = new RecipientService();
           
            CheckUser();
            CheckUserPermission(strPageID, iPageType);

            if (!this.Page.IsPostBack)
            {
                //取得登入者ID
                User = (UserInfo)Session["UserInfo"];
                hideLoginUseID.Value = User.UserID;

                //載入內容
                SetPageContent();
            }
        }

        #region 下拉選單的動作

        //群組權限下拉選單的動作
        protected void ddlSelRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            //載入內容
            SetPageContent();
        }

        //排序下拉選單的動作
        protected void ddlOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
            //載入內容
            SetPageContent();
        }

        //排序類型下拉選單的動作
        protected void ddlOrderType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //載入內容
            SetPageContent();
        }

        #endregion

        //查詢的動作
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //載入內容
            SetPageContent();
        }

        //新增按鈕的動作
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //清空
            txbRcpName2.Text = "";
            txbRcpEmail2.Text = "";
            txbRcpDesc2.Text = "";
            ckbIsEnable2.Checked = true;

            this.btnOK2.Visible = true;
            this.btnEdit2.Visible = false;
            this.MultiView1.ActiveViewIndex = 1;
        }

        //返回的動作
        protected void btnBack2_Click(object sender, EventArgs e)
        {
            this.MultiView1.ActiveViewIndex = 0;
        }

        //新增存檔
        protected void btnOK2_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new tbRecipient();
                string RcpID = Guid.NewGuid().ToString();
                model.RcpID = RcpID;
                model.RcpName = txbRcpName2.Text;
                model.RcpEmail = txbRcpEmail2.Text;
                model.RcpDesc = txbRcpDesc2.Text;

                model.RcpEnable = _common.GetChecked(ckbIsEnable2);
                model.RcpSort = 0;

                model.CreateUser = hideLoginUseID.Value;
                model.CreateDate = DateTime.Now;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;

                _recipientService.Create(model);

                //ShowMessage("儲存成功");

                SetPageContent();

                this.MultiView1.ActiveViewIndex = 0;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        //修改存檔
        protected void btnEdit2_Click(object sender, EventArgs e)
        {
            try
            {
                var model = _recipientService.GetByID(hideRcpID.Value);
                model.RcpName = txbRcpName2.Text;
                model.RcpEmail = txbRcpEmail2.Text;
                model.RcpDesc = txbRcpDesc2.Text;

                model.RcpEnable = _common.GetChecked(ckbIsEnable2);

                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;

                _recipientService.Update(model);

                //ShowMessage("儲存成功");

                SetPageContent();

                this.MultiView1.ActiveViewIndex = 0;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        #region GridView動作

        protected void grid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Modify":
                    {
                        //ID
                        string RcpID = e.CommandArgument.ToString();
                        //暫存
                        hideRcpID.Value = RcpID;

                        var model = _recipientService.GetByID(RcpID);
                        txbRcpName2.Text = model.RcpName;
                        txbRcpEmail2.Text = model.RcpEmail;
                        txbRcpDesc2.Text = model.RcpDesc;
                      
                        _common.SetChecked(ckbIsEnable2, Convert.ToInt32(model.RcpEnable));
                        
                         this.btnOK2.Visible = false;
                        this.btnEdit2.Visible = true;
                        this.MultiView1.ActiveViewIndex = 1;

                        break;
                    }
                case "Remove":
                    {
                        string RcpID = e.CommandArgument.ToString();

                        _recipientService.Delete(RcpID);

                        //載入內容
                        SetPageContent();

                        break;
                    }
                default:
                    {
                        return;
                    }
            }
        }

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //項次
            Label newlblNO = (Label)e.Row.FindControl("lblNO");
            if (newlblNO != null)
            {
                int NO = e.Row.RowIndex + 1;
                newlblNO.Text = NO.ToString();
            }

            Label newLabel = (Label)e.Row.FindControl("lblIsEnable");
            if (newLabel != null)
            {
                if (int.Parse(newLabel.Text) == 1)
                {
                    Label lblEnable = _common.ChangeIsEnable(1);
                    newLabel.Text = lblEnable.Text;
                }
                else if (int.Parse(newLabel.Text) == 0)
                {
                    Label lblEnable = _common.ChangeIsEnable(0);
                    newLabel.Text = lblEnable.Text;
                    newLabel.ForeColor = lblEnable.ForeColor;
                }
            }
        }

        #endregion

        #region 方法

        //載入內容
        protected void SetPageContent()
        {
            //取得類別
            List<tbRecipient> listU;

            listU = _recipientService.GetAll().Where(x => x.RcpEmail.Contains(txbSearchName.Text) || x.RcpName.Contains(txbSearchName.Text)).Where(x => x.RcpType == "S").OrderByDescending(s => s.LastUpdateDate).ToList();
          
            #region 排序設定

            string OrderBy = ddlOrder.SelectedValue;

            if (ddlOrderType.SelectedValue == "DESC")
            {
                switch (OrderBy)
                {
                    case "CreateDate":
                        {
                            listU = listU.OrderByDescending(o => o.CreateDate).ToList();
                            break;
                        }
                    case "RcpName":
                        {
                            listU = listU.OrderByDescending(o => o.RcpName).ToList();
                            break;
                        }
                    case "RcpEmail":
                        {
                            listU = listU.OrderByDescending(o => o.RcpEmail).ToList();
                            break;
                        }
                    case "RcpEnable":
                        {
                            listU = listU.OrderByDescending(o => o.RcpEnable).ToList();
                            break;
                        }
                }
            }
            else
            {
                switch (OrderBy)
                {
                    case "CreateDate":
                        {
                            listU = listU.OrderBy(o => o.CreateDate).ToList();
                            break;
                        }
                    case "RcpName":
                        {
                            listU = listU.OrderByDescending(o => o.RcpName).ToList();
                            break;
                        }
                    case "RcpEmail":
                        {
                            listU = listU.OrderByDescending(o => o.RcpEmail).ToList();
                            break;
                        }
                    case "RcpEnable":
                        {
                            listU = listU.OrderByDescending(o => o.RcpEnable).ToList();
                            break;
                        }
                }
            }

            #endregion

            grid.DataSource = listU;
            grid.DataBind();
        }

        #endregion



     


    }
}