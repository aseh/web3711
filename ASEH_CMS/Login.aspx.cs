﻿using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models;
using Service.Interface;
using Service.Misc;
using System.Web.Security;
using System.Web.Configuration;

namespace ASEH_CMS
{
    public partial class Login : System.Web.UI.Page
    {
        UserInfo User = new UserInfo();
        private IEventRecordService _eventRecordService;
        private IExceptionRecordService _exceptionRecordService;
        private IUsersService _usersService;

        private string strReturnPage = "";

        string IsByPass = WebConfigurationManager.AppSettings["IsByPass"].ToString();

        //起始載入
        protected void Page_Load(object sender, EventArgs e)
        {
            this._eventRecordService = new EventRecordService();
            this._exceptionRecordService = new ExceptionRecordService();
            this._usersService = new UsersService();

            if (!Page.IsPostBack)
            {
                if (object.Equals(Request.QueryString, null) == false)
                {
                    if (object.Equals(Request.QueryString["ReturnPage"], null) == false)
                    {
                        strReturnPage = Request.QueryString["ReturnPage"].ToString() + ".aspx";
                    }
                    else
                    {
                        this.Session.Clear();
                        strReturnPage = "./MainIndex.aspx";
                    }
                }
                else
                {
                    this.Session.Clear();
                    strReturnPage = "./MainIndex.aspx";
                }
                ViewState["ReturnPage"] = strReturnPage;
            }
            else
            {
                strReturnPage = (string)ViewState["ReturnPage"];
            }
        }

        //送出按鈕
        protected void imgSend_Click(object sender, ImageClickEventArgs e)
        {
               //是否快速通關
            if (IsByPass == "N")
            {
                //亂數驗證碼
                if (Request.Cookies["CAPTCHA"].Value == txbRanCode.Text)
                {
                    lblLoginMsg.Text = "";

                    string TT = UserID.Text + Password.Text;

                    try
                    {
                        //驗證角色(最高管理者)
                        bool Check = _usersService.IsExists(UserID.Text.Trim());
                        if (Check)
                        {
                            var itemUser = _usersService.GetByID(UserID.Text.Trim());
                            string RolID = itemUser.RolID;

                            if (RolID == "RW001")
                            {
                                //驗證帳號
                                User = _usersService.Authen(UserID.Text, Password.Text);

                                if (User.IsAuthen == true)
                                {
                                    Session["UserInfo"] = User;
                                    FormsAuthentication.RedirectFromLoginPage(User.UserID, false);

                                    //EVENT LOG
                                    User = (UserInfo)Session["UserInfo"];
                                    string strDesc = string.Format("使用者［{0}］後台登入", User.UserID);

                                    var model = new tbEventRecord();
                                    model.CreateTime = DateTime.Now;
                                    model.EvrTypeID = "EVT001";
                                    model.EvrMsg = strDesc;
                                    model.UseID = User.UserID;
                                    _eventRecordService.Create(model);

                                    //轉換頁面
                                    Response.Redirect(strReturnPage);
                                }
                                else
                                {
                                    lblLoginMsg.Text = "登入失敗";
                                }
                            }
                            else
                            {
                                lblLoginMsg.Text = "您沒有權限登入系統";
                            }
                        }
                        else
                        {
                            lblLoginMsg.Text = "帳號不存在";
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
                else
                {
                    txbRanCode.Text = "";
                    lblLoginMsg.Text = "驗證碼錯誤!";
                }
            }
            else//快速通關
            {
                //驗證帳號
                User = _usersService.Authen(UserID.Text, Password.Text);

                if (User.IsAuthen == true)
                {
                    Session["UserInfo"] = User;
                    FormsAuthentication.RedirectFromLoginPage(User.UserID, false);

                    //EVENT LOG
                    User = (UserInfo)Session["UserInfo"];
                    string strDesc = string.Format("使用者［{0}］後台登入", User.UserID);

                    var model = new tbEventRecord();
                    model.CreateTime = DateTime.Now;
                    model.EvrTypeID = "EVT001";
                    model.EvrMsg = strDesc;
                    model.UseID = User.UserID;
                    _eventRecordService.Create(model);

                    //轉換頁面
                    Response.Redirect(strReturnPage);
                }
                else
                {
                    lblLoginMsg.Text = "登入失敗";
                }
            }
        }

        //清除按鈕
        //protected void Cancel_Click(object sender, EventArgs e)
        //{
        //    UserID.Text = "";
        //    Password.Text = "";
        //}
    }
}