﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models;
using Service;
using Service.Interface;
using Service.Misc;
using ASEH_CMS.App_Code;
using System.Web.Configuration;
using System.IO;
using System.Text;

namespace ASEH_CMS
{
    public partial class Manage_Report_Total : PageBase
    {
        private IExceptionRecordService _exceptionRecordService;
        private IFileService _fileService;
        private IFileLogService _fileLogService;
        private CommonClass _common = new CommonClass();
        UserInfo User = new UserInfo();
        new protected int iPageType = 1;
        new protected string strPageID = "M52";

        //起始載入
        protected void Page_Load(object sender, EventArgs e)
        {
            this._exceptionRecordService = new ExceptionRecordService();
            this._fileService = new FileService();
            this._fileLogService = new FileLogService();

            CheckUser();
            CheckUserPermission(strPageID, iPageType);

            if (!this.Page.IsPostBack)
            {
                //取得登入者ID
                User = (UserInfo)Session["UserInfo"];
                hideLoginUseID.Value = User.UserID;

                //載入年度
                _common.SetWYearBeforeList(ddlYear, 2019);

                //載入內容
                SetPageContent("FIT001");
            }
        }

        //返回的動作
        protected void btnBack_Click(object sender, EventArgs e)
        {
            this.MultiView1.ActiveViewIndex = 0;
        }

        #region 下拉選單動作

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            //載入內容
            SetPageContent("FIT001");
        }

        protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            //載入內容
            SetPageContent("FIT001");
        }

        #endregion

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //載入內容
            SetPageContent("FIT001");
        }

        #region GridView動作

        protected void grid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            
        }

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //標題
            Label newlblName = (Label)e.Row.FindControl("lblName");
            if (newlblName != null)
            {
                string FIeID = newlblName.Text.Split(',')[0];

                var model = _fileService.GetByID(FIeID);
                if (model != null)
                {
                    newlblName.Text = string.Format("{0}／{1}", model.FieName, model.FieName_EN);
                }
            }

            //年月
            Label newlblDate = (Label)e.Row.FindControl("lblDate");
            if (newlblDate != null)
            {
                string FIeID = newlblDate.Text.Split(',')[0];
                string Month = newlblDate.Text.Split(',')[1];

                newlblDate.Text = string.Format("{0}／{1}", ddlYear.SelectedValue, Month);
            }

            //中文版人數
            Label lblDownloadCh = (Label)e.Row.FindControl("lblDownloadCh");

            if (lblDownloadCh != null)
            {
                string FIeID = lblDownloadCh.Text.Split(',')[0];
                string Month = lblDownloadCh.Text.Split(',')[1];

                if (FIeID != "")
                {
                    //先取得設定日期
                    DateTime dtSetS = DateTime.Now;
                    DateTime dtSetE = DateTime.Now;

                    if (ddlMonth.SelectedValue != "-1")
                    {
                        dtSetS = Convert.ToDateTime(string.Format("{0}/{1}/01 00:00:00", ddlYear.SelectedValue, ddlMonth.SelectedValue));
                        dtSetE = Convert.ToDateTime(string.Format("{0}/{1}/01 23:59:59", ddlYear.SelectedValue, ddlMonth.SelectedValue)).AddMonths(1).AddDays(-1);
                    }
                    else
                    {
                        dtSetS = Convert.ToDateTime(string.Format("{0}/{1}/01 00:00:00", ddlYear.SelectedValue, Month));
                        dtSetE = Convert.ToDateTime(string.Format("{0}/{1}/01 23:59:59", ddlYear.SelectedValue, Month)).AddMonths(1).AddDays(-1);
                    }

                    List<tbFileLog> listFL = _fileLogService.GetAll().Where(x => x.FIeID == FIeID).Where(x => x.CreateTime >= dtSetS && x.CreateTime <= dtSetE).Where(x => x.FigDesc == "Ch/Download").ToList();

                    lblDownloadCh.Text = listFL.Count.ToString();
                }
            }

            //英文版人數
            Label lblDownloadEn = (Label)e.Row.FindControl("lblDownloadEn");

            if (lblDownloadEn != null)
            {
                string FIeID = lblDownloadEn.Text.Split(',')[0];
                string Month = lblDownloadEn.Text.Split(',')[1];

                if (FIeID != "")
                {
                    //先取得設定日期
                    DateTime dtSetS = DateTime.Now;
                    DateTime dtSetE = DateTime.Now;

                    if (ddlMonth.SelectedValue != "-1")
                    {
                        dtSetS = Convert.ToDateTime(string.Format("{0}/{1}/01 00:00:00", ddlYear.SelectedValue, ddlMonth.SelectedValue));
                        dtSetE = Convert.ToDateTime(string.Format("{0}/{1}/01 23:59:59", ddlYear.SelectedValue, ddlMonth.SelectedValue)).AddMonths(1).AddDays(-1);
                    }
                    else
                    {
                        dtSetS = Convert.ToDateTime(string.Format("{0}/{1}/01 00:00:00", ddlYear.SelectedValue, Month));
                        dtSetE = Convert.ToDateTime(string.Format("{0}/{1}/01 23:59:59", ddlYear.SelectedValue, Month)).AddMonths(1).AddDays(-1);
                    }

                    List<tbFileLog> listFL = _fileLogService.GetAll().Where(x => x.FIeID == FIeID).Where(x => x.CreateTime >= dtSetS && x.CreateTime <= dtSetE).Where(x => x.FigDesc == "En/Download").ToList();

                    lblDownloadEn.Text = listFL.Count.ToString();
                }
            }
        }

        #endregion

        #region 方法

        public class FLResult
        {
            public string Key { get; set; } 	//Id
            public string Month { get; set; } 
        }

        //載入內容
        protected void SetPageContent(string FitID)
        {
            DateTime dtSetS = DateTime.Now;
            DateTime dtSetE = DateTime.Now;
            if (ddlMonth.SelectedValue != "-1")
            {
                List<FLResult> listFLResult = new List<FLResult>();

                dtSetS = Convert.ToDateTime(string.Format("{0}/{1}/01 00:00:00", ddlYear.SelectedValue, ddlMonth.SelectedValue));
                dtSetE = Convert.ToDateTime(string.Format("{0}/{1}/01 23:59:59", ddlYear.SelectedValue, ddlMonth.SelectedValue)).AddMonths(1).AddDays(-1);

                List<tbFileLog> listFL = _fileLogService.GetAll().Where(x => x.CreateTime >= dtSetS && x.CreateTime <= dtSetE).Where(x => x.FigDesc.Split('/')[1] == "Download").OrderBy(o => o.CreateTime).ToList();
                var FLTemp = listFL.GroupBy(p => p.FIeID).Where(g => g.Count() >= 1).ToList();

                for (int i = 0; i < FLTemp.Count; i++)
                {
                    FLResult FLResultTemp = new FLResult();
                    FLResultTemp.Key = FLTemp[i].Key.ToString();
                    FLResultTemp.Month = ddlMonth.SelectedValue;

                    listFLResult.Add(FLResultTemp);
                }

                var listResult = listFLResult.ToList();

                grid.DataSource = listResult;
                grid.DataBind();
            }
            else
            {
                List<FLResult> listFLResult = new List<FLResult>();
                for (int m = 1 ; m <= 12 ; m++)
                {
                    dtSetS = Convert.ToDateTime(string.Format("{0}/{1}/01 00:00:00", ddlYear.SelectedValue, m));
                    dtSetE = Convert.ToDateTime(string.Format("{0}/{1}/01 23:59:59", ddlYear.SelectedValue, m)).AddMonths(1).AddDays(-1);

                    List<tbFileLog> listFL = _fileLogService.GetAll().Where(x => x.CreateTime >= dtSetS && x.CreateTime <= dtSetE).Where(x => x.FigDesc.Split('/')[1] == "Download").OrderBy(o => o.CreateTime).ToList();
                    var FLTemp = listFL.GroupBy(p => p.FIeID).Where(g => g.Count() >= 1).ToList();

                    for (int i = 0; i < FLTemp.Count; i++)
                    {
                        FLResult FLResultTemp = new FLResult();
                        FLResultTemp.Key = FLTemp[i].Key.ToString();
                        FLResultTemp.Month = m.ToString();

                        listFLResult.Add(FLResultTemp);
                    }
                }

                var listResult = listFLResult.ToList();

                grid.DataSource = listResult;
                grid.DataBind();
            }
        }

        #endregion

        #region 匯出/匯入

        //匯出CSV
        protected void btnExport_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.ContentType = "text/comma-separated-values;charset=UTF-8";
            Response.AddHeader("content-disposition", "attachment; filename=Export_Total.csv");

            StreamWriter sw = new StreamWriter(Response.OutputStream, Encoding.GetEncoding("UTF-8"));

            #region 取得內容

            DateTime dtSetS = DateTime.Now;
            DateTime dtSetE = DateTime.Now;

            List<FLResult> listFLResult = new List<FLResult>();
            if (ddlMonth.SelectedValue != "-1")
            {
                dtSetS = Convert.ToDateTime(string.Format("{0}/{1}/01 00:00:00", ddlYear.SelectedValue, ddlMonth.SelectedValue));
                dtSetE = Convert.ToDateTime(string.Format("{0}/{1}/01 23:59:59", ddlYear.SelectedValue, ddlMonth.SelectedValue)).AddMonths(1).AddDays(-1);

                List<tbFileLog> listFL = _fileLogService.GetAll().Where(x => x.CreateTime >= dtSetS && x.CreateTime <= dtSetE).Where(x => x.FigDesc.Split('/')[1] == "Download").OrderBy(o => o.CreateTime).ToList();
                var FLTemp = listFL.GroupBy(p => p.FIeID).Where(g => g.Count() >= 1).ToList();

                for (int i = 0; i < FLTemp.Count; i++)
                {
                    FLResult FLResultTemp = new FLResult();
                    FLResultTemp.Key = FLTemp[i].Key.ToString();
                    FLResultTemp.Month = ddlMonth.SelectedValue;

                    listFLResult.Add(FLResultTemp);
                }
            }
            else
            {
                for (int m = 1; m <= 12; m++)
                {
                    dtSetS = Convert.ToDateTime(string.Format("{0}/{1}/01 00:00:00", ddlYear.SelectedValue, m));
                    dtSetE = Convert.ToDateTime(string.Format("{0}/{1}/01 23:59:59", ddlYear.SelectedValue, m)).AddMonths(1).AddDays(-1);

                    List<tbFileLog> listFL = _fileLogService.GetAll().Where(x => x.CreateTime >= dtSetS && x.CreateTime <= dtSetE).Where(x => x.FigDesc.Split('/')[1] == "Download").OrderBy(o => o.CreateTime).ToList();
                    var FLTemp = listFL.GroupBy(p => p.FIeID).Where(g => g.Count() >= 1).ToList();

                    for (int i = 0; i < FLTemp.Count; i++)
                    {
                        FLResult FLResultTemp = new FLResult();
                        FLResultTemp.Key = FLTemp[i].Key.ToString();
                        FLResultTemp.Month = m.ToString();

                        listFLResult.Add(FLResultTemp);
                    }
                }
            }

            var listResult = listFLResult.ToList();

            #endregion

            for (int i = 0; i < listResult.Count; i++)
            {
                if (i == 0)
                {
                    sw.Write("標題" + "," + "年/月" + "," + "中文版人數" + "," + "英文版人數" + "\r\n");
                }

                string FIeID = listResult[i].Key;
                string Month = listResult[i].Month;


                //標題
                string newName = "";

                var model = _fileService.GetByID(FIeID);
                if (model != null)
                {
                    newName = string.Format("{0}／{1}", model.FieName, model.FieName_EN);
                }

                //年月
                string newDate = "";

                newDate = string.Format("{0}／{1}", ddlYear.SelectedValue, Month);

                //人數
                string chDownload = "";
                string enDownload = "";

                //先取得設定日期
                DateTime dtSetS2 = DateTime.Now;
                DateTime dtSetE2 = DateTime.Now;
                if (ddlMonth.SelectedValue != "-1")
                {
                    dtSetS2 = Convert.ToDateTime(string.Format("{0}/{1}/01 00:00:00", ddlYear.SelectedValue, ddlMonth.SelectedValue));
                    dtSetE2 = Convert.ToDateTime(string.Format("{0}/{1}/01 23:59:59", ddlYear.SelectedValue, ddlMonth.SelectedValue)).AddMonths(1).AddDays(-1);
                }
                else
                {
                    dtSetS2 = Convert.ToDateTime(string.Format("{0}/{1}/01 00:00:00", ddlYear.SelectedValue, Month));
                    dtSetE2 = Convert.ToDateTime(string.Format("{0}/{1}/01 23:59:59", ddlYear.SelectedValue, Month)).AddMonths(1).AddDays(-1);
                }

                List<tbFileLog> listFLCh = _fileLogService.GetAll().Where(x => x.FIeID == FIeID).Where(x => x.CreateTime >= dtSetS2 && x.CreateTime <= dtSetE2).Where(x => x.FigDesc == "Ch/Download").ToList();
                List<tbFileLog> listFLEn = _fileLogService.GetAll().Where(x => x.FIeID == FIeID).Where(x => x.CreateTime >= dtSetS2 && x.CreateTime <= dtSetE2).Where(x => x.FigDesc == "En/Download").ToList();

                chDownload = listFLCh.Count.ToString();
                enDownload = listFLEn.Count.ToString();

                sw.Write(newName + "," + newDate + "," + chDownload + "," + enDownload + "\r\n");
            }
            sw.WriteLine();
            sw.Close();

            Response.End();
        }

        #endregion



       

        
    }
}