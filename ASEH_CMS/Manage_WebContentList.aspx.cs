﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models;
using Service;
using Service.Interface;
using Service.Misc;
using ASEH_CMS.App_Code;
using System.Web.Configuration;
using System.IO;

namespace ASEH_CMS
{
    public partial class Manage_WebContentList : PageBase
    {
        private IExceptionRecordService _exceptionRecordService;
        private IWebContentService _webContentService;
        private CommonClass _common = new CommonClass();
        UserInfo User = new UserInfo();
        new protected int iPageType = 1;
        new protected string strPageID = "M12";

        //起始載入
        protected void Page_Load(object sender, EventArgs e)
        {
            this._exceptionRecordService = new ExceptionRecordService();
            this._webContentService = new WebContentService();

            CheckUser();
            CheckUserPermission(strPageID, iPageType);

            if (!this.Page.IsPostBack)
            {
                //取得登入者ID
                User = (UserInfo)Session["UserInfo"];
                hideLoginUseID.Value = User.UserID;
            }
        }

        //返回的動作
        protected void btnBack_Click(object sender, EventArgs e)
        {
            this.MultiView1.ActiveViewIndex = 0;
        }

        //新增按鈕的動作
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            ckbIsEnable.Checked = true;

            #region 圖片

            imgWctPicPath.ImageUrl = "Images/default.jpg";

            #endregion

            this.btnOK.Visible = true;
            this.btnEdit.Visible = false;
            this.MultiView1.ActiveViewIndex = 1;
        }

        //新增儲存動作
        protected void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new tbWebContent();
                model.WctName = txbWctName2.Text;
                model.WctEnable = _common.GetChecked(ckbIsEnable);

                model.CreateUser = hideLoginUseID.Value;
                model.CreateDate = DateTime.Now;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;

                model.WctSort = 0;

                _webContentService.Create(model);

                #region 圖片

                string WctID = model.WctID.ToString();

                string newPicPathFileName = "";
                //上傳圖片
                bool PicCheck = true;
                if (uploadPicPath.HasFile)
                {
                    Boolean FileOK = false;

                    string fileExtension = System.IO.Path.GetExtension(uploadPicPath.FileName).ToLower();
                    string[] allowExtensions = { ".bmp", ".jpg", ".jpeg", ".png" };
                    for (int i = 0; i < allowExtensions.Length; i++)
                    {
                        if (fileExtension == allowExtensions[i])
                        {
                            FileOK = true;
                        }
                    }

                    if (FileOK)
                    {
                        //PNG特別處理
                        string FileNameOrg = uploadPicPath.FileName;
                        string Name = Path.GetFileNameWithoutExtension(FileNameOrg);
                        string Extension = Path.GetExtension(FileNameOrg);

                        if (Extension == ".png")
                        {
                            string FileName = Guid.NewGuid().ToString();
                            newPicPathFileName = Path.ChangeExtension(FileName, ".png");
                            string FolderPath = string.Format("Uploads/{0}/", WctID);
                            string RealFolderPath = Server.MapPath(FolderPath);

                            int returnCode = _common.UploadAnyFileWithNewName(uploadPicPath, RealFolderPath, FileName);
                        }
                        else
                        {
                            string FileName = Guid.NewGuid().ToString();
                            newPicPathFileName = Path.ChangeExtension(FileName, ".jpg");
                            string FolderPath = string.Format("Uploads/{0}/", WctID);
                            string RealFolderPath = Server.MapPath(FolderPath);

                            int returnCode = _common.UploadOrgJpgPicWithNewName(uploadPicPath, RealFolderPath, FileName);
                        }

                        PicCheck = true;
                    }
                    else
                    {
                        PicCheck = false;
                    }
                }

                if (!PicCheck)
                {
                    ShowMessage("圖片上傳失敗，請確認檔案格式是否正確");
                }

                #endregion

                model.WctPicPath = newPicPathFileName;

                _webContentService.Update(model);

                //ShowMessage("儲存成功");

                //更新列表
                grid.DataBind();

                this.MultiView1.ActiveViewIndex = 0;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        //確定修改動作
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var model = _webContentService.GetByID(hideID.Value);
                model.WctName = txbWctName2.Text;
                model.WctEnable = _common.GetChecked(ckbIsEnable);

                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;

                #region 圖片

                string WctID = model.WctID.ToString();

                string newPicPathFileName = "";
                //上傳圖片
                bool PicCheck = true;
                if (uploadPicPath.HasFile)
                {
                    Boolean FileOK = false;

                    string fileExtension = System.IO.Path.GetExtension(uploadPicPath.FileName).ToLower();
                    string[] allowExtensions = { ".bmp", ".jpg", ".jpeg", ".png" };
                    for (int i = 0; i < allowExtensions.Length; i++)
                    {
                        if (fileExtension == allowExtensions[i])
                        {
                            FileOK = true;
                        }
                    }

                    if (FileOK)
                    {
                        //PNG特別處理
                        string FileNameOrg = uploadPicPath.FileName;
                        string Name = Path.GetFileNameWithoutExtension(FileNameOrg);
                        string Extension = Path.GetExtension(FileNameOrg);

                        if (Extension == ".png")
                        {
                            string FileName = Guid.NewGuid().ToString();
                            newPicPathFileName = Path.ChangeExtension(FileName, ".png");
                            string FolderPath = string.Format("Uploads/{0}/", WctID);
                            string RealFolderPath = Server.MapPath(FolderPath);

                            int returnCode = _common.UploadAnyFileWithNewName(uploadPicPath, RealFolderPath, FileName);
                        }
                        else
                        {
                            string FileName = Guid.NewGuid().ToString();
                            newPicPathFileName = Path.ChangeExtension(FileName, ".jpg");
                            string FolderPath = string.Format("Uploads/{0}/", WctID);
                            string RealFolderPath = Server.MapPath(FolderPath);

                            int returnCode = _common.UploadOrgJpgPicWithNewName(uploadPicPath, RealFolderPath, FileName);
                        }

                        PicCheck = true;
                    }
                    else
                    {
                        PicCheck = false;
                    }
                }

                if (!PicCheck)
                {
                    ShowMessage("圖片上傳失敗，請確認檔案格式是否正確");
                }

                #endregion

                //圖片判斷
                if (uploadPicPath.FileName != "")
                {
                    model.WctPicPath = newPicPathFileName;
                }
                else
                {
                    model.WctPicPath = hideFileName.Value;
                }

                _webContentService.Update(model);

                //ShowMessage("儲存成功");

                //更新列表
                grid.DataBind();

                this.MultiView1.ActiveViewIndex = 0;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        #region GridView動作

        protected void grid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Modify":
                    {
                        //ID
                        string WctID = e.CommandArgument.ToString();
                        //暫存
                        hideID.Value = WctID;

                        var model = _webContentService.GetByID(WctID);

                        txbWctName2.Text = model.WctName;
                        _common.SetChecked(ckbIsEnable, Convert.ToInt32(model.WctEnable));

                        #region 圖片

                        //圖片
                        if (model.WctPicPath != null)
                        {
                            imgWctPicPath.ImageUrl = string.Format("Uploads/{0}/{1}"
                                                                  , model.WctID.ToString()
                                                                  , model.WctPicPath);
                        }
                        else
                        {
                            imgWctPicPath.ImageUrl = "Images/default.jpg";
                        }
                        hideFileName.Value = model.WctPicPath;

                        #endregion

                        lblMeg.Text = "";

                        this.btnOK.Visible = false;
                        this.btnEdit.Visible = true;
                        this.MultiView1.ActiveViewIndex = 1;

                        break;
                    }

                case "Remove":
                    {
                        //WctID
                        string WctID = e.CommandArgument.ToString();
                 
                        //刪除
                        _webContentService.Delete(WctID);

                        this.grid.DataBind();
                        break;
                    }

                default:
                    {
                        return;
                    }
            }
        }

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Image imgPic = (Image)e.Row.FindControl("imgPic");
            if (imgPic != null)
            {
                string Temp = imgPic.AlternateText;

                //ProID
                string ProID = Temp.Split(',')[0].Trim();
                //ProPicPath
                string ProPicPath = Temp.Split(',')[1].Trim();

                if (ProPicPath != "")
                {
                    imgPic.ImageUrl = string.Format("Uploads/{0}/{1}"
                                                    , ProID
                                                    , ProPicPath);
                }
                else
                {
                    imgPic.ImageUrl = "Images/default.jpg";
                }
            }

            //是否啟用
            Label newlblIsEnable = (Label)e.Row.FindControl("lblIsEnable");
            if (newlblIsEnable != null)
            {
                if (int.Parse(newlblIsEnable.Text) == 1)
                {
                    Label lblEnable = _common.ChangeIsEnable(1);
                    newlblIsEnable.Text = lblEnable.Text;
                }
                else if (int.Parse(newlblIsEnable.Text) == 0)
                {
                    Label lblEnable = _common.ChangeIsEnable(0);
                    newlblIsEnable.Text = lblEnable.Text;
                    newlblIsEnable.ForeColor = lblEnable.ForeColor;
                }
            }
        }

        #endregion

     

       

        
    }
}