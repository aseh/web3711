﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models;
using Service;
using Service.Interface;
using Service.Misc;
using ASEH_CMS.App_Code;
using System.IO;
using System.Web.Configuration;

namespace ASEH_CMS
{
    public partial class Manage_MessageList : PageBase
    {
        private IExceptionRecordService _exceptionRecordService;
        private IMessageService _messageService;

        private CommonClass _common = new CommonClass();
        UserInfo User = new UserInfo();
        protected int PageSize = 30;
        protected string TargetPage = "Manage_MessageList.aspx";

        //WebConfig參數
        protected string SiteDomain = WebConfigurationManager.AppSettings["SiteDomain"].ToString();

        new protected int iPageType = 1;
        new protected string strPageID = "M71";

        //起始載入
        protected void Page_Load(object sender, EventArgs e)
        {
            _exceptionRecordService = new ExceptionRecordService();
            _messageService = new MessageService();

            CheckUser();
            CheckUserPermission(strPageID, iPageType);

            if (!this.Page.IsPostBack)
            {
                //取得登入者ID
                User = (UserInfo)Session["UserInfo"];
                hideLoginUseID.Value = User.UserID;

                //判斷類別
                if (Request.QueryString["P"] != null)
                {
                    int Page = Convert.ToInt32(Request.QueryString["P"]);

                    //載入內容
                    SetPageContent(Page);
                }
                //沒有任何參數時
                else
                {
                    //重新導向有參數頁面
                    Response.Redirect(string.Format("{0}?P=1", TargetPage));
                }
            }
        }

        #region 下拉選單的動作

        //排序下拉選單的動作
        protected void ddlOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
            int Page = Convert.ToInt32(Request.QueryString["P"]);

            //載入內容
            SetPageContent(Page);
        }

        //排序類型下拉選單的動作
        protected void ddlOrderType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int Page = Convert.ToInt32(Request.QueryString["P"]);

            //載入內容
            SetPageContent(Page);
        }

        #endregion

        //查詢的動作
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            int Page = Convert.ToInt32(Request.QueryString["P"]);

            //載入內容
            SetPageContent(Page);
        }
        
        //返回按鈕
        protected void btnBack_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
        }

        #region GridView動作

        protected void grid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "View":
                    {
                        string MsgID = e.CommandArgument.ToString();

                        var model = _messageService.GetByID(MsgID);

                        this.lblCreateDate2.Text = Convert.ToDateTime(model.CreateDate).ToString("yyyy/MM/dd HH:mm");
                        this.lblMsgUserName2.Text = model.MsgUserName;
                        this.lblMsgUserTel2.Text = model.MsgUserTel;
                        this.lblMsgUserEmail2.Text = model.MsgUserEmail;
                        this.lblComCountryID2.Text = model.ComCountryID;
                        this.lblMsgUserCom2.Text = model.MsgUserCom;
                        this.lblMsgUserTitle2.Text = model.MsgUserTitle;
                        this.lblMsgContent2.Text = model.MsgContent.Replace(System.Environment.NewLine, "<br>");

                        this.MultiView1.ActiveViewIndex = 1;

                        break;
                    }
                case "Modify":
                    {
                        //string ComID = e.CommandArgument.ToString();

                        //var model = _companyService.GetByID(ComID);

                        //hideID.Value = model.ComID;//暫存原始ID

                        ////Type
                        //IEnumerable<tbMessageType> listCT = _companyTypeService.GetAll().Where(x => x.CmtEnable == 1);
                        //ListItem item = null;
                        //ddlType2.Items.Clear();
                        //foreach (tbMessageType element in listCT)
                        //{
                        //    item = new ListItem(element.CmtName, element.CmtID);
                        //    ddlType2.Items.Add(item);
                        //}

                        //ddlType2.Items.FindByValue(model.CmtID).Selected = true;

                        //txbEstablishDate2.Text = model.EstablishDate;

                        //txbComName2.Text = model.ComName;

                        //#region 國家城市

                        ////載入國家
                        //IEnumerable<tbWorldCountries> listCY = _worldCountriesService.GetAll().OrderBy(x => x.country);
                        //ListItem itemC = null;
                        //ddlComCountryID2.Items.Clear();

                        ////ALL
                        //itemC = new ListItem("請選擇", "-1");
                        //ddlComCountryID2.Items.Add(itemC);

                        //foreach (tbWorldCountries element in listCY)
                        //{
                        //    itemC = new ListItem(element.country, element.country);
                        //    ddlComCountryID2.Items.Add(itemC);
                        //}

                        //if (model.ComCountryID != null)
                        //{
                        //    ddlComCountryID2.Items.FindByValue(model.ComCountryID).Selected = true;
                        //}
                        //else
                        //{
                        //    ddlComCountryID2.Items.FindByValue("-1").Selected = true;
                        //}

                        ////載入城市
                        //IEnumerable<tbWorldCities> listC = _worldCitiesService.GetAll().Where(x => x.country == ddlComCountryID2.SelectedValue).OrderBy(x => x.city);
                        //ListItem itemR = null;
                        //ddlComCityID2.Items.Clear();

                        ////ALL
                        //itemR = new ListItem("請選擇", "-1");
                        //ddlComCityID2.Items.Add(itemR);

                        //foreach (tbWorldCities element in listC)
                        //{
                        //    itemR = new ListItem(element.city, element.id.ToString());
                        //    ddlComCityID2.Items.Add(itemR);
                        //}

                        //if (model.ComCityID != null)
                        //{
                        //    ddlComCityID2.Items.FindByValue(model.ComCityID).Selected = true;
                        //}
                        //else
                        //{
                        //    ddlComCityID2.Items.FindByValue("-1").Selected = true;
                        //}

                        //#endregion

                        //txbComPeople2.Text = model.ComPeople.ToString();
                        //txbComTaxID2.Text = model.ComTaxID;
                        //txbComUrl2.Text = model.ComUrl;

                        //#region 圖片

                        ////圖片
                        //if (model.ComPicPath != null)
                        //{
                        //    imgComPicPath2.ImageUrl = string.Format("Uploads/{0}/{1}"
                        //                                            , model.ComID.ToString()
                        //                                            , model.ComPicPath);
                        //}
                        //else
                        //{
                        //    imgComPicPath2.ImageUrl = "Images/default.jpg";
                        //}
                        //hideComPicPath2.Value = model.ComPicPath;

                        //#endregion

                        //txbComDesc2.Text = model.ComDesc;
                        //txbComContent2.Text = model.ComContent;
                        //txbComPartner2.Text = model.ComPartner;

                        ////是否啟用
                        //_common.SetChecked(ckbIsEnable, Convert.ToInt32(model.ComEnable));

                        //btnOK.Visible = false;
                        //btnEdit.Visible = true;
                        MultiView1.ActiveViewIndex = 1;
                        break;
                    }
                case "Remove":
                    {
                        //string ComID = e.CommandArgument.ToString();

                        ////先刪除關聯表
                        //List<tbComUser> list = _comUserService.GetAll().Where(x => x.ComID == ComID).ToList();
                        //foreach (tbComUser element in list)
                        //{
                        //    _comUserService.Delete(element.ID);
                        //}
                        
                        //_companyService.Delete(ComID);

                        ////重新導向有參數頁面
                        //int Page = Convert.ToInt32(Request.QueryString["P"]);
                        //Response.Redirect(string.Format("{0}?P={1}&Type={2}", TargetPage, Page, ddlType.SelectedValue));

                        break;
                    }
                default:
                    {
                        return;
                    }
            }
        }

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //項次
            Label newlblNO = (Label)e.Row.FindControl("lblNO");
            if (newlblNO != null)
            {
                int NO = e.Row.RowIndex + 1;
                newlblNO.Text = NO.ToString();
            }

            //暱稱
            Label newlblUserName = (Label)e.Row.FindControl("lblUserName");
            if (newlblUserName != null)
            {
                newlblUserName.Text = _common.GetShortText(newlblUserName.Text, 15);
            }

            //留言內容
            Label newlblContent = (Label)e.Row.FindControl("lblContent");
            if (newlblContent != null)
            {
                newlblContent.Text = _common.GetShortText(newlblContent.Text, 50);
            }

            ////狀態
            //Label newlblStatus = (Label)e.Row.FindControl("lblStatus");
            //if (newlblStatus != null)
            //{
            //    newlblStatus.Text = _common.ChangeMessageStatus(newlblStatus.Text);
            //}

            ////回覆時間
            //Label newlblLastUpdateDate = (Label)e.Row.FindControl("lblLastUpdateDate");
            //if (newlblLastUpdateDate != null)
            //{
            //    DateTime LastUpdateDate = Convert.ToDateTime(newlblLastUpdateDate.Text);

            //    if (LastUpdateDate.ToString("yyyy/MM/dd") == "1990/01/01")
            //    {
            //        newlblLastUpdateDate.Text = "-";
            //    }
            //    else
            //    {
            //        newlblLastUpdateDate.Text = LastUpdateDate.ToString("yyyy/MM/dd HH:mm");
            //    }
            //}
        }

        #endregion

        #region 方法

        //載入內容
        protected void SetPageContent(int Page)
        {
            //取得類別
            List<tbMessage> listOld;

            listOld = _messageService.GetAll().Where(x => x.MsgUserName.Contains(txbSearchName.Text)).OrderByDescending(s => s.CreateDate).ToList();

            #region 排序設定

            string OrderBy = ddlOrder.SelectedValue;

            if (ddlOrderType.SelectedValue == "DESC")
            {
                switch (OrderBy)
                {
                    case "MsgUserName":
                        {
                            listOld = listOld.OrderByDescending(o => o.MsgUserName).ToList();
                            break;
                        }
                }
            }
            else
            {
                switch (OrderBy)
                {
                    case "MsgUserName":
                        {
                            listOld = listOld.OrderBy(o => o.MsgUserName).ToList();
                            break;
                        }
                }
            }

            #endregion

            List<tbMessage> listNew = GetPagedTable(listOld, Page, PageSize);

            grid.DataSource = listNew;
            grid.DataBind();

            //取得分頁
            ltlPageMenu.Text = SetPageMenu(listOld, Page, TargetPage);
        }

        //取得分頁
        protected string SetPageMenu(List<tbMessage> list, int Page, string TargetPage)
        {
            string PageMenu = "";

            #region 判斷上下頁

            string RevPage = "<li class='page-item'><a class='page-link' href='#'> > </a></li>";
            string NextPage = "<li class='page-item'><a class='page-link' href='#'> < </a></li>";

            //只有一頁時
            if (list.Count <= PageSize)
            {
                RevPage = "";
                NextPage = "";
            }
            //還有下一頁時
            else if (list.Count >= (PageSize * Page) && Page == 1)
            {
                #region 取得連結

                string NextUrl = string.Format("{0}?P={1}", TargetPage, Page + 1);

                #endregion

                RevPage = "";
                NextPage = string.Format("<li class='page-item'><a class='page-link' href='{0}'> > </a></li>", NextUrl);
            }
            //最後一頁時
            else if ((list.Count % (PageSize * Page)) == list.Count || (list.Count % (PageSize * Page) == 0))
            {
                #region 取得連結

                string RevUrl = string.Format("{0}?P={1}", TargetPage, Page - 1);

                #endregion

                RevPage = string.Format("<li class='page-item'><a class='page-link' href='{0}'> < </a></li>", RevUrl);
                NextPage = "";
            }
            else
            {
                #region 取得連結

                string NextUrl = string.Format("{0}?P={1}", TargetPage, Page + 1);
                string RevUrl = string.Format("{0}?P={1}", TargetPage, Page - 1);

                #endregion

                RevPage = string.Format("<li class='page-item'><a class='page-link' href='{0}'> < </a></li>", RevUrl);
                NextPage = string.Format("<li class='page-item'><a class='page-link' href='{0}'> > </a></li>", NextUrl);
            }

            #endregion

            #region 取得頁數

            string PageItem = "";
            string ItemUrl = "";

            //取得總數量
            int count = (list.Count / PageSize);
            if (list.Count % PageSize != 0)
            {
                count = count + 1;
            }

            if (count > 1)
            {
                for (int e = 0; e < count; e++)
                {
                    ItemUrl = string.Format("{0}?P={1}", TargetPage, e + 1);

                    //當前頁數時
                    if (Page == e + 1)
                    {
                        PageItem += string.Format("<li class='page-item active'><a class='page-link' href='{0}'>{1}</a></li>", ItemUrl, e + 1);
                    }
                    else
                    {
                        PageItem += string.Format("<li class='page-item'><a class='page-link' href='{0}'>{1}</a></li>", ItemUrl, e + 1);
                    }
                }
            }

            #endregion

            PageMenu += "<ul class='pagination'>";

            PageMenu += RevPage;
            PageMenu += PageItem;
            PageMenu += NextPage;

            PageMenu += "</ul>";

            return PageMenu;
        }

        //進行分頁方法
        protected List<tbMessage> GetPagedTable(List<tbMessage> list, int PageIndex, int PageSize)
        {
            if (PageIndex == 0)
            {
                return list;
            }
            List<tbMessage> newList = new List<tbMessage>();

            //起始列   
            int rowbegin = (PageIndex - 1) * PageSize;
            //結束列   
            int rowend = PageIndex * PageSize;
            if (rowbegin >= list.Count)
            {
                return newList;
            }

            if (rowend > list.Count)
            {
                rowend = list.Count;
            }
            //產生新的List   
            for (int i = rowbegin; i <= rowend - 1; i++)
            {
                tbMessage item = new tbMessage();
                item = list[i];

                newList.Add(item);
            }
            return newList;
        }

        #endregion





    }
}