﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models;
using Service;
using Service.Interface;
using Service.Misc;
using ASEH_CMS.App_Code;
using System.Web.Configuration;
using System.IO;
using System.Text;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using Service.BLL;
using ASEH_CMS.Models.Common.Quest;
using Newtonsoft.Json;

namespace ASEH_CMS
{
    public partial class Manage_Climate_Questionnaire : PageBase
    {
        new protected int iPageType = 1;
        new protected string strPageID = "M53";
        ASEHService _serv = new ASEHService();

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckUser();
            CheckUserPermission(strPageID, iPageType);
        }

        public void Export(byte[] file, string fileName)
        {
            HttpContext context = HttpContext.Current;
            string fileNameOrExtension = fileName;
            string mimeType = "application/unknown";
            string ext = (fileNameOrExtension.Contains(".")) ? System.IO.Path.GetExtension(fileNameOrExtension).ToLower() : "." + fileNameOrExtension;

            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);

            if (regKey != null && regKey.GetValue("Content Type") != null) mimeType = regKey.GetValue("Content Type").ToString();

            context.Response.Clear();
            context.Response.ClearHeaders();
            context.Response.ClearContent();
            context.Response.AppendHeader("Content-Length", file.Length.ToString());
            context.Response.ContentType = mimeType;

            string disposition = "attachment";
            string[] name = fileName.Split('.');

            if (name.Length == 2)
            {
                if (name.GetValue(1).ToString().ToLower() == "pdf")
                {
                    disposition = "inline";
                }
            }

            if (context.Request.Browser.Type.Contains("Firefox"))
            {
                context.Response.AppendHeader("Content-Disposition", string.Format(
                                "{0}; filename*=utf-8\'{1}", disposition, HttpUtility.UrlEncode(fileName)
                            ));
            }
            else
            {
                context.Response.AppendHeader("Content-Disposition", string.Format(
                                "{0}; filename={1}", disposition, HttpUtility.UrlEncode(fileName)
                            ));
            }

            //防止出現「遠端主機已關閉連接。」的 Exception
            if (context.Response.IsClientConnected)
            {
                context.Response.BinaryWrite(file);
                context.Response.Flush();
            }
            else
            {
                context.ApplicationInstance.CompleteRequest();
                context.Response.End();
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            // 建立 WorkBook 及試算表
            HSSFWorkbook workbook = new HSSFWorkbook();
            MemoryStream ms = new MemoryStream();
            ISheet sheet = workbook.CreateSheet("氣候變遷與水衝擊程度調查表資料");
            int rowIndex = 0;

            // 建立標題列
            List<string> fields = new List<string> { "編號", "您的利害人的關係", "公司/組織名稱" };
            QuestBody questBody = JsonConvert.DeserializeObject<QuestBody>(System.IO.File.ReadAllText(Server.MapPath(String.Format("~/App_Data/quest2.json"))));

            int dynamicColumnNum = 0;

            foreach ( var Group in questBody.GroupList )
            {
                dynamicColumnNum += Group.TopicList.Count();
                foreach (var Topic in Group.TopicList )
                {
                    fields.Add(String.Format("{0}-{1}", Group.Title.Ch, Topic.Title.Ch));
                }
            }
            fields.Add("其它意見");
            fields.Add("填表日期");
            IRow rowHeaderField = sheet.CreateRow(rowIndex);
            int colCount = 0;

            foreach (var field in fields)
            {
                if (colCount == 0)
                {
                    sheet.SetColumnWidth(colCount, 20 * 80);
                }
                else
                {
                    sheet.SetColumnWidth(colCount, 20 * 256);
                }
                rowHeaderField.CreateCell(colCount).SetCellValue(field);

                colCount++;
            }

            rowIndex++;

            var list = _serv.GetQuestionnaireQuery("CLIMATE");

            // 建立資料列
            int i = list.Count();
            foreach (var data in list)
            {
                IRow row = sheet.CreateRow(rowIndex);
                Dictionary<string, string> formData = JsonConvert.DeserializeObject<Dictionary<string, string>>(data.form);
                
                row.CreateCell(0).SetCellValue(i.ToString());
                string stackholder = formData.ContainsKey("stakeholder") ? formData["stakeholder"] : "";
                if ( stackholder == "Other")
                {
                  stackholder = formData.ContainsKey("other") ? formData["other"] : "";
                }
                row.CreateCell(1).SetCellValue(stackholder);
                row.CreateCell(2).SetCellValue(formData.ContainsKey("org") ? formData["org"] : "");

                int cellIndex = 3;
                foreach (var Group in questBody.GroupList)
                {
                    foreach (var Topic in Group.TopicList)
                    {
                        row.CreateCell(cellIndex).SetCellValue(formData.ContainsKey(Topic.Key) ? formData[Topic.Key] : "");
                        cellIndex++;
                    }
                }
                row.CreateCell(cellIndex ).SetCellValue(formData.ContainsKey("additional") ? formData["additional"] : "");
                row.CreateCell(cellIndex + 1).SetCellValue((data.update_date.HasValue) ? data.update_date.Value.ToString("yyyy/MM/dd") : "");

                rowIndex++;
                i--;
            }

            MemoryStream stream = new MemoryStream();
            workbook.Write(stream);
            stream.Flush();

            this.Export(stream.ToArray(), string.Format("{0}.xls", "氣候變遷與水衝擊程度調查表"));
        }
    }
}