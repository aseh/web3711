﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Manage_FileList.aspx.cs" Inherits="ASEH_CMS.Manage_FileList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">

    var jQuery132 = jQuery.noConflict(true);

    //建立日期
    jQuery132(function () {
        jQuery132("#ContentPlaceHolder1_txbOpenDate2").datepicker({ dateFormat: "yy/mm/dd" });
        jQuery132("#ContentPlaceHolder1_txbCloseDate2").datepicker({ dateFormat: "yy/mm/dd" });
    });

</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Breadcrumb -->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="MainIndex.aspx">Dashboard</a></li>
        <li class="breadcrumb-item active">CSR報告上下架管理</li>
      </ol>
    <!-- Breadcrumb Menu-->

<div class="container-fluid">
<div class="animated fadeIn">


<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">

        <div class="row">
        <div class="col-lg-12">

            <%--控制項--%>
            <table style="width: 100%;" >
                <tr>
                    <td align="left">
                         <asp:Button ID="btnAdd" runat="server" BorderColor="Transparent" BorderWidth="0px" CssClass="SmallButton" onclick="btnAdd_Click" Text="新增" />
                    </td>
                    <td align="right" style="width: 20%;">
                      
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        &nbsp;</td>
                    <td align="right" style="width: 20%;">&nbsp;</td>
                </tr>
             </table>

            <div class="card">
            <div class="card-header">
            <i class="fa fa-align-justify"></i> 列表
            </div>

            <div class="card-body">

            <%--列表--%>
            <table style="width: 100%;" >
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" onrowcommand="grid_RowCommand" onrowdatabound="grid_RowDataBound" PageSize="100" Width="100%">
                            <PagerSettings Visible="False" />
                            <Columns>
                                <%--狀態--%>
                                <asp:BoundField DataField="FieSort" HeaderText="排序" Visible="false">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                </asp:BoundField>

                                <asp:BoundField DataField="OpenDate" 
                                    DataFormatString="{0:yyyy/MM/dd}" HeaderText="上架日期">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="160px" />
                                </asp:BoundField>

                                <asp:BoundField DataField="FieEndDate" 
                                    DataFormatString="{0:yyyy/MM/dd HH:mm}" HeaderText="下架日期" Visible="false">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="160px" />
                                </asp:BoundField>

                                <asp:BoundField DataField="FieName" HeaderText="標題" ReadOnly="True">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>

                                <asp:BoundField DataField="FieDesc" HeaderText="副標題" ReadOnly="True" Visible="false">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>

                                <asp:BoundField DataField="FieDesc" HeaderText="說明" Visible="False">
                                <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="是否啟用" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIsEnable" runat="server" Text='<%# Eval("FieEnable") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" Width="80px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="圖片">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Image ID="imgPic" runat="server" AlternateText='<%# Eval("FieID") + "," + Eval("FiePicPath") %>' ImageUrl="Images/default.jpg" Width="80px" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="80px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="向上" Visible="False">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnSortUp" runat="server" CausesValidation="False" CommandArgument='<%# Eval("FieID") + "," + Eval("FieSort") %>' CommandName="SortUp" ImageUrl="Images/SortUp.gif" ToolTip="向上" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="50px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="向下" Visible="False">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnSortDown" runat="server" CausesValidation="False" CommandArgument='<%# Eval("FieID") + "," + Eval("FieSort") %>' CommandName="SortDown" ImageUrl="Images/SortDown.gif" ToolTip="向下" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="50px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="修改">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("FieID") %>' CommandName="Modify" ImageUrl="Images/Edit.png" ToolTip="修改" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="70px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="刪除">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnDel" runat="server" CausesValidation="False" CommandArgument='<%# Eval("FieID") %>' CommandName="Remove" ImageUrl="Images/Delete.png" onclientclick="return window.confirm('確定要刪除嗎？');" ToolTip="刪除" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="70px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:HiddenField ID="hideLoginUseID" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;</td>
                </tr>
            </table>

            </div>
        </div>

        </div>
        </div>

        </asp:View>
        <asp:View ID="View2" runat="server">

        <div class="row">
        <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
            <i class="fa fa-align-justify"></i> 新增/編輯
            </div>

            <div class="card-body">

            <table style="width:100%;">

                                <tr>
                    <td align="left" style="width: 30%">
                        &nbsp;</td>
                    <td align="left" style="width: 70%">
                        &nbsp;</td>
                </tr>
                                <tr>
                                    <td align="left" style="width: 30%">
                                        <asp:Label ID="Label45" runat="server" SkinID="MainText" Text="上架日期"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 70%">
                                        <asp:TextBox ID="txbOpenDate2" runat="server" SkinID="MainText" Text="沒有資料" Width="100px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 30%">&nbsp;</td>
                                    <td align="left" style="width: 70%">&nbsp;</td>
                                </tr>

                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="lable88" runat="server" SkinID="MainText" Text="標題"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbFieName2" runat="server" MaxLength="50" Width="60%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvFieName" runat="server" 
                            ControlToValidate="txbFieName2" ErrorMessage="RequiredFieldValidator" 
                            ForeColor="Red" SkinID="MainAlert" ValidationGroup="OK">欄位不可為空</asp:RequiredFieldValidator>
                    </td>
                </tr>
                                <tr>
                                    <td align="left" style="width: 30%">
                                        <asp:Label ID="lable157" runat="server" SkinID="MainText" Text="標題(英文)"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 70%">
                                        <asp:TextBox ID="txbFieName_EN2" runat="server" MaxLength="50" Width="60%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvFieName0" runat="server" ControlToValidate="txbFieName2" ErrorMessage="RequiredFieldValidator" ForeColor="Red" SkinID="MainAlert" ValidationGroup="OK">欄位不可為空</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 30%">&nbsp;</td>
                                    <td align="left" style="width: 70%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 30%; vertical-align: top;">
                                        <asp:Label ID="lable155" runat="server" SkinID="MainText" Text="內容" Visible="False"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 70%; ">
                                        <asp:TextBox ID="txbFieDesc2" runat="server" MaxLength="300" Width="70%" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 30%; vertical-align: top; ">
                                        <asp:Label ID="lable158" runat="server" SkinID="MainText" Text="內容(英文)" Visible="False"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 70%; ">
                                        <asp:TextBox ID="txbFieDesc_EN2" runat="server" MaxLength="300" Width="70%" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 30%">
                                        <asp:Label ID="Label49" runat="server" SkinID="MainText" Text="檔案上傳"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 70%">
                                        <asp:Label ID="lblFieFilePath2" runat="server" SkinID="MainText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 30%">&nbsp;</td>
                                    <td align="left" style="width: 70%">
                                        <asp:FileUpload ID="uploadFieFilePath2" runat="server" Width="60%" />
                                        <asp:HiddenField ID="hideFieFilePath2" runat="server" Visible="False" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 30%">
                                        <asp:Label ID="Label50" runat="server" SkinID="MainText" Text="檔案上傳(英文)"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 70%">
                                        <asp:Label ID="lblFieFilePath_EN2" runat="server" SkinID="MainText"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 30%">&nbsp;</td>
                                    <td align="left" style="width: 70%">
                                        <asp:FileUpload ID="uploadFieFilePath_EN2" runat="server" Width="60%" />
                                        <asp:HiddenField ID="hideFieFilePath_EN2" runat="server" Visible="False" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 30%">&nbsp;</td>
                                    <td align="left" style="width: 70%">
                                        <asp:Label ID="lblPicDesc3_3" runat="server" SkinID="MainAlert">檔案支援（pdf）格式。</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 30%">&nbsp;</td>
                                    <td align="left" style="width: 70%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 30%; vertical-align: top;">
                                        <asp:Label ID="lable154" runat="server" SkinID="MainText" Text="圖片上傳"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 70%">
                                        <asp:Image ID="imgFiePicPath2" runat="server" ImageUrl="Images/default.jpg" Width="120px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 30%; vertical-align: top;">&nbsp;</td>
                                    <td align="left" style="width: 70%">
                                        <asp:FileUpload ID="uploadFiePicPath2" runat="server" Width="60%" />
                                        <asp:HiddenField ID="hideFiePicPath2" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 30%; vertical-align: top;">
                                        <asp:Label ID="lable159" runat="server" SkinID="MainText" Text="圖片上傳(英文)"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 70%">
                                        <asp:Image ID="imgFiePicPath_EN2" runat="server" ImageUrl="Images/default.jpg" Width="120px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 30%; vertical-align: top;">&nbsp;</td>
                                    <td align="left" style="width: 70%">
                                        <asp:FileUpload ID="uploadFiePicPath_EN2" runat="server" Width="60%" />
                                        <asp:HiddenField ID="hideFiePicPath_EN2" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 30%; vertical-align: top;">&nbsp;</td>
                                    <td align="left" style="width: 70%">
                                        <asp:Label ID="lblPicDesc3_2" runat="server" SkinID="MainAlert">圖片支援（bmp,jpg,jpeg,png）等格式。</asp:Label>
                                        <br />
                                        <asp:Label ID="lblPositionAlert15" runat="server" SkinID="MainAlert" Text="(圖片尺寸建議 1275 X 903px)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 30%">&nbsp;</td>
                                    <td align="left" style="width: 70%">&nbsp;</td>
                                </tr>
                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="lblIsEnable0" runat="server" SkinID="MainText" Text="是否啟用" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:CheckBox ID="ckbIsEnable" runat="server" Checked="True" Text="啟用請打勾" Visible="False" />
                    </td>
                </tr>
                                <tr>
                                    <td align="left" style="width: 30%" valign="top">&nbsp;</td>
                                    <td align="left" style="width: 70%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 30%" valign="top">
                                        <asp:Label ID="lable156" runat="server" SkinID="MainText" Text="圖片描述" Visible="False"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 70%">
                                        <asp:TextBox ID="txbFiePicDesc2" runat="server" MaxLength="50" Width="50%" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 30%" valign="top">&nbsp;</td>
                                    <td align="left" style="width: 70%">&nbsp;</td>
                                </tr>
                <tr>
                    <td align="left" colspan="2" style="text-align: center;" valign="top">
                        <asp:Label ID="lblMeg" runat="server" ForeColor="Red" SkinID="MainWarn"></asp:Label>
                    </td>
                </tr>
                                <tr>
                                    <td align="left" colspan="2" style="text-align: center;" valign="top">&nbsp;</td>
                                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnEdit" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="BigButton" Text="確定修改" 
                            ValidationGroup="OK" OnClick="btnEdit_Click" />
                        <asp:Button ID="btnOK" runat="server" BackColor="Transparent" BorderWidth="0px" 
                            CssClass="SmallButton" Text="確定" ValidationGroup="OK" OnClick="btnOK_Click" />
                        <asp:Button ID="btnBack" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="SmallButton" Text="返回" OnClick="btnBack_Click" />
                        <asp:HiddenField ID="hideID" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        &nbsp;</td>
                </tr>
            </table>

            </div>

        </div>
        </div>
        </div>

        </asp:View>
    </asp:MultiView>

</div>
</div>
<!-- /.conainer-fluid -->

</asp:Content>
