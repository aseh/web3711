﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models;
using Service;
using Service.Interface;
using Service.Misc;
using ASEH_CMS.App_Code;
using System.Web.Configuration;
using System.IO;

namespace ASEH_CMS
{
    public partial class Manage_MediaDB : PageBase
    {
        private IExceptionRecordService _exceptionRecordService;
        private IMediaTypeService _mediaTypeService;
        private IMediaService _mediaService;
        private CommonClass _common = new CommonClass();
        UserInfo User = new UserInfo();
        protected int PageSize = 10;

        //WebConfig參數
        protected string SiteDomain = WebConfigurationManager.AppSettings["SiteDomain"].ToString();

        new protected int iPageType = 1;

        //起始載入
        protected void Page_Load(object sender, EventArgs e)
        {
            this._exceptionRecordService = new ExceptionRecordService();
            this._mediaTypeService = new MediaTypeService();
            this._mediaService = new MediaService();

            CheckUser();

            if (!this.Page.IsPostBack)
            {
                //取得登入者ID
                User = (UserInfo)Session["UserInfo"];
                hideLoginUseID.Value = User.UserID;

                //類別
                IEnumerable<tbMediaType> listType = _mediaTypeService.GetAll();
                ListItem item = null;
                ddlFileType.Items.Clear();
                foreach (tbMediaType element in listType)
                {
                    item = new ListItem(element.MdtName, element.MdtID);
                    ddlFileType.Items.Add(item);
                }

                //判斷類別
                if (Request.QueryString["P"] != null)
                {
                    int Page = Convert.ToInt32(Request.QueryString["P"]);
                    string Type = Request.QueryString["Type"];

                    //載入內容
                    SetPageContent(Page, Type);
                }
                //沒有任何參數時
                else
                {
                    //重新導向有參數頁面
                    Response.Redirect("Manage_MediaDB.aspx?P=1&Type=MDT001");
                }
            }
        }

        //返回的動作
        protected void btnBack2_Click(object sender, EventArgs e)
        {
            this.MultiView1.ActiveViewIndex = 0;
        }

        //新增按鈕的動作
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Label35.Visible = false;
            lblLastUpdateDate2.Visible = false;

            lblLastUpdateDate2.Text = "";
            txbMdaName2.Text = "";
            txbMdaDesc2.Text = "";

            lblMdaFilePath2.Text = "請選擇檔案";

            //類別
            IEnumerable<tbMediaType> listType = _mediaTypeService.GetAll();
            ListItem item = null;
            ddlFileType2.Items.Clear();
            foreach (tbMediaType element in listType)
            {
                item = new ListItem(element.MdtName, element.MdtID);
                ddlFileType2.Items.Add(item);
            }

            //自動指向
            if (ddlFileType.SelectedValue != "-1")
            {
                ddlFileType2.Items.FindByValue(ddlFileType.SelectedValue).Selected = true;
            }

            //是否顯示控制

            #region 圖片

            imgMdaFilePath2.ImageUrl = "Images/default.jpg";
            Label4.Visible = false;
            imgMdaFilePath2.Visible = false;

            #endregion

            lblMeg2.Text = "";

            this.btnOK2.Visible = true;
            this.btnEdit2.Visible = false;
            this.MultiView1.ActiveViewIndex = 1;
        }

        //新增儲存動作
        protected void btnOK2_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new tbMedia();
                model.MdaName = txbMdaName2.Text;
                model.MdaDesc = txbMdaDesc2.Text;
                model.MdaEnable = 1;
                model.MdaSort = 1;
                model.CreateUser = hideLoginUseID.Value;
                model.CreateDate = DateTime.Now;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;
                model.MdtID = ddlFileType2.SelectedValue;

                _mediaService.Create(model);

                #region 檔案上傳

                string MdaID = model.MdaID.ToString();
                string MdaFileName = "";
                string MdaExtension = "";
                string MdaSize = "";
                string MdaFilePath = "";

                string newFileName = "";
                //上傳檔案
                bool FileCheck = true;
                if (FileUpload2.HasFile)
                {
                    try
                    {
                        string FileName = FileUpload2.FileName;
                        string FileExtension = System.IO.Path.GetExtension(FileUpload2.FileName).ToLower();
                        //取得亂數檔名
                        newFileName = Guid.NewGuid().ToString();
                        string FolderPath = string.Format("Uploads/{0}/", MdaID);
                        string RealFolderPath = Server.MapPath(FolderPath);

                        int returnCode = _common.UploadAnyFileWithNewName(FileUpload2, RealFolderPath, newFileName);
                        FileCheck = true;

                        //取得檔名
                        MdaFileName = newFileName + FileExtension;
                        //取得副檔名
                        MdaExtension = FileExtension;
                        //取得尺寸
                        MdaSize = FileUpload2.PostedFile.ContentLength.ToString(); ;
                        //取得完整檔案路徑
                        MdaFilePath = newFileName + FileExtension;
                    }
                    catch (Exception ex)
                    {
                        FileCheck = false;
                    }
                }

                #endregion

                if (FileCheck)
                {
                    lblMeg2.Text = "";

                    //再更新一次
                    model.MdaFileName = MdaFileName;
                    model.MdaExtension = MdaExtension;
                    model.MdaSize = MdaSize;
                    model.MdaFilePath = MdaFilePath;

                    _mediaService.Update(model);

                    //載入內容
                    int Page = Convert.ToInt32(Request.QueryString["P"]);
                    string Type = Request.QueryString["Type"];

                    SetPageContent(Page, Type);

                    this.MultiView1.ActiveViewIndex = 0;
                }
                else
                {
                    lblMeg2.Text = "檔案新增錯誤，請稍後再試..";
                }
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        //修改儲存動作
        protected void btnEdit2_Click(object sender, EventArgs e)
        {
            try
            {
                var model = _mediaService.GetByID(Convert.ToInt32(hideID.Value));
                model.MdaName = txbMdaName2.Text;
                model.MdaDesc = txbMdaDesc2.Text;

                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;
                model.MdtID = ddlFileType2.SelectedValue;

                #region 檔案上傳

                string MdaID = model.MdaID.ToString();
                string MdaFileName = "";
                string MdaExtension = "";
                string MdaSize = "";
                string MdaFilePath = "";

                string newFileName = "";
                //上傳檔案
                bool FileCheck = true;
                 //檔案不存在則略過
                if (FileUpload2.FileName != "")
                {
                    if (FileUpload2.HasFile)
                    {
                        try
                        {
                            string FileName = FileUpload2.FileName;
                            string FileExtension = System.IO.Path.GetExtension(FileUpload2.FileName).ToLower();
                            //取得亂數檔名
                            newFileName = Guid.NewGuid().ToString();
                            string FolderPath = string.Format("Uploads/{0}/", MdaID);
                            string RealFolderPath = Server.MapPath(FolderPath);

                            int returnCode = _common.UploadAnyFileWithNewName(FileUpload2, RealFolderPath, newFileName);
                            FileCheck = true;

                            //取得檔名
                            MdaFileName = newFileName + FileExtension;
                            //取得副檔名
                            MdaExtension = FileExtension;
                            //取得尺寸
                            MdaSize = FileUpload2.PostedFile.ContentLength.ToString(); ;
                            //取得完整檔案路徑
                            MdaFilePath = newFileName + FileExtension;
                        }
                        catch (Exception ex)
                        {
                            FileCheck = false;
                        }

                        model.MdaFileName = MdaFileName;
                        model.MdaExtension = MdaExtension;
                        model.MdaSize = MdaSize;
                        model.MdaFilePath = MdaFilePath;
                    }
                }

                #endregion

                if (FileCheck)
                {
                    lblMeg2.Text = "";

                    _mediaService.Update(model);

                    //載入內容
                    int Page = Convert.ToInt32(Request.QueryString["P"]);
                    string Type = Request.QueryString["Type"];

                    SetPageContent(Page, Type);

                    this.MultiView1.ActiveViewIndex = 0;
                }
                else
                {
                    lblMeg2.Text = "檔案修改錯誤，請稍後再試..";
                }
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        //類型切換的動作
        protected void ddlFileType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Type = ddlFileType.Text;

            //重新導向
            Response.Redirect(string.Format("Manage_MediaDB.aspx?P=1&Type={0}", Type));
        }

        #region GridView動作

        protected void grid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Modify":
                    {
                        //ID
                        string MdaID = e.CommandArgument.ToString();
                        //暫存
                        hideID.Value = MdaID;

                        Label35.Visible = true;
                        lblLastUpdateDate2.Visible = true;

                        var model = _mediaService.GetByID(Convert.ToInt32(MdaID));

                        lblLastUpdateDate2.Text = Convert.ToDateTime(model.LastUpdateDate).ToString("yyyy/MM/dd HH:mm:ss");
                        txbMdaName2.Text = model.MdaName;
                        txbMdaDesc2.Text = model.MdaDesc;
                        lblMdaFilePath2.Text = model.MdaFilePath;

                        //類別
                        IEnumerable<tbMediaType> listType = _mediaTypeService.GetAll();
                        ListItem item = null;
                        ddlFileType2.Items.Clear();
                        foreach (tbMediaType element in listType)
                        {
                            item = new ListItem(element.MdtName, element.MdtID);
                            ddlFileType2.Items.Add(item);
                        }
                        ddlFileType2.ClearSelection();
                        ddlFileType2.Items.FindByValue(model.MdtID).Selected = true;

                        #region 圖片

                        //圖片
                        if (model.MdaFilePath != "")
                        {
                            imgMdaFilePath2.AlternateText = model.MdaFilePath;

                            if (model.MdtID == "MDT001")
                            {
                                imgMdaFilePath2.ImageUrl = string.Format("Uploads/{0}/{1}"
                                                                      , model.MdaID
                                                                      , model.MdaFileName);
                            }
                            else if (model.MdtID == "MDT002")
                            {
                                imgMdaFilePath2.ImageUrl = "Images/video.png";
                            }
                            else if (model.MdtID == "MDT003")
                            {
                                imgMdaFilePath2.ImageUrl = "Images/File.png";
                            }
                        }
                        else
                        {
                            imgMdaFilePath2.ImageUrl = "~/Images/default.jpg";
                        }

                        hideFileUpload2.Value = model.MdaFilePath;

                        #endregion

                        lblMeg2.Text = "";

                        this.btnOK2.Visible = false;
                        this.btnEdit2.Visible = true;
                        this.MultiView1.ActiveViewIndex = 1;

                        break;
                    }
                case "Remove":
                    {
                        string MdaID = e.CommandArgument.ToString();
                        _mediaService.Delete(Convert.ToInt32(MdaID));

                        //載入內容
                        int Page = Convert.ToInt32(Request.QueryString["P"]);
                        string Type = Request.QueryString["Type"];
                        SetPageContent(Page, Type);

                        break;
                    }
                default:
                    {
                        return;
                    }
            }
        }

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //類別
            Label newlblMdtID = (Label)e.Row.FindControl("lblMdtID");
            if (newlblMdtID != null)
            {
                var model = _mediaTypeService.GetByID(newlblMdtID.Text);
                newlblMdtID.Text = model.MdtName;
            }
         
            //網址
            Label newlblLink = (Label)e.Row.FindControl("lblMdaFilePath");
            if (newlblLink != null)
            {
                //MdaID
                string MdaID = newlblLink.Text.ToString().Split(',')[0].Trim();
                //MdaFileName
                string MdaFileName = newlblLink.Text.ToString().Split(',')[1].Trim();
                newlblLink.Text = string.Format("http://{0}/Uploads/{1}/{2}", SiteDomain, MdaID, MdaFileName);
            }

            //縮圖
            Image imgPic = (Image)e.Row.FindControl("imgPic");
            if (imgPic != null)
            {
                string Temp = imgPic.AlternateText;

                //MdaID
                string MdaID = Temp.Split(',')[0].Trim();
                //MdaFileName
                string MdaFileName = Temp.Split(',')[1].Trim();
                //MdtID
                string MdtID = Temp.Split(',')[2].Trim();

                if (MdaFileName != "")
                {
                    imgPic.AlternateText = MdaFileName;

                    //圖片
                    if (MdtID == "MDT001")
                    {
                        imgPic.ImageUrl = string.Format("Uploads/{0}/{1}"
                                                    , MdaID
                                                    , MdaFileName);
                    }
                    //影片
                    else if (MdtID == "MDT002")
                    {
                        imgPic.ImageUrl = "Images/video.png";
                    }
                    //檔案
                    else if (MdtID == "MDT003")
                    {
                        imgPic.ImageUrl = "Images/File.png";
                    }
                }
                else
                {
                    imgPic.ImageUrl = "Images/default.jpg";
                    imgPic.AlternateText = "";
                }
            }
        }

        #endregion

        #region 方法

        //載入內容
        protected void SetPageContent(int Page, string Type)
        {
            ddlFileType.ClearSelection();
            ddlFileType.Items.FindByValue(Type).Selected = true;

            List<tbMedia> listOld = _mediaService.GetAllByMdtID(Type).ToList();
            List<tbMedia> listNew = GetPagedTable(listOld, Page, PageSize);

            grid.DataSource = listNew;
            grid.DataBind();

            //取得分頁
            ltlPageMenu.Text = SetPageMenu(listOld, Page, "Manage_MediaDB.aspx", Type);
        }

        //取得分頁
        protected string SetPageMenu(List<tbMedia> list, int Page, string TargetPage, string Type)
        {
            string PageMenu = "";

            #region 判斷上下頁

            string RevPage = "<li><a href='#'>»</a></li>";
            string NextPage = "<li><a href='#'>«</a></li>";

            //只有一頁時
            if (list.Count <= PageSize)
            {
                RevPage = "";
                NextPage = "";
            }
            //還有下一頁時
            else if (list.Count >= (PageSize * Page) && Page == 1)
            {
                #region 取得連結

                string NextUrl = string.Format("{0}?P={1}&Type={2}", TargetPage, Page + 1, Type);

                #endregion

                RevPage = "";
                NextPage = string.Format("<li><a href='{0}'>»</a></li>", NextUrl);
            }
            //最後一頁時
            else if ((list.Count % (PageSize * Page)) == list.Count || (list.Count % (PageSize * Page) == 0))
            {
                #region 取得連結

                string RevUrl = string.Format("{0}?P={1}&Type={2}", TargetPage, Page - 1, Type);

                #endregion

                RevPage = string.Format("<li><a href='{0}'>«</a></li>", RevUrl);
                NextPage = "";
            }
            else
            {
                #region 取得連結

                string NextUrl = string.Format("{0}?P={1}&Type={2}", TargetPage, Page + 1, Type);
                string RevUrl = string.Format("{0}?P={1}&Type={2}", TargetPage, Page - 1, Type);

                #endregion

                RevPage = string.Format("<li><a href='{0}'>«</a></li>", RevUrl);
                NextPage = string.Format("<li><a href='{0}'>»</a></li>", NextUrl);
            }

            #endregion

            #region 取得頁數

            string PageItem = "";
            string ItemUrl = "";

            //取得總數量
            int count = (list.Count / PageSize);
            if (list.Count % PageSize != 0)
            {
                count = count + 1;
            }

            if (count > 1)
            {
                for (int e = 0; e < count; e++)
                {
                    ItemUrl = string.Format("{0}?P={1}&Type={2}", TargetPage, e + 1, Type);

                    //當前頁數時
                    if (Page == e + 1)
                    {
                        PageItem += string.Format("<li><a class='active' href='{0}'>{1}</a></li>", ItemUrl, e + 1);
                    }
                    else
                    {
                        PageItem += string.Format("<li><a href='{0}'>{1}</a></li>", ItemUrl, e + 1);
                    }
                }
            }

            #endregion

            PageMenu += "<ul class='pagination'>";

            PageMenu += RevPage;
            PageMenu += PageItem;
            PageMenu += NextPage;

            PageMenu += "</ul>";

            return PageMenu;
        }

        //進行分頁方法
        protected List<tbMedia> GetPagedTable(List<tbMedia> list, int PageIndex, int PageSize)
        {
            if (PageIndex == 0)
            {
                return list;
            }
            List<tbMedia> newList = new List<tbMedia>();

            //起始列   
            int rowbegin = (PageIndex - 1) * PageSize;
            //結束列   
            int rowend = PageIndex * PageSize;
            if (rowbegin >= list.Count)
            {
                return newList;
            }

            if (rowend > list.Count)
            {
                rowend = list.Count;
            }
            //產生新的List   
            for (int i = rowbegin; i <= rowend - 1; i++)
            {
                tbMedia item = new tbMedia();
                item = list[i];

                newList.Add(item);
            }
            return newList;
        }

        #endregion

      

      

       

      
       
    }
}