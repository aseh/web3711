﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="System_RoleList.aspx.cs" Inherits="ASEH_CMS.System_RoleList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Breadcrumb -->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="MainIndex.aspx">Dashboard</a></li>
        <li class="breadcrumb-item active">群組權限管理</li>
      </ol>
    <!-- Breadcrumb Menu-->

<div class="container-fluid">
<div class="animated fadeIn">

 <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">

            <%--控制項--%>
            <table style="width: 100%;" >
                <tr>
                    <td align="left";">
                       <asp:Button ID="btnAdd" runat="server" BorderColor="Transparent" BorderWidth="0px" CssClass="SmallButton" onclick="btnAdd_Click" Text="新增" Visible="False" />
                    </td>
                    <td align="right";">
                      
                    </td>
                </tr>
                <tr>
                    <td align="left";">
                      
                    </td>
                    <td align="right";">
                     </td>
                </tr>
            </table>

            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> 列表
                </div>

                <div class="card-body">

                <%--列表--%>
                <table style="width: 100%;">
                    <tr>
                        <td colspan="2">
                            <asp:GridView ID="grid" runat="server" 
                                AutoGenerateColumns="False" DataSourceID="ObjectDataSource1" 
                                onrowcommand="grid_RowCommand" Width="100%" DataKeyNames="RolID" 
                                onrowdatabound="grid_RowDataBound">
                                <PagerSettings Visible="False" />
                                <Columns>
                                    <asp:BoundField DataField="RolID" HeaderText="群組編號" ReadOnly="True"><HeaderStyle HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Left" /></asp:BoundField>
                                    <asp:BoundField DataField="RolName" HeaderText="名稱" ReadOnly="True"><HeaderStyle HorizontalAlign="Center" /><ItemStyle 
                                        HorizontalAlign="Center" /></asp:BoundField>
                                    <asp:TemplateField HeaderText="是否啟用"><ItemTemplate><asp:Label ID="lblIsEnable" runat="server" Text='<%# Eval("RolEnable") %>'></asp:Label></ItemTemplate><HeaderStyle HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /></asp:TemplateField>
                                
                                    <asp:TemplateField HeaderText="修改"><ItemTemplate><asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" 
                                                CommandArgument='<%# Eval("RolID") %>' CommandName="Modify" 
                                                ImageUrl="Images/Edit.png" ToolTip="修改" /></ItemTemplate><ItemStyle HorizontalAlign="Center" /></asp:TemplateField>
                                    <asp:TemplateField HeaderText="刪除" Visible="False"><ItemTemplate><asp:ImageButton ID="imgbtnDel" runat="server" CausesValidation="False" 
                                                CommandArgument='<%# Eval("RolID") %>' CommandName="Cancel" 
                                                ImageUrl="Images/Delete.png" 
                                                onclientclick="return window.confirm('確定要刪除嗎？');" ToolTip="刪除" /></ItemTemplate><ItemStyle HorizontalAlign="Center" /></asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetAll" 
                                TypeName="Service.RolesService"></asp:ObjectDataSource>
                        </td>
                    </tr>
                </table>

            </div>
        </div>

        </asp:View>
        <asp:View ID="View2" runat="server">

        <div class="row">
        <div class="col-lg-12">
        <div class="card">
                <div class="card-header">
                <i class="fa fa-align-justify"></i> 新增/編輯
                </div>

            <div class="card-body">

            <table style="width:100%;">
                <tr>
                    <td align="center" colspan="2">
                       
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 25%">
                        <asp:Label ID="lblRolID" runat="server" SkinID="MainText" Text="群組編號"></asp:Label>
                    </td>
                    <td align="left" style="width: 75%">
                        <asp:TextBox ID="txbRolID" runat="server" MaxLength="15" Width="30%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvRolID" runat="server" 
                            ControlToValidate="txbRolID" ErrorMessage="RequiredFieldValidator" 
                            SkinID="MainWarn" ValidationGroup="OK">群組編號不可為空</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 25%">&nbsp;</td>
                    <td align="left" style="width: 75%">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" style="width: 25%" valign="top">
                        <asp:Label ID="lblRolName" runat="server" SkinID="MainText" Text="名稱"></asp:Label>
                    </td>
                    <td align="left" style="width: 75%">
                        <asp:TextBox ID="txbRolName" runat="server" 
                            Width="40%" MaxLength="30"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvRolName" runat="server" 
                            ControlToValidate="txbRolName" ErrorMessage="RequiredFieldValidator" 
                            SkinID="MainWarn" ValidationGroup="OK">名稱不可為空</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 25%" valign="top">
                        &nbsp;</td>
                    <td align="left" style="width: 75%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" style="width: 25%; height: 20px;" valign="top">
                        <asp:Label ID="lblIsEnable" runat="server" SkinID="MainText" Text="是否啟用"></asp:Label>
                    </td>
                    <td align="left" style="width: 75%; height: 20px;">
                        <asp:CheckBox ID="ckbIsEnable" runat="server" Checked="True" Text="啟用請打勾" />
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 25%; height: 20px;" valign="top">&nbsp;</td>
                    <td align="left" style="width: 75%; height: 20px;">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" style="width: 25%" valign="top">
                        <asp:Label ID="lblPermList" runat="server" SkinID="MainText" Text="權限設定"></asp:Label>
                    </td>
                    <td align="left" style="width: 75%">


                        <asp:CheckBoxList ID="ckblPermission" runat="server">
                        </asp:CheckBoxList>


                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 25%" valign="top">
                            &nbsp;</td>
                    <td align="left" style="width: 75%">
                            &nbsp;</td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnEdit" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="BigButton" onclick="btnEdit_Click" Text="確定修改" 
                            ValidationGroup="OK" />
                        <asp:Button ID="btnOK" runat="server" BackColor="Transparent" BorderWidth="0px" 
                            CssClass="SmallButton" onclick="btnOK_Click" Text="確定" ValidationGroup="OK" />
                        <asp:Button ID="btnBack" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="SmallButton" onclick="btnBack_Click" Text="返回" />
                        <asp:HiddenField ID="hideRolID" runat="server" />
                    </td>
                </tr>
            </table>
               
            </div>

        </div>
        </div>
        </div>

        </asp:View>
    </asp:MultiView>

</div>
</div>

</asp:Content>
