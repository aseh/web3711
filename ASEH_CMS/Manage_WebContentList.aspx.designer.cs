﻿//------------------------------------------------------------------------------
// <自動產生的>
//     這段程式碼是由工具產生的。
//
//     對這個檔案所做的變更可能會造成錯誤的行為，而且如果重新產生程式碼，
//     所做的變更將會遺失。 
// </自動產生的>
//------------------------------------------------------------------------------

namespace ASEH_CMS {
    
    
    public partial class Manage_WebContentList {
        
        /// <summary>
        /// MultiView1 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.MultiView MultiView1;
        
        /// <summary>
        /// View1 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.View View1;
        
        /// <summary>
        /// btnAdd 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnAdd;
        
        /// <summary>
        /// grid 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView grid;
        
        /// <summary>
        /// ObjectDataSource1 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.ObjectDataSource ObjectDataSource1;
        
        /// <summary>
        /// hideLoginUseID 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hideLoginUseID;
        
        /// <summary>
        /// View2 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.View View2;
        
        /// <summary>
        /// lable88 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lable88;
        
        /// <summary>
        /// txbWctName2 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbWctName2;
        
        /// <summary>
        /// rfvWctName 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvWctName;
        
        /// <summary>
        /// lblIsEnable0 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblIsEnable0;
        
        /// <summary>
        /// ckbIsEnable 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox ckbIsEnable;
        
        /// <summary>
        /// lable154 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lable154;
        
        /// <summary>
        /// imgWctPicPath 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image imgWctPicPath;
        
        /// <summary>
        /// uploadPicPath 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.FileUpload uploadPicPath;
        
        /// <summary>
        /// hideFileName 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hideFileName;
        
        /// <summary>
        /// lblPicDesc3_2 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblPicDesc3_2;
        
        /// <summary>
        /// lblPositionAlert15 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblPositionAlert15;
        
        /// <summary>
        /// lblMeg 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblMeg;
        
        /// <summary>
        /// btnEdit 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnEdit;
        
        /// <summary>
        /// btnOK 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnOK;
        
        /// <summary>
        /// btnBack 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnBack;
        
        /// <summary>
        /// hideID 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hideID;
    }
}
