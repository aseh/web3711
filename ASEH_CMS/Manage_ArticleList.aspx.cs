﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models;
using Service;
using Service.Interface;
using Service.Misc;
using ASEH_CMS.App_Code;
using System.Web.Configuration;
using System.IO;

namespace ASEH_CMS
{
    public partial class Manage_ArticleList : PageBase
    {
        private IExceptionRecordService _exceptionRecordService;
        private ILanguageService _languageService;
        private IArticleService _articleService;
        private IArticleClassService _articleClassService;
        private IArticleContentService _articleContentService;
        private IArticleSubService _articleSubService;
        private CommonClass _common = new CommonClass();
        UserInfo User = new UserInfo();
        protected int PageSize = 15;
        protected string TargetPage = "Manage_ArticleList.aspx";

        //WebConfig參數
        protected string SiteDomain = WebConfigurationManager.AppSettings["SiteDomain"].ToString();

        new protected int iPageType = 1;
        new protected string strPageID = "M31";

        //起始載入
        protected void Page_Load(object sender, EventArgs e)
        {
            this._exceptionRecordService = new ExceptionRecordService();
            this._languageService = new LanguageService();
            this._articleService = new ArticleService();
            this._articleClassService = new ArticleClassService();
            this._articleContentService = new ArticleContentService();
            this._articleSubService = new ArticleSubService();

            CheckUser();
            CheckUserPermission(strPageID, iPageType);

            if (!this.Page.IsPostBack)
            {
                //取得登入者ID
                User = (UserInfo)Session["UserInfo"];
                hideLoginUseID.Value = User.UserID;

                //載入語系
                //IEnumerable<tbLanguage> list = _languageService.GetAll().OrderBy(x => x.LgeSort);
                //ListItem itemL = null;
                //ddlLge.Items.Clear();
                //foreach (tbLanguage element in list)
                //{
                //    itemL = new ListItem(element.LgeName, element.LgeID);
                //    ddlLge.Items.Add(itemL);
                //}

                //載入Sub分類
                IEnumerable<tbArticleSub> listSub = _articleSubService.GetAll().Where(x => x.SubDesc == "AT001").OrderBy(x => x.SubSort);
                ListItem item = null;
                ddlSub.Items.Clear();

                //ALL
                item = new ListItem("全部", "-1");
                ddlSub.Items.Add(item);

                foreach (tbArticleSub element in listSub)
                {
                    item = new ListItem(element.SubName, element.SubID);
                    ddlSub.Items.Add(item);
                }

                //判斷類別
                if (Request.QueryString["P"] != null)
                {
                    int Page = Convert.ToInt32(Request.QueryString["P"]);

                    //設定類別
                    if (Request.QueryString["Type"] != null)
                    {
                        ddlSub.Items.FindByValue(Request.QueryString["Type"]).Selected = true;
                    }

                    //載入內容
                    SetPageContent(Page, ddlSub.SelectedValue);
                }
                //沒有任何參數時
                else
                {
                    //重新導向有參數頁面
                    Response.Redirect(string.Format("{0}?P=1&Type={1}", TargetPage, ddlSub.SelectedValue));
                }
            }
        }

        #region 下拉選單動作

        //語系下拉選單動作
        protected void ddlLge_SelectedIndexChanged(object sender, EventArgs e)
        {
            //int Page = Convert.ToInt32(Request.QueryString["P"]);

            ////載入內容
            //SetPageContent(Page, ddlSub.SelectedValue);
        }

        //語系2下拉選單動作
        protected void ddlLge2_SelectedIndexChanged(object sender, EventArgs e)
        {
            gridContent2.DataBind();
        }

        //類別下拉選單的動作
        protected void ddlSub_SelectedIndexChanged(object sender, EventArgs e)
        {
            //載入內容
            //int Page =1;
            //SetPageContent(Page, ddlSub.SelectedValue);

            //重新導向有參數頁面
            Response.Redirect(string.Format("{0}?P=1&Type={1}", TargetPage, ddlSub.SelectedValue));
        }

        //排序下拉選單的動作
        protected void ddlOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
            int Page = Convert.ToInt32(Request.QueryString["P"]);

            //設定類別
            if (Request.QueryString["Type"] != null)
            {
                ddlSub.Items.FindByValue(Request.QueryString["Type"]).Selected = true;
            }

            //載入內容
            SetPageContent(Page, ddlSub.SelectedValue);
        }

        //排序類型下拉選單的動作
        protected void ddlOrderType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int Page = Convert.ToInt32(Request.QueryString["P"]);

            //設定類別
            if (Request.QueryString["Type"] != null)
            {
                ddlSub.Items.FindByValue(Request.QueryString["Type"]).Selected = true;
            }

            //載入內容
            SetPageContent(Page, ddlSub.SelectedValue);
        }

        #endregion

        //查詢的動作
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            int Page = Convert.ToInt32(Request.QueryString["P"]);

            //設定類別
            if (Request.QueryString["Type"] != null)
            {
                ddlSub.Items.FindByValue(Request.QueryString["Type"]).Selected = true;
            }

            //載入內容
            SetPageContent(Page, ddlSub.SelectedValue);
        }

        //返回的動作
        protected void btnBack_Click(object sender, EventArgs e)
        {
            this.MultiView1.ActiveViewIndex = 0;
        }

        //新增按鈕的動作
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //載入Sub分類
            IEnumerable<tbArticleSub> listSub = _articleSubService.GetAll().Where(x => x.SubDesc == "AT001").OrderBy(x => x.SubSort);
            ListItem itemS = null;
            ddlSub2.Items.Clear();
            foreach (tbArticleSub element in listSub)
            {
                itemS = new ListItem(element.SubName, element.SubID);
                ddlSub2.Items.Add(itemS);
            }

            //設定分類
            if (ddlSub.SelectedValue != "-1")
            {
                ddlSub2.ClearSelection();
                ddlSub2.Items.FindByValue(ddlSub.SelectedValue).Selected = true;
            }

            //清空
            txbAtcName2.Text = "";
            txbAtcName_EN2.Text = "";
            txbAtcDesc2.Text = "";
            txbAtcDesc_EN2.Text = "";
            txbAtcAuthor2.Text = "";
            txbAtcAuthor_EN2.Text = "";

            ckbAtcEnable2.Checked = true;
            ckbAtcIsSign2.Checked = true;
            ckbAtcPopup2.Checked = false;
            lblMeg2.Text = "";
            
            //上架日期
            txbOpenDate2.Text = DateTime.Now.ToString("yyyy/MM/dd");
            ddlOpenDateH2.ClearSelection();
            ddlOpenDateH2.Items[0].Selected = true;
            ddlOpenDateM2.ClearSelection();
            ddlOpenDateM2.Items[0].Selected = true;

            //下架日期
            txbCloseDate2.Text = DateTime.Now.AddMonths(1).ToString("yyyy/MM/dd");
            ddlCloseDateH2.ClearSelection();
            ddlCloseDateH2.Items[0].Selected = true;
            ddlCloseDateM2.ClearSelection();
            ddlCloseDateM2.Items[0].Selected = true;

            #region 圖片

            imgAtcPicPath2.ImageUrl = "Images/default.jpg";

            #endregion

            #region FB圖片

            imgAtcFBPicPath2.ImageUrl = "Images/default.jpg";

            #endregion

            //圖片描述
            txbAtcPicDesc2.Text = "";

            //清空列表
            gridContent2.DataBind();

            var model = new tbArticle();
            model.AtcName = "";
            model.AtcName_EN = "";
            model.AtcDesc = "";
            model.AtcDesc_EN = "";
            model.AtcAuthor = "";
            model.AtcAuthor_EN = "";
            model.AtcStatus = 0;
            model.AtcEnable = 0;
            model.AtcPopup = 0;
            model.AtcSort = 0;
            model.AtcSetOD = 0;
            model.AtcOpenDate = DateTime.Now;
            model.AtcCloseDate = DateTime.Now;

            model.AtcSEOName = "";
            model.AtcSEODesc = "";

            model.CreateUser = hideLoginUseID.Value;
            model.CreateDate = DateTime.Now;
            model.LastUpdateUser = hideLoginUseID.Value;
            model.LastUpdateDate = DateTime.Now;
            model.LgeID = "-1";
            model.AttID = "99999";
            model.AtcSubID = ddlSub.SelectedValue;
            model.AtcTop = 0;
            model.AtcView = 0;

            _articleService.Create(model);

            //預先存一筆資料
            hideID.Value = model.AtcID.ToString();

            #region 載入語系

            IEnumerable<tbLanguage> listL = _languageService.GetAll().OrderBy(x => x.LgeSort);
            ListItem itemL = null;
            ddlLge2.Items.Clear();
            foreach (tbLanguage element in listL)
            {
                itemL = new ListItem(element.LgeName, element.LgeID);
                ddlLge2.Items.Add(itemL);
            }

            #endregion

            #region 文稿類型

            //Class
            IEnumerable<tbArticleClass> list = _articleClassService.GetAll().OrderBy(x => x.AtsSort);
            ListItem item = null;
            ddlArticleClass2.Items.Clear();
            foreach (tbArticleClass element in list)
            {
                item = new ListItem(element.AtsName, element.AtsID);
                ddlArticleClass2.Items.Add(item);
            }

            #endregion

            this.btnOK2.Visible = true;
            this.btnEdit2.Visible = false;
            this.MultiView1.ActiveViewIndex = 1;
        }

        //新增儲存動作
        protected void btnOK2_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new tbArticle();
                model.AtcID = Convert.ToInt32(hideID.Value);
                model.AtcName = txbAtcName2.Text;
                model.AtcName_EN = txbAtcName_EN2.Text;
                model.AtcDesc = txbAtcDesc2.Text;
                model.AtcDesc_EN = txbAtcDesc_EN2.Text;
                model.AtcAuthor = txbAtcAuthor2.Text;
                model.AtcAuthor_EN = txbAtcAuthor_EN2.Text;
                model.AtcStatus = 0;
                model.AtcEnable = _common.GetChecked(ckbAtcEnable2);
                model.AtcIsSign = _common.GetChecked(ckbAtcIsSign2);
                model.AtcPopup = _common.GetChecked(ckbAtcPopup2);
                model.AtcSort = 0;
                model.AtcSetOD = 1;
                model.AtcOpenDate = Convert.ToDateTime(string.Format("{0} {1}:{2}:00", txbOpenDate2.Text, ddlOpenDateH2.SelectedValue, ddlOpenDateM2.SelectedValue));
                model.AtcCloseDate = Convert.ToDateTime(string.Format("{0} {1}:{2}:00", txbCloseDate2.Text, ddlCloseDateH2.SelectedValue, ddlCloseDateM2.SelectedValue));

                model.AtcSEOName = txbAtcSEOName2.Text;
                model.AtcSEODesc = txbAtcSEODesc2.Text;

                model.CreateUser = hideLoginUseID.Value;
                model.CreateDate = DateTime.Now;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;
                model.LgeID = "-1";
                model.AttID = "AT001";
                model.AtcSubID = ddlSub2.SelectedValue;
                model.AtcTop = _common.GetChecked(ckbAtcTop2);
                model.AtcView = 0;

                //圖片
                model.AtcPicPath = hideAtcPicPath2.Value;
                model.AtcFBPicPath = hideAtcFBPicPath2.Value;

                model.AtcPicDeac = txbAtcPicDesc2.Text;

                _articleService.Update(model);

                //ShowMessage("儲存成功");
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }

            //載入內容
            int Page = Convert.ToInt32(Request.QueryString["P"]);
            //SetPageContent(Page, ddlSub.SelectedValue);

            //重新導向有參數頁面
            Response.Redirect(string.Format("{0}?P={1}&Type={2}", TargetPage, Page, ddlSub.SelectedValue));

            this.MultiView1.ActiveViewIndex = 0;
        }

        //確定修改動作
        protected void btnEdit2_Click(object sender, EventArgs e)
        {
            try
            {
                var model = _articleService.GetByID(Convert.ToInt32(hideID.Value));
                model.AtcID = Convert.ToInt32(hideID.Value);
                model.AtcName = txbAtcName2.Text;
                model.AtcName_EN = txbAtcName_EN2.Text;
                model.AtcDesc = txbAtcDesc2.Text;
                model.AtcDesc_EN = txbAtcDesc_EN2.Text;
                model.AtcAuthor = txbAtcAuthor2.Text;
                model.AtcAuthor_EN = txbAtcAuthor_EN2.Text;
                model.AtcStatus = 0;
                model.AtcEnable = _common.GetChecked(ckbAtcEnable2);
                model.AtcIsSign = _common.GetChecked(ckbAtcIsSign2);
                model.AtcPopup = _common.GetChecked(ckbAtcPopup2);
                model.AtcSort = 0;
                model.AtcSetOD = 1;
                model.AtcOpenDate = Convert.ToDateTime(string.Format("{0} {1}:{2}:00", txbOpenDate2.Text, ddlOpenDateH2.SelectedValue, ddlOpenDateM2.SelectedValue));
                model.AtcCloseDate = Convert.ToDateTime(string.Format("{0} {1}:{2}:00", txbCloseDate2.Text, ddlCloseDateH2.SelectedValue, ddlCloseDateM2.SelectedValue));

                if ( OgImageFileUpload.FileName != "" )
                {
                    string savePath = _common.ResizeAndSave(OgImageFileUpload, 512, 512, Server.MapPath("/Uploads"));
                    model.OgImage = String.Format("/Uploads/{0}", savePath);
                }

                model.SEODesc = txtSeoDesc.Text;
                model.SEODesc_EN = txtSeoDescEn.Text;
                
                model.AtcSEOName = txbAtcSEOName2.Text;
                model.AtcSEODesc = txbAtcSEODesc2.Text;

                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;
                model.AttID = "AT001";
                model.AtcSubID = ddlSub2.SelectedValue;
                model.AtcTop = _common.GetChecked(ckbAtcTop2);

                //圖片
                model.AtcPicPath = hideAtcPicPath2.Value;
                model.AtcFBPicPath = hideAtcFBPicPath2.Value;

                model.AtcPicDeac = txbAtcPicDesc2.Text;

                _articleService.Update(model);

                //ShowMessage("儲存成功");
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }

            //載入內容
            int Page = Convert.ToInt32(Request.QueryString["P"]);
            //SetPageContent(Page, ddlSub.SelectedValue);

            //重新導向有參數頁面
            Response.Redirect(string.Format("{0}?P={1}&Type={2}", TargetPage, Page, ddlSub.SelectedValue));

            this.MultiView1.ActiveViewIndex = 0;
        }

        #region 圖片處理

        //圖片上傳
        protected void btnUploadPic2_Click(object sender, EventArgs e)
        {
            string AtcID = hideID.Value;

            string newPicPathFileName = "";
            //上傳圖片
            bool PicCheck = true;
            if (uploadAtcPicPath2.HasFile)
            {
                Boolean FileOK = false;

                string fileExtension = System.IO.Path.GetExtension(uploadAtcPicPath2.FileName).ToLower();
                string[] allowExtensions = { ".bmp", ".jpg", ".jpeg", ".png" };
                for (int i = 0; i < allowExtensions.Length; i++)
                {
                    if (fileExtension == allowExtensions[i])
                    {
                        FileOK = true;
                    }
                }

                if (FileOK)
                {
                    //PNG特別處理
                    string FileNameOrg = uploadAtcPicPath2.FileName;
                    string Name = Path.GetFileNameWithoutExtension(FileNameOrg);
                    string Extension = Path.GetExtension(FileNameOrg);

                    if (Extension == ".png")
                    {
                        string FileName = Guid.NewGuid().ToString();
                        newPicPathFileName = Path.ChangeExtension(FileName, ".png");
                        string FolderPath = string.Format("Uploads/{0}/", AtcID);
                        string RealFolderPath = Server.MapPath(FolderPath);

                        int returnCode = _common.UploadAnyFileWithNewName(uploadAtcPicPath2, RealFolderPath, FileName);
                    }
                    else
                    {
                        string FileName = Guid.NewGuid().ToString();
                        newPicPathFileName = Path.ChangeExtension(FileName, ".jpg");
                        string FolderPath = string.Format("Uploads/{0}/", AtcID);
                        string RealFolderPath = Server.MapPath(FolderPath);

                        int returnCode = _common.UploadOrgJpgPicWithNewName(uploadAtcPicPath2, RealFolderPath, FileName);
                    }

                    //檔案名稱暫存起來
                    hideAtcPicPath2.Value = newPicPathFileName;

                    #region 圖片顯示

                    //圖片
                    if (newPicPathFileName != "")
                    {
                        imgAtcPicPath2.ImageUrl = string.Format("Uploads/{0}/{1}"
                                                              , AtcID
                                                              , newPicPathFileName);
                    }
                    else
                    {
                        imgAtcPicPath2.ImageUrl = "Images/default.jpg";
                    }

                    #endregion

                    PicCheck = true;
                }
                else
                {
                    PicCheck = false;
                }
            }

            if (!PicCheck)
            {
                ShowMessage("圖片上傳失敗，請確認檔案格式是否正確");
            }
        }


        //FB圖片上傳
        protected void btnUploadFBPic2_Click(object sender, EventArgs e)
        {
            string AtcID = hideID.Value;

            string newFBPicPathFileName = "";
            //上傳圖片
            bool FBPicCheck = true;
            if (uploadAtcFBPicPath2.HasFile)
            {
                Boolean FileOK = false;

                string fileExtension = System.IO.Path.GetExtension(uploadAtcFBPicPath2.FileName).ToLower();
                string[] allowExtensions = { ".bmp", ".jpg", ".jpeg", ".png" };
                for (int i = 0; i < allowExtensions.Length; i++)
                {
                    if (fileExtension == allowExtensions[i])
                    {
                        FileOK = true;
                    }
                }

                if (FileOK)
                {
                    //PNG特別處理
                    string FileNameOrg = uploadAtcFBPicPath2.FileName;
                    string Name = Path.GetFileNameWithoutExtension(FileNameOrg);
                    string Extension = Path.GetExtension(FileNameOrg);

                    if (Extension == ".png")
                    {
                        string FileName = Guid.NewGuid().ToString();
                        newFBPicPathFileName = Path.ChangeExtension(FileName, ".png");
                        string FolderPath = string.Format("Uploads/{0}/", AtcID);
                        string RealFolderPath = Server.MapPath(FolderPath);

                        int returnCode = _common.UploadAnyFileWithNewName(uploadAtcFBPicPath2, RealFolderPath, FileName);
                    }
                    else
                    {
                        string FileName = Guid.NewGuid().ToString();
                        newFBPicPathFileName = Path.ChangeExtension(FileName, ".jpg");
                        string FolderPath = string.Format("Uploads/{0}/", AtcID);
                        string RealFolderPath = Server.MapPath(FolderPath);

                        int returnCode = _common.UploadOrgJpgPicWithNewName(uploadAtcFBPicPath2, RealFolderPath, FileName);
                    }

                    //檔案名稱暫存起來
                    hideAtcFBPicPath2.Value = newFBPicPathFileName;

                    #region 圖片顯示

                    //圖片
                    if (newFBPicPathFileName != "")
                    {
                        imgAtcFBPicPath2.ImageUrl = string.Format("Uploads/{0}/{1}"
                                                              , AtcID
                                                              , newFBPicPathFileName);
                    }
                    else
                    {
                        imgAtcFBPicPath2.ImageUrl = "Images/default.jpg";
                    }

                    #endregion

                    FBPicCheck = true;
                }
                else
                {
                    FBPicCheck = false;
                }
            }

            if (!FBPicCheck)
            {
                ShowMessage("圖片上傳失敗，請確認檔案格式是否正確");
            }
        }

        #endregion

        #region GridView動作

        protected void grid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Modify":
                    {
                        //ID
                        string AtcID = e.CommandArgument.ToString();
                        //暫存
                        hideID.Value = AtcID;

                        var model = _articleService.GetByID(Convert.ToInt32(AtcID));

                        //載入Sub分類
                        IEnumerable<tbArticleSub> listSub = _articleSubService.GetAll().Where(x => x.SubDesc == "AT001").OrderBy(x => x.SubSort);
                        ListItem itemS = null;
                        ddlSub2.Items.Clear();
                        foreach (tbArticleSub element in listSub)
                        {
                            itemS = new ListItem(element.SubName, element.SubID);
                            ddlSub2.Items.Add(itemS);
                        }

                        //類別
                        ddlSub2.ClearSelection();
                        ddlSub2.Items.FindByValue(model.AtcSubID).Selected = true;

                        //上架日期
                        txbOpenDate2.Text = Convert.ToDateTime(model.AtcOpenDate).ToString("yyyy/MM/dd");
                        ddlOpenDateH2.ClearSelection();
                        ddlOpenDateH2.Items.FindByValue(Convert.ToDateTime(model.AtcOpenDate).ToString("HH")).Selected = true;
                        ddlOpenDateM2.ClearSelection();
                        ddlOpenDateM2.Items.FindByValue(Convert.ToDateTime(model.AtcOpenDate).ToString("mm")).Selected = true;

                        //下架日期
                        txbCloseDate2.Text = Convert.ToDateTime(model.AtcCloseDate).ToString("yyyy/MM/dd");
                        ddlCloseDateH2.ClearSelection();
                        ddlCloseDateH2.Items.FindByValue(Convert.ToDateTime(model.AtcCloseDate).ToString("HH")).Selected = true;
                        ddlCloseDateM2.ClearSelection();
                        ddlCloseDateM2.Items.FindByValue(Convert.ToDateTime(model.AtcCloseDate).ToString("mm")).Selected = true;

                        txbAtcName2.Text = model.AtcName;
                        txbAtcName_EN2.Text = model.AtcName_EN;
                        txbAtcDesc2.Text = model.AtcDesc;
                        txbAtcDesc_EN2.Text = model.AtcDesc_EN;
                        txbAtcAuthor2.Text = model.AtcAuthor;
                        txbAtcAuthor_EN2.Text = model.AtcAuthor_EN;

                        txtSeoDesc.Text = model.SEODesc;
                        txtSeoDescEn.Text = model.SEODesc_EN;
                        OgImagePreview.ImageUrl = model.OgImage;

                        txbAtcSEOName2.Text = model.AtcSEOName;
                        txbAtcSEODesc2.Text = model.AtcSEODesc;

                        _common.SetChecked(ckbAtcEnable2, Convert.ToInt32(model.AtcEnable));
                        _common.SetChecked(ckbAtcIsSign2, Convert.ToInt32(model.AtcIsSign));
                        _common.SetChecked(ckbAtcPopup2, Convert.ToInt32(model.AtcPopup));
                        _common.SetChecked(ckbAtcTop2, Convert.ToInt32(model.AtcTop));

                        #region 圖片

                        //圖片
                        if (model.AtcPicPath != "")
                        {
                            imgAtcPicPath2.ImageUrl = string.Format("Uploads/{0}/{1}"
                                                                    , model.AtcID.ToString()
                                                                    , model.AtcPicPath);
                        }
                        else
                        {
                            imgAtcPicPath2.ImageUrl = "Images/default.jpg";
                        }
                        hideAtcPicPath2.Value = model.AtcPicPath;

                        #endregion

                        #region FB圖片

                        //FB圖片
                        if (model.AtcFBPicPath != "")
                        {
                            imgAtcFBPicPath2.ImageUrl = string.Format("Uploads/{0}/{1}"
                                                                      , model.AtcID.ToString()
                                                                      , model.AtcFBPicPath);
                        }
                        else
                        {
                            imgAtcFBPicPath2.ImageUrl = "Images/default.jpg";
                        }
                        hideAtcFBPicPath2.Value = model.AtcFBPicPath;

                        #endregion

                        //圖片描述
                        txbAtcPicDesc2.Text = model.AtcPicDeac;

                        #region 載入語系

                        IEnumerable<tbLanguage> listL = _languageService.GetAll().OrderBy(x => x.LgeSort);
                        ListItem itemL = null;
                        ddlLge2.Items.Clear();
                        foreach (tbLanguage element in listL)
                        {
                            itemL = new ListItem(element.LgeName, element.LgeID);
                            ddlLge2.Items.Add(itemL);
                        }

                        #endregion

                        #region 文稿類型

                        //Class
                        IEnumerable<tbArticleClass> list = _articleClassService.GetAll().OrderBy(x => x.AtsSort);
                        ListItem item = null;
                        ddlArticleClass2.Items.Clear();
                        foreach (tbArticleClass element in list)
                        {
                            item = new ListItem(element.AtsName, element.AtsID);
                            ddlArticleClass2.Items.Add(item);
                        }

                        //更新列表
                        gridContent2.DataBind();

                        #endregion
                        
                        lblMeg2.Text = "";

                        this.btnOK2.Visible = false;
                        this.btnEdit2.Visible = true;
                        this.MultiView1.ActiveViewIndex = 1;

                        break;
                    }
                case "Remove":
                    {
                        string AtcID = e.CommandArgument.ToString();

                        //先刪除內容
                        IEnumerable<tbArticleContent> list = _articleContentService.GetAll().Where(p => p.AtcID == Convert.ToInt32(AtcID)).ToList();
                        foreach (tbArticleContent element in list)
                        {
                            _articleContentService.Delete(element.AtnID);
                        }

                        //再刪主表
                        _articleService.Delete(Convert.ToInt32(AtcID));

                        //載入內容
                        int Page = Convert.ToInt32(Request.QueryString["P"]);
                        //SetPageContent(Page, ddlSub.SelectedValue);

                        //重新導向有參數頁面
                        Response.Redirect(string.Format("{0}?P={1}&Type={2}", TargetPage, Page, ddlSub.SelectedValue));

                        break;
                    }
                default:
                    {
                        return;
                    }
            }
        }

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //是否置頂
            Literal newltlHome = (Literal)e.Row.FindControl("ltlHome");
            if (newltlHome != null)
            {
                if (int.Parse(newltlHome.Text) == 1)
                {
                    newltlHome.Text = "<img src='Images/home.png' width='40%' />";
                }
                else if (int.Parse(newltlHome.Text) == 0)
                {
                    newltlHome.Text = "";
                }
            }

            //類別
            Label newlblSubName = (Label)e.Row.FindControl("lblSubName");
            if (newlblSubName != null)
            {
                string SubID = newlblSubName.Text;

                var model = _articleSubService.GetByID(SubID);
                if (model != null)
                {
                    newlblSubName.Text = model.SubName;
                }
            }

            //是否簽名
            Label newlblIsSign = (Label)e.Row.FindControl("lblIsSign");
            if (newlblIsSign != null)
            {
                if (int.Parse(newlblIsSign.Text) == 1)
                {
                    Label lblEnable = _common.ChangeIsEnable(1);
                    newlblIsSign.Text = lblEnable.Text;
                }
                else if (int.Parse(newlblIsSign.Text) == 0)
                {
                    Label lblEnable = _common.ChangeIsEnable(0);
                    newlblIsSign.Text = lblEnable.Text;
                    newlblIsSign.ForeColor = lblEnable.ForeColor;
                }
            }

            //是否啟用
            Label newlblIsEnable = (Label)e.Row.FindControl("lblIsEnable");
            if (newlblIsEnable != null)
            {
                if (int.Parse(newlblIsEnable.Text) == 1)
                {
                    Label lblEnable = _common.ChangeIsEnable(1);
                    newlblIsEnable.Text = lblEnable.Text;
                }
                else if (int.Parse(newlblIsEnable.Text) == 0)
                {
                    Label lblEnable = _common.ChangeIsEnable(0);
                    newlblIsEnable.Text = lblEnable.Text;
                    newlblIsEnable.ForeColor = lblEnable.ForeColor;
                }
            }

            //是否彈出視窗
            Label newlblIsPopup = (Label)e.Row.FindControl("lblIsPopup");
            if (newlblIsPopup != null)
            {
                if (int.Parse(newlblIsPopup.Text) == 1)
                {
                    Label lblEnable = _common.ChangeIsEnable(1);
                    newlblIsPopup.Text = lblEnable.Text;
                }
                else if (int.Parse(newlblIsPopup.Text) == 0)
                {
                    Label lblEnable = _common.ChangeIsEnable(0);
                    newlblIsPopup.Text = lblEnable.Text;
                    newlblIsPopup.ForeColor = lblEnable.ForeColor;
                }
            }

            //縮圖
            Image imgPic = (Image)e.Row.FindControl("imgPic");
            if (imgPic != null)
            {
                string Temp = imgPic.AlternateText;

                //AtcID
                string AtcID = Temp.Split(',')[0].Trim();
                //AtcPicPath
                string AtcPicPath = Temp.Split(',')[1].Trim();

                if (AtcPicPath != "")
                {
                    imgPic.ImageUrl = string.Format("Uploads/{0}/{1}"
                                                    , AtcID
                                                    , AtcPicPath);
                }
                else
                {
                    imgPic.ImageUrl = "Images/default.jpg";
                }
            }
        }

        #endregion

        #region 內容管理

        //內容管理新增按鈕的動作
        protected void btnAdd2_Click(object sender, EventArgs e)
        {
            string AtsID = ddlArticleClass2.SelectedValue;

            switch (AtsID)
            {
                //副標區塊
                case "C01":
                    //清空
                    txbAtnSubject3.Text = "";
                    CKEditorControl3.Text = "";

                    this.btnOK3.Visible = true;
                    this.btnEdit3.Visible = false;

                    this.MultiView1.ActiveViewIndex = 2;
                    break;
                //文字區塊
                case "C02":
                    //清空
                    txbAtnSubject4.Text = "";
                    CKEditorControl4.Text = "";

                    this.btnOK4.Visible = true;
                    this.btnEdit4.Visible = false;

                    this.MultiView1.ActiveViewIndex = 3;
                    break;
                //圖片區塊
                case "C11":
                    //清空
                    txbAtnSubject5.Text = "";
                    imgAtnPicPath5.ImageUrl = "Images/default.jpg";

                    this.btnOK5.Visible = true;
                    this.btnEdit5.Visible = false;

                    this.MultiView1.ActiveViewIndex = 4;

                    break;
                //串流影片區塊(YouTube)
                case "C31":
                    //清空
                    txbAtnSubject6.Text = "";
                    txbAtnVideoPath6.Text = "";

                    this.btnOK6.Visible = true;
                    this.btnEdit6.Visible = false;

                    this.MultiView1.ActiveViewIndex = 5;

                    break;
                //串流影片區塊(Vimeo)
                case "C32":
                    //清空
                    txbAtnSubject7.Text = "";
                    txbAtnVideoPath7.Text = "";

                    this.btnOK7.Visible = true;
                    this.btnEdit7.Visible = false;

                    this.MultiView1.ActiveViewIndex = 6;

                    break;
                //串流影片區塊(優酷)
                case "C33":
                    //清空
                    txbAtnSubject8.Text = "";
                    txbAtnVideoPath8.Text = "";

                    this.btnOK8.Visible = true;
                    this.btnEdit8.Visible = false;

                    this.MultiView1.ActiveViewIndex = 7;

                    break;
                //串流影片區塊(土豆網)
                case "C34":
                    //清空
                    txbAtnSubject9.Text = "";
                    txbAtnVideoPath9.Text = "";

                    this.btnOK9.Visible = true;
                    this.btnEdit9.Visible = false;

                    this.MultiView1.ActiveViewIndex = 8;

                    break;
                //MP4影片區塊
                case "C41":
                    //清空
                    txbAtnSubject10.Text = "";
                    txbAtnVideoPath10.Text = "";

                    this.btnOK10.Visible = true;
                    this.btnEdit10.Visible = false;

                    this.MultiView1.ActiveViewIndex = 9;

                    break;

            }
        }

        //返回的動作
        protected void btnBack3_Click(object sender, EventArgs e)
        {
            this.MultiView1.ActiveViewIndex = 1;
        }

        #region 副標區塊

        //新增儲存副標區塊
        protected void btnOK3_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new tbArticleContent();
                model.AtsID = ddlArticleClass2.SelectedValue;
                model.AtnSubject = txbAtnSubject3.Text;
                model.AtnText = CKEditorControl3.Text;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;
                model.AtcID = Convert.ToInt32(hideID.Value);
                model.LgeID = ddlLge2.SelectedValue;

                #region 取得排序碼

                List<tbArticleContent> list = _articleContentService.GetAllByAtcIDAndLgeID(Convert.ToInt32(hideID.Value), ddlLge2.SelectedValue).ToList();
                if (list.Count != 0)
                {
                    int SortTemp = Convert.ToInt32(list[list.Count - 1].AtnSort) + 1;

                    model.AtnSort = SortTemp;
                }
                else
                {
                    model.AtnSort = 1;
                }

                #endregion

                _articleContentService.Create(model);

                //更新列表
                gridContent2.DataBind();

                this.MultiView1.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        //修改儲存副標區塊
        protected void btnEdit3_Click(object sender, EventArgs e)
        {
            try
            {
                var model = _articleContentService.GetByID(Convert.ToInt32(hideAtnID3.Value));
                model.AtnSubject = txbAtnSubject3.Text;
                model.AtnText = CKEditorControl3.Text;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;

                _articleContentService.Update(model);

                //更新列表
                gridContent2.DataBind();

                this.MultiView1.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        #endregion

        #region 文字區塊

        //新增儲存文字區塊
        protected void btnOK4_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new tbArticleContent();
                model.AtsID = ddlArticleClass2.SelectedValue;
                model.AtnSubject = txbAtnSubject4.Text;
                model.AtnText = CKEditorControl4.Text;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;
                model.AtcID = Convert.ToInt32(hideID.Value);
                model.LgeID = ddlLge2.SelectedValue;

                #region 取得排序碼

                List<tbArticleContent> list = _articleContentService.GetAllByAtcIDAndLgeID(Convert.ToInt32(hideID.Value), ddlLge2.SelectedValue).ToList();
                if (list.Count != 0)
                {
                    int SortTemp = Convert.ToInt32(list[list.Count - 1].AtnSort) + 1;

                    model.AtnSort = SortTemp;
                }
                else
                {
                    model.AtnSort = 1;
                }

                #endregion

                _articleContentService.Create(model);

                //更新列表
                gridContent2.DataBind();

                this.MultiView1.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        //修改儲存文字區塊
        protected void btnEdit4_Click(object sender, EventArgs e)
        {
            try
            {
                var model = _articleContentService.GetByID(Convert.ToInt32(hideAtnID4.Value));
                model.AtnSubject = txbAtnSubject4.Text;
                model.AtnText = CKEditorControl4.Text;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;

                _articleContentService.Update(model);

                //更新列表
                gridContent2.DataBind();

                this.MultiView1.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        #endregion

        #region 圖片區塊

        //新增儲存圖片區塊
        protected void btnOK5_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new tbArticleContent();
                model.AtsID = ddlArticleClass2.SelectedValue;
                model.AtnSubject = txbAtnSubject5.Text;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;
                model.AtcID = Convert.ToInt32(hideID.Value);
                model.LgeID = ddlLge2.SelectedValue;

                #region 取得排序碼

                List<tbArticleContent> list = _articleContentService.GetAllByAtcIDAndLgeID(Convert.ToInt32(hideID.Value), ddlLge2.SelectedValue).ToList();
                if (list.Count != 0)
                {
                    int SortTemp = Convert.ToInt32(list[list.Count - 1].AtnSort) + 1;

                    model.AtnSort = SortTemp;
                }
                else
                {
                    model.AtnSort = 1;
                }

                #endregion

                _articleContentService.Create(model);

                #region 圖片

                string AtnID = model.AtnID.ToString();

                string newPicPathFileName = "";
                //上傳圖片
                bool PicCheck = true;
                if (uploadAtnPicPath5.HasFile)
                {
                    Boolean FileOK = false;

                    string fileExtension = System.IO.Path.GetExtension(uploadAtnPicPath5.FileName).ToLower();
                    string[] allowExtensions = { ".bmp", ".jpg", ".jpeg", ".png" };
                    for (int i = 0; i < allowExtensions.Length; i++)
                    {
                        if (fileExtension == allowExtensions[i])
                        {
                            FileOK = true;
                        }
                    }

                    if (FileOK)
                    {
                        //PNG特別處理
                        string FileNameOrg = uploadAtnPicPath5.FileName;
                        string Name = Path.GetFileNameWithoutExtension(FileNameOrg);
                        string Extension = Path.GetExtension(FileNameOrg);

                        if (Extension == ".png")
                        {
                            string FileName = Guid.NewGuid().ToString();
                            newPicPathFileName = Path.ChangeExtension(FileName, ".png");
                            string FolderPath = string.Format("Uploads/{0}/", AtnID);
                            string RealFolderPath = Server.MapPath(FolderPath);

                            int returnCode = _common.UploadAnyFileWithNewName(uploadAtnPicPath5, RealFolderPath, FileName);
                        }
                        else
                        {
                            string FileName = Guid.NewGuid().ToString();
                            newPicPathFileName = Path.ChangeExtension(FileName, ".jpg");
                            string FolderPath = string.Format("Uploads/{0}/", AtnID);
                            string RealFolderPath = Server.MapPath(FolderPath);

                            int returnCode = _common.UploadOrgJpgPicWithNewName(uploadAtnPicPath5, RealFolderPath, FileName);
                        }

                        PicCheck = true;
                    }
                    else
                    {
                        PicCheck = false;
                    }
                }

                if (!PicCheck)
                {
                    ShowMessage("圖片上傳失敗，請確認檔案格式是否正確");
                }

                #endregion

                //再更新一次
                var modelTemp = _articleContentService.GetByID(model.AtnID);
                model.AtnPicPath = newPicPathFileName;
                _articleContentService.Update(modelTemp);

                //更新列表
                gridContent2.DataBind();

                this.MultiView1.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        //修改儲存圖片區塊
        protected void btnEdit5_Click(object sender, EventArgs e)
        {
            try
            {
                var model = _articleContentService.GetByID(Convert.ToInt32(hideAtnID5.Value));
                model.AtnSubject = txbAtnSubject5.Text;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;

                #region 圖片

                string AtnID = model.AtnID.ToString();

                string newPicPathFileName = "";
                //上傳圖片
                bool PicCheck = true;
                if (uploadAtnPicPath5.HasFile)
                {
                    Boolean FileOK = false;

                    string fileExtension = System.IO.Path.GetExtension(uploadAtnPicPath5.FileName).ToLower();
                    string[] allowExtensions = { ".bmp", ".jpg", ".jpeg", ".png" };
                    for (int i = 0; i < allowExtensions.Length; i++)
                    {
                        if (fileExtension == allowExtensions[i])
                        {
                            FileOK = true;
                        }
                    }

                    if (FileOK)
                    {
                        //PNG特別處理
                        string FileNameOrg = uploadAtnPicPath5.FileName;
                        string Name = Path.GetFileNameWithoutExtension(FileNameOrg);
                        string Extension = Path.GetExtension(FileNameOrg);

                        if (Extension == ".png")
                        {
                            string FileName = Guid.NewGuid().ToString();
                            newPicPathFileName = Path.ChangeExtension(FileName, ".png");
                            string FolderPath = string.Format("Uploads/{0}/", AtnID);
                            string RealFolderPath = Server.MapPath(FolderPath);

                            int returnCode = _common.UploadAnyFileWithNewName(uploadAtnPicPath5, RealFolderPath, FileName);
                        }
                        else
                        {
                            string FileName = Guid.NewGuid().ToString();
                            newPicPathFileName = Path.ChangeExtension(FileName, ".jpg");
                            string FolderPath = string.Format("Uploads/{0}/", AtnID);
                            string RealFolderPath = Server.MapPath(FolderPath);

                            int returnCode = _common.UploadOrgJpgPicWithNewName(uploadAtnPicPath5, RealFolderPath, FileName);
                        }

                        PicCheck = true;
                    }
                    else
                    {
                        PicCheck = false;
                    }
                }

                if (!PicCheck)
                {
                    ShowMessage("圖片上傳失敗，請確認檔案格式是否正確");
                }

                #endregion

                //圖片判斷
                if (uploadAtnPicPath5.FileName != "")
                {
                    model.AtnPicPath = newPicPathFileName;
                }
                else
                {
                    model.AtnPicPath = hideAtnPicPath5.Value;
                }

                _articleContentService.Update(model);

                //更新列表
                gridContent2.DataBind();

                this.MultiView1.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        #endregion

        #region 串流影片區塊(YouTube)

        //新增儲存串流影片區塊(YouTube)
        protected void btnOK6_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new tbArticleContent();
                model.AtsID = ddlArticleClass2.SelectedValue;
                model.AtnSubject = txbAtnSubject6.Text;
                model.AtnVideoPath = txbAtnVideoPath6.Text;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;
                model.AtcID = Convert.ToInt32(hideID.Value);
                model.LgeID = ddlLge2.SelectedValue;

                #region 取得排序碼

                List<tbArticleContent> list = _articleContentService.GetAllByAtcIDAndLgeID(Convert.ToInt32(hideID.Value), ddlLge2.SelectedValue).ToList();
                if (list.Count != 0)
                {
                    int SortTemp = Convert.ToInt32(list[list.Count - 1].AtnSort) + 1;

                    model.AtnSort = SortTemp;
                }
                else
                {
                    model.AtnSort = 1;
                }

                #endregion

                _articleContentService.Create(model);

                //更新列表
                gridContent2.DataBind();

                this.MultiView1.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        //修改儲存串流影片區塊(YouTube)
        protected void btnEdit6_Click(object sender, EventArgs e)
        {
            try
            {
                var model = _articleContentService.GetByID(Convert.ToInt32(hideAtnID6.Value));
                model.AtnSubject = txbAtnSubject6.Text;
                model.AtnVideoPath = txbAtnVideoPath6.Text;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;

                _articleContentService.Update(model);

                //更新列表
                gridContent2.DataBind();

                this.MultiView1.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        #endregion

        #region 串流影片區塊(Vimeo)

        //新增儲存串流影片區塊(Vimeo)
        protected void btnOK7_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new tbArticleContent();
                model.AtsID = ddlArticleClass2.SelectedValue;
                model.AtnSubject = txbAtnSubject7.Text;
                model.AtnVideoPath = txbAtnVideoPath7.Text;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;
                model.AtcID = Convert.ToInt32(hideID.Value);
                model.LgeID = ddlLge2.SelectedValue;

                #region 取得排序碼

                List<tbArticleContent> list = _articleContentService.GetAllByAtcIDAndLgeID(Convert.ToInt32(hideID.Value), ddlLge2.SelectedValue).ToList();
                if (list.Count != 0)
                {
                    int SortTemp = Convert.ToInt32(list[list.Count - 1].AtnSort) + 1;

                    model.AtnSort = SortTemp;
                }
                else
                {
                    model.AtnSort = 1;
                }

                #endregion

                _articleContentService.Create(model);

                //更新列表
                gridContent2.DataBind();

                this.MultiView1.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        //修改儲存串流影片區塊(Vimeo)
        protected void btnEdit7_Click(object sender, EventArgs e)
        {
            try
            {
                var model = _articleContentService.GetByID(Convert.ToInt32(hideAtnID7.Value));
                model.AtnSubject = txbAtnSubject7.Text;
                model.AtnVideoPath = txbAtnVideoPath7.Text;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;

                _articleContentService.Update(model);

                //更新列表
                gridContent2.DataBind();

                this.MultiView1.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        #endregion

        #region 串流影片區塊(優酷)

        //新增儲存串流影片區塊(優酷)
        protected void btnOK8_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new tbArticleContent();
                model.AtsID = ddlArticleClass2.SelectedValue;
                model.AtnSubject = txbAtnSubject8.Text;
                model.AtnVideoPath = txbAtnVideoPath8.Text;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;
                model.AtcID = Convert.ToInt32(hideID.Value);
                model.LgeID = ddlLge2.SelectedValue;

                #region 取得排序碼

                List<tbArticleContent> list = _articleContentService.GetAllByAtcIDAndLgeID(Convert.ToInt32(hideID.Value), ddlLge2.SelectedValue).ToList();
                if (list.Count != 0)
                {
                    int SortTemp = Convert.ToInt32(list[list.Count - 1].AtnSort) + 1;

                    model.AtnSort = SortTemp;
                }
                else
                {
                    model.AtnSort = 1;
                }

                #endregion

                _articleContentService.Create(model);

                //更新列表
                gridContent2.DataBind();

                this.MultiView1.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        //修改儲存串流影片區塊(優酷)
        protected void btnEdit8_Click(object sender, EventArgs e)
        {
            try
            {
                var model = _articleContentService.GetByID(Convert.ToInt32(hideAtnID8.Value));
                model.AtnSubject = txbAtnSubject8.Text;
                model.AtnVideoPath = txbAtnVideoPath8.Text;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;

                _articleContentService.Update(model);

                //更新列表
                gridContent2.DataBind();

                this.MultiView1.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        #endregion

        #region 串流影片區塊(土豆網)

        //新增儲存串流影片區塊(土豆網)
        protected void btnOK9_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new tbArticleContent();
                model.AtsID = ddlArticleClass2.SelectedValue;
                model.AtnSubject = txbAtnSubject9.Text;
                model.AtnVideoPath = txbAtnVideoPath9.Text;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;
                model.AtcID = Convert.ToInt32(hideID.Value);
                model.LgeID = ddlLge2.SelectedValue;

                #region 取得排序碼

                List<tbArticleContent> list = _articleContentService.GetAllByAtcIDAndLgeID(Convert.ToInt32(hideID.Value), ddlLge2.SelectedValue).ToList();
                if (list.Count != 0)
                {
                    int SortTemp = Convert.ToInt32(list[list.Count - 1].AtnSort) + 1;

                    model.AtnSort = SortTemp;
                }
                else
                {
                    model.AtnSort = 1;
                }

                #endregion

                _articleContentService.Create(model);

                //更新列表
                gridContent2.DataBind();

                this.MultiView1.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        //修改儲存串流影片區塊(土豆網)
        protected void btnEdit9_Click(object sender, EventArgs e)
        {
            try
            {
                var model = _articleContentService.GetByID(Convert.ToInt32(hideAtnID9.Value));
                model.AtnSubject = txbAtnSubject9.Text;
                model.AtnVideoPath = txbAtnVideoPath9.Text;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;

                _articleContentService.Update(model);

                //更新列表
                gridContent2.DataBind();

                this.MultiView1.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        #endregion

        #region 串流MP4影片區塊

        //新增串流MP4影片區塊
        protected void btnOK10_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new tbArticleContent();
                model.AtsID = ddlArticleClass2.SelectedValue;
                model.AtnSubject = txbAtnSubject10.Text;
                model.AtnVideoPath = txbAtnVideoPath10.Text;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;
                model.AtcID = Convert.ToInt32(hideID.Value);
                model.LgeID = ddlLge2.SelectedValue;

                #region 取得排序碼

                List<tbArticleContent> list = _articleContentService.GetAllByAtcIDAndLgeID(Convert.ToInt32(hideID.Value), ddlLge2.SelectedValue).ToList();
                if (list.Count != 0)
                {
                    int SortTemp = Convert.ToInt32(list[list.Count - 1].AtnSort) + 1;

                    model.AtnSort = SortTemp;
                }
                else
                {
                    model.AtnSort = 1;
                }

                #endregion

                _articleContentService.Create(model);

                //更新列表
                gridContent2.DataBind();

                this.MultiView1.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        //修改串流MP4影片區塊
        protected void btnEdit10_Click(object sender, EventArgs e)
        {
            try
            {
                var model = _articleContentService.GetByID(Convert.ToInt32(hideAtnID10.Value));
                model.AtnSubject = txbAtnSubject10.Text;
                model.AtnVideoPath = txbAtnVideoPath10.Text;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;

                _articleContentService.Update(model);

                //更新列表
                gridContent2.DataBind();

                this.MultiView1.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        #endregion

        #region gridProduct動作

        protected void gridContent2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "View":
                    {

                        break;
                    }

                case "SortUp":
                    {
                        //AtcID
                        string AtcID = e.CommandArgument.ToString().Split(',')[0].Trim();
                        //AtnID
                        string AtnID = e.CommandArgument.ToString().Split(',')[1].Trim();
                        //AtnSort
                        string AtnSort = e.CommandArgument.ToString().Split(',')[2].Trim();

                        if (AtnSort != "1")
                        {
                            List<tbArticleContent> list = _articleContentService.GetAllByAtcIDAndLgeID(Convert.ToInt32(hideID.Value), ddlLge2.SelectedValue).ToList();
                            if (list.Count != 0)
                            {
                                string nowAtnID = AtnID;
                                string nowAtnSort = AtnSort;

                                #region 找出上一個ROWS

                                string tempAtnSort = (Convert.ToInt32(AtnSort) - 1).ToString();

                                var model = list.Where(x => x.AtnSort == Convert.ToInt32(tempAtnSort)).First();
                                var upAtnID = model.AtnID;
                                var upAtnSort = model.AtnSort;

                                #endregion

                                //更新排序狀態(互換)
                                var modelNow = _articleContentService.GetByID(Convert.ToInt32(nowAtnID));
                                modelNow.AtnSort = Convert.ToInt32(nowAtnSort) - 1;
                                _articleContentService.Update(modelNow);

                                var modelUp = _articleContentService.GetByID(Convert.ToInt32(upAtnID));
                                modelUp.AtnSort = Convert.ToInt32(upAtnSort) + 1;
                                _articleContentService.Update(modelUp);

                                gridContent2.DataBind();
                            }
                        }
                        else
                        {
                            ShowMessage("已經是第一筆紀錄");
                        }

                        break;
                    }

                case "SortDown":
                    {
                        //AtcID
                        string AtcID = e.CommandArgument.ToString().Split(',')[0].Trim();
                        //AtnID
                        string AtnID = e.CommandArgument.ToString().Split(',')[1].Trim();
                        //AtnSort
                        string AtnSort = e.CommandArgument.ToString().Split(',')[2].Trim();

                        List<tbArticleContent> list = _articleContentService.GetAllByAtcIDAndLgeID(Convert.ToInt32(hideID.Value), ddlLge2.SelectedValue).ToList();
                        if (AtnSort != list.Count.ToString())
                        {
                            if (list.Count != 0)
                            {
                                string nowAtnID = AtnID;
                                string nowAtnSort = AtnSort;

                                #region 找出下一個ROWS

                                string tempAtnSort = (Convert.ToInt32(AtnSort) + 1).ToString();

                                var model = list.Where(x => x.AtnSort == Convert.ToInt32(tempAtnSort)).First();
                                var downAtnID = model.AtnID;
                                var downAtnSort = model.AtnSort;

                                #endregion

                                //更新排序狀態(互換)
                                var modelNow = _articleContentService.GetByID(Convert.ToInt32(nowAtnID));
                                modelNow.AtnSort = Convert.ToInt32(nowAtnSort) + 1;
                                _articleContentService.Update(modelNow);

                                var modelDown = _articleContentService.GetByID(Convert.ToInt32(downAtnID));
                                modelDown.AtnSort = Convert.ToInt32(downAtnSort) - 1;
                                _articleContentService.Update(modelDown);

                                gridContent2.DataBind();
                            }
                        }
                        else
                        {
                            ShowMessage("已經是最後一筆紀錄");
                        }

                        break;
                    }

                case "Modify":
                    {
                        //AtsID
                        string AtsID = e.CommandArgument.ToString().Split(',')[0].Trim();
                        //AtnID
                        string AtnID = e.CommandArgument.ToString().Split(',')[1].Trim();

                        var model = _articleContentService.GetByID(Convert.ToInt32(AtnID));

                        switch (AtsID)
                        {
                            //副標區塊
                            case "C01":

                                txbAtnSubject3.Text = model.AtnSubject;
                                CKEditorControl3.Text = model.AtnText;
                                //暫存
                                hideAtnID3.Value = AtnID;
                                
                                this.btnOK3.Visible = false;
                                this.btnEdit3.Visible = true;

                                this.MultiView1.ActiveViewIndex = 2;
                                break;
                            //文字區塊
                            case "C02":

                                txbAtnSubject4.Text = model.AtnSubject;
                                CKEditorControl4.Text = model.AtnText;
                                //暫存
                                hideAtnID4.Value = AtnID;
                                
                                this.btnOK4.Visible = false;
                                this.btnEdit4.Visible = true;

                                this.MultiView1.ActiveViewIndex = 3;

                                break;
                            //圖片區塊
                            case "C11":

                                txbAtnSubject5.Text = model.AtnSubject;
                                //暫存
                                hideAtnID5.Value = AtnID;

                                //圖片
                                if (model.AtnPicPath != "")
                                {
                                    imgAtnPicPath5.ImageUrl = string.Format("Uploads/{0}/{1}"
                                                                            , AtnID
                                                                            , model.AtnPicPath);
                                }
                                else
                                {
                                    imgAtnPicPath5.ImageUrl = "Images/default.jpg";
                                }
                                hideAtnPicPath5.Value = model.AtnPicPath;
                                
                                this.btnOK5.Visible = false;
                                this.btnEdit5.Visible = true;

                                this.MultiView1.ActiveViewIndex = 4;

                                break;
                            //串流影片區塊(TouTube)
                            case "C31":

                                txbAtnSubject6.Text = model.AtnSubject;
                                txbAtnVideoPath6.Text = model.AtnVideoPath;
                                //暫存
                                hideAtnID6.Value = AtnID;
                                
                                this.btnOK6.Visible = false;
                                this.btnEdit6.Visible = true;

                                this.MultiView1.ActiveViewIndex = 5;

                                break;
                            //串流影片區塊(Vimeo)
                            case "C32":

                                txbAtnSubject7.Text = model.AtnSubject;
                                txbAtnVideoPath7.Text = model.AtnVideoPath;
                                //暫存
                                hideAtnID7.Value = AtnID;
                                 
                                
                                this.btnOK7.Visible = false;
                                this.btnEdit7.Visible = true;

                                this.MultiView1.ActiveViewIndex = 6;

                                break;
                            //串流影片區塊(優酷)
                            case "C33":

                                txbAtnSubject8.Text = model.AtnSubject;
                                txbAtnVideoPath8.Text = model.AtnVideoPath;
                                //暫存
                                hideAtnID8.Value = AtnID;
                                 
                                this.btnOK8.Visible = false;
                                this.btnEdit8.Visible = true;

                                this.MultiView1.ActiveViewIndex = 7;

                                break;
                            //串流影片區塊(土豆網)
                            case "C34":

                                txbAtnSubject9.Text = model.AtnSubject;
                                txbAtnVideoPath9.Text = model.AtnVideoPath;
                                //暫存
                                hideAtnID9.Value = AtnID;
                                
                                this.btnOK9.Visible = false;
                                this.btnEdit9.Visible = true;

                                this.MultiView1.ActiveViewIndex = 8;

                                break;
                            //MP4影片區塊
                            case "C41":

                                txbAtnSubject10.Text = model.AtnSubject;
                                txbAtnVideoPath10.Text = model.AtnVideoPath;
                                //暫存
                                hideAtnID10.Value = AtnID;
                                
                                this.btnOK10.Visible = false;
                                this.btnEdit10.Visible = true;

                                this.MultiView1.ActiveViewIndex = 9;

                                break;
                        }

                        break;
                    }

                case "Remove":
                    {
                        //AtcID
                        string AtcID = e.CommandArgument.ToString().Split(',')[0].Trim();
                        //AtnID
                        string AtnID = e.CommandArgument.ToString().Split(',')[1].Trim();

                        //刪除
                        _articleContentService.Delete(Convert.ToInt32(AtnID));

                        //刪除後重新排序
                        List<tbArticleContent> list = _articleContentService.GetAllByAtcIDAndLgeID(Convert.ToInt32(hideID.Value), ddlLge2.SelectedValue).ToList();
                        int i = 0;
                        foreach (tbArticleContent element in list)
                        {
                            var model = _articleContentService.GetByID(element.AtnID);
                            model.AtnSort = i + 1;
                            _articleContentService.Update(model);

                            i++;
                        }

                        this.gridContent2.DataBind();
                        break;
                    }

                default:
                    {
                        return;
                    }
            }
        }

        protected void gridContent2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //類型名稱
            Label newlblAtsName = (Label)e.Row.FindControl("lblAtsName");
            if (newlblAtsName != null)
            {
                var model = _articleClassService.GetByID(newlblAtsName.Text);
                newlblAtsName.Text = model.AtsName;
            }

            //縮圖
            Image imgPic = (Image)e.Row.FindControl("imgACPic");
            if (imgPic != null)
            {
                string AtnID = imgPic.AlternateText;

                Literal newltlACText = (Literal)e.Row.FindControl("ltlACText");

                var model = _articleContentService.GetByID(Convert.ToInt32(AtnID));

                if (model != null)
                {
                    string AtsID = model.AtsID;

                    if (AtsID == "C01" || AtsID == "C02")
                    {
                        imgPic.Visible = false;
                        newltlACText.Visible = true;

                        //合法標籤
                        string[] reservedTagPool = { "p" };
                        string Temp = _common.StripTags(model.AtnText, reservedTagPool);
                        newltlACText.Text = _common.GetShortText(Temp, 60);
                    }
                    else if (AtsID == "C11")
                    {
                        imgPic.Visible = true;
                        newltlACText.Visible = false;

                        imgPic.ImageUrl = string.Format("Uploads/{0}/{1}", AtnID, model.AtnPicPath);
                        imgPic.AlternateText = model.AtnPicPath;
                    }
                    else
                    {
                        imgPic.Visible = false;
                        newltlACText.Visible = true;

                        newltlACText.Text = "(影片類型不顯示)";
                    }
                }
            }
        }

        #endregion


        #endregion

        #region 方法

        //載入內容
        protected void SetPageContent(int Page, string AtcSubID)
        {
            //取得類別
            List<tbArticle> listOld;

            if (AtcSubID != "-1")
            {
                listOld = _articleService.GetAll().Where(x => x.AttID == "AT001" && x.AtcSubID == AtcSubID).Where(x => x.AtcName.Contains(txbSearchName.Text) || x.AtcAuthor.Contains(txbSearchName.Text)).OrderByDescending(s => s.AtcOpenDate).ToList();
            }
            else
            {
                listOld = _articleService.GetAll().Where(x => x.AttID == "AT001").Where(x => x.AtcName.Contains(txbSearchName.Text) || x.AtcAuthor.Contains(txbSearchName.Text)).OrderByDescending(s => s.AtcOpenDate).ToList();
            }

            #region 排序設定

            string OrderBy = ddlOrder.SelectedValue;

            if (ddlOrderType.SelectedValue == "DESC")
            {
                switch (OrderBy)
                {
                    case "CreateDate"://發布日期
                        {
                            listOld = listOld.OrderByDescending(o => o.AtcOpenDate).ToList();
                            break;
                        }
                    case "AtcName":
                        {
                            listOld = listOld.OrderByDescending(o => o.AtcName).ToList();
                            break;
                        }
                    case "AtcAuthor":
                        {
                            listOld = listOld.OrderByDescending(o => o.AtcAuthor).ToList();
                            break;
                        }
                }
            }
            else
            {
                switch (OrderBy)
                {
                    case "CreateDate":
                        {
                            listOld = listOld.OrderBy(o => o.CreateDate).ToList();
                            break;
                        }
                    case "AtcName":
                        {
                            listOld = listOld.OrderBy(o => o.AtcName).ToList();
                            break;
                        }
                    case "AtcAuthor":
                        {
                            listOld = listOld.OrderBy(o => o.AtcAuthor).ToList();
                            break;
                        }
                }
            }

            #endregion

            List<tbArticle> listNew = GetPagedTable(listOld, Page, PageSize);

            grid.DataSource = listNew;
            grid.DataBind();

            //取得分頁
            ltlPageMenu.Text = SetPageMenu(listOld, Page, ddlSub.SelectedValue, TargetPage);
        }

        //取得分頁
        protected string SetPageMenu(List<tbArticle> list, int Page, string TypeID, string TargetPage)
        {
            string PageMenu = "";

            #region 判斷上下頁

            string RevPage = "<li class='page-item'><a class='page-link' href='#'> > </a></li>";
            string NextPage = "<li class='page-item'><a class='page-link' href='#'> < </a></li>";

            //只有一頁時
            if (list.Count <= PageSize)
            {
                RevPage = "";
                NextPage = "";
            }
            //還有下一頁時
            else if (list.Count >= (PageSize * Page) && Page == 1)
            {
                #region 取得連結

                string NextUrl = string.Format("{0}?P={1}&Type={2}", TargetPage, Page + 1, TypeID);

                #endregion

                RevPage = "";
                NextPage = string.Format("<li class='page-item'><a class='page-link' href='{0}'> > </a></li>", NextUrl);
            }
            //最後一頁時
            else if ((list.Count % (PageSize * Page)) == list.Count || (list.Count % (PageSize * Page) == 0))
            {
                #region 取得連結

                string RevUrl = string.Format("{0}?P={1}&Type={2}", TargetPage, Page - 1, TypeID);

                #endregion

                RevPage = string.Format("<li class='page-item'><a class='page-link' href='{0}'> < </a></li>", RevUrl);
                NextPage = "";
            }
            else
            {
                #region 取得連結

                string NextUrl = string.Format("{0}?P={1}&Type={2}", TargetPage, Page + 1, TypeID);
                string RevUrl = string.Format("{0}?P={1}&Type={2}", TargetPage, Page - 1, TypeID);

                #endregion

                RevPage = string.Format("<li class='page-item'><a class='page-link' href='{0}'> < </a></li>", RevUrl);
                NextPage = string.Format("<li class='page-item'><a class='page-link' href='{0}'> > </a></li>", NextUrl);
            }

            #endregion

            #region 取得頁數

            string PageItem = "";
            string ItemUrl = "";

            //取得總數量
            int count = (list.Count / PageSize);
            if (list.Count % PageSize != 0)
            {
                count = count + 1;
            }

            if (count > 1)
            {
                for (int e = 0; e < count; e++)
                {
                    ItemUrl = string.Format("{0}?P={1}&Type={2}", TargetPage, e + 1, TypeID);

                    //當前頁數時
                    if (Page == e + 1)
                    {
                        PageItem += string.Format("<li class='page-item active'><a class='page-link' href='{0}'>{1}</a></li>", ItemUrl, e + 1);
                    }
                    else
                    {
                        PageItem += string.Format("<li class='page-item'><a class='page-link' href='{0}'>{1}</a></li>", ItemUrl, e + 1);
                    }
                }
            }

            #endregion

            PageMenu += "<ul class='pagination'>";

            PageMenu += RevPage;
            PageMenu += PageItem;
            PageMenu += NextPage;

            PageMenu += "</ul>";

            return PageMenu;
        }

        //進行分頁方法
        protected List<tbArticle> GetPagedTable(List<tbArticle> list, int PageIndex, int PageSize)
        {
            if (PageIndex == 0)
            {
                return list;
            }
            List<tbArticle> newList = new List<tbArticle>();

            //起始列   
            int rowbegin = (PageIndex - 1) * PageSize;
            //結束列   
            int rowend = PageIndex * PageSize;
            if (rowbegin >= list.Count)
            {
                return newList;
            }

            if (rowend > list.Count)
            {
                rowend = list.Count;
            }
            //產生新的List   
            for (int i = rowbegin; i <= rowend - 1; i++)
            {
                tbArticle item = new tbArticle();
                item = list[i];

                newList.Add(item);
            }
            return newList;
        }

        #endregion






      
    }
}