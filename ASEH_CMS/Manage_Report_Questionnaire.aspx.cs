﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models;
using Service;
using Service.Interface;
using Service.Misc;
using ASEH_CMS.App_Code;
using System.Web.Configuration;
using System.IO;
using System.Text;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using Service.BLL;

namespace ASEH_CMS
{
    public partial class Manage_Report_Questionnaire : PageBase
    {
        private IExceptionRecordService _exceptionRecordService;
        private IReportQuestionnaireService _reportQueService;
        private CommonClass _common = new CommonClass();
        UserInfo User = new UserInfo();
        new protected int iPageType = 1;
        new protected string strPageID = "M53";
        ASEHService _serv = new ASEHService();

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckUser();
            CheckUserPermission(strPageID, iPageType);
        }

        public void Export(byte[] file, string fileName)
        {
            HttpContext context = HttpContext.Current;
            string fileNameOrExtension = fileName;
            string mimeType = "application/unknown";
            string ext = (fileNameOrExtension.Contains(".")) ? System.IO.Path.GetExtension(fileNameOrExtension).ToLower() : "." + fileNameOrExtension;

            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);

            if (regKey != null && regKey.GetValue("Content Type") != null) mimeType = regKey.GetValue("Content Type").ToString();

            context.Response.Clear();
            context.Response.ClearHeaders();
            context.Response.ClearContent();
            context.Response.AppendHeader("Content-Length", file.Length.ToString());
            context.Response.ContentType = mimeType;

            string disposition = "attachment";
            string[] name = fileName.Split('.');

            if (name.Length == 2)
            {
                if (name.GetValue(1).ToString().ToLower() == "pdf")
                {
                    disposition = "inline";
                }
            }

            if (context.Request.Browser.Type.Contains("Firefox"))
            {
                context.Response.AppendHeader("Content-Disposition", string.Format(
                                "{0}; filename*=utf-8\'{1}", disposition, HttpUtility.UrlEncode(fileName)
                            ));
            }
            else
            {
                context.Response.AppendHeader("Content-Disposition", string.Format(
                                "{0}; filename={1}", disposition, HttpUtility.UrlEncode(fileName)
                            ));
            }

            //防止出現「遠端主機已關閉連接。」的 Exception
            if (context.Response.IsClientConnected)
            {
                context.Response.BinaryWrite(file);
                context.Response.Flush();
            }
            else
            {
                context.ApplicationInstance.CompleteRequest();
                context.Response.End();
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            // 建立 WorkBook 及試算表
            HSSFWorkbook workbook = new HSSFWorkbook();
            MemoryStream ms = new MemoryStream();
            ISheet sheet = workbook.CreateSheet("CSR 問卷資料");
            int rowIndex = 0;

            // 建立標題列
            string[] fields = new string[] { "編號","您和日月光的關係", 
                                             "經濟-公司治理", "經濟-商業道德", "經濟-法律遵循", "經濟-利害關係人溝通", "經濟-經營策略與績效", "經濟-客戶關係管理", "經濟-稅務", "經濟-永續供應鏈", "經濟-創新研發管理", "經濟-風險管理",
                                             "環境-氣候變遷", "環境-廢棄物管理", "環境-水資源管理", "環境-能源管理", "環境-綠色解決方案", 
                                             "社會-人才培育", "社會-平等與多元", "社會-人權機制", "社會-勞資溝通", "社會-員工健康與安全", "社會-社區發展", "社會-環保公益", "社會-公共政策與倡議",
                                             "其他意見", "填表日期"};
            IRow rowHeaderField = sheet.CreateRow(rowIndex);
            int colCount = 0;

            foreach (var field in fields)
            {
                if (colCount == 0)
                {
                    sheet.SetColumnWidth(colCount, 20 * 80);
                }
                else
                {
                    sheet.SetColumnWidth(colCount, 20 * 256);
                }
                rowHeaderField.CreateCell(colCount).SetCellValue(field);

                colCount++;
            }

            rowIndex++;

            var list = _serv.GetReportQuestionnaireQuery();

            // 建立資料列
            int i = list.Count();
            foreach (var data in list)
            {
                IRow row = sheet.CreateRow(rowIndex);

                row.CreateCell(0).SetCellValue(i.ToString());
                row.CreateCell(1).SetCellValue(data.stakeholder);
                row.CreateCell(2).SetCellValue(data.q1);
                row.CreateCell(3).SetCellValue(data.q2);
                row.CreateCell(4).SetCellValue(data.q3);
                row.CreateCell(5).SetCellValue(data.q4);
                row.CreateCell(6).SetCellValue(data.q5);
                row.CreateCell(7).SetCellValue(data.q6);
                row.CreateCell(8).SetCellValue(data.q7);
                row.CreateCell(9).SetCellValue(data.q8);
                row.CreateCell(10).SetCellValue(data.q9);
                row.CreateCell(11).SetCellValue(data.q10);
                row.CreateCell(12).SetCellValue(data.q11);
                row.CreateCell(13).SetCellValue(data.q12);
                row.CreateCell(14).SetCellValue(data.q13);
                row.CreateCell(15).SetCellValue(data.q14);
                row.CreateCell(16).SetCellValue(data.q15);
                row.CreateCell(17).SetCellValue(data.q16);
                row.CreateCell(18).SetCellValue(data.q17);
                row.CreateCell(19).SetCellValue(data.q18);
                row.CreateCell(20).SetCellValue(data.q19);
                row.CreateCell(21).SetCellValue(data.q20);
                row.CreateCell(22).SetCellValue(data.q21);
                row.CreateCell(23).SetCellValue(data.q22);
                row.CreateCell(24).SetCellValue(data.q23);
                row.CreateCell(25).SetCellValue(data.additional);
                row.CreateCell(26).SetCellValue((data.update_date.HasValue) ? data.update_date.Value.ToString("yyyy/MM/dd") : "");

                rowIndex++;
                i--;
            }

            MemoryStream stream = new MemoryStream();
            workbook.Write(stream);
            stream.Flush();

            this.Export(stream.ToArray(), string.Format("{0}.xls", "CSR問卷資料"));
        }
    }
}