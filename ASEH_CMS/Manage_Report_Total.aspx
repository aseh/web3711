﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Manage_Report_Total.aspx.cs" Inherits="ASEH_CMS.Manage_Report_Total" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">

    var jQuery132 = jQuery.noConflict(true);

    //建立日期
    jQuery132(function () {
        jQuery132("#ContentPlaceHolder1_txbOpenDate2").datepicker({ dateFormat: "yy/mm/dd" });
        jQuery132("#ContentPlaceHolder1_txbCloseDate2").datepicker({ dateFormat: "yy/mm/dd" });
    });

</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Breadcrumb -->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="MainIndex.aspx">Dashboard</a></li>
        <li class="breadcrumb-item active">下載次數統計(總計)</li>
      </ol>
    <!-- Breadcrumb Menu-->

<div class="container-fluid">
<div class="animated fadeIn">


<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">

        <div class="row">
        <div class="col-lg-12">

            <%--控制項--%>
            <table style="width: 100%;" >
                <tr>
                    <td align="left">
                       <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True" Width="15%" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">

                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlMonth" runat="server" AutoPostBack="True" Width="15%" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged">
                            <asp:ListItem Value="-1">全年份</asp:ListItem>
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                            <asp:ListItem Value="6">6</asp:ListItem>
                            <asp:ListItem Value="7">7</asp:ListItem>
                            <asp:ListItem Value="8">8</asp:ListItem>
                            <asp:ListItem Value="9">9</asp:ListItem>
                            <asp:ListItem Value="10">10</asp:ListItem>
                            <asp:ListItem Value="11">11</asp:ListItem>
                            <asp:ListItem Value="12">12</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="right" style="width: 20%;">
                        <asp:Button ID="btnExport" runat="server" BackColor="Transparent" BorderWidth="0px" CssClass="BigButton" onclick="btnExport_Click" Text="匯出CSV" ValidationGroup="OK" />
                        <asp:Button ID="btnSearch" runat="server" BorderColor="Transparent" BorderWidth="0px" CssClass="SmallButton" Text="查詢" OnClick="btnSearch_Click" Visible="False" />
                    </td>
                </tr>
                <tr>
                    <td align="left">
                       
                    </td>
                    <td align="right" style="width: 20%;">
                        
                    </td>
                </tr>
             </table>

            <div class="card">
            <div class="card-header">
            <i class="fa fa-align-justify"></i> 列表
            </div>

            <div class="card-body">

            <%--列表--%>
            <table style="width: 100%;" >
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" onrowcommand="grid_RowCommand" onrowdatabound="grid_RowDataBound" PageSize="100" Width="100%">
                            <PagerSettings Visible="False" />
                            <Columns>

                                 <asp:TemplateField HeaderText="標題">
                                    <ItemTemplate>
                                        <asp:Label ID="lblName" runat="server" Text='<%# Eval("Key") + "," + Eval("Month") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                               <asp:TemplateField HeaderText="年/月">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDate" runat="server" Text='<%# Eval("Key")  + "," + Eval("Month") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="中文版人數">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDownloadCh" runat="server" Text='<%# Eval("Key")  + "," + Eval("Month") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="英文版人數">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDownloadEn" runat="server" Text='<%# Eval("Key")  + "," + Eval("Month") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:HiddenField ID="hideLoginUseID" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;</td>
                </tr>
            </table>

            </div>
        </div>

        </div>
        </div>

        </asp:View>
        
    </asp:MultiView>

</div>
</div>
<!-- /.conainer-fluid -->

</asp:Content>
