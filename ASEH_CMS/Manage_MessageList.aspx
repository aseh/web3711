﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Manage_MessageList.aspx.cs" Inherits="ASEH_CMS.Manage_MessageList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Breadcrumb -->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="MainIndex.aspx">Dashboard</a></li>
        <li class="breadcrumb-item active">留言內容瀏覽</li>
      </ol>
    <!-- Breadcrumb Menu-->

<div class="container-fluid">
<div class="animated fadeIn">

  <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">

                <%--控制項--%>
                <table style="width: 100%;" >
                   <tr>
                        <td align="left" >
                            <asp:Label ID="Label1" runat="server" SkinID="MainText" Text="排序"></asp:Label>
                            <asp:DropDownList ID="ddlOrder" runat="server" AutoPostBack="True" Width="35%" OnSelectedIndexChanged="ddlOrder_SelectedIndexChanged">
                                <asp:ListItem Value="ComName">姓名</asp:ListItem>
                            </asp:DropDownList>
                             <asp:DropDownList ID="ddlOrderType" runat="server" AutoPostBack="True" Width="15%" OnSelectedIndexChanged="ddlOrderType_SelectedIndexChanged">
                                 <asp:ListItem Value="DESC">遞減</asp:ListItem>
                                 <asp:ListItem Value="ASC">遞增</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="right" >
                            <asp:TextBox ID="txbSearchName" runat="server" MaxLength="50" Width="25%"></asp:TextBox>
                            <asp:Button ID="btnSearch" runat="server" BorderColor="Transparent" BorderWidth="0px" CssClass="SmallButton" Text="查詢" OnClick="btnSearch_Click" />
                        </td>
                    </tr>
                   <tr>
                        <td align="left";">
                            <asp:Button ID="btnAdd" runat="server" BorderColor="Transparent" BorderWidth="0px" CssClass="SmallButton" Text="新增" Visible="false" />
                        </td>
                        <td align="right";">
                            <asp:Label ID="Label34" runat="server" SkinID="MainText" Text="產業類別" Visible="False"></asp:Label>
                            <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True" Width="25%" Visible="False">
                            </asp:DropDownList>
                        </td>
                   </tr>
                 </table>

            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> 列表
                </div>

                <div class="card-body">

                <%--列表--%>
                <table style="width: 100%;" >
                    <tr>
                        <td colspan="2">
                            <asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" DataKeyNames="MsgID" onrowcommand="grid_RowCommand" onrowdatabound="grid_RowDataBound" Width="100%">
                                <PagerSettings Visible="False" />
                                <Columns>

                                  
                                    <asp:TemplateField HeaderText="項次">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNO" runat="server"></asp:Label>
                                        </ItemTemplate><HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" Width="80px" />
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="CreateDate" HeaderText="建立時間" ReadOnly="True" DataFormatString="{0:yyyy/MM/dd HH:mm}"><HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" Width="120px" /></asp:BoundField>

                                    <asp:TemplateField HeaderText="姓名"><ItemTemplate>
                                    <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("MsgUserName") %>'></asp:Label>
                                    </ItemTemplate><ItemStyle HorizontalAlign="Left" Width="15%"/></asp:TemplateField>

                                    <asp:TemplateField HeaderText="留言內容"><ItemTemplate>
                                    <asp:Label ID="lblContent" runat="server" Text='<%# Eval("MsgContent") %>'></asp:Label>
                                    </ItemTemplate><ItemStyle HorizontalAlign="Left" Width="400px" /></asp:TemplateField>

                                    <asp:TemplateField HeaderText="瀏覽">
                                        <ItemTemplate><asp:ImageButton ID="imgbtnView" runat="server" CausesValidation="False" CommandArgument='<%# Eval("MsgID") %>' CommandName="View" ImageUrl="Images/View.gif" ToolTip="瀏覽" /></ItemTemplate><ItemStyle HorizontalAlign="Center" Width="70px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="修改" Visible="false">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("MsgID") %>' CommandName="Modify" ImageUrl="Images/Edit.png" ToolTip="修改" />
                                        </ItemTemplate>
                                        <HeaderStyle />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="刪除" Visible="false">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnDel" runat="server" CausesValidation="False" CommandArgument='<%# Eval("MsgID") %>' CommandName="Remove" ImageUrl="Images/Delete.png" onclientclick="return window.confirm('確定要刪除嗎？');" ToolTip="刪除" />
                                        </ItemTemplate>
                                        <HeaderStyle />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:Literal ID="ltlPageMenu" runat="server"></asp:Literal>
                            <asp:HiddenField ID="hideLoginUseID" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;</td>
                    </tr>
                </table>

                </div>
            </div>



        </asp:View>
        <asp:View ID="View2" runat="server">

        <div class="row">
        <div class="col-lg-12">
        <div class="card">
                <div class="card-header">
                <i class="fa fa-align-justify"></i> 瀏覽留言內容
                </div>

            <div class="card-body">

            <table style="width:100%;">
                <tr>
                        <td align="left" style="width: 30%">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                    </tr>
                <tr>
                    <td align="left" style="width: 30%" >
                        <asp:Label ID="Label2" runat="server" SkinID="MainText" Text="建立時間"></asp:Label>
                    </td>
                    <td align="left" >
                        <asp:Label ID="lblCreateDate2" runat="server" SkinID="MainText"></asp:Label>
                    </td>
                </tr>
                    <tr>
                        <td align="left" style="width: 30%">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                </tr>
                    <tr>
                        <td align="left" style="width: 30%">
                           <asp:Label ID="Label4" runat="server" SkinID="MainText" Text="姓名"></asp:Label>
                        </td>
                        <td align="left">
                          <asp:Label ID="lblMsgUserName2" runat="server" SkinID="MainText"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 30%">
                           <asp:Label ID="Label7" runat="server" SkinID="MainText" Text="聯絡電話"></asp:Label>
                        </td>
                        <td align="left">
                          <asp:Label ID="lblMsgUserTel2" runat="server" SkinID="MainText"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 30%">
                            <asp:Label ID="Label5" runat="server" SkinID="MainText" Text="Email"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblMsgUserEmail2" runat="server" SkinID="MainText"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 30%">
                            <asp:Label ID="Label8" runat="server" SkinID="MainText" Text="國家"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblComCountryID2" runat="server" SkinID="MainText"></asp:Label>
                        </td>
                    </tr>
                     <tr>
                        <td align="left" style="width: 30%">
                            <asp:Label ID="Label9" runat="server" SkinID="MainText" Text="公司名稱"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblMsgUserCom2" runat="server" SkinID="MainText"></asp:Label>
                        </td>
                    </tr>
                     <tr>
                        <td align="left" style="width: 30%">
                            <asp:Label ID="Label11" runat="server" SkinID="MainText" Text="職稱"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblMsgUserTitle2" runat="server" SkinID="MainText"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 30%">
                            <asp:Label ID="Label6" runat="server" SkinID="MainText" Text="留言內容"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblMsgContent2" runat="server" SkinID="MainText"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 30%">
                            <asp:Label ID="Label3" runat="server" Text="描述" Visible="False"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txbSubDesc2" runat="server" MaxLength="300" Width="80%" Visible="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 30%">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Label ID="lblMeg2" runat="server" ForeColor="Red" SkinID="MainWarn"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnBack2" runat="server" BackColor="Transparent" BorderWidth="0px" CssClass="SmallButton" Text="返回" onclick="btnBack_Click" />
                        </td>
                    </tr>
                <tr>
                    <td align="left" style="width: 30%" >
                        &nbsp;</td>
                    <td align="left" >
                        &nbsp;</td>
                </tr>
            </table>

            </div>

        </div>
        </div>
        </div>

        </asp:View>

        </asp:MultiView>

</div>
</div>
    
</asp:Content>
