﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models;
using Service;
using Service.Interface;
using Service.Misc;
using ASEH_CMS.App_Code;

namespace ASEH_CMS
{
    public partial class System_Password : PageBase
    {
        private IExceptionRecordService _exceptionRecordService;
        private IUsersService _usersService;
        private CommonClass _common = new CommonClass();
        UserInfo User = new UserInfo();
        Security _security = new Security();

        new protected int iPageType = 1;
        new protected string strPageID = "S02";

        protected void Page_Load(object sender, EventArgs e)
        {
            this._exceptionRecordService = new ExceptionRecordService();
            this._usersService = new UsersService();

            CheckUser();
            CheckUserPermission(strPageID, iPageType);

            //載入帳號
            User = (UserInfo)Session["UserInfo"];
            lblUseID1.Text = User.UserID;
        }

        //儲存修改
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var model = _usersService.GetByID(lblUseID1.Text);

                string strOrgPW = _security.Decrypt(model.UsePassword);
                if (txbOrgPW.Text != strOrgPW)
                {
                    ShowMessage("舊密碼輸入不正確");
                    return;
                }

                if (txbPW.Text != txbPW2.Text)
                {
                    ShowMessage("兩組新密碼請輸入相同名稱");
                    return;
                }

                model.UseID = lblUseID1.Text;
                model.UsePassword = _security.Encrypt(txbPW.Text);
                _usersService.Update(model);

                //ShowMessage("儲存成功");
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

    }
}