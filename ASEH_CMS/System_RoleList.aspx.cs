﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models;
using Service;
using Service.Interface;
using Service.Misc;
using ASEH_CMS.App_Code;

namespace ASEH_CMS
{
    public partial class System_RoleList : PageBase
    {
        private IExceptionRecordService _exceptionRecordService;
        private IRolesService _rolesService;
        private IPermissionService _permissionService;
        private IPermRoleService _permRoleService;
        private CommonClass _common = new CommonClass();

        new protected int iPageType = 1;
        new protected string strPageID = "S04";

        protected void Page_Load(object sender, EventArgs e)
        {
            this._exceptionRecordService = new ExceptionRecordService();
            this._rolesService = new RolesService();
            this._permissionService = new PermissionService();
            this._permRoleService = new PermRoleService();
            
            CheckUser();
            CheckUserPermission(strPageID, iPageType);

        }

        //新增按鈕的動作
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            this.txbRolID.Enabled = true;
            this.txbRolID.Text = "";
            this.txbRolName.Text = "";
            this.ckbIsEnable.Checked = true;

            //權限清單
            IEnumerable<tbPermission> listPermission = _permissionService.GetAll();
            ListItem item = null;
            ckblPermission.Items.Clear();
            foreach (tbPermission element in listPermission)
            {
                item = new ListItem(element.PerName, element.PerID);
                ckblPermission.Items.Add(item);
            }

            this.btnOK.Visible = true;
            this.btnEdit.Visible = false;
            this.MultiView1.ActiveViewIndex = 1;
        }

        //返回按鈕
        protected void btnBack_Click(object sender, EventArgs e)
        {
            this.MultiView1.ActiveViewIndex = 0;
        }

        //新增存檔
        protected void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new tbRoles();
                model.RolID = txbRolID.Text;
                model.RolName = txbRolName.Text;
                model.RolEnable = _common.GetChecked(ckbIsEnable);

                _rolesService.Create(model);

                //更新帳號權限
                UpdatePermRole(ckblPermission, txbRolID.Text);

                //ShowMessage("儲存成功");
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }

            this.MultiView1.ActiveViewIndex = 0;
            this.grid.DataBind();
        }

        //修改存檔
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new tbRoles();
                model.RolID = txbRolID.Text;
                model.RolName = txbRolName.Text;
                model.RolEnable = _common.GetChecked(ckbIsEnable);

                _rolesService.Update(model);

                //更新帳號權限
                UpdatePermRole(ckblPermission, txbRolID.Text);

                //ShowMessage("儲存成功");
            }
            catch (Exception ex)
            {
                ShowMessage("儲存失敗");
            }

            this.MultiView1.ActiveViewIndex = 0;
            this.grid.DataBind();
        }

        #region GridView動作

        protected void grid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Modify":
                    {
                        string RolID = e.CommandArgument.ToString();

                        var model = _rolesService.GetByID(RolID);

                        this.txbRolID.Text = model.RolID;
                        this.txbRolID.Enabled = false;//禁止修改角色編號
                        this.hideRolID.Value = model.RolID;
                        this.txbRolName.Text = model.RolName;
                        _common.SetChecked(ckbIsEnable, Convert.ToInt32(model.RolEnable));

                        //權限清單
                        IEnumerable<tbPermission> listPermission = _permissionService.GetAll();
                        ListItem item = null;
                        ckblPermission.Items.Clear();
                        foreach (tbPermission element in listPermission)
                        {
                            item = new ListItem(element.PerName, element.PerID);
                            ckblPermission.Items.Add(item);
                        }

                        //設定權限
                        SetPermRole(ckblPermission, RolID);

                        this.btnOK.Visible = false;
                        this.btnEdit.Visible = true;
                        this.MultiView1.ActiveViewIndex = 1;
                        break;
                    }
                case "Cancel":
                    {
                        string RolID = e.CommandArgument.ToString();

                        _rolesService.Delete(RolID);
                        this.grid.DataBind();
                        break;
                    }
                default:
                    {
                        return;
                    }
            }
        }

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Label newLabel = (Label)e.Row.FindControl("lblIsEnable");
            if (newLabel != null)
            {
                if (int.Parse(newLabel.Text) == 1)
                {
                    Label lblEnable = _common.ChangeIsEnable(1);
                    newLabel.Text = lblEnable.Text;
                }
                else if (int.Parse(newLabel.Text) == 0)
                {
                    Label lblEnable = _common.ChangeIsEnable(0);
                    newLabel.Text = lblEnable.Text;
                    newLabel.ForeColor = lblEnable.ForeColor;
                }
            }
        }

        #endregion

        #region 方法

        //更新帳號權限
        protected void UpdatePermRole(CheckBoxList ckbl, string RolID)
        {
            string PerId = "";

            for (int i = 0; i < ckbl.Items.Count; i++)
            {
                PerId = ckbl.Items[i].Value;

                if (ckbl.Items[i].Selected == true)
                {
                    var model = new tbPermRole();
                    model.RolID = RolID;
                    model.PerID = PerId;
                    model.RUPermission = 15;
                    _permRoleService.Create(model);
                }
                else
                {
                    var model = _permRoleService.GetByPerIDAndRolID(PerId, RolID);
                    if (model != null)
                    {
                        _permRoleService.Delete(model.ID);
                    }
                }
            }
        }

        //設定帳號權限
        protected void SetPermRole(CheckBoxList ckbl, string RolID)
        {
            IEnumerable<tbPermRole> listPermRole = _permRoleService.GetAllByRolID(RolID);
            foreach(tbPermRole element in listPermRole)
            {
                ckbl.Items.FindByValue(element.PerID).Selected = true;
            }
        }

        #endregion

    }
}