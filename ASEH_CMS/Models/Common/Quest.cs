﻿using System.Collections.Generic;

namespace ASEH_CMS.Models.Common.Quest
{
  public class Text
  {
    public string Ch { get; set; }
    public string En { get; set; }
  }
  public class Topic
  {
    public string Key { get; set; }
    public Text Title { get; set; }
    public Text Content { get; set; }
  }

  public class Group
  {
    public Text Title { get; set; }

    public Text Head1 { get; set; }
    public Text Head2 { get; set; }
    public Text Head3 { get; set; }
    public List<Topic> TopicList { get; set; }
  }

  public class Option
  {
    public string Value { get; set; }
    public Text Title { get; set; }
  }

  public class Common
  {
    public Text Title { get; set; }
    public Text Description { get; set; }
    public Text Stakeholder { get; set; }
    public Text Organization { get; set; }
    public Text TableHead1 { get; set; }
    public Text TableHead2 { get; set; }
    public Text TableHead3 { get; set; }
    public Text Suggest { get; set; }

    public Text Valid { get; set; }

    public Text ReGenValid { get; set; }
    public Text Submit { get; set; }
    public Text Reset { get; set; }

  }
  //共用Quest物件
  public class QuestBody
  {
    public Common Common { get; set; }
    public List<Option> StakeholderList { get; set; }
    public List<Option> OptionList { get; set; }
    public List<Group> GroupList { get; set; }

    public string Lang { get; set; }


    public QuestBody()
    {

    }

    public string GetText(Text Field)
    {
      if (Field == null)
      {
        return "NULL";
      }
      if (this.Lang == "Ch")
      {
        return Field.Ch;
      }
      else
      {
        return Field.En;
      }
    }
  }
}