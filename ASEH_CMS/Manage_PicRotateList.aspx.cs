﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models;
using Service;
using Service.Interface;
using Service.Misc;
using ASEH_CMS.App_Code;
using System.Web.Configuration;
using System.IO;
using System.Web.UI.HtmlControls;


class MediaObj
{
    public FileUpload File { get; set; }
    public Image Image { get; set; }
    public HtmlVideo Video { get; set; }
    public HiddenField FileName { get; set; }
}


namespace ASEH_CMS
{
    public partial class Manage_PicRotateList : PageBase
    {
        private IExceptionRecordService _exceptionRecordService;
        private IPicRotateService _picRotateService;
        private CommonClass _common = new CommonClass();
        UserInfo User = new UserInfo();
        new protected int iPageType = 1;
        new protected string strPageID = "M21";

        Dictionary<string, MediaObj> MediaMap;

        //起始載入
        protected void Page_Load(object sender, EventArgs e)
        {
            this._exceptionRecordService = new ExceptionRecordService();
            this._picRotateService = new PicRotateService();

            CheckUser();
            CheckUserPermission(strPageID, iPageType);

            if (!this.Page.IsPostBack)
            {
                //取得登入者ID
                User = (UserInfo)Session["UserInfo"];
                hideLoginUseID.Value = User.UserID;
            }

            MediaMap = new Dictionary<string, MediaObj>()
            {
                { 
                    "ProPicPath", 
                    new MediaObj() { 
                        File = uploadPicPath,
                        Video = imgProPicPathVideo1,
                        Image = imgProPicPath,
                        FileName = hideFileName
                    } 
                }, {
                    "ProPicPath_M",
                    new MediaObj() {
                        File = uploadPicPath2,
                        Video = imgProPicPathVideo2,
                        Image = imgProPicPath2,
                        FileName = hideFileName2
                    }
                }, {
                    "ProPicPath_EN",
                    new MediaObj() {
                        File = uploadPicPath3,
                        Video = imgProPicPathVideo3,
                        Image = imgProPicPath3,
                        FileName = hideFileName3
                    }
                }, {
                    "ProPicPath_M_EN",
                    new MediaObj() {
                        File = uploadPicPath4,
                        Video = imgProPicPathVideo4,
                        Image = imgProPicPath4,
                        FileName = hideFileName4
                    }
                },
            };
        }

        //返回的動作
        protected void btnBack_Click(object sender, EventArgs e)
        {
            this.MultiView1.ActiveViewIndex = 0;
        }

        //新增按鈕的動作
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //上架日期
            txbOpenDate2.Text = DateTime.Now.ToString("yyyy/MM/dd");
            ddlOpenDateH2.ClearSelection();
            ddlOpenDateH2.Items[0].Selected = true;
            ddlOpenDateM2.ClearSelection();
            ddlOpenDateM2.Items[0].Selected = true;

            //下架日期
            txbCloseDate2.Text = DateTime.Now.AddMonths(1).ToString("yyyy/MM/dd");
            ddlCloseDateH2.ClearSelection();
            ddlCloseDateH2.Items[0].Selected = true;
            ddlCloseDateM2.ClearSelection();
            ddlCloseDateM2.Items[0].Selected = true;

            txbProName2.Text = "";
            txbProName_EN2.Text = "";
            txbProDesc2.Text = "";
            txbProDesc_EN2.Text = "";
            txbProUrl2.Text = "";
            txbProUrl_EN2.Text = "";
            ckbIsEnable.Checked = true;

            #region 圖片

            //imgProPicPath.ImageUrl = "Images/default.jpg";
            //imgProPicPath2.ImageUrl = "Images/default.jpg";
            //imgProPicPath3.ImageUrl = "Images/default.jpg";
            //imgProPicPath4.ImageUrl = "Images/default.jpg";
            imgProPicPath.Visible = false;
            imgProPicPath2.Visible = false;
            imgProPicPath3.Visible = false;
            imgProPicPath4.Visible = false;
            imgProPicPathVideo1.Visible = false;
            imgProPicPathVideo2.Visible = false;
            imgProPicPathVideo3.Visible = false;
            imgProPicPathVideo4.Visible = false;

            #endregion

            //圖片描述
            txbProPicDesc2.Text = "";

            this.btnOK.Visible = true;
            this.btnEdit.Visible = false;
            this.MultiView1.ActiveViewIndex = 1;
        }

        private bool processImages(tbPicRotate model)
        {
            string ProID = model.ProID.ToString();
            bool PicCheck = true;
            string[] allowExtensions = { ".bmp", ".jpg", ".jpeg", ".png", ".gif", ".mp4" };
            // check file extension
            foreach ( var Media in MediaMap )
            {
                var FileControl = Media.Value.File;
                if ( FileControl.HasFile )
                {
                    string fileExtension = System.IO.Path.GetExtension(FileControl.FileName).ToLower();
                    
                    if ( !allowExtensions.Contains(fileExtension) )
                    {
                        return false;
                    }
                }
            }


            foreach (var Media in MediaMap)
            {
                var FileControl = Media.Value.File;
                string newPicPathFileName = "";
                if (FileControl.HasFile)
                {
                    //PNG特別處理
                    string FileNameOrg = FileControl.FileName;
                    string Name = Path.GetFileNameWithoutExtension(FileNameOrg);
                    string Extension = Path.GetExtension(FileNameOrg);

                    if (Extension != ".jpg")
                    {
                        string FileName = Guid.NewGuid().ToString();
                        newPicPathFileName = Path.ChangeExtension(FileName, Extension);
                        string FolderPath = string.Format("Uploads/{0}/", ProID);
                        string RealFolderPath = Server.MapPath(FolderPath);
                        int returnCode = _common.UploadAnyFileWithNewName(FileControl, RealFolderPath, FileName);
                    }
                    else
                    {
                        string FileName = Guid.NewGuid().ToString();
                        newPicPathFileName = Path.ChangeExtension(FileName, Extension);
                        string FolderPath = string.Format("Uploads/{0}/", ProID);
                        string RealFolderPath = Server.MapPath(FolderPath);

                        int returnCode = _common.UploadOrgJpgPicWithNewName(FileControl, RealFolderPath, FileName);
                    }

                    switch (Media.Key)
                    {
                        case "ProPicPath":
                            model.ProPicPath = newPicPathFileName;
                            break;
                        case "ProPicPath_M":
                            model.ProPicPath_M = newPicPathFileName;
                            break;
                        case "ProPicPath_EN":
                            model.ProPicPath_EN = newPicPathFileName;
                            break;
                        case "ProPicPath_M_EN":
                            model.ProPicPath_M_EN = newPicPathFileName;
                            break;
                    }
                }
            }

            return PicCheck;
        }

        //新增儲存動作
        protected void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new tbPicRotate();
                model.ProName = txbProName2.Text;
                model.ProName_EN = txbProName_EN2.Text;
                model.ProDesc = txbProDesc2.Text;
                model.ProDesc_EN = txbProDesc_EN2.Text;
                model.ProUrl = txbProUrl2.Text;
                model.ProUrl_EN = txbProUrl_EN2.Text;

                model.ProEnable = _common.GetChecked(ckbIsEnable);

                model.ProSetOD = 1;
                model.ProStartDate = Convert.ToDateTime(string.Format("{0} {1}:{2}:00", txbOpenDate2.Text, ddlOpenDateH2.SelectedValue, ddlOpenDateM2.SelectedValue));
                model.ProEndDate = Convert.ToDateTime(string.Format("{0} {1}:{2}:00", txbCloseDate2.Text, ddlCloseDateH2.SelectedValue, ddlCloseDateM2.SelectedValue));

                model.CreateUser = hideLoginUseID.Value;
                model.CreateDate = DateTime.Now;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;
                model.ProShowText = _common.GetChecked(ckbIsShowText);
                model.ProShowMoreBtn = _common.GetChecked(ckbIsShowMoreBtn);
                model.ProShowBgBorder = _common.GetChecked(ckbIsShowBgBorder);

                #region 取得排序碼

                List<tbPicRotate> list = _picRotateService.GetAllBySort().ToList();
                if (list.Count != 0)
                {
                    int SortTemp = Convert.ToInt32(list[list.Count - 1].ProSort) + 1;

                    model.ProSort = SortTemp;
                }
                else
                {
                    model.ProSort = 1;
                }

                #endregion

                _picRotateService.Create(model);

                #region 圖片

                string ProID = model.ProID.ToString();

                //上傳圖片

                var PicCheck = this.processImages(model);
                if (!PicCheck)
                {
                    ShowMessage("圖片上傳失敗，請確認檔案格式是否正確");
                }

                #endregion

                //圖片描述
                model.ProPicDesc = txbProPicDesc2.Text;

                _picRotateService.Update(model);

                //ShowMessage("儲存成功");

                //更新列表
                grid.DataBind();

                this.MultiView1.ActiveViewIndex = 0;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        //確定修改動作
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            List<tbPicRotate> list = _picRotateService.GetAll().Where(x => x.ProEnable == 1 && x.ProID != Convert.ToInt32(hideID.Value)).ToList();
            try
            {
                var model = _picRotateService.GetByID(Convert.ToInt32(hideID.Value));
                model.ProName = txbProName2.Text;
                model.ProName_EN = txbProName_EN2.Text;
                model.ProDesc = txbProDesc2.Text;
                model.ProDesc_EN = txbProDesc_EN2.Text;
                model.ProUrl = txbProUrl2.Text;
                model.ProUrl_EN = txbProUrl_EN2.Text;
                model.ProEnable = _common.GetChecked(ckbIsEnable);
                model.ProShowText = _common.GetChecked(ckbIsShowText);
                model.ProShowMoreBtn = _common.GetChecked(ckbIsShowMoreBtn);
                model.ProShowBgBorder = _common.GetChecked(ckbIsShowBgBorder);

                model.ProSetOD = 1;
                model.ProStartDate = Convert.ToDateTime(string.Format("{0} {1}:{2}:00", txbOpenDate2.Text, ddlOpenDateH2.SelectedValue, ddlOpenDateM2.SelectedValue));
                model.ProEndDate = Convert.ToDateTime(string.Format("{0} {1}:{2}:00", txbCloseDate2.Text, ddlCloseDateH2.SelectedValue, ddlCloseDateM2.SelectedValue));

                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;

                #region 圖片

                string ProID = model.ProID.ToString();

                var PicCheck = this.processImages(model);
                if (!PicCheck)
                {
                    ShowMessage("圖片上傳失敗，請確認檔案格式是否正確");
                }

                #endregion

                _picRotateService.Update(model);

                //ShowMessage("儲存成功");

                //更新列表
                grid.DataBind();

                this.MultiView1.ActiveViewIndex = 0;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        #region GridView動作

        protected void grid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "SortUp":
                    {
                        //ProID
                        string ProID = e.CommandArgument.ToString().Split(',')[0].Trim();
                        //ProSort
                        string ProSort = e.CommandArgument.ToString().Split(',')[1].Trim();

                        if (ProSort != "1")
                        {
                            List<tbPicRotate> list = _picRotateService.GetAllBySort().ToList();
                            if (list.Count != 0)
                            {
                                string nowProID = ProID;
                                string nowProSort = ProSort;

                                #region 找出上一個ROWS

                                string tempProSort = (Convert.ToInt32(ProSort) - 1).ToString();

                                var model = list.Where(x => x.ProSort == Convert.ToInt32(tempProSort)).First();
                                var upProID = model.ProID;
                                var upProSort = model.ProSort;

                                #endregion

                                //更新排序狀態(互換)
                                var modelNow = _picRotateService.GetByID(Convert.ToInt32(nowProID));
                                modelNow.ProSort = Convert.ToInt32(nowProSort) - 1;
                                _picRotateService.Update(modelNow);

                                var modelUp = _picRotateService.GetByID(Convert.ToInt32(upProID));
                                modelUp.ProSort = Convert.ToInt32(upProSort) + 1;
                                _picRotateService.Update(modelUp);

                                grid.DataBind();
                            }
                        }
                        else
                        {
                            ShowMessage("已經是第一筆紀錄");
                        }

                        break;
                    }

                case "SortDown":
                    {
                        //ProID
                        string ProID = e.CommandArgument.ToString().Split(',')[0].Trim();
                        //ProSort
                        string ProSort = e.CommandArgument.ToString().Split(',')[1].Trim();

                        List<tbPicRotate> list = _picRotateService.GetAllBySort().ToList();
                        if (ProSort != list.Count.ToString())
                        {
                            if (list.Count != 0)
                            {
                                string nowProID = ProID;
                                string nowProSort = ProSort;

                                #region 找出下一個ROWS

                                string tempProSort = (Convert.ToInt32(ProSort) + 1).ToString();

                                var model = list.Where(x => x.ProSort == Convert.ToInt32(tempProSort)).First();
                                var downProID = model.ProID;
                                var downProSort = model.ProSort;

                                #endregion

                                //更新排序狀態(互換)
                                var modelNow = _picRotateService.GetByID(Convert.ToInt32(nowProID));
                                modelNow.ProSort = Convert.ToInt32(nowProSort) + 1;
                                _picRotateService.Update(modelNow);

                                var modelDown = _picRotateService.GetByID(Convert.ToInt32(downProID));
                                modelDown.ProSort = Convert.ToInt32(downProSort) - 1;
                                _picRotateService.Update(modelDown);

                                grid.DataBind();
                            }
                        }
                        else
                        {
                            ShowMessage("已經是最後一筆紀錄");
                        }

                        break;
                    }

                case "Modify":
                    {
                        //ID
                        string ProID = e.CommandArgument.ToString();
                        //暫存
                        hideID.Value = ProID;

                        var model = _picRotateService.GetByID(Convert.ToInt32(ProID));


                        //上架日期
                        txbOpenDate2.Text = Convert.ToDateTime(model.ProStartDate).ToString("yyyy/MM/dd");
                        ddlOpenDateH2.ClearSelection();
                        ddlOpenDateH2.Items.FindByValue(Convert.ToDateTime(model.ProStartDate).ToString("HH")).Selected = true;
                        ddlOpenDateM2.ClearSelection();
                        ddlOpenDateM2.Items.FindByValue(Convert.ToDateTime(model.ProStartDate).ToString("mm")).Selected = true;

                        //下架日期
                        txbCloseDate2.Text = Convert.ToDateTime(model.ProEndDate).ToString("yyyy/MM/dd");
                        ddlCloseDateH2.ClearSelection();
                        ddlCloseDateH2.Items.FindByValue(Convert.ToDateTime(model.ProEndDate).ToString("HH")).Selected = true;
                        ddlCloseDateM2.ClearSelection();
                        ddlCloseDateM2.Items.FindByValue(Convert.ToDateTime(model.ProEndDate).ToString("mm")).Selected = true;

                        txbProName2.Text = model.ProName;
                        txbProName_EN2.Text = model.ProName_EN;
                        txbProDesc2.Text = model.ProDesc;
                        txbProDesc_EN2.Text = model.ProDesc_EN;
                        txbProUrl2.Text = model.ProUrl;
                        txbProUrl_EN2.Text = model.ProUrl_EN;
                        _common.SetChecked(ckbIsEnable, Convert.ToInt32(model.ProEnable));
                        _common.SetChecked(ckbIsShowText, Convert.ToInt32(model.ProShowText));
                        _common.SetChecked(ckbIsShowMoreBtn, Convert.ToInt32(model.ProShowMoreBtn));
                        _common.SetChecked(ckbIsShowBgBorder, Convert.ToInt32(model.ProShowBgBorder));

                        #region 圖片

                        var PathMap = new Dictionary<string, string>
                        {
                            { "ProPicPath", model.ProPicPath },
                            { "ProPicPath_M", model.ProPicPath_M },
                            { "ProPicPath_EN", model.ProPicPath_EN },
                            { "ProPicPath_M_EN", model.ProPicPath_M_EN },
                        };

                        foreach ( var Media in MediaMap )
                        {
                            var ImagePath = PathMap[Media.Key];
                            if ( ImagePath != null )
                            {
                                string imgUrl = string.Format("Uploads/{0}/{1}"
                                                                  , model.ProID.ToString()
                                                                  , ImagePath);
                                if ( ImagePath.Contains(".mp4") ) {
                                    Media.Value.Video.Src = imgUrl;
                                    Media.Value.Video.Visible = true;
                                    Media.Value.Image.Visible = false;
                                } else {
                                    Media.Value.Image.ImageUrl = imgUrl;
                                    Media.Value.Image.Visible = true;
                                    Media.Value.Video.Visible = false;
                                }
                                Media.Value.FileName.Value = ImagePath;
                            }
                        }
                        


                        ////圖片
                        //if (model.ProPicPath != null)
                        //{
                        //    string imgUrl = string.Format("Uploads/{0}/{1}"
                        //                                          , model.ProID.ToString()
                        //                                          , model.ProPicPath);
                        //    if ( imgUrl.Contains("mp4"))
                        //    {
                        //        imgProPicPathVideo1.Src = imgUrl;
                        //        imgProPicPathVideo1.Visible = true;
                        //        imgProPicPath.Visible = false;
                        //    } else
                        //    {
                        //        imgProPicPath.ImageUrl = imgUrl;
                        //        imgProPicPathVideo1.Visible = false;
                        //        imgProPicPath.Visible = true;

                        //    }
                            
                        //}
                        //else
                        //{
                        //    imgProPicPath.ImageUrl = "Images/default.jpg";
                        //}
                        //hideFileName.Value = model.ProPicPath;

                        ////圖片2
                        //if (model.ProPicPath_M != null)
                        //{
                        //    string imgUrl = string.Format("Uploads/{0}/{1}"
                        //                                         , model.ProID.ToString()
                        //                                         , model.ProPicPath_M);
                        //    if (imgUrl.Contains("mp4"))
                        //    {
                        //        imgProPicPathVideo2.Src = imgUrl;
                        //        imgProPicPathVideo2.Visible = true;
                        //        imgProPicPath2.Visible = false;
                        //    }
                        //    else
                        //    {
                        //        imgProPicPath2.ImageUrl = imgUrl;
                        //        imgProPicPathVideo2.Visible = false;
                        //        imgProPicPath2.Visible = true;

                        //    }
                        //}
                        //else
                        //{
                        //    imgProPicPath2.ImageUrl = "Images/default.jpg";
                        //}
                        //hideFileName2.Value = model.ProPicPath_M;

                        #endregion

                        //圖片描述
                        txbProPicDesc2.Text = model.ProPicDesc;

                        lblMeg.Text = "";

                        this.btnOK.Visible = false;
                        this.btnEdit.Visible = true;
                        this.MultiView1.ActiveViewIndex = 1;

                        break;
                    }

                case "Remove":
                    {
                        //ProID
                        string ProID = e.CommandArgument.ToString();
                 
                        //刪除
                        _picRotateService.Delete(Convert.ToInt32(ProID));

                        //刪除後重新排序
                        List<tbPicRotate> list = _picRotateService.GetAllBySort().ToList();
                        int i = 0;
                        foreach (tbPicRotate element in list)
                        {
                            var model = _picRotateService.GetByID(element.ProID);
                            model.ProSort = i + 1;
                            _picRotateService.Update(model);

                            i++;
                        }

                        this.grid.DataBind();
                        break;
                    }

                default:
                    {
                        return;
                    }
            }
        }

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Image imgPic = (Image)e.Row.FindControl("imgPic");
            HtmlVideo video = (HtmlVideo)e.Row.FindControl("videoImg");

            if (imgPic != null && video != null)
            {
                string Temp = imgPic.AlternateText;

                //ProID
                string ProID = Temp.Split(',')[0].Trim();
                //ProPicPath
                string ProPicPath = Temp.Split(',')[1].Trim();

                if (ProPicPath.Contains(".mp4"))
                {
                    imgPic.Visible = false;
                    video.Visible = true;
                    video.Src = string.Format("Uploads/{0}/{1}"
                                                        , ProID
                                                        , ProPicPath); ;
                } else
                {
                    imgPic.Visible = true;
                    video.Visible = false;
                    if (ProPicPath != "")
                    {
                        imgPic.ImageUrl = string.Format("Uploads/{0}/{1}"
                                                        , ProID
                                                        , ProPicPath);
                    }
                    else
                    {
                        imgPic.ImageUrl = "Images/default.jpg";
                    }
                }
               
            }

            //是否啟用
            Label newlblIsEnable = (Label)e.Row.FindControl("lblIsEnable");
            if (newlblIsEnable != null)
            {
                if (int.Parse(newlblIsEnable.Text) == 1)
                {
                    Label lblEnable = _common.ChangeIsEnable(1);
                    newlblIsEnable.Text = lblEnable.Text;
                }
                else if (int.Parse(newlblIsEnable.Text) == 0)
                {
                    Label lblEnable = _common.ChangeIsEnable(0);
                    newlblIsEnable.Text = lblEnable.Text;
                    newlblIsEnable.ForeColor = lblEnable.ForeColor;
                }
            }

            Label newLblIsShowText = (Label)e.Row.FindControl("lblIsShowText");
            if ( newLblIsShowText != null )
            {
                Label transLabel = _common.BooleanToLabel(int.Parse(newLblIsShowText.Text));
                newLblIsShowText.Text = transLabel.Text;
                newLblIsShowText.ForeColor = transLabel.ForeColor;
            }
            Label newLblIsShowMoreBtn = (Label)e.Row.FindControl("lblIsShowMoreBtn");
            if (newLblIsShowMoreBtn != null)
            {
                Label transLabel = _common.BooleanToLabel(int.Parse(newLblIsShowMoreBtn.Text));
                newLblIsShowMoreBtn.Text = transLabel.Text;
                newLblIsShowMoreBtn.ForeColor = transLabel.ForeColor;
            }
            Label newLblIsShowBgBorder = (Label)e.Row.FindControl("lblIsShowBgBorder");
            if (newLblIsShowBgBorder != null)
            {
                Label transLabel = _common.BooleanToLabel(int.Parse(newLblIsShowBgBorder.Text));
                newLblIsShowBgBorder.Text = transLabel.Text;
                newLblIsShowBgBorder.ForeColor = transLabel.ForeColor;
            }
        }

        #endregion

     

       

        
    }
}