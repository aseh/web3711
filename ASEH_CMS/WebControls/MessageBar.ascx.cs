﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ASEH_CMS.WebControls
{
    public partial class MessageBar : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string Text
        {
            get { return this.lblMessage.Text; }
            set { this.lblMessage.Text = value; }
        }
    }
}