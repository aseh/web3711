﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Manage_SignatureList.aspx.cs" Inherits="ASEH_CMS.Manage_SignatureList" %>

<%@ Register assembly="CKEditor.NET" namespace="CKEditor.NET" tagprefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">

    var jQuery132 = jQuery.noConflict(true);

    //建立日期
    jQuery132(function () {
        jQuery132("#ContentPlaceHolder1_txbOpenDate2").datepicker({ dateFormat: "yy/mm/dd" });
        jQuery132("#ContentPlaceHolder1_txbCloseDate2").datepicker({ dateFormat: "yy/mm/dd" });
    });

</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Breadcrumb -->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="MainIndex.aspx">Dashboard</a></li>
        <li class="breadcrumb-item active">Press room簽名檔管理</li>
      </ol>
    <!-- Breadcrumb Menu-->

<div class="container-fluid">
<div class="animated fadeIn">


<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">

        <div class="row">
        <div class="col-lg-12">

            <%--控制項--%>
            <table style="width: 100%;" >
            <tr>
                <td align="left" >
                        <asp:Label ID="Label1" runat="server" SkinID="MainText" Text="排序" Visible="false"></asp:Label>
                        <asp:DropDownList ID="ddlOrder" runat="server" AutoPostBack="True" Width="35%" OnSelectedIndexChanged="ddlOrder_SelectedIndexChanged" Visible="false">
                            <asp:ListItem Value="CreateDate">發佈日期</asp:ListItem>
                            <asp:ListItem Value="AtcName">標題</asp:ListItem>
<%--                            <asp:ListItem Value="AtcAuthor">作者</asp:ListItem>--%>
                        </asp:DropDownList>
                            <asp:DropDownList ID="ddlOrderType" runat="server" AutoPostBack="True" Width="15%" OnSelectedIndexChanged="ddlOrderType_SelectedIndexChanged" Visible="false"> 
                                <asp:ListItem Value="DESC">遞減</asp:ListItem>
                                <asp:ListItem Value="ASC">遞增</asp:ListItem>
                        </asp:DropDownList>
                </td>
                <td align="right" >
                    <asp:TextBox ID="txbSearchName" runat="server" MaxLength="50" Width="25%" Visible="False"></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" BorderColor="Transparent" BorderWidth="0px" CssClass="SmallButton" Text="查詢" OnClick="btnSearch_Click" Visible="False" />
                </td>
            </tr>

            <tr>
                <td align="left" >
                    <asp:Button ID="btnAdd" runat="server" BorderColor="Transparent" BorderWidth="0px" CssClass="SmallButton" Text="新增" OnClick="btnAdd_Click" Visible="false" />
                </td>
                <td align="right" >
                    <asp:Label ID="Label34" runat="server" SkinID="MainText" Text="語系：" Visible="false"></asp:Label>
                    <asp:DropDownList ID="ddlLge" runat="server" style="margin-bottom: 0px" Width="10%" AutoPostBack="True" OnSelectedIndexChanged="ddlLge_SelectedIndexChanged" Visible="false">
                    </asp:DropDownList>
                    <asp:Label ID="Label16" runat="server" SkinID="MainText" Text="類別" Visible="false"></asp:Label>
                    <asp:DropDownList ID="ddlSub" runat="server" AutoPostBack="True" Width="27%" OnSelectedIndexChanged="ddlSub_SelectedIndexChanged" Visible="false" >
                    </asp:DropDownList>
                </td>
            </tr>
            </table>

        <div class="card">
            <div class="card-header">
            <i class="fa fa-align-justify"></i> 列表
            </div>

            <div class="card-body">

            <%--列表--%>
            <table style="width: 100%;" >
            <tr>
                <td colspan="2">
                    <asp:GridView ID="grid" runat="server" class="table table-responsive-sm table-bordered table-striped table-sm"
                        AutoGenerateColumns="False" Width="100%" 
                        onrowcommand="grid_RowCommand" onrowdatabound="grid_RowDataBound">
                        <PagerSettings Visible="False" />
                        <Columns>
                           
                            <asp:TemplateField HeaderText="置首頁" Visible="False">
                                <ItemTemplate>
                                <asp:Literal ID="ltlHome" Text='<%# Eval("AtcTop") %>' runat="server"></asp:Literal>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="類別" Visible="false">  <ItemTemplate>
                                <asp:Label ID="lblSubName" runat="server" Text='<%# Eval("AtcSubID") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" /></asp:TemplateField>

                            <asp:BoundField DataField="AtcCloseDate" 
                                DataFormatString="{0:yyyy/MM/dd HH:mm}" HeaderText="下架日期" Visible="false">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="160px" />
                            </asp:BoundField>

                            <asp:BoundField DataField="AtcName" HeaderText="標題" ReadOnly="True">
                                <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>

                            <asp:BoundField DataField="AtcAuthor" HeaderText="作者" ReadOnly="True" Visible="false">
                                <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>

                            <asp:BoundField DataField="AtcOpenDate" 
                                DataFormatString="{0:yyyy/MM/dd HH:mm}" HeaderText="發佈日期" Visible="False">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="160px" />
                            </asp:BoundField>

                            <asp:TemplateField HeaderText="網址" Visible="False">
                            <ItemTemplate>
                            <asp:Label ID="lblLink" runat="server" Text='<%# Eval("AttID") + "," + Eval("AtcID") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="left" Width="200px" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="預計上架日期" Visible="False">
                            <ItemTemplate>
                            <asp:Label ID="lblOpenDate" runat="server" Text='<%# Eval("AtcID") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="是否啟用" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblIsEnable" runat="server" Text='<%# Eval("AtcEnable") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="90px" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="是否簽名" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblIsSign" runat="server" Text='<%# Eval("AtcIsSign") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="90px" />
                            </asp:TemplateField>

                             <asp:TemplateField HeaderText="是否彈出視窗" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblIsPopup" runat="server" Text='<%# Eval("AtcPopup") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="80px" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="圖片" Visible="False" >
                                <ItemTemplate>
                                    <asp:Image ID="imgPic" AlternateText = '<%# Eval("AtcID") + "," + Eval("AtcPicPath") %>' runat="server" ImageUrl="Images/default.jpg" 
                                        Height="80px" Width="80px" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="80px" />
                            </asp:TemplateField>
                                <asp:TemplateField HeaderText="預覽" Visible="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnView" runat="server" CausesValidation="False" CommandArgument='<%# Eval("AtcID") %>' CommandName="View" ImageUrl="Images/View.gif" ToolTip="瀏覽" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="70px" />
                            </asp:TemplateField>
                                <asp:TemplateField HeaderText="編輯">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate><asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" 
                                        CommandArgument='<%# Eval("AtcID") %>' CommandName="Modify" 
                                        ImageUrl="Images/Edit.png" ToolTip="編輯" /></ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="70px" /></asp:TemplateField>

                                    <asp:TemplateField HeaderText="刪除" Visible="False">
                                        <HeaderStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnDel" runat="server" CausesValidation="False" 
                                        CommandArgument='<%# Eval("AtcID") %>' CommandName="Remove" 
                                        ImageUrl="Images/Delete.png" 
                                        onclientclick="return window.confirm('確定要刪除嗎？');" ToolTip="刪除" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="70px" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Literal ID="ltlPageMenu" runat="server"></asp:Literal>
                    <asp:HiddenField ID="hideLoginUseID" runat="server" />
                </td>
            </tr>
        </table>

            </div>
        </div>

        </div>
        </div>

    </asp:View>
    <asp:View ID="View2" runat="server">

        <div class="row">
        <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
            <i class="fa fa-align-justify"></i> 新增/編輯
            </div>

            <div class="card-body">

            <table style="width:100%;">
                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="lblIsEnable5" runat="server" SkinID="MainText" Text="是否置首頁" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:CheckBox ID="ckbAtcTop2" runat="server" Text="啟用請打勾" Visible="False" />
                        <asp:Label ID="lblPositionAlert14" runat="server" SkinID="MainAlert" Text="(勾選後會將XXXXX置於首頁，最多三則)" Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="lblIsEnable6" runat="server" SkinID="MainText" Text="類別" Visible="false"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:DropDownList ID="ddlSub2" runat="server" Width="35%" Visible="false"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="Label45" runat="server" SkinID="MainText" Text="發佈日期" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbOpenDate2" runat="server" SkinID="MainText" Text="沒有資料" Width="100px" Visible="False"></asp:TextBox>
                        &nbsp;<asp:DropDownList ID="ddlOpenDateH2" runat="server" Width="50px" Visible="False">
                            <asp:ListItem>00</asp:ListItem>
                            <asp:ListItem>01</asp:ListItem>
                            <asp:ListItem>02</asp:ListItem>
                            <asp:ListItem>03</asp:ListItem>
                            <asp:ListItem>04</asp:ListItem>
                            <asp:ListItem>05</asp:ListItem>
                            <asp:ListItem>06</asp:ListItem>
                            <asp:ListItem>07</asp:ListItem>
                            <asp:ListItem>08</asp:ListItem>
                            <asp:ListItem>09</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                            <asp:ListItem>13</asp:ListItem>
                            <asp:ListItem>14</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>16</asp:ListItem>
                            <asp:ListItem>17</asp:ListItem>
                            <asp:ListItem>18</asp:ListItem>
                            <asp:ListItem>19</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>21</asp:ListItem>
                            <asp:ListItem>22</asp:ListItem>
                            <asp:ListItem>23</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Label ID="lable149" runat="server" SkinID="MainText" Text="時" Visible="False"></asp:Label>
                        &nbsp;<asp:DropDownList ID="ddlOpenDateM2" runat="server" Width="50px" Visible="False">
                            <asp:ListItem>00</asp:ListItem>
                            <asp:ListItem>01</asp:ListItem>
                            <asp:ListItem>02</asp:ListItem>
                            <asp:ListItem>03</asp:ListItem>
                            <asp:ListItem>04</asp:ListItem>
                            <asp:ListItem>05</asp:ListItem>
                            <asp:ListItem>06</asp:ListItem>
                            <asp:ListItem>07</asp:ListItem>
                            <asp:ListItem>08</asp:ListItem>
                            <asp:ListItem>09</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                            <asp:ListItem>13</asp:ListItem>
                            <asp:ListItem>14</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>16</asp:ListItem>
                            <asp:ListItem>17</asp:ListItem>
                            <asp:ListItem>18</asp:ListItem>
                            <asp:ListItem>19</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>21</asp:ListItem>
                            <asp:ListItem>22</asp:ListItem>
                            <asp:ListItem>23</asp:ListItem>
                            <asp:ListItem>24</asp:ListItem>
                            <asp:ListItem>25</asp:ListItem>
                            <asp:ListItem>26</asp:ListItem>
                            <asp:ListItem>27</asp:ListItem>
                            <asp:ListItem>28</asp:ListItem>
                            <asp:ListItem>29</asp:ListItem>
                            <asp:ListItem>30</asp:ListItem>
                            <asp:ListItem>31</asp:ListItem>
                            <asp:ListItem>32</asp:ListItem>
                            <asp:ListItem>33</asp:ListItem>
                            <asp:ListItem>34</asp:ListItem>
                            <asp:ListItem>35</asp:ListItem>
                            <asp:ListItem>36</asp:ListItem>
                            <asp:ListItem>37</asp:ListItem>
                            <asp:ListItem>38</asp:ListItem>
                            <asp:ListItem>39</asp:ListItem>
                            <asp:ListItem>40</asp:ListItem>
                            <asp:ListItem>41</asp:ListItem>
                            <asp:ListItem>42</asp:ListItem>
                            <asp:ListItem>43</asp:ListItem>
                            <asp:ListItem>44</asp:ListItem>
                            <asp:ListItem>45</asp:ListItem>
                            <asp:ListItem>46</asp:ListItem>
                            <asp:ListItem>47</asp:ListItem>
                            <asp:ListItem>48</asp:ListItem>
                            <asp:ListItem>49</asp:ListItem>
                            <asp:ListItem>50</asp:ListItem>
                            <asp:ListItem>51</asp:ListItem>
                            <asp:ListItem>52</asp:ListItem>
                            <asp:ListItem>53</asp:ListItem>
                            <asp:ListItem>54</asp:ListItem>
                            <asp:ListItem>55</asp:ListItem>
                            <asp:ListItem>56</asp:ListItem>
                            <asp:ListItem>57</asp:ListItem>
                            <asp:ListItem>58</asp:ListItem>
                            <asp:ListItem>59</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Label ID="lable150" runat="server" SkinID="MainText" Text="分" Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="Label48" runat="server" SkinID="MainText" Text="下架日期" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbCloseDate2" runat="server" SkinID="MainText" Text="沒有資料" Width="100px" Visible="False"></asp:TextBox>
                        &nbsp;<asp:DropDownList ID="ddlCloseDateH2" runat="server" Width="50px" Visible="False">
                            <asp:ListItem>00</asp:ListItem>
                            <asp:ListItem>01</asp:ListItem>
                            <asp:ListItem>02</asp:ListItem>
                            <asp:ListItem>03</asp:ListItem>
                            <asp:ListItem>04</asp:ListItem>
                            <asp:ListItem>05</asp:ListItem>
                            <asp:ListItem>06</asp:ListItem>
                            <asp:ListItem>07</asp:ListItem>
                            <asp:ListItem>08</asp:ListItem>
                            <asp:ListItem>09</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                            <asp:ListItem>13</asp:ListItem>
                            <asp:ListItem>14</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>16</asp:ListItem>
                            <asp:ListItem>17</asp:ListItem>
                            <asp:ListItem>18</asp:ListItem>
                            <asp:ListItem>19</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>21</asp:ListItem>
                            <asp:ListItem>22</asp:ListItem>
                            <asp:ListItem>23</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Label ID="lable151" runat="server" SkinID="MainText" Text="時" Visible="False"></asp:Label>
                        &nbsp;<asp:DropDownList ID="ddlCloseDateM2" runat="server" Width="50px" Visible="False">
                            <asp:ListItem>00</asp:ListItem>
                            <asp:ListItem>01</asp:ListItem>
                            <asp:ListItem>02</asp:ListItem>
                            <asp:ListItem>03</asp:ListItem>
                            <asp:ListItem>04</asp:ListItem>
                            <asp:ListItem>05</asp:ListItem>
                            <asp:ListItem>06</asp:ListItem>
                            <asp:ListItem>07</asp:ListItem>
                            <asp:ListItem>08</asp:ListItem>
                            <asp:ListItem>09</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                            <asp:ListItem>13</asp:ListItem>
                            <asp:ListItem>14</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>16</asp:ListItem>
                            <asp:ListItem>17</asp:ListItem>
                            <asp:ListItem>18</asp:ListItem>
                            <asp:ListItem>19</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>21</asp:ListItem>
                            <asp:ListItem>22</asp:ListItem>
                            <asp:ListItem>23</asp:ListItem>
                            <asp:ListItem>24</asp:ListItem>
                            <asp:ListItem>25</asp:ListItem>
                            <asp:ListItem>26</asp:ListItem>
                            <asp:ListItem>27</asp:ListItem>
                            <asp:ListItem>28</asp:ListItem>
                            <asp:ListItem>29</asp:ListItem>
                            <asp:ListItem>30</asp:ListItem>
                            <asp:ListItem>31</asp:ListItem>
                            <asp:ListItem>32</asp:ListItem>
                            <asp:ListItem>33</asp:ListItem>
                            <asp:ListItem>34</asp:ListItem>
                            <asp:ListItem>35</asp:ListItem>
                            <asp:ListItem>36</asp:ListItem>
                            <asp:ListItem>37</asp:ListItem>
                            <asp:ListItem>38</asp:ListItem>
                            <asp:ListItem>39</asp:ListItem>
                            <asp:ListItem>40</asp:ListItem>
                            <asp:ListItem>41</asp:ListItem>
                            <asp:ListItem>42</asp:ListItem>
                            <asp:ListItem>43</asp:ListItem>
                            <asp:ListItem>44</asp:ListItem>
                            <asp:ListItem>45</asp:ListItem>
                            <asp:ListItem>46</asp:ListItem>
                            <asp:ListItem>47</asp:ListItem>
                            <asp:ListItem>48</asp:ListItem>
                            <asp:ListItem>49</asp:ListItem>
                            <asp:ListItem>50</asp:ListItem>
                            <asp:ListItem>51</asp:ListItem>
                            <asp:ListItem>52</asp:ListItem>
                            <asp:ListItem>53</asp:ListItem>
                            <asp:ListItem>54</asp:ListItem>
                            <asp:ListItem>55</asp:ListItem>
                            <asp:ListItem>56</asp:ListItem>
                            <asp:ListItem>57</asp:ListItem>
                            <asp:ListItem>58</asp:ListItem>
                            <asp:ListItem>59</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Label ID="lable152" runat="server" SkinID="MainText" Text="分" Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="lable154" runat="server" SkinID="MainText" Text="標題"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbAtcName2" runat="server" MaxLength="100" Width="80%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvAtcName2" runat="server" ControlToValidate="txbAtcName2" ErrorMessage="RequiredFieldValidator" ForeColor="Red" SkinID="MainAlert" ValidationGroup="OK">標體不可為空</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="lable156" runat="server" SkinID="MainText" Text="標題(英文)" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbAtcName_EN2" runat="server" MaxLength="100" Width="80%" Visible="False"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvAtcName_EN2" runat="server" ControlToValidate="txbAtcName_EN2" ErrorMessage="RequiredFieldValidator" ForeColor="Red" SkinID="MainAlert" ValidationGroup="OK" Visible="False">標體不可為空</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%; vertical-align: top;">
                        <asp:Label ID="Label46" runat="server" SkinID="MainText" Text="簡述" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbAtcDesc2" runat="server" Height="60px" MaxLength="500" TextMode="MultiLine" Width="80%" Visible="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%; vertical-align: top;">
                        <asp:Label ID="Label50" runat="server" SkinID="MainText" Text="簡述(英文)" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbAtcDesc_EN2" runat="server" Height="60px" MaxLength="500" TextMode="MultiLine" Width="80%" Visible="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%; ">
                        &nbsp;</td>
                    <td align="left" style="width: 70%">
                        <asp:Label ID="Label47" runat="server" SkinID="MainAlert" Text="(簡述只顯示於列表頁上，字數限制500字內)" Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%; ">
                        <asp:Label ID="lable155" runat="server" SkinID="MainText" Text="作者" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbAtcAuthor2" runat="server" MaxLength="50" Width="30%" Visible="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%;">
                        <asp:Label ID="lable158" runat="server" SkinID="MainText" Text="作者(英文)" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbAtcAuthor_EN2" runat="server" MaxLength="50" Width="30%" Visible="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="lblIsEnable3" runat="server" SkinID="MainText" Text="是否啟用" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:CheckBox ID="ckbAtcEnable2" runat="server" Checked="True" Text="啟用請打勾" Visible="False" />
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="lblIsEnable8" runat="server" SkinID="MainText" Text="是否啟用簽名檔" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:CheckBox ID="ckbAtcIsSign2" runat="server" Checked="True" Text="啟用請打勾" Visible="False" />
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="lblIsEnable7" runat="server" SkinID="MainText" Text="是否熱門推薦" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:CheckBox ID="ckbAtcPopup2" runat="server" Checked="True" Text="啟用請打勾" Visible="False" />
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="lable153" runat="server" SkinID="MainText" Text="SEO名稱" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbAtcSEOName2" runat="server" MaxLength="100" Width="80%" Visible="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%; vertical-align: top;">
                        <asp:Label ID="Label49" runat="server" SkinID="MainText" Text="SEO簡述" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbAtcSEODesc2" runat="server" Height="60px" MaxLength="500" TextMode="MultiLine" Width="80%" Visible="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="lable157" runat="server" SkinID="MainText" Text="語系"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:DropDownList ID="ddlLge2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlLge2_SelectedIndexChanged" style="margin-bottom: 0px" Width="15%">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="lable93" runat="server" SkinID="MainText" Text="內容類型"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:DropDownList ID="ddlArticleClass2" runat="server" Width="30%">
                        </asp:DropDownList>
                        <asp:Button ID="btnAdd2" runat="server" BorderColor="Transparent" BorderWidth="0px" CssClass="SmallButton" OnClick="btnAdd2_Click" Text="新增" />
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">&nbsp;</td>
                    <td align="left" style="width: 70%">
                        <asp:GridView ID="gridContent2" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="AtnID" DataSourceID="ObjectDataSource2" ForeColor="Black" GridLines="Vertical" OnRowCommand="gridContent2_RowCommand" OnRowDataBound="gridContent2_RowDataBound" SkinID="NOSET" Width="80%">
                            <FooterStyle BackColor="#CCCC99" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                            <PagerSettings Visible="False" />
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField DataField="AtnSort" HeaderText="排序">
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="內容類型">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAtsName" runat="server" Text='<%# Eval("AtsID") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="AtnSubject" HeaderText="標題" ReadOnly="True" Visible="False">
                                <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>

                               <asp:TemplateField HeaderText="圖片/內文" >
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlACText" runat="server" Text='<%# Eval("AtnID") %>'></asp:Literal>
                                        <asp:Image ID="imgACPic" AlternateText = '<%# Eval("AtnID") %>' runat="server" ImageUrl="Images/default.jpg" 
                                            Height="80px" Width="80px" />
                                    </ItemTemplate>
                                    <HeaderStyle />
                                </asp:TemplateField>

                                <asp:BoundField DataField="LastUpdateDate" DataFormatString="{0:yyyy/MM/dd}" HeaderText="最後修改日期" ReadOnly="True" Visible="False">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="向上" Visible="True">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnSortUp" runat="server" CausesValidation="False" CommandArgument='<%# Eval("AtcID") + "," + Eval("AtnID") + "," + Eval("AtnSort") %>' CommandName="SortUp" ImageUrl="Images/SortUp.gif" ToolTip="向上" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="50px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="向下" Visible="True">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnSortDown" runat="server" CausesValidation="False" CommandArgument='<%# Eval("AtcID") + "," + Eval("AtnID") + "," + Eval("AtnSort") %>' CommandName="SortDown" ImageUrl="Images/SortDown.gif" ToolTip="向下" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="50px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="編輯" Visible="True">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("AtsID") + "," + Eval("AtnID") %>' CommandName="Modify" ImageUrl="Images/Edit.png" ToolTip="編輯" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="50px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="刪除">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnDel" runat="server" CausesValidation="False" CommandArgument='<%# Eval("AtcID") + "," + Eval("AtnID") %>' CommandName="Remove" ImageUrl="Images/Delete.png" onclientclick="return window.confirm('確定要刪除嗎？');" ToolTip="刪除" />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" Width="50px" />
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <RowStyle BackColor="#F7F7DE" />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#FBFBF2" />
                            <SortedAscendingHeaderStyle BackColor="#848384" />
                            <SortedDescendingCellStyle BackColor="#EAEAD3" />
                            <SortedDescendingHeaderStyle BackColor="#575357" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%; vertical-align: top;">
                        <asp:Label ID="Label36" runat="server" SkinID="MainText" Text="圖片" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:Image ID="imgAtcPicPath2" runat="server" Height="150px" ImageUrl="Images/default.jpg" Visible="False" />
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%; vertical-align: top;">&nbsp;</td>
                    <td align="left" style="width: 70%">
                        <asp:FileUpload ID="uploadAtcPicPath2" runat="server" Visible="False" />
                        <asp:Button ID="btnUploadPic2" runat="server" OnClick="btnUploadPic2_Click" Text="圖片上傳" Visible="False" />
                        <asp:HiddenField ID="hideAtcPicPath2" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">
                        &nbsp;</td>
                    <td align="left" style="width: 70%">
                        <asp:Label ID="lblPicDesc3_2" runat="server" SkinID="MainAlert" Visible="False">圖片支援（bmp,jpg,jpeg,png）等格式。<%--，建議尺寸1920 X 800 px--%></asp:Label>
                        <br />
                        <asp:Label ID="lblPositionAlert15" runat="server" SkinID="MainAlert" Text="(圖片尺寸建議 800 X 600px)" Visible="False"></asp:Label>
                    </td>
                </tr>
          
                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="lable159" runat="server" SkinID="MainText" Text="圖片描述" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbAtcPicDesc2" runat="server" MaxLength="50" Visible="False" Width="50%"></asp:TextBox>
                    </td>
                </tr>
          
                <tr>
                    <td align="left" style="width: 30%" valign="top">
                        <asp:Label ID="Label38" runat="server" SkinID="MainText" Text="FaceBook分享圖片" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:Image ID="imgAtcFBPicPath2" runat="server" Height="100px" ImageUrl="Images/default.jpg" Visible="False" />
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%" valign="top">&nbsp;</td>
                    <td align="left" style="width: 70%">
                        <asp:FileUpload ID="uploadAtcFBPicPath2" runat="server" Visible="False" />
                        <asp:Button ID="btnUploadFBPic2" runat="server" Text="圖片上傳" OnClick="btnUploadFBPic2_Click" Visible="False" />
                        <asp:HiddenField ID="hideAtcFBPicPath2" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" colspan="2">

                        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetAllByAtcIDAndLgeID" TypeName="Service.ArticleContentService">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hideID" Name="AtcID" PropertyName="Value" Type="Int64" />
                                <asp:ControlParameter ControlID="ddlLge2" Name="LgeID" PropertyName="SelectedValue" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>

                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2" style="text-align: center;" valign="top">
                        <asp:Label ID="lblMeg2" runat="server" ForeColor="Red" SkinID="MainWarn"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnEdit2" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="BigButton" Text="確定編輯" 
                            ValidationGroup="OK" OnClick="btnEdit2_Click" />
                        <asp:Button ID="btnOK2" runat="server" BackColor="Transparent" BorderWidth="0px" 
                            CssClass="SmallButton" Text="確定" ValidationGroup="OK" OnClick="btnOK2_Click" />
                        <asp:Button ID="btnBack" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="SmallButton" Text="返回" 
                            onclick="btnBack_Click" />
                        <asp:HiddenField ID="hideID" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        &nbsp;</td>
                </tr>
            </table>

            </div>

        </div>
        </div>
        </div>

    </asp:View>

    <asp:View ID="View3" runat="server">

        <div class="row">
        <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
            <i class="fa fa-align-justify"></i> 新增/編輯副標區塊
            </div>

            <div class="card-body">

            <table style="width:100%;">
                <tr>
                    <td align="left" style="width: 10%" valign="top">
                        <asp:Label ID="Label39" runat="server" SkinID="MainText" Text="標題" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 90%">
                        <asp:TextBox ID="txbAtnSubject3" runat="server" MaxLength="100" Width="80%" Visible="False"></asp:TextBox>
                    </td>
                </tr>
          
                <tr>
                    <td align="left" style="width: 10%" valign="top">
                        <asp:Label ID="Label2" runat="server" SkinID="MainText" Text="內容"></asp:Label>
                    </td>
                    <td align="left" style="width: 90%">
                        <CKEditor:CKEditorControl ID="CKEditorControl3" runat="server" BasePath="js/ckeditor" Height="100px"></CKEditor:CKEditorControl>
                    </td>
                </tr>
          
                <tr>
                    <td align="left" colspan="2" style="text-align: center;" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnEdit3" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="BigButton" Text="確定編輯" 
                            ValidationGroup="OK" OnClick="btnEdit3_Click" />
                        <asp:Button ID="btnOK3" runat="server" BackColor="Transparent" BorderWidth="0px" CssClass="SmallButton" Text="確定" ValidationGroup="OK" OnClick="btnOK3_Click" />
                        <asp:Button ID="btnBack3" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="SmallButton" Text="返回" OnClick="btnBack3_Click" />
                        <asp:HiddenField ID="hideAtnID3" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        &nbsp;</td>
                </tr>
            </table>

            </div>

        </div>
        </div>
        </div>

    </asp:View>

    <asp:View ID="View4" runat="server">

        <div class="row">
        <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
            <i class="fa fa-align-justify"></i> 新增/編輯文字區塊
            </div>

            <div class="card-body">

            <table style="width:100%;">
                <tr>
                    <td align="left" style="width: 10%" valign="top">
                        <asp:Label ID="Label4" runat="server" SkinID="MainText" Text="標題" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 90%">
                        <asp:TextBox ID="txbAtnSubject4" runat="server" MaxLength="100" Width="80%" Visible="False"></asp:TextBox>
                    </td>
                </tr>
          
                <tr>
                    <td align="left" style="width: 10%" valign="top">
                        <asp:Label ID="Label5" runat="server" SkinID="MainText" Text="內容"></asp:Label>
                    </td>
                    <td align="left" style="width: 90%">
                        <CKEditor:CKEditorControl ID="CKEditorControl4" runat="server" BasePath="js/ckeditor" Height="500px"></CKEditor:CKEditorControl>
                    </td>
                </tr>
          
                <tr>
                    <td align="left" colspan="2" style="text-align: center;" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnEdit4" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="BigButton" Text="確定編輯" 
                            ValidationGroup="OK" OnClick="btnEdit4_Click" />
                        <asp:Button ID="btnOK4" runat="server" BackColor="Transparent" BorderWidth="0px" CssClass="SmallButton" Text="確定" ValidationGroup="OK" OnClick="btnOK4_Click" />
                        <asp:Button ID="btnBack4" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="SmallButton" Text="返回" OnClick="btnBack3_Click" />
                        <asp:HiddenField ID="hideAtnID4" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        &nbsp;</td>
                </tr>
            </table>

            </div>

        </div>
        </div>
        </div>

    </asp:View>

    <asp:View ID="View5" runat="server">

        <div class="row">
        <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
            <i class="fa fa-align-justify"></i> 新增/編輯圖片區塊
            </div>

            <div class="card-body">

            <table style="width:100%;">
                <tr>
                    <td align="left" style="width: 10%" valign="top">
                        <asp:Label ID="Label7" runat="server" SkinID="MainText" Text="圖片說明"></asp:Label>
                    </td>
                    <td align="left" style="width: 90%">
                        <asp:TextBox ID="txbAtnSubject5" runat="server" MaxLength="100" Width="80%"></asp:TextBox>
                    </td>
                </tr>
          
                <tr>
                    <td align="left" style="width: 10%" valign="top">
                        <asp:Label ID="Label40" runat="server" SkinID="MainText" Text="圖片"></asp:Label>
                    </td>
                    <td align="left" style="width: 90%">
                        <asp:Image ID="imgAtnPicPath5" runat="server" Height="400px" ImageUrl="Images/default.jpg" />
                    </td>
                </tr>
          
                <tr>
                    <td align="left" style="width: 10%" valign="top">&nbsp;</td>
                    <td align="left" style="width: 90%">
                        <asp:FileUpload ID="uploadAtnPicPath5" runat="server" Width="40%" />
                        <asp:HiddenField ID="hideAtnPicPath5" runat="server" />
                    </td>
                </tr>
          
                <tr>
                    <td align="left" style="width: 10%" valign="top">&nbsp;</td>
                    <td align="left" style="width: 90%">
                        <asp:Label ID="lblPicDesc3_3" runat="server" SkinID="MainAlert">圖片支援（bmp,jpg,jpeg,png）等格式。<%--，建議尺寸1920 X 800 px--%></asp:Label>
                        <br />
                        <asp:Label ID="lblPositionAlert16" runat="server" SkinID="MainAlert" Text="(圖片尺寸建議 800 X 600px)"></asp:Label>
                    </td>
                </tr>
          
                <tr>
                    <td align="left" colspan="2" style="text-align: center;" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnEdit5" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="BigButton" Text="確定編輯" 
                            ValidationGroup="OK" OnClick="btnEdit5_Click" />
                        <asp:Button ID="btnOK5" runat="server" BackColor="Transparent" BorderWidth="0px" CssClass="SmallButton" Text="確定" ValidationGroup="OK" OnClick="btnOK5_Click" />
                        <asp:Button ID="btnBack5" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="SmallButton" Text="返回" OnClick="btnBack3_Click" />
                        <asp:HiddenField ID="hideAtnID5" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        &nbsp;</td>
                </tr>
            </table>

            </div>

        </div>
        </div>
        </div>

    </asp:View>

    <asp:View ID="View6" runat="server">

        <div class="row">
        <div class="col-lg-12">
        <div class="card">
           <div class="card-header">
           <i class="fa fa-align-justify"></i> 新增/編輯串流影片區塊(YouTube)
           </div>

           <div class="card-body">

           <table style="width:100%;">
                <tr>
                    <td align="left" style="width: 10%" valign="top">
                        <asp:Label ID="Label9" runat="server" SkinID="MainText" Text="標題" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 90%">
                        <asp:TextBox ID="txbAtnSubject6" runat="server" MaxLength="100" Width="80%" Visible="False"></asp:TextBox>
                    </td>
                </tr>
          
                <tr>
                    <td align="left" style="width: 10%" valign="top">
                        <asp:Label ID="Label41" runat="server" SkinID="MainText" Text="影片連結"></asp:Label>
                    </td>
                    <td align="left" style="width: 90%">
                        <asp:TextBox ID="txbAtnVideoPath6" runat="server" MaxLength="500" Width="80%"></asp:TextBox>
                        <br />
                        <asp:Label ID="Label42" runat="server" SkinID="MainAlert" Text="(請由影片分享選嵌入(Embed Video)，網址請輸入：https://www.youtube.com/embed/XXXXXX)"></asp:Label>
                    </td>
                </tr>
          
                <tr>
                    <td align="left" colspan="2" style="text-align: center;" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnEdit6" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="BigButton" Text="確定編輯" 
                            ValidationGroup="OK" OnClick="btnEdit6_Click" />
                        <asp:Button ID="btnOK6" runat="server" BackColor="Transparent" BorderWidth="0px" CssClass="SmallButton" Text="確定" ValidationGroup="OK" OnClick="btnOK6_Click" />
                        <asp:Button ID="btnBack6" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="SmallButton" Text="返回" OnClick="btnBack3_Click" />
                        <asp:HiddenField ID="hideAtnID6" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        &nbsp;</td>
                </tr>
            </table>

           </div>

        </div>
        </div>
        </div>

    </asp:View>

    <asp:View ID="View7" runat="server">

        <div class="row">
        <div class="col-lg-12">
        <div class="card">
           <div class="card-header">
           <i class="fa fa-align-justify"></i> 新增/編輯串流影片區塊(Vimeo)
           </div>

            <div class="card-body">

            <table style="width:100%;">

                <tr>
                    <td align="left" style="width: 10%" valign="top">
                        <asp:Label ID="Label11" runat="server" SkinID="MainText" Text="標題"></asp:Label>
                    </td>
                    <td align="left" style="width: 90%">
                        <asp:TextBox ID="txbAtnSubject7" runat="server" MaxLength="100" Width="80%"></asp:TextBox>
                    </td>
                </tr>
          
                <tr>
                    <td align="left" style="width: 10%" valign="top">
                        <asp:Label ID="Label12" runat="server" SkinID="MainText" Text="影片連結"></asp:Label>
                    </td>
                    <td align="left" style="width: 90%">
                        <asp:TextBox ID="txbAtnVideoPath7" runat="server" MaxLength="500" Width="80%"></asp:TextBox>
                        <br />
                        <asp:Label ID="Label13" runat="server" SkinID="MainAlert" Text="(網址請輸入：http://XXX.XXXXXX.XXX)"></asp:Label>
                    </td>
                </tr>
          
                <tr>
                    <td align="left" colspan="2" style="text-align: center;" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnEdit7" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="BigButton" Text="確定編輯" 
                            ValidationGroup="OK" OnClick="btnEdit7_Click" />
                        <asp:Button ID="btnOK7" runat="server" BackColor="Transparent" BorderWidth="0px" CssClass="SmallButton" Text="確定" ValidationGroup="OK" OnClick="btnOK7_Click" />
                        <asp:Button ID="btnBack7" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="SmallButton" Text="返回" OnClick="btnBack3_Click" />
                        <asp:HiddenField ID="hideAtnID7" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        &nbsp;</td>
                </tr>
            </table>

             </div>

        </div>
        </div>
        </div>

    </asp:View>

    <asp:View ID="View8" runat="server">

        <div class="row">
        <div class="col-lg-12">
        <div class="card">
           <div class="card-header">
           <i class="fa fa-align-justify"></i> 新增/編輯串流影片區塊(優酷)
           </div>

            <div class="card-body">

            <table style="width:100%;">
               
                <tr>
                    <td align="left" style="width: 10%" valign="top">
                        <asp:Label ID="Label15" runat="server" SkinID="MainText" Text="標題"></asp:Label>
                    </td>
                    <td align="left" style="width: 90%">
                        <asp:TextBox ID="txbAtnSubject8" runat="server" MaxLength="100" Width="80%"></asp:TextBox>
                    </td>
                </tr>
          
                <tr>
                    <td align="left" style="width: 10%" valign="top">
                        <asp:Label ID="Label17" runat="server" SkinID="MainText" Text="影片連結"></asp:Label>
                    </td>
                    <td align="left" style="width: 90%">
                        <asp:TextBox ID="txbAtnVideoPath8" runat="server" MaxLength="500" Width="80%"></asp:TextBox>
                        <br />
                        <asp:Label ID="Label18" runat="server" SkinID="MainAlert" Text="(網址請輸入：http://XXX.XXXXXX.XXX)"></asp:Label>
                    </td>
                </tr>
          
                <tr>
                    <td align="left" colspan="2" style="text-align: center;" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnEdit8" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="BigButton" Text="確定編輯" 
                            ValidationGroup="OK" OnClick="btnEdit8_Click" />
                        <asp:Button ID="btnOK8" runat="server" BackColor="Transparent" BorderWidth="0px" CssClass="SmallButton" Text="確定" ValidationGroup="OK" OnClick="btnOK8_Click" />
                        <asp:Button ID="btnBack8" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="SmallButton" Text="返回" OnClick="btnBack3_Click" />
                        <asp:HiddenField ID="hideAtnID8" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        &nbsp;</td>
                </tr>
            </table>

            </div>

        </div>
        </div>
        </div>

    </asp:View>

    <asp:View ID="View9" runat="server">
        <div class="row">
        <div class="col-lg-12">
        <div class="card">
           <div class="card-header">
           <i class="fa fa-align-justify"></i> 新增/編輯串流影片區塊(土豆網)
           </div>

            <div class="card-body">

            <table style="width:100%;">
                
                <tr>
                    <td align="left" style="width: 10%" valign="top">
                        <asp:Label ID="Label20" runat="server" SkinID="MainText" Text="標題"></asp:Label>
                    </td>
                    <td align="left" style="width: 90%">
                        <asp:TextBox ID="txbAtnSubject9" runat="server" MaxLength="100" Width="80%"></asp:TextBox>
                    </td>
                </tr>
          
                <tr>
                    <td align="left" style="width: 10%" valign="top">
                        <asp:Label ID="Label21" runat="server" SkinID="MainText" Text="影片連結"></asp:Label>
                    </td>
                    <td align="left" style="width: 90%">
                        <asp:TextBox ID="txbAtnVideoPath9" runat="server" MaxLength="500" Width="80%"></asp:TextBox>
                        <br />
                        <asp:Label ID="Label43" runat="server" SkinID="MainAlert" Text="(請複製HTML代碼，只取SRC內網址即可＜embed src='http://XXX-XXXXX-XXX'＞＜/embed＞)"></asp:Label>
                    </td>
                </tr>
          
                <tr>
                    <td align="left" colspan="2" style="text-align: center;" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnEdit9" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="BigButton" Text="確定編輯" 
                            ValidationGroup="OK" OnClick="btnEdit9_Click" />
                        <asp:Button ID="btnOK9" runat="server" BackColor="Transparent" BorderWidth="0px" CssClass="SmallButton" Text="確定" ValidationGroup="OK" OnClick="btnOK9_Click" />
                        <asp:Button ID="btnBack9" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="SmallButton" Text="返回" OnClick="btnBack3_Click" />
                        <asp:HiddenField ID="hideAtnID9" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        &nbsp;</td>
                </tr>
            </table>

            </div>

        </div>
        </div>
        </div>

    </asp:View>

    <asp:View ID="View10" runat="server">

        <div class="row">
        <div class="col-lg-12">
        <div class="card">
           <div class="card-header">
           <i class="fa fa-align-justify"></i> 新增/編輯MP4影片區塊
           </div>

            <div class="card-body">

            <table style="width:100%;">
              
                <tr>
                    <td align="left" style="width: 10%" valign="top">
                        <asp:Label ID="Label23" runat="server" SkinID="MainText" Text="標題"></asp:Label>
                    </td>
                    <td align="left" style="width: 90%">
                        <asp:TextBox ID="txbAtnSubject10" runat="server" MaxLength="100" Width="80%"></asp:TextBox>
                    </td>
                </tr>
          
                <tr>
                    <td align="left" style="width: 10%" valign="top">
                        <asp:Label ID="Label24" runat="server" SkinID="MainText" Text="影片連結"></asp:Label>
                    </td>
                    <td align="left" style="width: 90%">
                        <asp:TextBox ID="txbAtnVideoPath10" runat="server" MaxLength="500" Width="80%"></asp:TextBox>
                        <br />
                        <asp:Label ID="Label44" runat="server" SkinID="MainAlert" Text="(網址請輸入：http://XXX.XXXXXX.XXX)"></asp:Label>
                    </td>
                </tr>
          
                <tr>
                    <td align="left" colspan="2" style="text-align: center;" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnEdit10" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="BigButton" Text="確定編輯" 
                            ValidationGroup="OK" OnClick="btnEdit10_Click" />
                        <asp:Button ID="btnOK10" runat="server" BackColor="Transparent" BorderWidth="0px" CssClass="SmallButton" Text="確定" ValidationGroup="OK" OnClick="btnOK10_Click" />
                        <asp:Button ID="btnBack10" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="SmallButton" Text="返回" OnClick="btnBack3_Click" />
                        <asp:HiddenField ID="hideAtnID10" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        &nbsp;</td>
                </tr>
            </table>

            </div>

        </div>
        </div>
        </div>

    </asp:View>

</asp:MultiView>

</div>
</div>
<!-- /.conainer-fluid -->

</asp:Content>
