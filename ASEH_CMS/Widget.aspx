﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Widget.aspx.cs" Inherits="ASEH_CMS.Widget" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%= System.IO.File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/React/build/head.txt")
            .Replace("__PUBLIC_URL__", (HttpContext.Current.Request.ApplicationPath + "/React/build").Replace("//", "/"))
            .Replace("__API_HOST__", "/Backend2/api")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="root"></div>
    <%= System.IO.File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/React/build/body.txt").Replace("__PUBLIC_URL__", (HttpContext.Current.Request.ApplicationPath + "/React/build").Replace("//", "/")) %>
</asp:Content>
