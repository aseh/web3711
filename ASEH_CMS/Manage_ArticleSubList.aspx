﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Manage_ArticleSubList.aspx.cs" Inherits="ASEH_CMS.Manage_ArticleSubList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Breadcrumb -->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="MainIndex.aspx">Dashboard</a></li>
        <li class="breadcrumb-item active">產業新知類別管理</li>
      </ol>
    <!-- Breadcrumb Menu-->
   

<div class="container-fluid">
<div class="animated fadeIn">

    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">

        <div class="row">
        <div class="col-lg-12">

                <%--控制項--%>
                <table style="width:100%;">
                <tr>
                    <td style="text-align: left" >
                       
                    </td>
                </tr>
                    <tr>
                        <td style="text-align: left">
                             <asp:Button ID="btnAdd" runat="server" BorderColor="Transparent" BorderWidth="0px" CssClass="SmallButton" Text="新增" OnClick="btnAdd_Click" />
                        </td>
                        <td align="left">&nbsp;</td>
                    </tr>
                </table>

            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> 列表
                </div>

                <div class="card-body">

                        <%--列表--%>
                        <table style="width:100%;">
                        <tr>
                            <td colspan="2">
                                <asp:GridView ID="grid" runat="server" AutoGenerateColumns="False"  Width="100%" OnRowCommand="grid_RowCommand" PageSize="50" OnRowDataBound="grid_RowDataBound" >
                                    <Columns>
                                        <asp:BoundField DataField="SubSort" HeaderText="排序" Visible="false">
                                            <HeaderStyle HorizontalAlign="Center" />
                                           <ItemStyle HorizontalAlign="Center" Width="70px" />
                                        </asp:BoundField>

                                        <asp:BoundField DataField="SubName" HeaderText="類別名稱">
                                            <HeaderStyle HorizontalAlign="Center" />
                                           <ItemStyle Width="300px" />
                                        </asp:BoundField>

                                        <asp:TemplateField HeaderText="是否啟用">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIsEnable" runat="server" Text='<%# Eval("SubEnable") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="90px" />
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="SubDesc" HeaderText="描述" Visible="False">
                                            <HeaderStyle HorizontalAlign="Center" />
                                           <ItemStyle Width="200px" />
                                        </asp:BoundField>

                                        <asp:TemplateField HeaderText="往上" Visible="False">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnUp" runat="server" CausesValidation="False" CommandArgument='<%# Eval("SubID") + "," + Eval("SubSort") %>' CommandName="SortUp" ImageUrl="Images/SortUp.gif"  />
                                            </ItemTemplate>
                                            <ItemStyle  HorizontalAlign="Center" Width="70px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="往下" Visible="False">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnDown" runat="server" CausesValidation="False" CommandArgument='<%# Eval("SubID") + "," + Eval("SubSort") %>' CommandName="SortDown" ImageUrl="Images/SortDown.gif" />
                                            </ItemTemplate>
                                            <ItemStyle  HorizontalAlign="Center" Width="70px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="編輯">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("SubID") %>' CommandName="Modify" ImageUrl="Images/Edit.png" ToolTip="編輯" />
                                            </ItemTemplate>
                                            <ItemStyle  HorizontalAlign="Center" Width="70px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="刪除">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" CommandArgument='<%# Eval("SubID") %>' CommandName="Remove" ImageUrl="Images/Delete.png" OnClientClick="return window.confirm('確定要刪除嗎？');" ToolTip="刪除" />
                                            </ItemTemplate>
                                            <ItemStyle  HorizontalAlign="Center" Width="70px" />
                                        </asp:TemplateField>
                            
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:HiddenField ID="hideLoginUseID" runat="server" />
                                <asp:HiddenField ID="hideID" runat="server" />
                                <asp:HiddenField ID="hideSort" runat="server" />
                                <br />
                            </td>
                        </tr>
                    </table>

                </div>
            </div>

        </div>
        </div>

        </asp:View>

        <asp:View ID="View2" runat="server">

        <div class="row">
        <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> 新增/編輯
            </div>

            <div class="card-body">

                <table style="width: 100%;">
                   <tr>
                        <td align="left" style="width: 30%">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                    </tr>
                <tr>
                    <td align="left" style="width: 30%" >
                        <asp:Label ID="Label2" runat="server" SkinID="MainText" Text="類別名稱" style="text-align: center"></asp:Label>
                    </td>
                    <td align="left" >
                        <asp:TextBox ID="txbSubName2" runat="server" MaxLength="50" Width="50%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvSubName2" runat="server" ControlToValidate="txbSubName2" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="OK">名稱不可為空</asp:RequiredFieldValidator>
                    </td>
                </tr>
                    <tr>
                        <td align="left" style="width: 30%">
                            <asp:Label ID="Label4" runat="server" SkinID="MainText" style="text-align: center" Text="類別名稱"></asp:Label>
                            (英文)</td>
                        <td align="left">
                            <asp:TextBox ID="txbSubName_EN2" runat="server" MaxLength="50" Width="50%"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvSubName_EN2" runat="server" ControlToValidate="txbSubName_EN2" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="OK">名稱不可為空</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 30%">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 30%">
                            <asp:Label ID="lblIsEnable3" runat="server" SkinID="MainText" Text="是否啟用"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:CheckBox ID="ckbSubEnable2" runat="server" Checked="True" Text="啟用請打勾" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 30%">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 30%">
                            <asp:Label ID="Label3" runat="server" Text="描述" Visible="False"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txbSubDesc2" runat="server" MaxLength="300" Width="80%" Visible="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 30%">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Label ID="lblMeg2" runat="server" ForeColor="Red" SkinID="MainWarn"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" >
                            <asp:Button ID="btnEdit2" runat="server" BorderColor="Transparent" BorderWidth="0px" CssClass="BigButton" OnClick="btnEdit2_Click" Text="確定編輯" ValidationGroup="OK" />
                            <asp:Button ID="btnOK2" runat="server" BorderColor="Transparent" BorderWidth="0px" CssClass="SmallButton" OnClick="btnOK2_Click" Text="確定" ValidationGroup="OK" />
                            <asp:Button ID="btnBack2" runat="server" BorderColor="Transparent" BorderWidth="0px" CssClass="SmallButton" OnClick="btnBack2_Click" Text="返回" />
                        </td>
                    </tr>
                <tr>
                    <td align="left" style="width: 30%" >
                        &nbsp;</td>
                    <td align="left" >
                        &nbsp;</td>
                </tr>
            </table>

            </div>
        </div>
        </div>
        </div>

        </asp:View>

    </asp:MultiView>

</div>
</div>



</asp:Content>
