﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Manage_RecipientList2.aspx.cs" Inherits="ASEH_CMS.Manage_RecipientList2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Breadcrumb -->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="MainIndex.aspx">Dashboard</a></li>
        <li class="breadcrumb-item active">信箱設定_聯絡我們</li>
      </ol>
    <!-- Breadcrumb Menu-->

<div class="container-fluid">
<div class="animated fadeIn">

  <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">

                <%--控制項--%>
                <table style="width: 100%;" >
                    <tr>
                        <td align="left";">
                              <asp:Label ID="Label1" runat="server" SkinID="MainText" Text="排序"></asp:Label>
                            <asp:DropDownList ID="ddlOrder" runat="server" AutoPostBack="True" Width="35%" OnSelectedIndexChanged="ddlOrder_SelectedIndexChanged">
                                <asp:ListItem Value="CreateDate">建立日期</asp:ListItem>
                                <asp:ListItem Value="RcpName">名稱</asp:ListItem>
                                <asp:ListItem Value="RcpEmail">電子信箱</asp:ListItem>
                                <asp:ListItem Value="RcpEnable">是否啟用</asp:ListItem>
                            </asp:DropDownList>
                             <asp:DropDownList ID="ddlOrderType" runat="server" AutoPostBack="True" Width="15%" OnSelectedIndexChanged="ddlOrderType_SelectedIndexChanged">
                                 <asp:ListItem Value="DESC">遞減</asp:ListItem>
                                 <asp:ListItem Value="ASC">遞增</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="right";">
                            <asp:TextBox ID="txbSearchName" runat="server" MaxLength="50" Width="25%"></asp:TextBox>
                            <asp:Button ID="btnSearch" runat="server" BorderColor="Transparent" BorderWidth="0px" CssClass="SmallButton" Text="查詢" OnClick="btnSearch_Click" />
                        </td>
                    </tr>

                   <tr>
                        <td align="left";">
                            <asp:Button ID="btnAdd" runat="server" BorderColor="Transparent" BorderWidth="0px" CssClass="SmallButton" onclick="btnAdd_Click" Text="新增" />
                        </td>
                        <td align="right";">
                        </td>
                   </tr>
                 </table>

            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> 列表
                </div>

                <div class="card-body">

                <%--列表--%>
                <table style="width: 100%;" >
                    <tr>
                        <td colspan="2">
                            <asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" DataKeyNames="RcpID" onrowcommand="grid_RowCommand" onrowdatabound="grid_RowDataBound" Width="100%">
                                <PagerSettings Visible="False" />
                                <Columns>

                                         <asp:TemplateField HeaderText="項次">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNO" runat="server"></asp:Label>
                                        </ItemTemplate><HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" Width="80px" />
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="CreateDate" DataFormatString="{0:yyyy/MM/dd HH:mm}" HeaderText="建立日期" ReadOnly="True" Visible="false">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" Width="130px" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="RcpName" HeaderText="名稱" ReadOnly="True">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="RcpEmail" HeaderText="電子信箱" ReadOnly="True" >
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>

                                    <asp:TemplateField HeaderText="是否啟用" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblIsEnable" runat="server" Text='<%# Eval("RcpEnable") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="修改">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("RcpID") %>' CommandName="Modify" ImageUrl="Images/Edit.png" ToolTip="修改" />
                                        </ItemTemplate>
                                        <HeaderStyle />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="刪除">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnDel" runat="server" CausesValidation="False" CommandArgument='<%# Eval("RcpID") %>' CommandName="Remove" ImageUrl="Images/Delete.png" onclientclick="return window.confirm('確定要刪除嗎？');" ToolTip="刪除" />
                                        </ItemTemplate>
                                        <HeaderStyle />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;</td>
                    </tr>
                </table>

                </div>
            </div>



        </asp:View>
        <asp:View ID="View2" runat="server">

        <div class="row">
        <div class="col-lg-12">
        <div class="card">
                <div class="card-header">
                <i class="fa fa-align-justify"></i> 新增/編輯
                </div>

            <div class="card-body">

            <table style="width:100%;">

                <tr>
                    <td align="center" colspan="2">
                        &nbsp;</td>
                </tr>

                <tr>
                    <td align="left" style="width: 30%" >
                        <asp:Label ID="lblName" runat="server" SkinID="MainText" Text="名稱"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbRcpName2" runat="server" MaxLength="50" Width="40%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txbRcpName2" ErrorMessage="RequiredFieldValidator" SkinID="MainWarn" ValidationGroup="OK">名稱不可為空</asp:RequiredFieldValidator>
                    </td>
                </tr>

                <tr>
                    <td align="left" style="width: 30%" >&nbsp;</td>
                    <td align="left" style="width: 70%">&nbsp;</td>
                </tr>

                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="lblUseID" runat="server" SkinID="MainText" Text="電子信箱"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <span>
                        <asp:Label ID="lblUseID2" runat="server" SkinID="MainAlert">(請輸入E-mail格式)</asp:Label>
                        </span>
                        <br />
                        <asp:TextBox ID="txbRcpEmail2" runat="server" MaxLength="300" Width="60%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvUseID" runat="server" 
                        ControlToValidate="txbRcpEmail2" ErrorMessage="RequiredFieldValidator" 
                        SkinID="MainWarn" ValidationGroup="OK">電子信箱不可為空</asp:RequiredFieldValidator>
                    </td>
                </tr>
            
                <tr>
                    <td align="left" style="width: 30%" >
                        &nbsp;</td>
                    <td align="left" style="width: 70%">
                        <asp:RegularExpressionValidator ID="revUseID" runat="server" ControlToValidate="txbRcpEmail2" ErrorMessage="電子信箱輸入錯誤" ForeColor="Red" ValidationExpression="[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" ValidationGroup="OK"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">&nbsp;</td>
                    <td align="left" style="width: 70%">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="lblUseID3" runat="server" SkinID="MainText" Text="備註"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbRcpDesc2" runat="server" MaxLength="300" Width="60%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">&nbsp;</td>
                    <td align="left" style="width: 70%">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%" >
                        <asp:Label ID="lblIsEnable" runat="server" SkinID="MainText" Text="是否啟用"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:CheckBox ID="ckbIsEnable2" runat="server" Checked="True" Text="啟用請打勾" />
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%" >
                        &nbsp;</td>
                    <td align="left" style="width: 70%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">&nbsp;</td>
                    <td align="left" style="width: 70%">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnEdit2" runat="server" BackColor="Transparent" 
                        BorderWidth="0px" CssClass="BigButton" onclick="btnEdit2_Click" Text="確定修改" 
                        ValidationGroup="OK" />
                        <asp:Button ID="btnOK2" runat="server" BackColor="Transparent" BorderWidth="0px" 
                        CssClass="SmallButton" onclick="btnOK2_Click" Text="確定" ValidationGroup="OK" />
                        <asp:Button ID="btnBack2" runat="server" BackColor="Transparent" 
                        BorderWidth="0px" CssClass="SmallButton" onclick="btnBack2_Click" Text="返回" />
                        <asp:HiddenField ID="hideRcpID" runat="server" />
                        <asp:HiddenField ID="hideLoginUseID" runat="server" />
                    </td>
                </tr>
            </table>

            </div>

        </div>
        </div>
        </div>

        </asp:View>

          

        </asp:MultiView>

</div>
</div>
    
</asp:Content>
