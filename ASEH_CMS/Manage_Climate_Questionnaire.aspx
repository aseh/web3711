﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Manage_Climate_Questionnaire.aspx.cs" Inherits="ASEH_CMS.Manage_Climate_Questionnaire" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="MainIndex.aspx">Dashboard</a></li>
        <li class="breadcrumb-item active">氣候變遷與水衝擊程度調查表</li>
    </ol>
    <!-- Breadcrumb Menu-->

    <div class="container-fluid">
        <div class="animated fadeIn">
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                <asp:View ID="View1" runat="server">
                    <div class="row">
                        <div class="col-lg-12">
                            <asp:Button ID="btnExport" runat="server" BackColor="#66CCFF" BorderWidth="0px" OnClick="btnExport_Click" Text="氣候變遷與水衝擊程度調查表匯出" ValidationGroup="OK" />
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </div>
    </div>
</asp:Content>
