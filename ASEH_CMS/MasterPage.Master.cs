﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models;
using Service;
using Service.Interface;
using Service.Misc;
using ASEH_CMS.App_Code;

namespace ASEH_CMS
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        private IUsersService _usersService;
        UserInfo User = new UserInfo();

        protected void Page_Load(object sender, EventArgs e)
        {
            this._usersService = new UsersService();

            if (!this.Page.IsPostBack)
            {
                this.GetUserInfo();
            }
        }

        //取得帳號及角色
        protected void GetUserInfo()
        {
            //載入帳號
            User = (UserInfo)Session["UserInfo"];
            if ( User != null )
            {
                string strUserID = User.UserID;
                string strUserName = "";

                var model = _usersService.GetByID(strUserID);
                strUserName = model.UseName;

                lblUser.Text = string.Format("{0}［{1}］", strUserID, strUserName);
            }
            
        }
    }
}