﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginErr.aspx.cs" Inherits="ASEH_CMS.LoginErr" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="shortcut icon" href="img/favicon.png" />
<title>後端管理系統</title>
</head>
<body style="background-position: top; background-image: url(Images/login_bg.jpg); background-repeat: repeat-x;">
    <form id="form1" runat="server">
    <div id="login">
    
        
        <table style="width:100%;height:200px;">
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>

    <table align="center" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
        <tr>
            <td colspan="2">
                </td>
        </tr>
        <tr>
            <td colspan="2">
                <img alt="" height="83" src="Images/logout_05.gif" width="480" /></td>
        </tr>
        <tr>
            <td rowspan="2">
                <img alt="" height="276" src="Images/logout_02.gif" width="192" /></td>
            <td bgcolor="White">
                <img alt="" 
                    src="Images/no_right_04.gif" width="288" style="height: 51px" /></td>
        </tr>
        <tr>
            <td align="right" height="196" valign="top">
                <p align="left">
                    <font size="2">您沒有回應的時間已經超過系統安全設定</font></p>
                <p align="left">
                    <font size="2">為保障您的資料安全</font></p>
                <p align="left">
                    <font size="2">系統已經將您自動登出。</font></p>
                <p align="left">
                    <font id="FONT1" runat="server" size="2">
                    <asp:LinkButton ID="LinkButtonRetLogin" runat="server" Font-Size="X-Small" 
                        onclick="LinkButtonRetLogin_Click" style="font-size: small" Width="136px">請按一下這裡重新登入</asp:LinkButton>
                    <a href="#"></a>
                </p>
                <p>
                    &nbsp;</p>
                <p>
                    &nbsp;</p>

            </td>
        </tr>
        <tr>
            <td colspan="2">
                <img alt="" height="83" src="Images/logout_05.gif" width="480" /></td>
        </tr>
    </table>
    </font>
    
    </div>
    
    </form>
</body>
</html>
