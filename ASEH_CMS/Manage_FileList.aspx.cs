﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models;
using Service;
using Service.Interface;
using Service.Misc;
using ASEH_CMS.App_Code;
using System.Web.Configuration;
using System.IO;

namespace ASEH_CMS
{
    public partial class Manage_FileList : PageBase
    {
        private IExceptionRecordService _exceptionRecordService;
        private IFileService _fileService;
        private IFileLogService _fileLogService;
        private CommonClass _common = new CommonClass();
        UserInfo User = new UserInfo();
        new protected int iPageType = 1;
        new protected string strPageID = "M51";

        //起始載入
        protected void Page_Load(object sender, EventArgs e)
        {
            this._exceptionRecordService = new ExceptionRecordService();
            this._fileService = new FileService();
            this._fileLogService = new FileLogService();

            CheckUser();
            CheckUserPermission(strPageID, iPageType);

            if (!this.Page.IsPostBack)
            {
                //取得登入者ID
                User = (UserInfo)Session["UserInfo"];
                hideLoginUseID.Value = User.UserID;

                //載入內容
                SetPageContent("FIT001");
            }
        }

        //返回的動作
        protected void btnBack_Click(object sender, EventArgs e)
        {
            this.MultiView1.ActiveViewIndex = 0;
        }

        //新增按鈕的動作
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //上架日期
            txbOpenDate2.Text = DateTime.Now.ToString("yyyy/MM/dd");

            txbFieName2.Text = "";
            txbFieName_EN2.Text = "";
            txbFieDesc2.Text = "";
            txbFieDesc_EN2.Text = "";

            ckbIsEnable.Checked = true;

            #region 檔案

            lblFieFilePath2.Text = "請選擇檔案";
            lblFieFilePath_EN2.Text = "請選擇檔案";

            #endregion

            #region 圖片

            imgFiePicPath2.ImageUrl = "Images/default.jpg";
            imgFiePicPath_EN2.ImageUrl = "Images/default.jpg";

            #endregion

            //圖片描述
            txbFiePicDesc2.Text = "";

            this.btnOK.Visible = true;
            this.btnEdit.Visible = false;
            this.MultiView1.ActiveViewIndex = 1;
        }

        //新增儲存動作
        protected void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new tbFile();
                string FieID = Guid.NewGuid().ToString();
                model.FieID = FieID;
                model.FitID = "FIT001";

                model.OpenDate = Convert.ToDateTime(txbOpenDate2.Text);
                model.FieYear = Convert.ToDateTime(txbOpenDate2.Text).Year;
                model.FieMonth = Convert.ToDateTime(txbOpenDate2.Text).Month;
                model.FieDay = Convert.ToDateTime(txbOpenDate2.Text).Day;

                model.FieName = txbFieName2.Text;
                model.FieName_EN = txbFieName_EN2.Text;
                model.FieDesc = txbFieDesc2.Text;
                model.FieDesc_EN = txbFieDesc_EN2.Text;
                model.FieEnable = _common.GetChecked(ckbIsEnable);

                model.CreateUser = hideLoginUseID.Value;
                model.CreateDate = DateTime.Now;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;

                model.FieView = 0;
                model.FieDownload = 0;

                #region 取得排序碼

                List<tbFile> list = _fileService.GetAll().OrderBy(o => o.FieSort).ToList();
                if (list.Count != 0)
                {
                    int SortTemp = Convert.ToInt32(list[list.Count - 1].FieSort) + 1;

                    model.FieSort = SortTemp;
                }
                else
                {
                    model.FieSort = 1;
                }

                #endregion

                #region 檔案

                string newFileName = "";
                string newFileName_EN = "";

                //上傳檔案
                bool FileCheck = true;
                if (uploadFieFilePath2.HasFile)
                {
                    try
                    {
                        string FileName = uploadFieFilePath2.FileName;
                        string FileExtension = System.IO.Path.GetExtension(uploadFieFilePath2.FileName).ToLower();
                        string Name = Path.GetFileNameWithoutExtension(FileName);
                        //不轉換
                        newFileName = FileName;
                        string FolderPath = string.Format("Uploads/{0}/", FieID);
                        string RealFolderPath = Server.MapPath(FolderPath);

                        int returnCode = _common.UploadAnyFile(uploadFieFilePath2, RealFolderPath);
                        FileCheck = true;

                        ////取得副檔名
                        //FieExtension = FileExtension;
                        ////取得尺寸
                        //FieSize = uploadFieFilePath2.PostedFile.ContentLength.ToString();
                    }
                    catch (Exception ex)
                    {
                        FileCheck = false;
                    }
                }

                //上傳檔案(EN)
                bool FileCheck_EN = true;
                if (uploadFieFilePath_EN2.HasFile)
                {
                    try
                    {
                        string FileName = uploadFieFilePath_EN2.FileName;
                        string FileExtension = System.IO.Path.GetExtension(uploadFieFilePath_EN2.FileName).ToLower();
                        string Name = Path.GetFileNameWithoutExtension(FileName);
                        //不轉換
                        newFileName_EN = FileName;
                        string FolderPath = string.Format("Uploads/{0}/", FieID);
                        string RealFolderPath = Server.MapPath(FolderPath);

                        int returnCode = _common.UploadAnyFile(uploadFieFilePath_EN2, RealFolderPath);
                        FileCheck_EN = true;

                        ////取得副檔名
                        //FieExtension = FileExtension;
                        ////取得尺寸
                        //FieSize = uploadFieFilePath_EN2.PostedFile.ContentLength.ToString();
                    }
                    catch (Exception ex)
                    {
                        FileCheck_EN = false;
                    }
                }

                #endregion

                #region 圖片

                string newPicPathFileName = "";
                string newPicPathFileName_EN = "";

                //上傳圖片
                bool PicCheck = true;
                if (uploadFiePicPath2.HasFile)
                {
                    Boolean FileOK = false;

                    string fileExtension = System.IO.Path.GetExtension(uploadFiePicPath2.FileName).ToLower();
                    string[] allowExtensions = { ".bmp", ".jpg", ".jpeg", ".png" };
                    for (int i = 0; i < allowExtensions.Length; i++)
                    {
                        if (fileExtension == allowExtensions[i])
                        {
                            FileOK = true;
                        }
                    }

                    if (FileOK)
                    {
                        //PNG特別處理
                        string FileNameOrg = uploadFiePicPath2.FileName;
                        string Name = Path.GetFileNameWithoutExtension(FileNameOrg);
                        string Extension = Path.GetExtension(FileNameOrg);

                        if (Extension == ".png")
                        {
                            string FileName = Guid.NewGuid().ToString();
                            newPicPathFileName = Path.ChangeExtension(FileName, ".png");
                            string FolderPath = string.Format("Uploads/{0}/", FieID);
                            string RealFolderPath = Server.MapPath(FolderPath);

                            int returnCode = _common.UploadAnyFileWithNewName(uploadFiePicPath2, RealFolderPath, FileName);
                        }
                        else
                        {
                            string FileName = Guid.NewGuid().ToString();
                            newPicPathFileName = Path.ChangeExtension(FileName, ".jpg");
                            string FolderPath = string.Format("Uploads/{0}/", FieID);
                            string RealFolderPath = Server.MapPath(FolderPath);

                            int returnCode = _common.UploadOrgJpgPicWithNewName(uploadFiePicPath2, RealFolderPath, FileName);
                        }

                        PicCheck = true;
                    }
                    else
                    {
                        PicCheck = false;
                    }
                }

                //上傳圖片(EN)
                bool PicCheck_EN = true;
                if (uploadFiePicPath_EN2.HasFile)
                {
                    Boolean FileOK = false;

                    string fileExtension = System.IO.Path.GetExtension(uploadFiePicPath_EN2.FileName).ToLower();
                    string[] allowExtensions = { ".bmp", ".jpg", ".jpeg", ".png" };
                    for (int i = 0; i < allowExtensions.Length; i++)
                    {
                        if (fileExtension == allowExtensions[i])
                        {
                            FileOK = true;
                        }
                    }

                    if (FileOK)
                    {
                        //PNG特別處理
                        string FileNameOrg = uploadFiePicPath_EN2.FileName;
                        string Name = Path.GetFileNameWithoutExtension(FileNameOrg);
                        string Extension = Path.GetExtension(FileNameOrg);

                        if (Extension == ".png")
                        {
                            string FileName = Guid.NewGuid().ToString();
                            newPicPathFileName_EN = Path.ChangeExtension(FileName, ".png");
                            string FolderPath = string.Format("Uploads/{0}/", FieID);
                            string RealFolderPath = Server.MapPath(FolderPath);

                            int returnCode = _common.UploadAnyFileWithNewName(uploadFiePicPath_EN2, RealFolderPath, FileName);
                        }
                        else
                        {
                            string FileName = Guid.NewGuid().ToString();
                            newPicPathFileName_EN = Path.ChangeExtension(FileName, ".jpg");
                            string FolderPath = string.Format("Uploads/{0}/", FieID);
                            string RealFolderPath = Server.MapPath(FolderPath);

                            int returnCode = _common.UploadOrgJpgPicWithNewName(uploadFiePicPath_EN2, RealFolderPath, FileName);
                        }

                        PicCheck_EN = true;
                    }
                    else
                    {
                        PicCheck_EN = false;
                    }
                }

                if (PicCheck != true && PicCheck_EN != true)
                {
                    ShowMessage("圖片上傳失敗，請確認檔案格式是否正確");
                }

                #endregion

                model.FieFilePath = newFileName;
                model.FieFilePath_EN = newFileName_EN;
                model.FiePicPath = newPicPathFileName;
                model.FiePicPath_EN = newPicPathFileName_EN;

                _fileService.Create(model);

                //ShowMessage("儲存成功");

                //更新列表
                SetPageContent("FIT001");

                this.MultiView1.ActiveViewIndex = 0;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        //確定修改動作
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                string FieID = hideID.Value;

                var model = _fileService.GetByID(hideID.Value);

                model.OpenDate = Convert.ToDateTime(txbOpenDate2.Text);
                model.FieYear = Convert.ToDateTime(txbOpenDate2.Text).Year;
                model.FieMonth = Convert.ToDateTime(txbOpenDate2.Text).Month;
                model.FieDay = Convert.ToDateTime(txbOpenDate2.Text).Day;

                model.FieName = txbFieName2.Text;
                model.FieName_EN = txbFieName_EN2.Text;
                model.FieDesc = txbFieDesc2.Text;
                model.FieDesc_EN = txbFieDesc_EN2.Text;
                model.FieEnable = _common.GetChecked(ckbIsEnable);

                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;

                #region 檔案

                string newFileName = "";
                string newFileName_EN = "";

                //上傳檔案
                bool FileCheck = true;
                if (uploadFieFilePath2.HasFile)
                {
                    try
                    {
                        string FileName = uploadFieFilePath2.FileName;
                        string FileExtension = System.IO.Path.GetExtension(uploadFieFilePath2.FileName).ToLower();
                        string Name = Path.GetFileNameWithoutExtension(FileName);
                        //不轉換
                        newFileName = FileName;
                        string FolderPath = string.Format("Uploads/{0}/", FieID);
                        string RealFolderPath = Server.MapPath(FolderPath);

                        int returnCode = _common.UploadAnyFile(uploadFieFilePath2, RealFolderPath);
                        FileCheck = true;

                        ////取得副檔名
                        //FieExtension = FileExtension;
                        ////取得尺寸
                        //FieSize = uploadFieFilePath2.PostedFile.ContentLength.ToString();
                    }
                    catch (Exception ex)
                    {
                        FileCheck = false;
                    }
                }

                //上傳檔案(EN)
                bool FileCheck_EN = true;
                if (uploadFieFilePath_EN2.HasFile)
                {
                    try
                    {
                        string FileName = uploadFieFilePath_EN2.FileName;
                        string FileExtension = System.IO.Path.GetExtension(uploadFieFilePath_EN2.FileName).ToLower();
                        string Name = Path.GetFileNameWithoutExtension(FileName);
                        //不轉換
                        newFileName_EN = FileName;
                        string FolderPath = string.Format("Uploads/{0}/", FieID);
                        string RealFolderPath = Server.MapPath(FolderPath);

                        int returnCode = _common.UploadAnyFile(uploadFieFilePath_EN2, RealFolderPath);
                        FileCheck_EN = true;

                        ////取得副檔名
                        //FieExtension = FileExtension;
                        ////取得尺寸
                        //FieSize = uploadFieFilePath_EN2.PostedFile.ContentLength.ToString();
                    }
                    catch (Exception ex)
                    {
                        FileCheck_EN = false;
                    }
                }

                #endregion

                #region 圖片

                string newPicPathFileName = "";
                string newPicPathFileName_EN = "";

                //上傳圖片
                bool PicCheck = true;
                if (uploadFiePicPath2.HasFile)
                {
                    Boolean FileOK = false;

                    string fileExtension = System.IO.Path.GetExtension(uploadFiePicPath2.FileName).ToLower();
                    string[] allowExtensions = { ".bmp", ".jpg", ".jpeg", ".png" };
                    for (int i = 0; i < allowExtensions.Length; i++)
                    {
                        if (fileExtension == allowExtensions[i])
                        {
                            FileOK = true;
                        }
                    }

                    if (FileOK)
                    {
                        //PNG特別處理
                        string FileNameOrg = uploadFiePicPath2.FileName;
                        string Name = Path.GetFileNameWithoutExtension(FileNameOrg);
                        string Extension = Path.GetExtension(FileNameOrg);

                        if (Extension == ".png")
                        {
                            string FileName = Guid.NewGuid().ToString();
                            newPicPathFileName = Path.ChangeExtension(FileName, ".png");
                            string FolderPath = string.Format("Uploads/{0}/", FieID);
                            string RealFolderPath = Server.MapPath(FolderPath);

                            int returnCode = _common.UploadAnyFileWithNewName(uploadFiePicPath2, RealFolderPath, FileName);
                        }
                        else
                        {
                            string FileName = Guid.NewGuid().ToString();
                            newPicPathFileName = Path.ChangeExtension(FileName, ".jpg");
                            string FolderPath = string.Format("Uploads/{0}/", FieID);
                            string RealFolderPath = Server.MapPath(FolderPath);

                            int returnCode = _common.UploadOrgJpgPicWithNewName(uploadFiePicPath2, RealFolderPath, FileName);
                        }

                        PicCheck = true;
                    }
                    else
                    {
                        PicCheck = false;
                    }
                }

                //上傳圖片(EN)
                bool PicCheck_EN = true;
                if (uploadFiePicPath_EN2.HasFile)
                {
                    Boolean FileOK = false;

                    string fileExtension = System.IO.Path.GetExtension(uploadFiePicPath_EN2.FileName).ToLower();
                    string[] allowExtensions = { ".bmp", ".jpg", ".jpeg", ".png" };
                    for (int i = 0; i < allowExtensions.Length; i++)
                    {
                        if (fileExtension == allowExtensions[i])
                        {
                            FileOK = true;
                        }
                    }

                    if (FileOK)
                    {
                        //PNG特別處理
                        string FileNameOrg = uploadFiePicPath_EN2.FileName;
                        string Name = Path.GetFileNameWithoutExtension(FileNameOrg);
                        string Extension = Path.GetExtension(FileNameOrg);

                        if (Extension == ".png")
                        {
                            string FileName = Guid.NewGuid().ToString();
                            newPicPathFileName_EN = Path.ChangeExtension(FileName, ".png");
                            string FolderPath = string.Format("Uploads/{0}/", FieID);
                            string RealFolderPath = Server.MapPath(FolderPath);

                            int returnCode = _common.UploadAnyFileWithNewName(uploadFiePicPath_EN2, RealFolderPath, FileName);
                        }
                        else
                        {
                            string FileName = Guid.NewGuid().ToString();
                            newPicPathFileName_EN = Path.ChangeExtension(FileName, ".jpg");
                            string FolderPath = string.Format("Uploads/{0}/", FieID);
                            string RealFolderPath = Server.MapPath(FolderPath);

                            int returnCode = _common.UploadOrgJpgPicWithNewName(uploadFiePicPath_EN2, RealFolderPath, FileName);
                        }

                        PicCheck_EN = true;
                    }
                    else
                    {
                        PicCheck_EN = false;
                    }
                }

                if (PicCheck != true && PicCheck_EN != true)
                {
                    ShowMessage("圖片上傳失敗，請確認檔案格式是否正確");
                }

                #endregion

                //檔案判斷
                if (uploadFieFilePath2.FileName != "")
                {
                    model.FieFilePath = newFileName;
                }
                else
                {
                    model.FieFilePath = hideFieFilePath2.Value;
                }

                //檔案判斷(EN)
                if (uploadFieFilePath_EN2.FileName != "")
                {
                    model.FieFilePath_EN = newFileName_EN;
                }
                else
                {
                    model.FieFilePath_EN = hideFieFilePath_EN2.Value;
                }

                //圖片判斷
                if (uploadFiePicPath2.FileName != "")
                {
                    model.FiePicPath = newPicPathFileName;
                }
                else
                {
                    model.FiePicPath = hideFiePicPath2.Value;
                }

                //圖片判斷(EN)
                if (uploadFiePicPath_EN2.FileName != "")
                {
                    model.FiePicPath_EN = newPicPathFileName_EN;
                }
                else
                {
                    model.FiePicPath_EN = hideFiePicPath_EN2.Value;
                }

                _fileService.Update(model);

                //ShowMessage("儲存成功");

                //更新列表
                SetPageContent("FIT001");

                this.MultiView1.ActiveViewIndex = 0;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        #region GridView動作

        protected void grid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "SortUp":
                    {
                        //FieID
                        string FieID = e.CommandArgument.ToString().Split(',')[0].Trim();
                        //FieSort
                        string FieSort = e.CommandArgument.ToString().Split(',')[1].Trim();

                        if (FieSort != "1")
                        {
                            List<tbFile> list = _fileService.GetAll().OrderBy(o => o.FieSort).ToList();
                            if (list.Count != 0)
                            {
                                string nowFieID = FieID;
                                string nowFieSort = FieSort;

                                #region 找出上一個ROWS

                                string tempFieSort = (Convert.ToInt32(FieSort) - 1).ToString();

                                var model = list.Where(x => x.FieSort == Convert.ToInt32(tempFieSort)).First();
                                var upFieID = model.FieID;
                                var upFieSort = model.FieSort;

                                #endregion

                                //更新排序狀態(互換)
                                var modelNow = _fileService.GetByID(nowFieID);
                                modelNow.FieSort = Convert.ToInt32(nowFieSort) - 1;
                                _fileService.Update(modelNow);

                                var modelUp = _fileService.GetByID(upFieID);
                                modelUp.FieSort = Convert.ToInt32(upFieSort) + 1;
                                _fileService.Update(modelUp);

                                //更新列表
                                SetPageContent("FIT001");
                            }
                        }
                        else
                        {
                            ShowMessage("已經是第一筆紀錄");
                        }

                        break;
                    }

                case "SortDown":
                    {
                        //FieID
                        string FieID = e.CommandArgument.ToString().Split(',')[0].Trim();
                        //FieSort
                        string FieSort = e.CommandArgument.ToString().Split(',')[1].Trim();

                        List<tbFile> list = _fileService.GetAll().OrderBy(o => o.FieSort).ToList();
                        if (FieSort != list.Count.ToString())
                        {
                            if (list.Count != 0)
                            {
                                string nowFieID = FieID;
                                string nowFieSort = FieSort;

                                #region 找出下一個ROWS

                                string tempFieSort = (Convert.ToInt32(FieSort) + 1).ToString();

                                var model = list.Where(x => x.FieSort == Convert.ToInt32(tempFieSort)).First();
                                var downFieID = model.FieID;
                                var downFieSort = model.FieSort;

                                #endregion

                                //更新排序狀態(互換)
                                var modelNow = _fileService.GetByID(nowFieID);
                                modelNow.FieSort = Convert.ToInt32(nowFieSort) + 1;
                                _fileService.Update(modelNow);

                                var modelDown = _fileService.GetByID(downFieID);
                                modelDown.FieSort = Convert.ToInt32(downFieSort) - 1;
                                _fileService.Update(modelDown);

                                //更新列表
                                SetPageContent("FIT001");
                            }
                        }
                        else
                        {
                            ShowMessage("已經是最後一筆紀錄");
                        }

                        break;
                    }

                case "Modify":
                    {
                        //ID
                        string FieID = e.CommandArgument.ToString();
                        //暫存
                        hideID.Value = FieID;

                        var model = _fileService.GetByID(FieID);

                        //上架日期
                        txbOpenDate2.Text = Convert.ToDateTime(model.OpenDate).ToString("yyyy/MM/dd");

                        txbFieName2.Text = model.FieName;
                        txbFieName_EN2.Text = model.FieName_EN;
                        txbFieDesc2.Text = model.FieDesc;
                        txbFieDesc_EN2.Text = model.FieDesc_EN;
                      
                        _common.SetChecked(ckbIsEnable, Convert.ToInt32(model.FieEnable));

                        #region 檔案

                        lblFieFilePath2.Text = model.FieFilePath;
                        hideFieFilePath2.Value = model.FieFilePath;

                        lblFieFilePath_EN2.Text = model.FieFilePath_EN;
                        hideFieFilePath_EN2.Value = model.FieFilePath_EN;

                        #endregion

                        #region 圖片

                        //圖片
                        if (model.FiePicPath != null)
                        {
                            imgFiePicPath2.ImageUrl = string.Format("Uploads/{0}/{1}"
                                                                  , model.FieID.ToString()
                                                                  , model.FiePicPath);
                        }
                        else
                        {
                            imgFiePicPath2.ImageUrl = "Images/default.jpg";
                        }
                        hideFiePicPath2.Value = model.FiePicPath;

                        //圖片(EN)
                        if (model.FiePicPath_EN != null)
                        {
                            imgFiePicPath_EN2.ImageUrl = string.Format("Uploads/{0}/{1}"
                                                                  , model.FieID.ToString()
                                                                  , model.FiePicPath_EN);
                        }
                        else
                        {
                            imgFiePicPath_EN2.ImageUrl = "Images/default.jpg";
                        }
                        hideFiePicPath_EN2.Value = model.FiePicPath_EN;

                        #endregion

                        lblMeg.Text = "";

                        this.btnOK.Visible = false;
                        this.btnEdit.Visible = true;
                        this.MultiView1.ActiveViewIndex = 1;

                        break;
                    }

                case "Remove":
                    {
                        //FieID
                        string FieID = e.CommandArgument.ToString();
                 
                        //刪除
                        _fileService.Delete(FieID);

                        //刪除後重新排序
                        List<tbFile> list = _fileService.GetAll().OrderBy(o => o.FieSort).ToList();
                        int i = 0;
                        foreach (tbFile element in list)
                        {
                            var model = _fileService.GetByID(element.FieID);
                            model.FieSort = i + 1;
                            _fileService.Update(model);

                            i++;
                        }

                        //同時移除FileLog
                        List<tbFileLog> listL = _fileLogService.GetAll().Where(x => x.FIeID == FieID).ToList();
                        foreach (tbFileLog element in listL)
                        {
                            //刪除
                            _fileLogService.Delete(element.FigID);
                        }

                        //更新列表
                        SetPageContent("FIT001");

                        break;
                    }

                default:
                    {
                        return;
                    }
            }
        }

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Image imgPic = (Image)e.Row.FindControl("imgPic");
            if (imgPic != null)
            {
                string Temp = imgPic.AlternateText;

                //FieID
                string FieID = Temp.Split(',')[0].Trim();
                //FiePicPath
                string FiePicPath = Temp.Split(',')[1].Trim();

                if (FiePicPath != "")
                {
                    imgPic.ImageUrl = string.Format("Uploads/{0}/{1}"
                                                    , FieID
                                                    , FiePicPath);
                }
                else
                {
                    imgPic.ImageUrl = "Images/default.jpg";
                }
            }

            //是否啟用
            Label newlblIsEnable = (Label)e.Row.FindControl("lblIsEnable");
            if (newlblIsEnable != null)
            {
                if (int.Parse(newlblIsEnable.Text) == 1)
                {
                    Label lblEnable = _common.ChangeIsEnable(1);
                    newlblIsEnable.Text = lblEnable.Text;
                }
                else if (int.Parse(newlblIsEnable.Text) == 0)
                {
                    Label lblEnable = _common.ChangeIsEnable(0);
                    newlblIsEnable.Text = lblEnable.Text;
                    newlblIsEnable.ForeColor = lblEnable.ForeColor;
                }
            }
        }

        #endregion

        #region 方法

        //載入內容
        protected void SetPageContent(string FitID)
        {
            //取得類別
            List<tbFile> list = _fileService.GetAll().OrderByDescending(o => o.OpenDate).ToList();

            //#region 排序設定

            //string OrderBy = ddlOrder.SelectedValue;

            //if (ddlOrderType.SelectedValue == "DESC")
            //{
            //    switch (OrderBy)
            //    {
            //        case "CreateDate"://發布日期
            //            {
            //                listOld = listOld.OrderByDescending(o => o.AtcOpenDate).ToList();
            //                break;
            //            }
            //        case "AtcName":
            //            {
            //                listOld = listOld.OrderByDescending(o => o.AtcName).ToList();
            //                break;
            //            }
            //        case "AtcAuthor":
            //            {
            //                listOld = listOld.OrderByDescending(o => o.AtcAuthor).ToList();
            //                break;
            //            }
            //    }
            //}
            //else
            //{
            //    switch (OrderBy)
            //    {
            //        case "CreateDate":
            //            {
            //                listOld = listOld.OrderBy(o => o.CreateDate).ToList();
            //                break;
            //            }
            //        case "AtcName":
            //            {
            //                listOld = listOld.OrderBy(o => o.AtcName).ToList();
            //                break;
            //            }
            //        case "AtcAuthor":
            //            {
            //                listOld = listOld.OrderBy(o => o.AtcAuthor).ToList();
            //                break;
            //            }
            //    }
            //}

            //#endregion

            grid.DataSource = list;
            grid.DataBind();
        }

        #endregion

       

        
    }
}