﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Manage_Report_Questionnaire2.aspx.cs" Inherits="ASEH_CMS.Manage_Report_Questionnaire2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="MainIndex.aspx">Dashboard</a></li>
        <li class="breadcrumb-item active">CSR 問卷報告管理</li>
    </ol>
    <!-- Breadcrumb Menu-->

    <div class="container-fluid">
        <div class="animated fadeIn">
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                <asp:View ID="View1" runat="server">
                    <div class="row">
                        <div class="col-lg-12">
                            <%--控制項--%>
                            <table style="width: 100%;">
                                <tr>
                                    <td align="left">
                                    </td>
                                    <td align="right" style="width: 20%;">
                                        <asp:Button ID="btnExport" runat="server" BackColor="Transparent" BorderWidth="0px" CssClass="BigButton" OnClick="btnExport_Click" Text="CSR 問卷匯出" ValidationGroup="OK" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </div>
    </div>
</asp:Content>
