﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ASEH_CMS.App_Code;
using System.Net;

namespace ASEH_CMS
{
    public partial class MainIndex : PageBase
    {
        private CommonClass _common = new CommonClass();

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckUser();

            //進入招呼語
            lblYourIP.Text = string.Format("Your IP:{0}", GetIP());
            lblLoginTime.Text = string.Format("Login Time:{0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

        }

        #region 

        protected string GetIP()
        {
            string UIP = "";

            // 取得本機名稱
            string strHostName = Dns.GetHostName();
            // 取得本機的IpHostEntry類別實體，用這個會提示已過時
            //IPHostEntry iphostentry = Dns.GetHostByName(strHostName);

            // 取得本機的IpHostEntry類別實體，MSDN建議新的用法
            IPHostEntry iphostentry = Dns.GetHostEntry(strHostName);

            // 取得所有 IP 位址
            foreach (IPAddress ipaddress in iphostentry.AddressList)
            {
                // 只取得IP V4的Address
                if (ipaddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    UIP = ipaddress.ToString();
                }
            }

            return UIP;
        }

        #endregion
    }
}