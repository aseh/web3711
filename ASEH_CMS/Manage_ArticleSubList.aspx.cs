﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models;
using Service;
using Service.Interface;
using Service.Misc;
using ASEH_CMS.App_Code;
using System.Web.Configuration;
using System.IO;

namespace ASEH_CMS
{
    public partial class Manage_ArticleSubList : PageBase
    {
        private IExceptionRecordService _exceptionRecordService;
        private IArticleService _articleService;
        private IArticleSubService _articleSubService;
        private CommonClass _common = new CommonClass();
        UserInfo User = new UserInfo();

        new protected int iPageType = 1;
        new protected string strPageID = "M21";

        //起始載入
        protected void Page_Load(object sender, EventArgs e)
        {
            this._exceptionRecordService = new ExceptionRecordService();
            this._articleService = new ArticleService();
            this._articleSubService = new ArticleSubService();

            CheckUser();
            CheckUserPermission(strPageID, iPageType);

            if (!this.Page.IsPostBack)
            {
                //取得登入者ID
                User = (UserInfo)Session["UserInfo"];
                hideLoginUseID.Value = User.UserID;

                //載入內容
                SetContent();
            }
        }

        //新增按鈕的動作
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //清空
            txbSubName2.Text = "";
            txbSubName_EN2.Text = "";
            //txbSubDesc2.Text = "";
            ckbSubEnable2.Checked = true;

            this.btnOK2.Visible = true;
            this.btnEdit2.Visible = false;
            this.MultiView1.ActiveViewIndex = 1;
        }

        //返回的動作
        protected void btnBack2_Click(object sender, EventArgs e)
        {
            this.MultiView1.ActiveViewIndex = 0;
        }

        //新增存檔
        protected void btnOK2_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new tbArticleSub();
                string SubID = Guid.NewGuid().ToString();
                model.SubID = SubID;
                model.SubName = txbSubName2.Text;
                model.SubName_EN = txbSubName_EN2.Text;
                //產業新知
                model.SubDesc = "AT001";
                model.SubEnable = _common.GetChecked(ckbSubEnable2);
                model.SubSort = grid.Rows.Count + 1;
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;

                _articleSubService.Create(model);

                //ShowMessage("儲存成功");

                SetContent();

                this.MultiView1.ActiveViewIndex = 0;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        //修改存檔
        protected void btnEdit2_Click(object sender, EventArgs e)
        {
            try
            {
                var model = _articleSubService.GetByID(hideID.Value);
                model.SubName = txbSubName2.Text;
                model.SubName_EN = txbSubName_EN2.Text;
                //產業新知
                model.SubDesc = "AT001";
                model.SubEnable = _common.GetChecked(ckbSubEnable2);
                model.SubSort = Convert.ToInt32(hideSort.Value);
                model.LastUpdateUser = hideLoginUseID.Value;
                model.LastUpdateDate = DateTime.Now;

                _articleSubService.Update(model);

                //ShowMessage("儲存成功");

                SetContent();

                this.MultiView1.ActiveViewIndex = 0;
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }
        }

        #region GridView動作

        protected void grid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Modify":
                    {
                        //ID
                        string SubID = e.CommandArgument.ToString();
                        //暫存
                        hideID.Value = SubID;

                        var model = _articleSubService.GetByID(SubID);
                        txbSubName2.Text = model.SubName;
                        txbSubName_EN2.Text = model.SubName_EN;
                        //產業新知
                        txbSubDesc2.Text = "AT001";
                        _common.SetChecked(ckbSubEnable2, Convert.ToInt32(model.SubEnable));
                        hideSort.Value = model.SubSort.ToString();

                        lblMeg2.Text = "";

                        this.btnOK2.Visible = false;
                        this.btnEdit2.Visible = true;
                        this.MultiView1.ActiveViewIndex = 1;

                        break;
                    }
                case "Remove":
                    {
                        string SubID = e.CommandArgument.ToString();

                        //檢查是否有文章
                        List<tbArticle> listA = _articleService.GetAll().Where(x => x.AtcSubID == SubID).ToList();
                        int ACount = listA.Count;

                        if (ACount == 0)
                        {
                            //刪除
                            _articleSubService.Delete(SubID);

                            //刪除後重新排序
                            List<tbArticleSub> list = _articleSubService.GetAll().Where(x => x.SubDesc == "AT001").OrderBy(x => x.SubSort).ToList();
                            int i = 0;
                            foreach (tbArticleSub element in list)
                            {
                                var model = _articleSubService.GetByID(element.SubID);
                                model.SubSort = i + 1;
                                _articleSubService.Update(model);

                                i++;
                            }

                            //載入內容
                            SetContent();
                        }
                        else
                        {
                            ShowMessage("分類下有文章無法刪除");
                        }
                     
                        break;
                    }
                case "SortUp":
                    {
                        string SubID = e.CommandArgument.ToString().Split(',')[0].Trim();
                        string SubSort = e.CommandArgument.ToString().Split(',')[1].Trim();

                        if (SubSort != "1")
                        {
                            List<tbArticleSub> list = _articleSubService.GetAll().Where(x => x.SubDesc == "AT001").OrderBy(x => x.SubSort).ToList();
                            if (list.Count != 0)
                            {
                                string nowSubID = SubID;
                                string nowSubSort = SubSort;

                                #region 找出上一個ROWS

                                string tempSubSort = (Convert.ToInt32(SubSort) - 1).ToString();

                                var model = list.Where(x => x.SubSort == Convert.ToInt32(tempSubSort)).First();
                                var upSubID = model.SubID;
                                var upSubSort = model.SubSort;

                                #endregion

                                //更新排序狀態(互換)
                                var modelNow = _articleSubService.GetByID(nowSubID);
                                modelNow.SubSort = Convert.ToInt32(nowSubSort) - 1;
                                _articleSubService.Update(modelNow);

                                var modelUp = _articleSubService.GetByID(upSubID);
                                modelUp.SubSort = Convert.ToInt32(upSubSort) + 1;
                                _articleSubService.Update(modelUp);

                                SetContent();
                            }
                        }
                        else
                        {
                            ShowMessage("已經是第一筆紀錄");
                        }

                        break;
                    }

                case "SortDown":
                    {
                        string SubID = e.CommandArgument.ToString().Split(',')[0].Trim();
                        string SubSort = e.CommandArgument.ToString().Split(',')[1].Trim();

                        List<tbArticleSub> list = _articleSubService.GetAll().Where(x => x.SubDesc == "AT001").OrderBy(x => x.SubSort).ToList();
                        if (SubSort != list.Count.ToString())
                        {
                            if (list.Count != 0)
                            {
                                string nowSubID = SubID;
                                string nowSubSort = SubSort;

                                #region 找出下一個ROWS

                                string tempSubSort = (Convert.ToInt32(SubSort) + 1).ToString();

                                var model = list.Where(x => x.SubSort == Convert.ToInt32(tempSubSort)).First();
                                var downSubID = model.SubID;
                                var downSubSort = model.SubSort;

                                #endregion

                                //更新排序狀態(互換)
                                var modelNow = _articleSubService.GetByID(nowSubID);
                                modelNow.SubSort = Convert.ToInt32(nowSubSort) + 1;
                                _articleSubService.Update(modelNow);

                                var modelDown = _articleSubService.GetByID(downSubID);
                                modelDown.SubSort = Convert.ToInt32(downSubSort) - 1;
                                _articleSubService.Update(modelDown);

                                SetContent();
                            }
                        }
                        else
                        {
                            ShowMessage("已經是最後一筆紀錄");
                        }

                        break;
                    }
                default:
                    {
                        return;
                    }
            }
        }

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //是否啟用
            Label newlblIsEnable = (Label)e.Row.FindControl("lblIsEnable");
            if (newlblIsEnable != null)
            {
                if (int.Parse(newlblIsEnable.Text) == 1)
                {
                    Label lblEnable = _common.ChangeIsEnable(1);
                    newlblIsEnable.Text = lblEnable.Text;
                }
                else if (int.Parse(newlblIsEnable.Text) == 0)
                {
                    Label lblEnable = _common.ChangeIsEnable(0);
                    newlblIsEnable.Text = lblEnable.Text;
                    newlblIsEnable.ForeColor = lblEnable.ForeColor;
                }
            }
        }

        #endregion

        #region 方法

        //載入內容
        protected void SetContent()
        {
            List<tbArticleSub> list = _articleSubService.GetAll().Where(x => x.SubDesc == "AT001").OrderBy(s => s.SubSort).ToList();

            grid.DataSource = list;
            grid.DataBind();
        }

        #endregion

       

      
    }
}