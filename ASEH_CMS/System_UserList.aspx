﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="System_UserList.aspx.cs" Inherits="ASEH_CMS.System_UserList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Breadcrumb -->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="MainIndex.aspx">Dashboard</a></li>
        <li class="breadcrumb-item active">使用者管理</li>
      </ol>
    <!-- Breadcrumb Menu-->

<div class="container-fluid">
<div class="animated fadeIn">

  <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">

                <%--控制項--%>
                <table style="width: 100%;" >
                   <tr>
                        <td align="left";">
                            <asp:Button ID="btnAdd" runat="server" BorderColor="Transparent" BorderWidth="0px" CssClass="SmallButton" onclick="btnAdd_Click" Text="新增" />
                        </td>
                        <td align="right";">
                            <asp:Label ID="Label34" runat="server" SkinID="MainText" Text="群組權限"></asp:Label>
                            <asp:DropDownList ID="ddlSelRole" runat="server" AutoPostBack="True" Width="25%" >
                            </asp:DropDownList>
                        </td>
                   </tr>
                   <tr>
                        <td align="left";">
                            <asp:Label ID="Label35" runat="server" SkinID="MainText" Text="帳號名稱" Visible="False"></asp:Label>
                            <asp:TextBox ID="txbSelUseID" runat="server" MaxLength="15" Visible="False" Width="30%"></asp:TextBox>
                            <asp:Button ID="btnSearch" runat="server" BorderColor="Transparent" BorderWidth="0px" CssClass="SmallButton" onclick="btnSearch_Click" Text="查詢" Visible="False" />
                        </td>
                        <td align="right";">
                            &nbsp;</td>
                    </tr>
                 </table>

            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> 列表
                </div>

                <div class="card-body">

                <%--列表--%>
                <table style="width: 100%;" >
                    <tr>
                        <td colspan="2">
                            <asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" DataKeyNames="UseID" DataSourceID="ObjectDataSource1" onrowcommand="grid_RowCommand" onrowdatabound="grid_RowDataBound" Width="100%">
                                <PagerSettings Visible="False" />
                                <Columns>

                                     <asp:TemplateField HeaderText="最後登入時間">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLoginDate" runat="server" Text='<%# Eval("UseID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="UseID" HeaderText="帳號" ReadOnly="True">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UseName" HeaderText="中文名稱" ReadOnly="True">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="UseEmail" HeaderText="電子信箱" ReadOnly="True" Visible="false">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>

                                    <%--<asp:BoundField DataField="RolName" HeaderText="群組權限" ReadOnly="True"><HeaderStyle HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /></asp:BoundField>--%>
                                    <asp:TemplateField HeaderText="群組權限" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblRolName" runat="server" Text='<%# Eval("RolID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="是否啟用" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblIsEnable" runat="server" Text='<%# Eval("UseEnable") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="修改">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("UseID") %>' CommandName="Modify" ImageUrl="Images/Edit.png" ToolTip="修改" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="刪除">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnDel" runat="server" CausesValidation="False" CommandArgument='<%# Eval("UseID") %>' CommandName="Cancel" ImageUrl="Images/Delete.png" onclientclick="return window.confirm('確定要刪除嗎？');" ToolTip="刪除" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetAllByRolID" 
                            TypeName="Service.UsersService">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="ddlSelRole" Name="RolID" PropertyName="SelectedValue" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                </table>

                </div>
            </div>



        </asp:View>
        <asp:View ID="View2" runat="server">

        <div class="row">
        <div class="col-lg-12">
        <div class="card">
                <div class="card-header">
                <i class="fa fa-align-justify"></i> 新增/編輯
                </div>

            <div class="card-body">

            <table style="width:100%;">

                <tr>
                    <td align="center" colspan="2">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="lblIsEnable0" runat="server" SkinID="MainText" Text="最後登入時間"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:Label ID="lblLoginDate2" runat="server" SkinID="MainText"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">&nbsp;</td>
                    <td align="left" style="width: 70%">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="lblUseID" runat="server" SkinID="MainText" Text="帳號"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%"><span>
                        <asp:Label ID="lblUseID2" runat="server" SkinID="MainAlert">(請輸入英文或數字的組合)</asp:Label>
                        </span>
                        <br />
                        <asp:TextBox ID="txbUseID" runat="server" MaxLength="15" Width="30%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvUseID" runat="server" ControlToValidate="txbUseID" ErrorMessage="RequiredFieldValidator" SkinID="MainWarn" ValidationGroup="OK">帳號不可為空</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revUseID" runat="server" ControlToValidate="txbUseID" ErrorMessage="帳號輸入錯誤" ForeColor="Red" ValidationExpression="^[A-Za-z0-9]+$" ValidationGroup="OK"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">&nbsp;</td>
                    <td align="left" style="width: 70%">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="lblPW" runat="server" SkinID="MainText" Text="密碼"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbPW" runat="server" MaxLength="20" Width="30%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvPW" runat="server" ControlToValidate="txbPW" ErrorMessage="RequiredFieldValidator" SkinID="MainWarn" ValidationGroup="OK">密碼不可為空</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">&nbsp;</td>
                    <td align="left" style="width: 70%">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%" valign="top">
                        <asp:Label ID="lblName" runat="server" SkinID="MainText" Text="中文名稱"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbName" runat="server" 
                        Width="50%" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvName" runat="server" 
                        ControlToValidate="txbName" ErrorMessage="RequiredFieldValidator" 
                        SkinID="MainWarn" ValidationGroup="OK">名稱不可為空</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%" valign="top">&nbsp;</td>
                    <td align="left" style="width: 70%">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%" valign="top">
                        <asp:Label ID="lblName3" runat="server" SkinID="MainText" Text="電子信箱"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbEmail" runat="server" MaxLength="250" Width="50%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%" valign="top">&nbsp;</td>
                    <td align="left" style="width: 70%">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%" valign="top">
                        <asp:Label ID="lblIsEnable" runat="server" SkinID="MainText" Text="是否啟用"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:CheckBox ID="ckbIsEnable" runat="server" Checked="True" Text="啟用請打勾" />
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%" valign="top">
                        <asp:Label ID="lblName0" runat="server" SkinID="MainText" Text="國家/地區" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:DropDownList ID="ddlCountry" runat="server" 
                            Width="30%" Visible="False">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%" valign="top">
                        <asp:Label ID="lblName4" runat="server" SkinID="MainText" Text="郵遞區號" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbZipCode" runat="server" MaxLength="30" Width="20%" Visible="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%" valign="top">
                        <asp:Label ID="lblName1" runat="server" SkinID="MainText" Text="地址" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbAddr" runat="server" MaxLength="500" Width="50%" Visible="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%" valign="top">
                        <asp:Label ID="lblName2" runat="server" SkinID="MainText" Text="手機號碼" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbCell" runat="server" MaxLength="30" Width="20%" Visible="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%" valign="top">
                        <asp:Label ID="lblRole" runat="server" SkinID="MainText" Text="群組權限"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:DropDownList ID="ddlRole" runat="server" Width="30%" >
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%" valign="top">&nbsp;</td>
                    <td align="left" style="width: 70%">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnEdit" runat="server" BackColor="Transparent" 
                        BorderWidth="0px" CssClass="BigButton" onclick="btnEdit_Click" Text="確定修改" 
                        ValidationGroup="OK" />
                        <asp:Button ID="btnOK" runat="server" BackColor="Transparent" BorderWidth="0px" 
                        CssClass="SmallButton" onclick="btnOK_Click" Text="確定" ValidationGroup="OK" />
                        <asp:Button ID="btnBack" runat="server" BackColor="Transparent" 
                        BorderWidth="0px" CssClass="SmallButton" onclick="btnBack_Click" Text="返回" />
                        <asp:HiddenField ID="hideUseID" runat="server" />
                        <asp:HiddenField ID="hideLoginUseID" runat="server" />
                    </td>
                </tr>
            </table>

            </div>

        </div>
        </div>
        </div>

        </asp:View>

        </asp:MultiView>

</div>
</div>
    
</asp:Content>
