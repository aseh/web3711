﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Models;
using System.IO;
using System.Text.RegularExpressions;
using Service;
using Service.Interface;
using Service.Misc;
using System.Net;
using System.Net.Mail;
using System.ComponentModel;
using System.Drawing.Imaging;

namespace ASEH_CMS.App_Code
{
    public class CommonClass
    {
        //取得CheckBox的值(1:啟用0:停用)
        public int GetChecked(CheckBox ckb)
        {
            int checkValue = 0;
            if (ckb.Checked)
            {
                checkValue = 1;
            }
            else
            {
                checkValue = 0;
            }
            return checkValue;
        }

        //設定CheckBox的值(1:啟用0:停用)
        public void SetChecked(CheckBox ckb, int checkValue)
        {
            if (checkValue == 1)
            {
                ckb.Checked = true;
            }
            else if (checkValue == 0)
            {
                ckb.Checked = false;
            }
        }

        //設定民國年下拉選單(從今年到之前哪一年)
        public void SetTYearList(DropDownList ddl, int LastTYear)
        {
            ListItem item = null;
            ddl.Items.Clear();

            int NowTY = GetTaiwanYear(DateTime.Now);

            for (int i = NowTY; i >= LastTYear; i--)
            {
                item = new ListItem(SetFixCode(i.ToString(), 3), SetFixCode(i.ToString(), 3));

                ddl.Items.Add(item);
            }
        }

        //設定西元年下拉選單(從今年到之前哪一年)
        public void SetWYearBeforeList(DropDownList ddl, int LastTYear)
        {
            ListItem item = null;
            ddl.Items.Clear();

            int NowWY = DateTime.Now.Year;

            for (int i = NowWY; i >= LastTYear; i--)
            {
                item = new ListItem(i.ToString(), i.ToString());

                ddl.Items.Add(item);
            }
        }

        //取得民國年
        public int GetTaiwanYear(DateTime dtDate)
        {
            int TYear = dtDate.Year - 1911;

            return TYear;
        }

        //取得西元年
        public int GetWestYear(int TYear)
        {
            int CYear = TYear + 1911;

            return CYear;
        }

        //數碼不足(FIX)自動補0
        public string SetFixCode(string orgCode, int Fix)
        {
            if (orgCode.Length < Fix)
            {
                orgCode = "0" + orgCode;
            }

            return orgCode;
        }

        #region 狀態處理

        //轉換啟停狀態
        public Label ChangeIsEnable(int IsEnable)
        {
            Label lblIsEnable = new Label();

            if (IsEnable == 1)
            {
                lblIsEnable.Text = "啟用";
            }
            else if (IsEnable == 0)
            {
                lblIsEnable.Text = "停用";
                lblIsEnable.ForeColor = Color.Red;
            }

            return lblIsEnable;
        }

        public Label BooleanToLabel(int isCheck, string text1 = "是", string text2 = "否")
        {
            Label label = new Label();
            label.Text = isCheck == 1 ? text1 : text2;

            if ( isCheck != 1 )
            {
                label.ForeColor = Color.Red;
            } 

            return label;
        }

        //轉換發送狀態
        public Label ChangeIsSend(int IsSend)
        {
            Label lblIsSend = new Label();

            if (IsSend == 1)
            {
                lblIsSend.Text = "已發送";
                lblIsSend.ForeColor = Color.Green;
            }
            else if (IsSend == 0)
            {
                lblIsSend.Text = "未發送";
                lblIsSend.ForeColor = Color.Red;
            }

            return lblIsSend;
        }

        //轉換讀取狀態
        public Label ChangeIsRead(int IsRead)
        {
            Label lblIsRead = new Label();

            if (IsRead == 1)
            {
                lblIsRead.Text = "已讀取";
                lblIsRead.ForeColor = Color.Green;
            }
            else if (IsRead == 0)
            {
                lblIsRead.Text = "未讀取";
                lblIsRead.ForeColor = Color.Red;
            }

            return lblIsRead;
        }

        #endregion

        #region 字串處理

        //裁減字數限制
        public string GetShortText(string FullText, int MaxWords)
        {
            string ShortText = "";

            if (FullText.Length >= MaxWords)
            {
                ShortText = FullText.Substring(0, MaxWords) + "...";
            }
            else
            {
                ShortText = FullText;
            }

            return ShortText;
        }

        //裁減字數限制,加上超連結
        public string GetShortText(string FullText, int MaxWords, string LinkPage)
        {
            string ShortText = "";

            if (FullText.Length >= MaxWords)
            {
                ShortText = FullText.Substring(0, MaxWords) + "<a href='" + LinkPage + "'>...more</a>";
            }
            else
            {
                ShortText = FullText;
            }

            return ShortText;
        }

        //復原字串斷行(HTML)
        public string RestoreHTMLText(string OrgText)
        {
            string FixText = "";

            FixText = OrgText.Replace("\r\n", "<br>");

            return FixText;
        }

        //判斷為匯出可能有問題的工號做轉換
        public string ExchangeEmpID(string EmpID)
        {
            string fixEmpID = "";

            char[] array = EmpID.ToCharArray();
            if (array[2] == 'E')
            {
                fixEmpID = string.Format("{0}.", EmpID);
            }
            else
            {
                fixEmpID = EmpID;
            }

            return fixEmpID;
        }

        //去除 HTML 標籤，可自訂合法標籤加以保留
        public string StripTags(string src, string[] reservedTagPool)
        {
            return Regex.Replace(
                src,
                String.Format("<(?!{0}).*?>", string.Join("|", reservedTagPool)),
                String.Empty);
        }

        #endregion

        #region 圖片管理


        public string ResizeAndSave(FileUpload ful, int maxWidth, int maxHeight, string uploadDir)
        {
            Guid guid = Guid.NewGuid();
            string savePath = String.Format("{0}{1}", String.Concat(DateTime.Now.ToString("yyyyMMddHHmmss"), guid.ToString()), ".png");
            using (System.Drawing.Image image = System.Drawing.Image.FromStream(ful.PostedFile.InputStream))
            {
                Double percentW = ((Double)maxWidth / (Double)image.Width) * 100;
                Double percentH = ((Double)maxHeight / (Double)image.Height) * 100;
                Double percent = Math.Min(percentW, percentH) / 100;

                using (System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(image, new System.Drawing.Size((int)(image.Width * percent), (int)(image.Height * percent))))
                {
                    bitmap.Save(String.Format("{0}/{1}", uploadDir, savePath), System.Drawing.Imaging.ImageFormat.Png);
                }
            }
            return savePath;
        }

        //上傳圖片（自動轉為JPG）(不縮圖)
        public int UploadOrgJpgPic(FileUpload ful, string UploadFolderPath)
        {
            int status = 0;//正常

            if (ful.HasFile)
            {
                if (IsValidImage(ful.FileName))
                {
                    string RealFolderPath = UploadFolderPath;
                    //檢查資料夾是否存在
                    if (!Directory.Exists(RealFolderPath))
                    {
                        Directory.CreateDirectory(RealFolderPath);
                    }

                    string RealFilePath = RealFolderPath + Path.ChangeExtension(ful.FileName, ".jpg");
                    using (System.Drawing.Image image = System.Drawing.Image.FromStream(ful.PostedFile.InputStream))
                    {

                        int thumbWidth = 0;
                        int thumbHeight = 0;

                        thumbWidth = image.Width;
                        thumbHeight = image.Height;

                        using (System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(image, new System.Drawing.Size(thumbWidth, thumbHeight)))
                        {
                            if (Directory.Exists(RealFolderPath))
                            {
                                if (!File.Exists(RealFilePath))
                                {
                                    bitmap.Save(RealFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    status = 0;
                                }
                                else
                                {
                                    status = 1;
                                }
                            }
                        }
                    }
                }
                else
                {
                    status = 2;
                }
            }

            return status;
        }

        //上傳圖片（自動轉為JPG）
        public int UploadJpgPic(FileUpload ful, string UploadFolderPath)
        {
            int status = 0;//正常

            if (ful.HasFile)
            {
                if (IsValidImage(ful.FileName))
                {
                    string RealFolderPath = UploadFolderPath;
                    //檢查資料夾是否存在
                    if (!Directory.Exists(RealFolderPath))
                    {
                        Directory.CreateDirectory(RealFolderPath);
                    }

                    string RealFilePath = RealFolderPath + Path.ChangeExtension(ful.FileName, ".jpg");
                    using (System.Drawing.Image image = System.Drawing.Image.FromStream(ful.PostedFile.InputStream))
                    {
                        //圖片解析度
                        //取得圖像的水平解析度（像素/英寸）  
                        float dpiX = image.HorizontalResolution;
                        //取得圖像的垂直解析度  
                        float dpiY = image.VerticalResolution;

                        //圖片尺寸若大於1200px才進行縮小
                        int thumbWidth = 0;
                        int thumbHeight = 0;
                        if (image.Height > 1200 || image.Width > 1200)
                        {
                            decimal sizeRatio = ((decimal)image.Height / image.Width);
                            thumbWidth = 1200;
                            thumbHeight = decimal.ToInt32(sizeRatio * thumbWidth);
                        }
                        else
                        {
                            thumbWidth = image.Width;
                            thumbHeight = image.Height;
                        }

                        using (System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(image, new System.Drawing.Size(thumbWidth, thumbHeight)))
                        {
                            if (Directory.Exists(RealFolderPath))
                            {
                                if (!File.Exists(RealFilePath))
                                {
                                    bitmap.SetResolution(dpiX, dpiY);
                                    bitmap.Save(RealFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    status = 0;
                                }
                                else
                                {
                                    status = 1;
                                }
                            }
                        }
                    }
                }
                else
                {
                    status = 2;
                }
            }

            return status;
        }

        //上傳圖片(檔名自訂義) (不縮圖)
        public int UploadOrgJpgPicWithNewName(FileUpload ful, string UploadFolderPath, string newFileName)
        {
            int status = 0;//正常

            if (ful.HasFile)
            {
                if (IsValidImage(ful.FileName))
                {
                    string RealFolderPath = UploadFolderPath;
                    //檢查資料夾是否存在
                    if (!Directory.Exists(RealFolderPath))
                    {
                        Directory.CreateDirectory(RealFolderPath);
                    }

                    string RealFilePath = RealFolderPath + Path.ChangeExtension(newFileName, ".jpg");
                    using (System.Drawing.Image image = System.Drawing.Image.FromStream(ful.PostedFile.InputStream))
                    {
                        int thumbWidth = 0;
                        int thumbHeight = 0;

                        thumbWidth = image.Width;
                        thumbHeight = image.Height;

                        using (System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(image, new System.Drawing.Size(thumbWidth, thumbHeight)))
                        {
                            if (Directory.Exists(RealFolderPath))
                            {
                                if (!File.Exists(RealFilePath))
                                {
                                    bitmap.Save(RealFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    status = 0;
                                }
                                else
                                {
                                    status = 1;
                                }
                            }
                        }
                    }
                }
                else
                {
                    status = 2;
                }
            }

            return status;
        }

        //檢查檔案格式是否正確
        public bool IsValidImage(string path)
        {
            return Regex.IsMatch(path, @"(.*?)\.(bmp|jpg|jpeg|png|gif|mp4)$", RegexOptions.IgnoreCase);
        }

        //根據錯誤代碼取得說明字串
        public string GetErrorText(int ErrorCode)
        {
            string ErrorText = "";

            switch (ErrorCode)
            {
                case 0:
                    ErrorText = "正常";
                    break;
                case 1:
                    ErrorText = "上傳失敗，檔案已存在";
                    break;
                case 2:
                    ErrorText = "檔案格式錯誤，僅支援（bmp,jpg,jpeg,png）等格式";
                    break;
                case 3:
                    ErrorText = "請先選擇檔案路徑";
                    break;
                case 4:
                    ErrorText = "上傳失敗，超過圖片上限";
                    break;
            }

            return ErrorText;
        }

        #endregion

        #region 檔案管理

        //上傳檔案(不限類型)
        public int UploadAnyFile(FileUpload ful, string UploadFolderPath)
        {
            int status = 0;//正常

            if (ful.HasFile)
            {
                string RealFolderPath = UploadFolderPath;
                //檢查資料夾是否存在
                if (!Directory.Exists(RealFolderPath))
                {
                    Directory.CreateDirectory(RealFolderPath);
                }

                //檔案名稱
                string RealFilePath = RealFolderPath + ful.FileName;

                if (Directory.Exists(RealFolderPath))
                {
                    if (!File.Exists(RealFilePath))
                    {
                        //上傳
                        ful.SaveAs(RealFilePath);
                        status = 0;
                    }
                    else
                    {
                        status = 1;
                    }
                }
            }

            return status;
        }

        //上傳檔案(不限類型)(檔名自訂義)
        public int UploadAnyFileWithNewName(FileUpload ful, string UploadFolderPath, string newFileName)
        {
            int status = 0;//正常

            if (ful.HasFile)
            {
                string RealFolderPath = UploadFolderPath;
                //檢查資料夾是否存在
                if (!Directory.Exists(RealFolderPath))
                {
                    Directory.CreateDirectory(RealFolderPath);
                }

                //檔案名稱
                string Extension = Path.GetExtension(ful.FileName);
                string RealFilePath = RealFolderPath + newFileName + Extension;

                if (Directory.Exists(RealFolderPath))
                {
                    if (!File.Exists(RealFilePath))
                    {
                        //上傳
                        ful.SaveAs(RealFilePath);
                        status = 0;
                    }
                    else
                    {
                        status = 1;
                    }
                }
            }

            return status;
        }

        //上傳圖片(檔名為序號)（自動裁剪指定大小）
        public int UploadBigJpgPicWithNewNameAndCut(FileUpload ful, string UploadFolderPath, string newFileName, int SmlWidth, int SmlHeight, int CutWidth, int CutHeight)
        {
            int status = 0;//正常

            if (ful.HasFile)
            {
                if (IsValidImage(ful.FileName))
                {
                    string RealFolderPath = UploadFolderPath;
                    //檢查資料夾是否存在
                    if (!Directory.Exists(RealFolderPath))
                    {
                        Directory.CreateDirectory(RealFolderPath);
                    }

                    string RealFilePath = RealFolderPath + Path.ChangeExtension(newFileName, ".jpg");
                    using (System.Drawing.Image image = System.Drawing.Image.FromStream(ful.PostedFile.InputStream))
                    {
                        //實際圖片大小
                        int thumbWidth = 0;
                        int thumbHeight = 0;

                        thumbWidth = image.Width;
                        thumbHeight = image.Height;

                        using (System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(image, new System.Drawing.Size(thumbWidth, thumbHeight)))
                        {
                            if (Directory.Exists(RealFolderPath))
                            {
                                if (!File.Exists(RealFilePath))
                                {
                                    System.Drawing.Bitmap bitmap2 = new System.Drawing.Bitmap(image, new System.Drawing.Size(SmlWidth, SmlHeight));
                                    //bitmap2.Save(RealFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);

                                    //取得圖片中心
                                    int StartPicWidth = (SmlWidth / 2) - (CutWidth / 2);
                                    int StartPicHeight = (SmlHeight / 2) - (CutHeight / 2);

                                    Rectangle cloneRect = new Rectangle(StartPicWidth, StartPicHeight, CutWidth, CutHeight);
                                    PixelFormat format = bitmap2.PixelFormat;
                                    Bitmap cloneBitmap = bitmap2.Clone(cloneRect, format);

                                    cloneBitmap.Save(RealFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    status = 0;
                                }
                                else
                                {
                                    status = 1;
                                }
                            }
                        }
                    }
                }
                else
                {
                    status = 2;
                }
            }

            return status;
        }


        #endregion

        #region 帳號管理

        //驗證使用者是否有權限
        public int CheckPermission(string UseID, string PerID)
        {
            IUsersService _usersService = new UsersService();
            IRolesService _rolesService = new RolesService();
            IPermRoleService _permroleService = new PermRoleService();

            int iUserPermission = 0;

            //取得角色
            string RolID = "";

            var itemUser = _usersService.GetByID(UseID);
            RolID = itemUser.RolID;

            //根據角色找出權限
            IEnumerable<tbPermRole> list = _permroleService.GetAll().Where(p => p.RolID == RolID).ToList();
            foreach (tbPermRole element in list)
            {
                if (PerID == element.PerID)
                {
                    iUserPermission = 1;
                    break;
                }
            }

            return iUserPermission;
        }

        #endregion

        #region 信件寄發

        //建立CheckBoxList的方法
        public void SendMail(string MailFrom, string MailTo, string MaiSubject, string MaiBody)
        {
            string strHost = System.Configuration.ConfigurationManager.AppSettings["Mail_Host"].ToString();
            string strPort = System.Configuration.ConfigurationManager.AppSettings["Mail_Port"].ToString();

            string UserAccount = System.Configuration.ConfigurationManager.AppSettings["UserAccount"].ToString();
            string UserPassword = System.Configuration.ConfigurationManager.AppSettings["UserPassword"].ToString();

            string[] strSplit = { ";" };
            string[] strToList = MailTo.Split(strSplit, StringSplitOptions.RemoveEmptyEntries);

            EmailAlert clsMail = new EmailAlert();
            clsMail.Mail_Host = strHost;
            clsMail.Mail_Port = int.Parse(strPort);

            clsMail.Subject = MaiSubject;
            clsMail.Body = MaiBody;

            clsMail.UserAccount = UserAccount;
            clsMail.UserPassword = UserPassword;

            clsMail.EmailSent(MaiSubject, MailFrom, strToList, strToList.Length, "", UserAccount, UserPassword);
        }

        public class EmailAlert
        {
            public string Mail_Host { get; set; }
            public int Mail_Port { get; set; }
            public string Subject { get; set; }
            public string Body { get; set; }
            public string UserAccount { get; set; }
            public string UserPassword { get; set; }

            MailMessage myMailMessage;

            void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
            {
                string subject = (string)e.UserState;

                if (e.Cancelled)
                {
                    string cancelled = string.Format("[{0}] Send canceled.", subject);
                    Console.WriteLine(cancelled);
                }
                if (e.Error != null)
                {
                    string error = String.Format("[{0}] {1}", subject, e.Error.ToString());
                    Console.WriteLine(error);
                }
                else
                {
                    Console.WriteLine("Email sent.");
                }
            }

            public string EmailSent(string strEmailSubject, string strEmailAddrFrom, string[] strEmailAddrTo, int intTotalEmailTo, string strAttachement, string strUserAccount, string strUserPassword)
            {
                string strSent = " ";
                try
                {
                    // Initializes a new instance of the System.Net.Mail.MailMessage class. 
                    myMailMessage = new MailMessage();

                    // Obtains the e-mail address of the person the e-mail is being sent to. 

                    for (int NumberOfEmails = 0; NumberOfEmails < intTotalEmailTo; NumberOfEmails++)
                    {
                        myMailMessage.To.Add(new MailAddress(strEmailAddrTo[NumberOfEmails]));
                    }

                    string MailFromDesc = strEmailSubject;
                    string MailFrom = strEmailAddrFrom;

                    // Obtains the e-mail address of the person sending the message. 
                    myMailMessage.From = new MailAddress(MailFrom, MailFromDesc);

                    // You can add additional addresses by simply calling .Add again. 
                    // Support not added in the current example UI. 
                    // 
                    // myMailMessage.To.Add( new System.Net.Mail.MailAddress( "addressOne@example.com" )); 
                    // myMailMessage.To.Add( new System.Net.Mail.MailAddress( "addressTwo@example.com" )); 
                    // myMailMessage.To.Add( new System.Net.Mail.MailAddress( "addressThree@example.com" )); 


                    // You can also specify a friendly name to be displayed within the e-mail 
                    // application on the client-side for the To Address. 
                    // Support not added in the current example UI. 
                    // See the example below: 
                    // 
                    // myMailMessage.To.Add(new System.Net.Mail.MailAddress( this.txtToAddress.Text, "My Name Here" )); 
                    // myMailMessage.From(new System.Net.Mail.MailAddress( this.txtToAddress.Text, "Another Name Here" )); 

                    // System.Net.Mail also supports Carbon Copy(CC) and Blind Carbon Copy (BCC) 
                    // Support not added in the current example UI. 
                    // See the example below: 
                    // 
                    // myMailMessage.CC.Add ( new System.Net.Mail.MailAddress( "carbonCopy@example.com" )); 
                    // myMailMessage.Bcc.Add( new System.Net.Mail.MailAddress( "blindCarbonCopy@example.com" )); 

                    // Obtains the subject of the e-mail message 
                    myMailMessage.Subject = Subject;

                    // Obtains the body of the e-mail message. 
                    myMailMessage.Body = Body;

                    // Listed below are the two message formats that can be used: 
                    // 1. Text 
                    // 2. HTML 
                    // 
                    // The default format is Text.                     
                    myMailMessage.IsBodyHtml = true;


                    // Listed below are the three priority levels that can be used: 
                    // 1. High 
                    // 2. Normal 
                    // 3. Low 
                    // 
                    // The default priority level is Normal. 
                    // 
                    // This section of code determines which priority level 
                    // was checked by the user. 

                    myMailMessage.Priority = MailPriority.High;

                    //myMailMessage.Priority = MailPriority.Normal; 

                    //myMailMessage.Priority = MailPriority.Low; 

                    // Not Yet Implement
                    // This section of code determines if the e-mail message is going to 
                    // have an attachment. 
                    //if (strAttachement != "" || strAttachement != null)
                    //{
                    //    Attachment att = new Attachment(strAttachement);
                    //    myMailMessage.Attachments.Add(att);
                    //}


                    // Custom headers can also be added to the MailMessage. 
                    // These custom headers can be used to tag an e-mail message 
                    // with information that can be useful in tracking an e-mail 
                    // message. 
                    // 
                    // Support not added in the current example UI. 
                    // See the example below: 
                    // myMailMessage.Headers.Add( "Titan-Company", "Titan Company Name" ); 

                    // Initializes a new instance of the System.Net.Mail.SmtpClient class. 
                    SmtpClient myMailClient = new SmtpClient();

                    // Obtains the email server name or IP address to use when sending the e-mail. 
                    myMailClient.Host = Mail_Host;

                    // Defines the port number to use when connecting to the mail server. 
                    // The default port number for SMTP is 25/TCP. 
                    myMailClient.Port = Mail_Port;

                    // Specifies the delivery method to use when sending the e-mail 
                    // message. Listed below are the three delivery methods 
                    // that can be used by namespace System.Net.Mail 
                    // 
                    // 1. Network = sent through the network to an SMTP server. 
                    // 2. PickupDirectoryFromIis = copied to the pickup directory used by a local IIS server. 
                    // 3. SpecifiedPickupDirectory    = is copied to the directory specified by the 
                    // SmtpClient.PickupDirectoryLocation property. 

                    myMailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                    //SSL驗證
                    myMailClient.EnableSsl = true;

                    // Initializes a new instance of the System.Net.NetworkCredential class. 
                    NetworkCredential myMailCredential = new NetworkCredential();

                    // Obtains the user account needed to authenticate to the mail server. 
                    myMailCredential.UserName = UserAccount;

                    // Obtains the user password needed to authenticate to the mail server. 
                    myMailCredential.Password = UserPassword;

                    // In this example we are providing credentials to use to authenticate to 
                    // the e-mail server. Your can also use the default credentials of the 
                    // currently logged on user. For client applications, this is the desired 
                    // behavior in most scenarios. In those cases the bool value would be set to true. 
                    myMailClient.UseDefaultCredentials = false;

                    // Obtains the credentials needed to authenticate the sender. 
                    myMailClient.Credentials = myMailCredential;

                    // Set the method that is called back when the send operation ends. 
                    myMailClient.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);

                    // Sends the message to the defined e-mail for processing 
                    // and delivery with feedback. 
                    // 
                    // In the current example randomToken generation was not added. 
                    // 
                    //string randomToken = "randonTokenTestValue"; 
                    //myMailClient.SendAsync( myMailMessage, randomToken );
                    object userState = myMailMessage;
                    try
                    {
                        //you can also call myMailClient.SendAsync(myMailMessage, userState);
                        Console.WriteLine("Mail Sending In progress");
                        myMailClient.Send(myMailMessage);
                    }
                    catch (System.Net.Mail.SmtpException ex)
                    {
                        Console.WriteLine(ex.Message, "Send Mail Error");
                        strSent = strSent + ex.Message;

                        var model = new tbExceptionRecord();
                        model.CreateTime = DateTime.Now;
                        model.ExrTypeID = "EXT001";
                        model.ExrMsg = strSent;
                        IExceptionRecordService _exceptionRecordService = new ExceptionRecordService();
                        _exceptionRecordService.Create(model);
                    }
                    myMailMessage.Dispose();
                    strSent = "Mail Sent !!";
                }

                // Catches an exception that is thrown when the SmtpClient is not able to complete a 
                // Send or SendAsync operation to a particular recipient. 
                catch (System.Net.Mail.SmtpException exSmtp)
                {
                    Console.WriteLine("Exception occurred:" + exSmtp.Message, "SMTP Exception Error");
                    strSent = strSent + "Exception occurred:" + exSmtp.Message;

                    var model = new tbExceptionRecord();
                    model.CreateTime = DateTime.Now;
                    model.ExrTypeID = "EXT001";
                    model.ExrMsg = strSent;
                    IExceptionRecordService _exceptionRecordService = new ExceptionRecordService();
                    _exceptionRecordService.Create(model);
                }

                // Catches general exception not thrown using the System.Net.Mail.SmtpException above. 
                // This general exception also will catch invalid formatted e-mail addresses, because 
                // a regular expression has not been added to this example to catch this problem. 
                catch (System.Exception exGen)
                {
                    Console.WriteLine("Exception occurred:" + exGen.Message, "General Exception Error");
                    strSent = strSent + "Exception occurred:" + exGen.Message;

                    var model = new tbExceptionRecord();
                    model.CreateTime = DateTime.Now;
                    model.ExrTypeID = "EXT001";
                    model.ExrMsg = strSent;
                    IExceptionRecordService _exceptionRecordService = new ExceptionRecordService();
                    _exceptionRecordService.Create(model);
                }
                return strSent;
            }
        }

        #endregion

    }
}