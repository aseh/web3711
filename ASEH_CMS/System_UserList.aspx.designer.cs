﻿//------------------------------------------------------------------------------
// <自動產生的>
//     這段程式碼是由工具產生的。
//
//     對這個檔案所做的變更可能會造成錯誤的行為，而且如果重新產生程式碼，
//     所做的變更將會遺失。 
// </自動產生的>
//------------------------------------------------------------------------------

namespace ASEH_CMS {
    
    
    public partial class System_UserList {
        
        /// <summary>
        /// MultiView1 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.MultiView MultiView1;
        
        /// <summary>
        /// View1 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.View View1;
        
        /// <summary>
        /// btnAdd 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnAdd;
        
        /// <summary>
        /// Label34 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label34;
        
        /// <summary>
        /// ddlSelRole 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlSelRole;
        
        /// <summary>
        /// Label35 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label35;
        
        /// <summary>
        /// txbSelUseID 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbSelUseID;
        
        /// <summary>
        /// btnSearch 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnSearch;
        
        /// <summary>
        /// grid 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView grid;
        
        /// <summary>
        /// ObjectDataSource1 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.ObjectDataSource ObjectDataSource1;
        
        /// <summary>
        /// View2 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.View View2;
        
        /// <summary>
        /// lblIsEnable0 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblIsEnable0;
        
        /// <summary>
        /// lblLoginDate2 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblLoginDate2;
        
        /// <summary>
        /// lblUseID 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblUseID;
        
        /// <summary>
        /// lblUseID2 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblUseID2;
        
        /// <summary>
        /// txbUseID 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbUseID;
        
        /// <summary>
        /// rfvUseID 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvUseID;
        
        /// <summary>
        /// revUseID 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.RegularExpressionValidator revUseID;
        
        /// <summary>
        /// lblPW 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblPW;
        
        /// <summary>
        /// txbPW 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbPW;
        
        /// <summary>
        /// rfvPW 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvPW;
        
        /// <summary>
        /// lblName 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblName;
        
        /// <summary>
        /// txbName 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbName;
        
        /// <summary>
        /// rfvName 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvName;
        
        /// <summary>
        /// lblName3 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblName3;
        
        /// <summary>
        /// txbEmail 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbEmail;
        
        /// <summary>
        /// lblIsEnable 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblIsEnable;
        
        /// <summary>
        /// ckbIsEnable 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox ckbIsEnable;
        
        /// <summary>
        /// lblName0 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblName0;
        
        /// <summary>
        /// ddlCountry 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlCountry;
        
        /// <summary>
        /// lblName4 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblName4;
        
        /// <summary>
        /// txbZipCode 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbZipCode;
        
        /// <summary>
        /// lblName1 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblName1;
        
        /// <summary>
        /// txbAddr 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbAddr;
        
        /// <summary>
        /// lblName2 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblName2;
        
        /// <summary>
        /// txbCell 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbCell;
        
        /// <summary>
        /// lblRole 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblRole;
        
        /// <summary>
        /// ddlRole 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlRole;
        
        /// <summary>
        /// btnEdit 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnEdit;
        
        /// <summary>
        /// btnOK 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnOK;
        
        /// <summary>
        /// btnBack 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnBack;
        
        /// <summary>
        /// hideUseID 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hideUseID;
        
        /// <summary>
        /// hideLoginUseID 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hideLoginUseID;
    }
}
