﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Manage_MediaDB.aspx.cs" Inherits="ASEH_CMS.Manage_MediaDB" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>媒體資源庫</title>
    <style type="text/css">
        .auto-style1 {
            height: 20px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="ContentBody">
    
        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
           
            <asp:View ID="View1" runat="server">
                <table style="width: 90%;" align="center">
                    <tr>
                        <td align="left" class="style1">&nbsp;</td>
                        <td align="right" style="width: 20%;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" class="style1">
                            <asp:Label ID="Label1" runat="server" SkinID="MainText" Text="類型："></asp:Label>
                            <asp:DropDownList ID="ddlFileType" runat="server" AutoPostBack="True" Width="15%" OnSelectedIndexChanged="ddlFileType_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td align="right" style="width: 20%;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" class="style1">
                            &nbsp;</td>
                        <td align="right" style="width: 20%;">
                            <asp:Button ID="btnAdd" runat="server" BorderColor="Transparent" BorderWidth="0px" CssClass="SmallButton" Text="新增" OnClick="btnAdd_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:GridView ID="grid" runat="server"
                                AutoGenerateColumns="False" Width="100%"
                                OnRowCommand="grid_RowCommand" OnRowDataBound="grid_RowDataBound"
                                PageSize="100">
                                <PagerSettings Visible="False" />
                                <Columns>

                                    <asp:TemplateField HeaderText="類型" Visible="True"><ItemTemplate><asp:Label ID="lblMdtID" runat="server" Text='<%# Eval("MdtID") %>'></asp:Label></ItemTemplate><HeaderStyle HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" Width="40px" /></asp:TemplateField>

                                    <asp:BoundField DataField="MdaName" HeaderText="名稱" ReadOnly="True"><ItemStyle HorizontalAlign="Left" Width="70px" /></asp:BoundField>

                                    <asp:BoundField DataField="MdaFileName" HeaderText="檔案名稱" ReadOnly="True" Visible="False"><HeaderStyle HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Left" Width="150px" /></asp:BoundField>

                                    <asp:TemplateField HeaderText="檔案路徑"><ItemTemplate><asp:Label ID="lblMdaFilePath" runat="server" Text='<%# Eval("MdaID") + "," + Eval("MdaFileName") %>'></asp:Label></ItemTemplate><HeaderStyle HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Left"/></asp:TemplateField>

                                    <asp:TemplateField HeaderText="縮圖"><ItemTemplate><asp:Image ID="imgPic" runat="server" AlternateText='<%# Eval("MdaID") + "," + Eval("MdaFileName") + "," + Eval("MdtID") %>'
                                                Height="50px" Width="50px" ImageUrl="Images/default.jpg"/></ItemTemplate><HeaderStyle HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" Width="60px" /></asp:TemplateField>

                                    <asp:TemplateField HeaderText="修改" Visible="True"><ItemTemplate><asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False"
                                                CommandArgument='<%# Eval("MdaID") %>' CommandName="Modify"
                                                ImageUrl="Images/Edit.png" ToolTip="修改" /></ItemTemplate><ItemStyle HorizontalAlign="Center" Width="70px" /></asp:TemplateField>

                                    <asp:TemplateField HeaderText="瀏覽" Visible="False"><ItemTemplate><asp:ImageButton ID="imgbtnView" runat="server" CausesValidation="False"
                                                CommandArgument='<%# Eval("MdaID") %>' CommandName="View"
                                                ImageUrl="Images/View.gif" ToolTip="瀏覽" /></ItemTemplate><ItemStyle HorizontalAlign="Center" Width="70px" /></asp:TemplateField>

                                    <asp:TemplateField HeaderText="刪除"><ItemTemplate><asp:ImageButton ID="imgbtnDel" runat="server" CausesValidation="False"
                                                CommandArgument='<%# Eval("MdaID") %>' CommandName="Remove"
                                                ImageUrl="Images/Delete.png"
                                                OnClientClick="return window.confirm('確定要刪除嗎？');" ToolTip="刪除" /></ItemTemplate><HeaderStyle HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" Width="60px" /></asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <asp:Literal ID="ltlPageMenu" runat="server"></asp:Literal>
                            <asp:HiddenField ID="hideLoginUseID" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:View>

            <asp:View ID="View2" runat="server">
                <table style="width: 100%;">
                    <tr>
                        <td align="center" colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Label ID="Label14" runat="server" SkinID="MainSubject" Text="新增/修改檔案"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="right" class="auto-style1">
                            <asp:Label ID="Label35" runat="server" SkinID="MainText" Text="最後修改日期："></asp:Label>
                        </td>
                        <td align="left" class="auto-style1">
                            <asp:Label ID="lblLastUpdateDate2" runat="server" SkinID="MainText"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                    <td align="right" style="width: 30%">
                        <asp:Label ID="lable93" runat="server" SkinID="MainText" Text="類型："></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:DropDownList ID="ddlFileType2" runat="server" Width="15%">
                        </asp:DropDownList>

                    </td>
                </tr>
                    <tr>
                        <td align="right" class="style2">
                            <asp:Label ID="Label15" runat="server" SkinID="MainText" Text="名稱："></asp:Label>
                        </td>
                        <td align="left" class="style3">
                            <asp:TextBox ID="txbMdaName2" runat="server" MaxLength="50" Width="60%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="style2" valign="top">
                            <asp:Label ID="Label36" runat="server" SkinID="MainText" Text="說明："></asp:Label>
                        </td>
                        <td align="left" class="style3">
                            <asp:TextBox ID="txbMdaDesc2" runat="server" MaxLength="200" Width="60%" Height="40px" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="style2">
                            <asp:Label ID="lblIsEnable2" runat="server" SkinID="MainText" Text="是否啟用：" Visible="False"></asp:Label>
                        </td>
                        <td align="left" class="style3">
                            <asp:CheckBox ID="ckbIsEnable2" runat="server" Checked="True" Text="啟用請打勾" Visible="False" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="style2">
                            <asp:Label ID="Label18" runat="server" SkinID="MainText" Text="檔案上傳："></asp:Label>
                        </td>
                        <td align="left" class="style3">
                            <asp:Label ID="lblMdaFilePath2" runat="server" SkinID="MainText"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="style2">&nbsp;</td>
                        <td align="left" class="style3">
                            <asp:FileUpload ID="FileUpload2" runat="server" Width="60%" />
                            <asp:HiddenField ID="hideFileUpload2" runat="server" Visible="False" />
                        </td>
                    </tr>
                    <tr>
                    <td align="right" style="width: 30%" valign="top">
                        <asp:Label ID="Label4" runat="server" SkinID="MainText" Text="圖片："></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:Image ID="imgMdaFilePath2" runat="server" Height="150px" ImageUrl="Images/default.jpg" />
                    </td>
                </tr>
                    <tr>
                        <td align="right" class="style2">&nbsp;</td>
                        <td align="left" class="style3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" class="style2" colspan="2">
                            <asp:Label ID="lblMeg2" runat="server" ForeColor="Red" SkinID="MainWarn"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="style2" colspan="2">
                            <asp:Button ID="btnEdit2" runat="server" BackColor="Transparent" BorderWidth="0px" CssClass="BigButton" Text="確定修改" ValidationGroup="OK" OnClick="btnEdit2_Click" />
                            <asp:Button ID="btnOK2" runat="server" BackColor="Transparent" BorderWidth="0px" CssClass="SmallButton" Text="確定" ValidationGroup="OK" OnClick="btnOK2_Click" />
                            <asp:Button ID="btnBack2" runat="server" BackColor="Transparent" BorderWidth="0px" CssClass="SmallButton" Text="返回" OnClick="btnBack2_Click" />
                            <asp:HiddenField ID="hideID" runat="server" />
                        </td>
                    </tr>
      
                </table>
            </asp:View>

        </asp:MultiView>

    </div>
    </form>
</body>
</html>
