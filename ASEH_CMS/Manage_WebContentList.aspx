﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Manage_WebContentList.aspx.cs" Inherits="ASEH_CMS.Manage_WebContentList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Breadcrumb -->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="MainIndex.aspx">Dashboard</a></li>
        <li class="breadcrumb-item active">好康底圖管理</li>
      </ol>
    <!-- Breadcrumb Menu-->

<div class="container-fluid">
<div class="animated fadeIn">


<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">

        <div class="row">
        <div class="col-lg-12">

            <%--控制項--%>
            <table style="width: 100%;" >
                <tr>
                    <td align="left">
                         <asp:Button ID="btnAdd" Visible="false" runat="server" BorderColor="Transparent" BorderWidth="0px" CssClass="SmallButton" onclick="btnAdd_Click" Text="新增" />
                    </td>
                    <td align="right" style="width: 20%;">
                      
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        &nbsp;</td>
                    <td align="right" style="width: 20%;">&nbsp;</td>
                </tr>
             </table>

            <div class="card">
            <div class="card-header">
            <i class="fa fa-align-justify"></i> 列表
            </div>

            <div class="card-body">

            <%--列表--%>
            <table style="width: 100%;" >
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSource1" onrowcommand="grid_RowCommand" onrowdatabound="grid_RowDataBound" PageSize="100" Width="100%">
                            <PagerSettings Visible="False" />
                            <Columns>
                                <%--狀態--%>
                                <asp:BoundField DataField="WctSort" HeaderText="排序" Visible="false">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                </asp:BoundField>

                                <asp:BoundField DataField="WctName" HeaderText="名稱" ReadOnly="True">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>

                                <asp:BoundField DataField="WctDesc" HeaderText="說明" Visible="False">
                                <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>

                                <asp:TemplateField HeaderText="是否啟用" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIsEnable" runat="server" Text='<%# Eval("WctEnable") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" Width="80px" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="圖片">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Image ID="imgPic" runat="server" AlternateText='<%# Eval("WctID") + "," + Eval("WctPicPath") %>' ImageUrl="Images/default.jpg" Width="180px" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="80px" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="修改">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("WctID") %>' CommandName="Modify" ImageUrl="Images/Edit.png" ToolTip="修改" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="70px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="刪除" Visible="false">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnDel" runat="server" CausesValidation="False" CommandArgument='<%# Eval("WctID") %>' CommandName="Remove" ImageUrl="Images/Delete.png" onclientclick="return window.confirm('確定要刪除嗎？');" ToolTip="刪除" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="70px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                            SelectMethod="GetAll" 
                            TypeName="Service.WebContentService">
                        </asp:ObjectDataSource>
                        <asp:HiddenField ID="hideLoginUseID" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;</td>
                </tr>
            </table>

            </div>
        </div>

        </div>
        </div>

        </asp:View>
        <asp:View ID="View2" runat="server">

        <div class="row">
        <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
            <i class="fa fa-align-justify"></i> 新增/編輯
            </div>

            <div class="card-body">

            <table style="width:100%;">


                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="lable88" runat="server" SkinID="MainText" Text="名稱"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:TextBox ID="txbWctName2" runat="server" MaxLength="30" Width="25%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvWctName" runat="server" 
                            ControlToValidate="txbWctName2" ErrorMessage="RequiredFieldValidator" 
                            ForeColor="Red" SkinID="MainAlert" ValidationGroup="OK">欄位不可為空</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%">&nbsp;</td>
                    <td align="left" style="width: 70%">&nbsp;</td>
                </tr>

                <tr>
                    <td align="left" style="width: 30%">
                        <asp:Label ID="lblIsEnable0" runat="server" SkinID="MainText" Text="是否啟用" Visible="False"></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:CheckBox ID="ckbIsEnable" runat="server" Checked="True" Text="啟用請打勾" Visible="False" />
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%" valign="top">    
                        <asp:Label ID="lable154" runat="server" SkinID="MainText" Text="圖片上傳："></asp:Label>
                    </td>
                    <td align="left" style="width: 70%">
                        <asp:Image ID="imgWctPicPath" runat="server" ImageUrl="Images/default.jpg" 
                            Width="250px" />
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%" valign="top">
                        &nbsp;</td>
                    <td align="left" style="width: 70%">
                        <asp:FileUpload ID="uploadPicPath" runat="server" Width="60%" />
                        <asp:HiddenField ID="hideFileName" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 30%" valign="top">
                        &nbsp;</td>
                    <td align="left" style="width: 70%">
                        <asp:Label ID="lblPicDesc3_2" runat="server" SkinID="MainAlert">圖片支援（bmp,jpg,jpeg,png）等格式。</asp:Label>
                        <br />
                        <asp:Label ID="lblPositionAlert15" runat="server" SkinID="MainAlert" Text="(圖片尺寸建議 3800 X 3238px)"></asp:Label>
                        <br />
                    </td>
                </tr>
                                <tr>
                                    <td align="left" style="width: 30%" valign="top">&nbsp;</td>
                                    <td align="left" style="width: 70%">&nbsp;</td>
                                </tr>
                              
                <tr>
                    <td align="left" colspan="2" style="text-align: center;" valign="top">
                        <asp:Label ID="lblMeg" runat="server" ForeColor="Red" SkinID="MainWarn"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnEdit" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="BigButton" Text="確定修改" 
                            ValidationGroup="OK" OnClick="btnEdit_Click" />
                        <asp:Button ID="btnOK" runat="server" BackColor="Transparent" BorderWidth="0px" 
                            CssClass="SmallButton" Text="確定" ValidationGroup="OK" OnClick="btnOK_Click" />
                        <asp:Button ID="btnBack" runat="server" BackColor="Transparent" 
                            BorderWidth="0px" CssClass="SmallButton" Text="返回" OnClick="btnBack_Click" />
                        <asp:HiddenField ID="hideID" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        &nbsp;</td>
                </tr>
            </table>

            </div>

        </div>
        </div>
        </div>

        </asp:View>
    </asp:MultiView>

</div>
</div>
<!-- /.conainer-fluid -->

</asp:Content>
