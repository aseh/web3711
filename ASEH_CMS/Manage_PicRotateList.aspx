﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Manage_PicRotateList.aspx.cs" Inherits="ASEH_CMS.Manage_PicRotateList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">

        var jQuery132 = jQuery.noConflict(true);

        //建立日期
        jQuery132(function () {
            jQuery132("#ContentPlaceHolder1_txbOpenDate2").datepicker({ dateFormat: "yy/mm/dd" });
            jQuery132("#ContentPlaceHolder1_txbCloseDate2").datepicker({ dateFormat: "yy/mm/dd" });
        });

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item"><a href="MainIndex.aspx">Dashboard</a></li>
        <li class="breadcrumb-item active">首頁橫幅管理</li>
    </ol>
    <!-- Breadcrumb Menu-->

    <div class="container-fluid">
        <div class="animated fadeIn">


            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                <asp:View ID="View1" runat="server">

                    <div class="row">
                        <div class="col-lg-12">

                            <%--控制項--%>
                            <table style="width: 100%;">
                                <tr>
                                    <td align="left">
                                        <asp:Button ID="btnAdd" runat="server" BorderColor="Transparent" BorderWidth="0px" CssClass="SmallButton" OnClick="btnAdd_Click" Text="新增" />
                                    </td>
                                    <td align="right" style="width: 20%;"></td>
                                </tr>
                                <tr>
                                    <td align="left">&nbsp;</td>
                                    <td align="right" style="width: 20%;">&nbsp;</td>
                                </tr>
                            </table>

                            <div class="card">
                                <div class="card-header">
                                    <i class="fa fa-align-justify"></i>列表
                                </div>

                                <div class="card-body">

                                    <%--列表--%>
                                    <table style="width: 100%;">
                                        <tr>
                                            <td colspan="2">
                                                <asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSource1" OnRowCommand="grid_RowCommand" OnRowDataBound="grid_RowDataBound" PageSize="100" Width="100%">
                                                    <PagerSettings Visible="False" />
                                                    <Columns>
                                                        <%--狀態--%>
                                                        <asp:BoundField DataField="ProSort" HeaderText="排序">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                                                        </asp:BoundField>

                                                        <asp:BoundField DataField="ProStartDate"
                                                            DataFormatString="{0:yyyy/MM/dd HH:mm}" HeaderText="上架日期" Visible="false">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" Width="160px" />
                                                        </asp:BoundField>

                                                        <asp:BoundField DataField="ProEndDate"
                                                            DataFormatString="{0:yyyy/MM/dd HH:mm}" HeaderText="下架日期" Visible="false">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" Width="160px" />
                                                        </asp:BoundField>

                                                        <asp:BoundField DataField="ProName" HeaderText="標題" ReadOnly="True">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>

                                                        <asp:BoundField DataField="ProDesc" HeaderText="副標題" ReadOnly="True" Visible="false">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>

                                                        <asp:BoundField DataField="ProDesc" HeaderText="說明" Visible="False">
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="是否啟用">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblIsEnable" runat="server" Text='<%# Eval("ProEnable") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" Width="80px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="是否顯示文字">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblIsShowText" runat="server" Text='<%# Eval("ProShowText") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" Width="80px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="是否顯示更多按鈕">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblIsShowMoreBtn" runat="server" Text='<%# Eval("ProShowMoreBtn") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" Width="80px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="是否顯示灰框">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblIsShowBgBorder" runat="server" Text='<%# Eval("ProShowBgBorder") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" Width="80px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="圖片">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:Image ID="imgPic" runat="server" AlternateText='<%# Eval("ProID") + "," + Eval("ProPicPath") %>' ImageUrl="Images/default.jpg" Width="220px" />
                                                                <video runat="server" id="videoImg" width="200" controls />
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="220px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="向上">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgbtnSortUp" runat="server" CausesValidation="False" CommandArgument='<%# Eval("ProID") + "," + Eval("ProSort") %>' CommandName="SortUp" ImageUrl="Images/SortUp.gif" ToolTip="向上" />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="向下">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgbtnSortDown" runat="server" CausesValidation="False" CommandArgument='<%# Eval("ProID") + "," + Eval("ProSort") %>' CommandName="SortDown" ImageUrl="Images/SortDown.gif" ToolTip="向下" />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="修改">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("ProID") %>' CommandName="Modify" ImageUrl="Images/Edit.png" ToolTip="修改" />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="70px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="刪除">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgbtnDel" runat="server" CausesValidation="False" CommandArgument='<%# Eval("ProID") %>' CommandName="Remove" ImageUrl="Images/Delete.png" OnClientClick="return window.confirm('確定要刪除嗎？');" ToolTip="刪除" />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="70px" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
                                                    SelectMethod="GetAllBySort"
                                                    TypeName="Service.PicRotateService"></asp:ObjectDataSource>
                                                <asp:HiddenField ID="hideLoginUseID" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                    </table>

                                </div>
                            </div>

                        </div>
                    </div>

                </asp:View>
                <asp:View ID="View2" runat="server">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <i class="fa fa-align-justify"></i>新增/編輯
                                </div>

                                <div class="card-body">

                                    <table style="width: 100%;">

                                        <tr>
                                            <td align="left" style="width: 30%">
                                                <asp:Label ID="Label45" runat="server" SkinID="MainText" Text="上架日期" Visible="False"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 70%">
                                                <asp:TextBox ID="txbOpenDate2" runat="server" SkinID="MainText" Text="沒有資料" Width="100px" Visible="False"></asp:TextBox>
                                                &nbsp;<asp:DropDownList ID="ddlOpenDateH2" runat="server" Width="50px" Visible="False">
                                                    <asp:ListItem>00</asp:ListItem>
                                                    <asp:ListItem>01</asp:ListItem>
                                                    <asp:ListItem>02</asp:ListItem>
                                                    <asp:ListItem>03</asp:ListItem>
                                                    <asp:ListItem>04</asp:ListItem>
                                                    <asp:ListItem>05</asp:ListItem>
                                                    <asp:ListItem>06</asp:ListItem>
                                                    <asp:ListItem>07</asp:ListItem>
                                                    <asp:ListItem>08</asp:ListItem>
                                                    <asp:ListItem>09</asp:ListItem>
                                                    <asp:ListItem>10</asp:ListItem>
                                                    <asp:ListItem>11</asp:ListItem>
                                                    <asp:ListItem>12</asp:ListItem>
                                                    <asp:ListItem>13</asp:ListItem>
                                                    <asp:ListItem>14</asp:ListItem>
                                                    <asp:ListItem>15</asp:ListItem>
                                                    <asp:ListItem>16</asp:ListItem>
                                                    <asp:ListItem>17</asp:ListItem>
                                                    <asp:ListItem>18</asp:ListItem>
                                                    <asp:ListItem>19</asp:ListItem>
                                                    <asp:ListItem>20</asp:ListItem>
                                                    <asp:ListItem>21</asp:ListItem>
                                                    <asp:ListItem>22</asp:ListItem>
                                                    <asp:ListItem>23</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:Label ID="lable149" runat="server" SkinID="MainText" Text="時" Visible="False"></asp:Label>
                                                &nbsp;<asp:DropDownList ID="ddlOpenDateM2" runat="server" Width="50px" Visible="False">
                                                    <asp:ListItem>00</asp:ListItem>
                                                    <asp:ListItem>01</asp:ListItem>
                                                    <asp:ListItem>02</asp:ListItem>
                                                    <asp:ListItem>03</asp:ListItem>
                                                    <asp:ListItem>04</asp:ListItem>
                                                    <asp:ListItem>05</asp:ListItem>
                                                    <asp:ListItem>06</asp:ListItem>
                                                    <asp:ListItem>07</asp:ListItem>
                                                    <asp:ListItem>08</asp:ListItem>
                                                    <asp:ListItem>09</asp:ListItem>
                                                    <asp:ListItem>10</asp:ListItem>
                                                    <asp:ListItem>11</asp:ListItem>
                                                    <asp:ListItem>12</asp:ListItem>
                                                    <asp:ListItem>13</asp:ListItem>
                                                    <asp:ListItem>14</asp:ListItem>
                                                    <asp:ListItem>15</asp:ListItem>
                                                    <asp:ListItem>16</asp:ListItem>
                                                    <asp:ListItem>17</asp:ListItem>
                                                    <asp:ListItem>18</asp:ListItem>
                                                    <asp:ListItem>19</asp:ListItem>
                                                    <asp:ListItem>20</asp:ListItem>
                                                    <asp:ListItem>21</asp:ListItem>
                                                    <asp:ListItem>22</asp:ListItem>
                                                    <asp:ListItem>23</asp:ListItem>
                                                    <asp:ListItem>24</asp:ListItem>
                                                    <asp:ListItem>25</asp:ListItem>
                                                    <asp:ListItem>26</asp:ListItem>
                                                    <asp:ListItem>27</asp:ListItem>
                                                    <asp:ListItem>28</asp:ListItem>
                                                    <asp:ListItem>29</asp:ListItem>
                                                    <asp:ListItem>30</asp:ListItem>
                                                    <asp:ListItem>31</asp:ListItem>
                                                    <asp:ListItem>32</asp:ListItem>
                                                    <asp:ListItem>33</asp:ListItem>
                                                    <asp:ListItem>34</asp:ListItem>
                                                    <asp:ListItem>35</asp:ListItem>
                                                    <asp:ListItem>36</asp:ListItem>
                                                    <asp:ListItem>37</asp:ListItem>
                                                    <asp:ListItem>38</asp:ListItem>
                                                    <asp:ListItem>39</asp:ListItem>
                                                    <asp:ListItem>40</asp:ListItem>
                                                    <asp:ListItem>41</asp:ListItem>
                                                    <asp:ListItem>42</asp:ListItem>
                                                    <asp:ListItem>43</asp:ListItem>
                                                    <asp:ListItem>44</asp:ListItem>
                                                    <asp:ListItem>45</asp:ListItem>
                                                    <asp:ListItem>46</asp:ListItem>
                                                    <asp:ListItem>47</asp:ListItem>
                                                    <asp:ListItem>48</asp:ListItem>
                                                    <asp:ListItem>49</asp:ListItem>
                                                    <asp:ListItem>50</asp:ListItem>
                                                    <asp:ListItem>51</asp:ListItem>
                                                    <asp:ListItem>52</asp:ListItem>
                                                    <asp:ListItem>53</asp:ListItem>
                                                    <asp:ListItem>54</asp:ListItem>
                                                    <asp:ListItem>55</asp:ListItem>
                                                    <asp:ListItem>56</asp:ListItem>
                                                    <asp:ListItem>57</asp:ListItem>
                                                    <asp:ListItem>58</asp:ListItem>
                                                    <asp:ListItem>59</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:Label ID="lable150" runat="server" SkinID="MainText" Text="分" Visible="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%">
                                                <asp:Label ID="Label48" runat="server" SkinID="MainText" Text="下架日期" Visible="False"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 70%">
                                                <asp:TextBox ID="txbCloseDate2" runat="server" SkinID="MainText" Text="沒有資料" Width="100px" Visible="False"></asp:TextBox>
                                                &nbsp;<asp:DropDownList ID="ddlCloseDateH2" runat="server" Width="50px" Visible="False">
                                                    <asp:ListItem>00</asp:ListItem>
                                                    <asp:ListItem>01</asp:ListItem>
                                                    <asp:ListItem>02</asp:ListItem>
                                                    <asp:ListItem>03</asp:ListItem>
                                                    <asp:ListItem>04</asp:ListItem>
                                                    <asp:ListItem>05</asp:ListItem>
                                                    <asp:ListItem>06</asp:ListItem>
                                                    <asp:ListItem>07</asp:ListItem>
                                                    <asp:ListItem>08</asp:ListItem>
                                                    <asp:ListItem>09</asp:ListItem>
                                                    <asp:ListItem>10</asp:ListItem>
                                                    <asp:ListItem>11</asp:ListItem>
                                                    <asp:ListItem>12</asp:ListItem>
                                                    <asp:ListItem>13</asp:ListItem>
                                                    <asp:ListItem>14</asp:ListItem>
                                                    <asp:ListItem>15</asp:ListItem>
                                                    <asp:ListItem>16</asp:ListItem>
                                                    <asp:ListItem>17</asp:ListItem>
                                                    <asp:ListItem>18</asp:ListItem>
                                                    <asp:ListItem>19</asp:ListItem>
                                                    <asp:ListItem>20</asp:ListItem>
                                                    <asp:ListItem>21</asp:ListItem>
                                                    <asp:ListItem>22</asp:ListItem>
                                                    <asp:ListItem>23</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:Label ID="lable151" runat="server" SkinID="MainText" Text="時" Visible="False"></asp:Label>
                                                &nbsp;<asp:DropDownList ID="ddlCloseDateM2" runat="server" Width="50px" Visible="False">
                                                    <asp:ListItem>00</asp:ListItem>
                                                    <asp:ListItem>01</asp:ListItem>
                                                    <asp:ListItem>02</asp:ListItem>
                                                    <asp:ListItem>03</asp:ListItem>
                                                    <asp:ListItem>04</asp:ListItem>
                                                    <asp:ListItem>05</asp:ListItem>
                                                    <asp:ListItem>06</asp:ListItem>
                                                    <asp:ListItem>07</asp:ListItem>
                                                    <asp:ListItem>08</asp:ListItem>
                                                    <asp:ListItem>09</asp:ListItem>
                                                    <asp:ListItem>10</asp:ListItem>
                                                    <asp:ListItem>11</asp:ListItem>
                                                    <asp:ListItem>12</asp:ListItem>
                                                    <asp:ListItem>13</asp:ListItem>
                                                    <asp:ListItem>14</asp:ListItem>
                                                    <asp:ListItem>15</asp:ListItem>
                                                    <asp:ListItem>16</asp:ListItem>
                                                    <asp:ListItem>17</asp:ListItem>
                                                    <asp:ListItem>18</asp:ListItem>
                                                    <asp:ListItem>19</asp:ListItem>
                                                    <asp:ListItem>20</asp:ListItem>
                                                    <asp:ListItem>21</asp:ListItem>
                                                    <asp:ListItem>22</asp:ListItem>
                                                    <asp:ListItem>23</asp:ListItem>
                                                    <asp:ListItem>24</asp:ListItem>
                                                    <asp:ListItem>25</asp:ListItem>
                                                    <asp:ListItem>26</asp:ListItem>
                                                    <asp:ListItem>27</asp:ListItem>
                                                    <asp:ListItem>28</asp:ListItem>
                                                    <asp:ListItem>29</asp:ListItem>
                                                    <asp:ListItem>30</asp:ListItem>
                                                    <asp:ListItem>31</asp:ListItem>
                                                    <asp:ListItem>32</asp:ListItem>
                                                    <asp:ListItem>33</asp:ListItem>
                                                    <asp:ListItem>34</asp:ListItem>
                                                    <asp:ListItem>35</asp:ListItem>
                                                    <asp:ListItem>36</asp:ListItem>
                                                    <asp:ListItem>37</asp:ListItem>
                                                    <asp:ListItem>38</asp:ListItem>
                                                    <asp:ListItem>39</asp:ListItem>
                                                    <asp:ListItem>40</asp:ListItem>
                                                    <asp:ListItem>41</asp:ListItem>
                                                    <asp:ListItem>42</asp:ListItem>
                                                    <asp:ListItem>43</asp:ListItem>
                                                    <asp:ListItem>44</asp:ListItem>
                                                    <asp:ListItem>45</asp:ListItem>
                                                    <asp:ListItem>46</asp:ListItem>
                                                    <asp:ListItem>47</asp:ListItem>
                                                    <asp:ListItem>48</asp:ListItem>
                                                    <asp:ListItem>49</asp:ListItem>
                                                    <asp:ListItem>50</asp:ListItem>
                                                    <asp:ListItem>51</asp:ListItem>
                                                    <asp:ListItem>52</asp:ListItem>
                                                    <asp:ListItem>53</asp:ListItem>
                                                    <asp:ListItem>54</asp:ListItem>
                                                    <asp:ListItem>55</asp:ListItem>
                                                    <asp:ListItem>56</asp:ListItem>
                                                    <asp:ListItem>57</asp:ListItem>
                                                    <asp:ListItem>58</asp:ListItem>
                                                    <asp:ListItem>59</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:Label ID="lable152" runat="server" SkinID="MainText" Text="分" Visible="False"></asp:Label>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left" style="width: 30%">
                                                <asp:Label ID="lable88" runat="server" SkinID="MainText" Text="標題"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 70%">
                                                <asp:TextBox ID="txbProName2" runat="server" MaxLength="30" Width="35%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvProName" runat="server"
                                                    ControlToValidate="txbProName2" ErrorMessage="RequiredFieldValidator"
                                                    ForeColor="Red" SkinID="MainAlert" ValidationGroup="OK">欄位不可為空</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%">
                                                <asp:Label ID="lable157" runat="server" SkinID="MainText" Text="標題(英文)"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 70%">
                                                <asp:TextBox ID="txbProName_EN2" runat="server" MaxLength="30" Width="35%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvProName0" runat="server" ControlToValidate="txbProName2" ErrorMessage="RequiredFieldValidator" ForeColor="Red" SkinID="MainAlert" ValidationGroup="OK">欄位不可為空</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%">&nbsp;</td>
                                            <td align="left" style="width: 70%">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%; vertical-align: top;">
                                                <asp:Label ID="lable155" runat="server" SkinID="MainText" Text="內容"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 70%; height: 29px;">
                                                <asp:TextBox ID="txbProDesc2" runat="server" MaxLength="300" Width="70%" Height="80px" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%; vertical-align: top;">
                                                <asp:Label ID="lable158" runat="server" SkinID="MainText" Text="內容(英文)"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 70%">
                                                <asp:TextBox ID="txbProDesc_EN2" runat="server" MaxLength="300" Width="70%" Height="80px" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%">&nbsp;</td>
                                            <td align="left" style="width: 70%">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%">
                                                <asp:Label ID="lable153" runat="server" SkinID="MainText" Text="對外連結"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 70%">
                                                <asp:TextBox ID="txbProUrl2" runat="server" MaxLength="300" Width="70%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%">
                                                <asp:Label ID="lable159" runat="server" SkinID="MainText" Text="對外連結(英文)"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 70%">
                                                <asp:TextBox ID="txbProUrl_EN2" runat="server" MaxLength="300" Width="70%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%">&nbsp;</td>
                                            <td align="left" style="width: 70%">
                                                <asp:Label ID="Label49" runat="server" SkinID="MainAlert" Text="網址請輸入Domain Name後面網址(例如： /ch/csr_download.html)"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%">&nbsp;</td>
                                            <td align="left" style="width: 70%">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%">
                                                <asp:Label ID="lblIsEnable0" runat="server" SkinID="MainText" Text="是否啟用"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 70%">
                                                <asp:CheckBox ID="ckbIsEnable" runat="server" Checked="True" Text="啟用請打勾" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%">
                                                <asp:Label ID="Label50" runat="server" SkinID="MainText" Text="顯示文字"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 70%">
                                                <asp:CheckBox ID="ckbIsShowText" runat="server" Checked="True" Text="顯示請打勾" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%; height: 26px;">
                                                <asp:Label ID="Label51" runat="server" SkinID="MainText" Text="顯示「了解更多」按鈕"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 70%; height: 26px;">
                                                <asp:CheckBox ID="ckbIsShowMoreBtn" runat="server" Checked="True" Text="顯示請打勾" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%">
                                                <asp:Label ID="Label52" runat="server" SkinID="MainText" Text="顯示灰框"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 70%">
                                                <asp:CheckBox ID="ckbIsShowBgBorder" runat="server" Checked="True" Text="顯示請打勾" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%">&nbsp;</td>
                                            <td align="left" style="width: 70%">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%" valign="top">
                                                <asp:Label ID="lable154" runat="server" SkinID="MainText" Text="電腦版圖片上傳(中文)"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 70%">
                                                <asp:Image ID="imgProPicPath" runat="server" ImageUrl="Images/default.jpg"
                                                    Width="250px" />
                                                <video id="imgProPicPathVideo1" runat="server" controls width="400"></video>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%" valign="top">&nbsp;</td>
                                            <td align="left" style="width: 70%">
                                                <asp:FileUpload ID="uploadPicPath" runat="server" Width="60%" />
                                                <asp:HiddenField ID="hideFileName" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%" valign="top">&nbsp;</td>
                                            <td align="left" style="width: 70%">
                                                <asp:Label ID="lblPicDesc3_2" runat="server" SkinID="MainAlert">圖片支援（bmp,jpg,jpeg,png,gif,mp4）等格式。</asp:Label>
                                                <br />
                                                <asp:Label ID="lblPositionAlert15" runat="server" SkinID="MainAlert" Text="(圖片尺寸建議 1920 X 480px)"></asp:Label>
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%" valign="top">&nbsp;</td>
                                            <td align="left" style="width: 70%">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%" valign="top">
                                                <asp:Label ID="Label1" runat="server" SkinID="MainText" Text="手機板圖片上傳(中文)"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 70%">
                                                <asp:Image ID="imgProPicPath2" runat="server" ImageUrl="Images/default.jpg"
                                                    Width="150px" />
                                                <video id="imgProPicPathVideo2" runat="server" controls width="400"></video>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%" valign="top">&nbsp;</td>
                                            <td align="left" style="width: 70%">
                                                <asp:FileUpload ID="uploadPicPath2" runat="server" Width="60%" />
                                                <asp:HiddenField ID="hideFileName2" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%" valign="top">&nbsp;</td>
                                            <td align="left" style="width: 70%">
                                                <asp:Label ID="Label2" runat="server" SkinID="MainAlert">圖片支援（bmp,jpg,jpeg,png,gif,mp4）等格式。</asp:Label>
                                                <br />
                                                <asp:Label ID="lblPositionAlert16" runat="server" SkinID="MainAlert" Text="(圖片尺寸建議 900 X 900px)"></asp:Label>
                                                <br />
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td align="left" style="width: 30%">&nbsp;</td>
                                            <td align="left" style="width: 70%">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%" valign="top">
                                                <asp:Label ID="Label3" runat="server" SkinID="MainText" Text="電腦版圖片上傳(英文)"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 70%">
                                                <asp:Image ID="imgProPicPath3" runat="server" ImageUrl="Images/default.jpg"
                                                    Width="250px" />
                                                <video id="imgProPicPathVideo3" runat="server" controls width="400"></video>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%" valign="top">&nbsp;</td>
                                            <td align="left" style="width: 70%">
                                                <asp:FileUpload ID="uploadPicPath3" runat="server" Width="60%" />
                                                <asp:HiddenField ID="hideFileName3" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%" valign="top">&nbsp;</td>
                                            <td align="left" style="width: 70%">
                                                <asp:Label ID="Label4" runat="server" SkinID="MainAlert">圖片支援（bmp,jpg,jpeg,png,gif,mp4）等格式。</asp:Label>
                                                <br />
                                                <asp:Label ID="Label5" runat="server" SkinID="MainAlert" Text="(圖片尺寸建議 1920 X 480px)"></asp:Label>
                                                <br />
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td align="left" style="width: 30%" valign="top">&nbsp;</td>
                                            <td align="left" style="width: 70%">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%" valign="top">
                                                <asp:Label ID="Label6" runat="server" SkinID="MainText" Text="手機板圖片上傳(英文)"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 70%">
                                                <asp:Image ID="imgProPicPath4" runat="server" ImageUrl="Images/default.jpg"
                                                    Width="150px" />
                                                <video id="imgProPicPathVideo4" runat="server" controls width="400"></video>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%" valign="top">&nbsp;</td>
                                            <td align="left" style="width: 70%">
                                                <asp:FileUpload ID="uploadPicPath4" runat="server" Width="60%" />
                                                <asp:HiddenField ID="hideFileName4" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%" valign="top">&nbsp;</td>
                                            <td align="left" style="width: 70%">
                                                <asp:Label ID="Label7" runat="server" SkinID="MainAlert">圖片支援（bmp,jpg,jpeg,png,gif,mp4）等格式。</asp:Label>
                                                <br />
                                                <asp:Label ID="Label8" runat="server" SkinID="MainAlert" Text="(圖片尺寸建議 900 X 900px)"></asp:Label>
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%" valign="top">&nbsp;</td>
                                            <td align="left" style="width: 70%">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%" valign="top">
                                                <asp:Label ID="lable156" runat="server" SkinID="MainText" Text="圖片描述" Visible="False"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 70%">
                                                <asp:TextBox ID="txbProPicDesc2" runat="server" MaxLength="50" Width="50%" Visible="False"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 30%" valign="top">&nbsp;</td>
                                            <td align="left" style="width: 70%">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="2" style="text-align: center;" valign="top">
                                                <asp:Label ID="lblMeg" runat="server" ForeColor="Red" SkinID="MainWarn"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="2" style="text-align: center;" valign="top">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2">
                                                <asp:Button ID="btnEdit" runat="server" BackColor="Transparent"
                                                    BorderWidth="0px" CssClass="BigButton" Text="確定修改"
                                                    ValidationGroup="OK" OnClick="btnEdit_Click" />
                                                <asp:Button ID="btnOK" runat="server" BackColor="Transparent" BorderWidth="0px"
                                                    CssClass="SmallButton" Text="確定" ValidationGroup="OK" OnClick="btnOK_Click" />
                                                <asp:Button ID="btnBack" runat="server" BackColor="Transparent"
                                                    BorderWidth="0px" CssClass="SmallButton" Text="返回" OnClick="btnBack_Click" />
                                                <asp:HiddenField ID="hideID" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2">&nbsp;</td>
                                        </tr>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>

                </asp:View>
            </asp:MultiView>

        </div>
    </div>
    <!-- /.conainer-fluid -->

</asp:Content>
