﻿using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models;
using Service.Interface;
using Service.Misc;
using System.Web.Security;

namespace ASEH_CMS
{
    public partial class Logout : System.Web.UI.Page
    {
        UserInfo User = new UserInfo();
        private IEventRecordService _eventRecordService;

        protected void Page_Load(object sender, EventArgs e)
        {
            this._eventRecordService = new EventRecordService();

            //寫入登入LOG
            User = (UserInfo)Session["UserInfo"];
            if (User != null)
            {
                //EVENT LOG
                User = (UserInfo)Session["UserInfo"];
                string strDesc = string.Format("使用者［{0}］後台登出", User.UserID);

                var model = new tbEventRecord();
                model.CreateTime = DateTime.Now;
                model.EvrTypeID = "EVT002";
                model.EvrMsg = strDesc;
                model.UseID = User.UserID;
                _eventRecordService.Create(model);
            }
            Session["UserInfo"] = null;
            Server.Transfer("Login.aspx"); 
        }
    }
}