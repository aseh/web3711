﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models;
using Service;
using Service.Interface;
using Service.Misc;
using ASEH_CMS.App_Code;

namespace ASEH_CMS
{
    public partial class System_PermissionList : PageBase
    {
        private IExceptionRecordService _exceptionRecordService;
        private IPermissionService _permissionService;

        new protected int iPageType = 1;
        new protected string strPageID = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            this._exceptionRecordService = new ExceptionRecordService();
            this._permissionService = new PermissionService();

            CheckUser();
            //CheckUserPermission(strPageID, iPageType);
        }

        //新增按鈕的動作
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            this.txbPerID.Enabled = true;
            this.txbPerID.Text = "";
            this.txbPerName.Text = "";
            this.txbPerPage.Text = "";

            this.btnOK.Visible = true;
            this.btnEdit.Visible = false;
            this.MultiView1.ActiveViewIndex = 1;
        }

        //返回按鈕
        protected void btnBack_Click(object sender, EventArgs e)
        {
            this.MultiView1.ActiveViewIndex = 0;
        }

        //新增存檔
        protected void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new tbPermission();
                model.PerID = txbPerID.Text;
                model.PerName = txbPerName.Text;
                model.PerPage = txbPerPage.Text;

                _permissionService.Create(model);

                //ShowMessage("儲存成功");
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }

            this.MultiView1.ActiveViewIndex = 0;
            this.grid.DataBind();
        }

        //修改存檔
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var model = new tbPermission();
                model.PerID = txbPerID.Text;
                model.PerName = txbPerName.Text;
                model.PerPage = txbPerPage.Text;

                _permissionService.Update(model);

                //ShowMessage("儲存成功");
            }
            catch (Exception ex)
            {
                var model = new tbExceptionRecord();
                model.CreateTime = DateTime.Now;
                model.ExrTypeID = "EXT001";
                model.ExrMsg = ex.ToString();
                _exceptionRecordService.Create(model);

                ShowMessage("儲存失敗");
            }

            this.MultiView1.ActiveViewIndex = 0;
            this.grid.DataBind();
        }

        #region GridView動作

        protected void grid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Modify":
                    {
                        string PerID = e.CommandArgument.ToString();

                        var model = _permissionService.GetByID(PerID);

                        this.txbPerID.Text = model.PerID;
                        this.txbPerID.Enabled = false;//禁止修改帳號
                        this.hideID.Value = model.PerID;
                        this.txbPerName.Text = model.PerName;
                        this.txbPerPage.Text = model.PerPage;

                        this.btnOK.Visible = false;
                        this.btnEdit.Visible = true;
                        this.MultiView1.ActiveViewIndex = 1;
                        break;
                    }
                case "Cancel":
                    {
                        string PerID = e.CommandArgument.ToString();

                        _permissionService.Delete(PerID);

                        this.grid.DataBind();
                        break;
                    }
                default:
                    {
                        return;
                    }
            }
        }

        #endregion
    }
}