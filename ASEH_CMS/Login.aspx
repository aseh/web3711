﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ASEH_CMS.Login" %>

<!DOCTYPE html>

<html dir="ltr" lang="zh-TW" itemscope itemtype="http://schema.org/Blog">
<!--<![endif]-->
<head runat="server">
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="海棠設計"> 
<meta name="keywords" content=""> 
<meta name="description" content=""> 
<link rel="shortcut icon" href="img/favicon.png" />

<title>後端管理系統</title>
<link href="css/index.css?1" rel="stylesheet" type="text/css" />
</head>

<body>
<form id="form3" runat="server">
    <div id="WRAPPER">
        <article id="CONTENT">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="52" height="26" align="left">帳&nbsp;&nbsp;&nbsp;&nbsp;號：</td>
              <td colspan="2" align="left" style="background-image:url(images/login.png); background-repeat:no-repeat; padding-left:8px; background-position:left 3px;">
             
                  <%-- <input type="text" name="textfield" id="textfield" style=" color:#fff; width:132px; height:14px; border:0; background-color:#fc958d;" />--%>
                    <asp:TextBox ID="UserID" runat="server" style=" color:#fff; width:132px; height:14px; border:0; background-color:#fc958d;"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvUserID" runat="server" 
                    ControlToValidate="UserID" ErrorMessage="RequiredFieldValidator" 
                    SkinID="MainAlert" ValidationGroup="OK" ForeColor="Red">*</asp:RequiredFieldValidator>

              </td>
              </tr>
            <tr>
              <td height="26" align="left">密&nbsp;&nbsp;&nbsp;&nbsp;碼：</td>
              <td colspan="2" align="left" style="background-image:url(images/main_input_bg.jpg); background-repeat:no-repeat; padding-left:8px; background-position:left 3px;">
            
                   <%-- <input type="password" name="textfield" id="textfield" style=" color:#fff; width:132px; height:14px; border:0; background-color:#fc958d;" />--%>
                    <asp:TextBox ID="Password" runat="server" TextMode="Password" style=" color:#fff; width:132px; height:14px; border:0; background-color:#fc958d;"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" 
                    ControlToValidate="Password" ErrorMessage="RequiredFieldValidator" 
                    SkinID="MainAlert" ValidationGroup="OK" ForeColor="Red">*</asp:RequiredFieldValidator>

              </td>
              </tr>
            <tr>
              <td height="45" align="left">驗證碼：</td>
              <td width="54" align="left" valign="top" style="background-image:url(images/main_input_bg2.jpg); background-repeat:no-repeat; background-position:left 11px; padding-left:4px; padding-top:10px;"><form id="form2" name="form1" method="post" action="">
                
                  <%--<input type="text" name="textfield2" id="textfield2" style=" font-size:11px; color:#fff; width:44px; height:14px; border:0; background-color:#fc958d;" />--%>
                  <asp:TextBox ID="txbRanCode" runat="server" style=" font-size:11px; color:#fff; width:44px; height:14px; border:0; background-color:#fc958d;"></asp:TextBox>

              </form></td>
              <td width="104">

                 <%-- <img src="images/passcode.jpg" width="100" height="35">--%>
                 <span style=" position:relative; top:8px;"><img id="captcha" src="CAPTCHA.ashx" style=" position:relative; width:100px; height:40px;" /></span>
                 <%--<input id="contact_button" class="button_s" type="button" value="看不到!" onclick="captcha.src = 'CAPTCHA.ashx?r=' + Math.random()" />--%>

              </td>
            </tr>
            <tr>
              <td height="58" colspan="3" align="left" valign="top" style="padding-top:11px; padding-left:57px;">
                  
                  <%--<a href="page.html"><img src="images/btn_login.jpg" width="106" height="47" class="imgover"></a>--%>
                  <asp:ImageButton ID="imgSend" runat="server" TabIndex="0" OnClick="imgSend_Click" Width="106px" Height="47px" ValidationGroup="OK" OnClientClick='setCookie();' ImageUrl="~/Images/btn_login.jpg" /><br />
                  &nbsp;&nbsp;&nbsp;
                  <asp:Label ID="lblLoginMsg" runat="server" Font-Size="Smaller" ForeColor="White"></asp:Label><br />

              </td>
              </tr>
          </table>
        </article>
    </div>
<script type="text/javascript" src="js/rollover.js"></script>
</form>
</body>
</html>

