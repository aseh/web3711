﻿using System.Net.Http;
using System.Net;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
using ASEH_Backend.Services;
using System;

namespace ASEH_Backend.Models
{
    public class ErrorAttribute: ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnException(actionExecutedContext);
            if ( actionExecutedContext.Exception != null )
            {
                actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse(new BaseResponse(actionExecutedContext.Exception.Message));
            }
            
        }
    }

    public class UserAuthAttribute: AuthorizationFilterAttribute
    {
        UserService Service = new UserService();
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            base.OnAuthorization(actionContext);
            var token = actionContext.Request.Headers.GetCookies("token");
            
            try
            {
                if (token.Count == 0)
                {
                    throw new Exception();
                }
                var tokenData = Service.ValidToken(token[0].ToString());
                var dbData = Service.GetById(tokenData.Id);
            } catch ( Exception )
            {
                throw new Exception("尚未登入");
            }

        }
    }

    public class AdminAuthAttribute: AuthorizationFilterAttribute
    {
        UserService Service = new UserService();
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            base.OnAuthorization(actionContext);
            var token = actionContext.Request.Headers.GetCookies("token");
            if (token.Count == 0)
            {
                throw new Exception("尚未登入");
            }

        }
    }
}