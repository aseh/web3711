﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASEH_Backend.Models
{
    public class BaseResponse
    {
        public int ErrCode { get; set; }
        public string ErrMessage { get; set; }
        public BaseResponse()
        {
            this.ErrMessage = "OK";
            this.ErrCode = 200;
        }

        public BaseResponse(string ErrMessage )
        {
            this.ErrMessage = ErrMessage;
            this.ErrCode = 400;
        }
        public BaseResponse(string ErrMessage, int ErrCode )
        {
            this.ErrMessage = ErrMessage;
            this.ErrCode = ErrCode;
        }
    }
    public class ListResponse<T>: BaseResponse
    {
        public List<T> Items;
        public int Total;

        public ListResponse()
        {

        }
        public ListResponse(List<T> Items) 
        {
            this.Items = Items;
        }
        
    }
    public class ResultResponse<T>: BaseResponse
    {
        public T Result;
        public ResultResponse (T Result)
        {
            this.Result = Result;
        }
    }
}