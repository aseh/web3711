﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASEH_Backend.Models
{

    [System.AttributeUsage(System.AttributeTargets.Class | System.AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class ConsumesAttribute : Attribute
    {
        public string[] ContentTypes { get; set; }
        public ConsumesAttribute(params string[] contentTypes)
        {
            ContentTypes = contentTypes;
        }
    }

    [AttributeUsage(AttributeTargets.Parameter | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class FromFormAttribute : Attribute
    {
        public string Name { get; set; }
    }
}