﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASEH_Backend.Models;
using Models;

namespace ASEH_Backend.Services
{

    public class NewsBody : News
    {
        public bool IsSendTester { get; set; }
        public bool IsSendSubscribe { get; set; }
    }

    public class NewsItem: News
    {
        public NewsTag NewsTag;
        public NewsItem( News a )
        {
            Id = a.Id;
            Title = a.Title;
            Content = a.Content;
            UpdatedAt = a.UpdatedAt;
            CreatedAt = a.CreatedAt;
            DeletedAt = a.DeletedAt;
            TagId = a.TagId;
            Tester = a.Tester;
            SendCount = a.SendCount;
            ExpectDate = a.ExpectDate;
            SendAt = a.SendAt;
        }

        public NewsItem( News a, IEnumerable<NewsTag> b )
        {
            Id = a.Id;
            Title = a.Title;
            Content = a.Content;
            UpdatedAt = a.UpdatedAt;
            CreatedAt = a.CreatedAt;
            DeletedAt = a.DeletedAt;
            TagId = a.TagId;
            Tester = a.Tester;
            ExpectDate = a.ExpectDate;
            SendAt = a.SendAt;
            SendCount = a.SendCount;
            NewsTag = b.Count() > 0 ? b.First() : null;

        }
    }
    public class NewsService: BaseService
    {
        public enum SubscribeStatus {
            ALL,
            YES,
            NO
        }
        public News AddNews(NewsBody data)
        {
            
            var AddData = new News();
            AddData.Content = data.Content;
            AddData.ExpectDate = data.ExpectDate;
            AddData.Title = data.Title;
            AddData.TagId = data.TagId;
            AddData.Tester = data.Tester;
            AddData.CreatedAt = DateTime.Now;
            AddData.UpdatedAt = DateTime.Now;
            Db.News.Add(AddData);
            Db.SaveChanges();


            if ( data.IsSendSubscribe )
            {
                SendById(AddData.Id);
            }
            if ( data.IsSendTester )
            {
                SendToTester(AddData.Id);
            }
                
            return AddData;
        }

        public News UpdateNews(int Id, NewsBody data )
        {
            var dbData = Db.News.First(x => x.Id == Id);
            dbData.UpdatedAt = DateTime.Now;
            dbData.Title = data.Title;
            dbData.Content = data.Content;
            dbData.ExpectDate = data.ExpectDate;
            dbData.TagId = data.TagId;
            dbData.Tester = data.Tester;
            Db.SaveChanges();

            if (data.IsSendSubscribe)
            {
                SendById(data.Id);
            }
            if (data.IsSendTester)
            {
                SendToTester(data.Id);
            }
            return data;
        }

        public News DeleteNews(int Id)
        {
            var dbData = Db.News.First(x => x.Id == Id);
            Db.News.Remove(dbData);
            Db.SaveChanges();
            return dbData;
        }

        public ListResponse<NewsItem> Search(
            string Keyword, 
            int? TagId, 
            SubscribeStatus? Status, 
            DateTime? StartAt, 
            DateTime? EndAt,
            int? page,
            int? pageSize
        )
        {
            var News = Db.News.Where(x => x.DeletedAt == null).AsQueryable();
            if ( StartAt != null )
            {
                News = News.Where(x => x.ExpectDate >= StartAt).AsQueryable();
            }
            if ( EndAt != null )
            {
                News = News.Where(x => x.ExpectDate <= EndAt).AsQueryable();
            }
            if ( Status == SubscribeStatus.YES )
            {
                News = News.Where(x => x.SendAt != null).AsQueryable(); 
            } else if ( Status == SubscribeStatus.NO )
            {
                News = News.Where(x => x.SendAt == null).AsQueryable();
            }

            if ( TagId != null )
            {
                News = News.Where(x => x.TagId == TagId).AsQueryable();
            }

            if ( Keyword != "" && Keyword != null )
            {
                News = News.Where(x => x.Content.Contains(Keyword) || x.Title.Contains(Keyword)).AsQueryable();
            }

            int Total = News.Count();
            if ( page != null && pageSize != null )
            {
                News = News.OrderByDescending(x => x.ExpectDate).Skip(((int)page - 1) * (int)pageSize).Take((int)pageSize);
            }
            var Tags = Db.NewsTag.ToList();
            return new ListResponse<NewsItem>()
            {
                Total = Total,
                Items = News.ToList().GroupJoin(
                    Tags,
                    x => x.TagId,
                    x => x.Id,
                    (a, b) => new NewsItem(a, b)
                ).ToList()

            };
        }

        public void SendById(long Id)
        {
            var emailService = new EmailService();
            var News = Db.News.First(x => x.Id == Id);

            var SendUsers = Db.NewsUser.Where(x => x.IsSubscribe && x.DeletedAt == null).ToList();
            var SendMails = SendUsers.Select(x => x.Email).ToList();
            emailService.Send(SendMails, News.Title, News.Content);

            News.SendCount = SendUsers.Count();
            News.SendAt = DateTime.Now;
            News.ExpectDate = DateTime.Now;
            
            
            //foreach (var User in SendUsers)
            //{
            //    User.LastSendAt = News.ExpectDate;
            //}

            Db.SaveChanges();
        }

        public void SendByDate(DateTime Date)
        {
            var emailService = new EmailService();
            var ToDayNews = Db.News.Where(x => x.ExpectDate == Date.Date && x.DeletedAt == null && x.SendAt == null).ToList();
            var TagIds = ToDayNews.Select(x => x.TagId).ToList();
            var Tags = Db.NewsTag.Where(x => TagIds.Contains(x.Id)).ToList();

            var Items = ToDayNews.GroupJoin(Tags, x => x.TagId, x => x.Id, (a, b) => new NewsItem(a, b)).ToList();

            var SendUsers = Db.NewsUser.Where(x => x.IsSubscribe && x.DeletedAt == null).ToList();
            var SendMails = SendUsers.Select(x => x.Email).ToList();

            if( Items.Count > 0 )
            {
                foreach (var Item in Items)
                {
                    emailService.Send(SendMails, Item.Title, Item.Content);
                }

                foreach (var News in ToDayNews)
                {
                    News.SendCount = SendUsers.Count();
                    News.SendAt = DateTime.Now;
                }
                //foreach (var User in SendUsers)
                //{
                //    User.LastSendAt = Date;
                //}
            }
            

            Db.SaveChanges();
        }

        public void SendToTester(long Id)
        {
            var emailService = new EmailService();
            var News = Db.News.First(x => x.Id == Id);
            if ( News.Tester == "" || News.Tester == null )
            {
                throw new Exception("發送失敗：測試 Email 為空");
            }
            var Mails = News.Tester.Split(',').ToList();

            emailService.Send(Mails, News.Title, News.Content);
        }
    }
}