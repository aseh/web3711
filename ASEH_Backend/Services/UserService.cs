﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using ASEH_Backend.Models;
using JWT;
using JWT.Algorithms;
using JWT.Exceptions;
using JWT.Serializers;
using Models;

namespace ASEH_Backend.Services
{
    public class UserService: BaseService
    {
        readonly EmailService Email = new EmailService();
        const string secret = "GQDstcKsx0NHjPOuXOYg5MbeJ1XT0uFiwDVvVBrk";
        public enum SubscribeStatus
        {
            ALL,
            IS_SUBSCRIBE,
            NOT_SUBSCRIBE
        }

        public NewsUser Login(string Account, string Password)
        {
            var hashPwd = Crypto.SHA256(Password);
            var dbData = Db.NewsUser.First(x => x.Account == Account && x.Password == hashPwd);
            return dbData;
        }

        public NewsUser GetById(long Id)
        {
            return Db.NewsUser.First(x => x.Id == Id);
        }
        public string GetToken(NewsUser data, int? expired = 60)
        {

            var provider = new UtcDateTimeProvider();
            var now = provider.GetNow();
            now = now.AddMinutes((double)expired);
            var secondsSinceEpoch = UnixEpoch.GetSecondsSince(now);

            IJwtAlgorithm algorithm = new HMACSHA256Algorithm(); // symmetric
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

            var payload = new Dictionary<string, object>
                {
                    { "exp", secondsSinceEpoch },
                    { "Id", data.Id }
                };

            
            return encoder.Encode(payload, secret);
        }

        public NewsUser ValidToken(string Token)
        {
            try
            {
                IJsonSerializer serializer = new JsonNetSerializer();
                var provider = new UtcDateTimeProvider();
                IJwtValidator validator = new JwtValidator(serializer, provider);
                IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                IJwtAlgorithm algorithm = new HMACSHA256Algorithm(); // symmetric
                IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder, algorithm);

                var payload = decoder.DecodeToObject<IDictionary<string, object>>(Token, secret, verify: true);
                long Id = (long)payload["Id"];
                var userData = Db.NewsUser.First(x => x.Id == Id);

                return userData;
            }
            catch (TokenExpiredException err)
            {
                Console.WriteLine("Token has expired");
                throw new Exception("Token 過期");
            }
            catch (SignatureVerificationException)
            {
                Console.WriteLine("Token has invalid signature");
                throw new Exception("Token 異常");
            } catch( Exception )
            {
                throw new Exception("查無使用者");
            }
        }

        public NewsUser EncryptPassword(NewsUser data)
        {
            if ( data.Password != "" )
            {
                data.Password = Crypto.SHA256(data.Password);
            }
            return data;
        }
        public NewsUser Add(NewsUser data)
        {
            var IsSame = Db.NewsUser.Any(x => x.Account == data.Account && x.DeletedAt == null);
            if ( IsSame )
            {
                throw new Exception("帳號重覆");
            }
            data = EncryptPassword(data);
            data.UpdatedAt = DateTime.Now;
            data.CreatedAt = DateTime.Now;
            data.DeletedAt = null;
            data.LastSendAt = DateTime.Now;
            data.isDisabled = false;
            data.IsSubscribe = true;
            Db.NewsUser.Add(data);
            Db.SaveChanges();


            var ListenAdmin = Db.tbUsers.FirstOrDefault(x => x.UseID == "NewsletterAdmin");
            
            if ( ListenAdmin != null )
            {
                var HtmlBody = Email.LoadTemplate(Email.LoadTemplatePath("NewsLetterAdmin"), new Dictionary<string, string>() {
                    { "Email", data.Email },
                    { "NickName", data.NickName },
                    { "CompanyName", data.CompanyName },
                    { "JobTitle", data.CompanyJob },
                    { "Country", data.Country }
                });
                Email.Send(new List<string>() {
                    ListenAdmin.UseEmail
                }, "電子報會員訂閱通知", HtmlBody);
            }

            

            return data;
        }



        public ListResponse<NewsUser> Search(
            string Keyword, 
            SubscribeStatus? Status,
            int? Page = null,
            int? PageSize = null
        ) 
        {
            var NewsUsers = Db.NewsUser.Where(x => x.DeletedAt == null).AsQueryable();
            if ( Keyword != "" && Keyword != null)
            {
                NewsUsers = NewsUsers.Where(x => x.NickName.Contains(Keyword)
                || x.Email.Contains(Keyword)
                || x.CompanyJob.Contains(Keyword)
                || x.CompanyName.Contains(Keyword)
                || x.ContactCode.Contains(Keyword)
                || x.ContactExt.Contains(Keyword)
                || x.ContactFax.Contains(Keyword)
                || x.ContactPhone.Contains(Keyword)
                || x.Account.Contains(Keyword)
                ).AsQueryable();
            }

            if ( Status == SubscribeStatus.IS_SUBSCRIBE)
            {
                NewsUsers = NewsUsers.Where(x => x.IsSubscribe).AsQueryable();
            } else if ( Status == SubscribeStatus.NOT_SUBSCRIBE)
            {
                NewsUsers = NewsUsers.Where(x => !x.IsSubscribe).AsQueryable();
            }

            var Total = NewsUsers.Count();

            if ( Page != null && PageSize != null )
            {
                NewsUsers = NewsUsers.OrderByDescending(x => x.Id).Skip(((int)Page - 1) * (int)PageSize).Take((int)PageSize).AsQueryable();
            }

            return new ListResponse<NewsUser>() { 
                Items = NewsUsers.ToList(),
                Total = Total
            };

        }

        public HttpResponseMessage Export(
            string Keyword,
            SubscribeStatus? Status,
            int? Page = null,
            int? PageSize = null
        )
        {
            var Searched = this.Search(Keyword, Status);

            var Items = Searched.Items;

            var RowCount = Searched.Total;

            var ResService = new ResService();

            var ExcelService = new ExcelService();

            var ExcelColumns = new Dictionary<string, List<string>>() {
                {"名稱", Items.Select(x => x.NickName).ToList() },
                {"Email", Items.Select(x => x.Email).ToList() },
                {"公司名稱", Items.Select(x => x.CompanyName).ToList() },
                {"職稱", Items.Select(x => x.CompanyJob).ToList() },
                {"地區/國家", Items.Select(x => x.Country).ToList() },
                {"訂閱電子報", Items.Select(x => x.IsSubscribe ? "已訂閱" : "").ToList() },
                {"訂閱日期", Items.Select(x => x.LastSendAt != null ? x.LastSendAt.Value.ToString("yyyy-MM-dd") : "").ToList() },
            };
            var MS = ExcelService.CreateExcel(RowCount, ExcelColumns);
            var FileName = string.Format("{0}_會員名單.xlsx", DateTime.Now.ToString("yyyyMMddHHmmss"));
            return ResService.CreateDownload(new StreamContent(MS), FileName);

        }


        public NewsUser GetByEmail(string Email)
        {
            return Db.NewsUser.First(x => x.Email == Email);
        }

        public void ResetPassword(long Id, string Password)
        {
            var dbData = Db.NewsUser.First(x => x.Id == Id);
            dbData.Password = Password;
            dbData = EncryptPassword(dbData);
            Db.SaveChanges();
        }

        public NewsUser Update(long Id, NewsUser data )
        {
            if ( Db.NewsUser.Any(x => x.Id != Id && x.Email == data.Email && x.DeletedAt == null ))
            {
                throw new Exception("此Email已經被人註冊");
            }
            var dbData = Db.NewsUser.First(x => x.Id == Id);
            dbData.NickName = data.NickName;
            dbData.CompanyName = data.CompanyName;
            dbData.CompanyJob = data.CompanyJob;
            dbData.ContactCode = data.ContactCode;
            dbData.ContactExt = data.ContactExt;
            dbData.ContactFax = data.ContactFax;
            dbData.ContactPhone = data.ContactPhone;
            dbData.Country = data.Country;
            
            if ( !dbData.IsSubscribe && data.IsSubscribe )
            {
                dbData.LastSendAt = DateTime.Now;
            }
            dbData.IsSubscribe = data.IsSubscribe;
            dbData.Email = data.Email;
            dbData.UpdatedAt = DateTime.Now;

            Db.SaveChanges();
            return dbData;
        }

        public NewsUser Delete(long Id )
        {
            var dbData = Db.NewsUser.First(x => x.Id == Id);
            dbData.DeletedAt = DateTime.Now;
            return dbData;
        }

        public void SendForgetEmail(long Id)
        {
            var dbData = GetById(Id);
            var token = GetToken(dbData, 180);

            

            var email = new EmailService();
            var Host = GetCurrentUrl();
            var ForgetUrl = string.Format("{0}/ch/news_forget?token={1}", Host, token);
            var EmailTempate = email.LoadTemplate(email.LoadTemplatePath("ForgetPassword"), new Dictionary<string, string> {
                { "FORGET_URL", ForgetUrl },
                { "NAME", dbData.NickName }
            });

            email.Send(new List<string> {
                dbData.Email
            }, "密碼重置通知", EmailTempate);
        }
    }
}