﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using Models;
using System.Reflection;
using System.IO;

namespace ASEH_Backend.Services
{
    public class EmailService: BaseService
    {
        private string FromMail = "ct15504@dm.cloudmax.com.tw";
        private string FromName = "ASE Group";
        private string SmtpHost = "dm.cloudmax.com.tw";
        private string Username = "ct15504@dm.cloudmax.com.tw";
        private string Password = "Q3^@08D5";
        private int Port = 2525;
        
        public string LoadTemplatePath(string TemplateName)
        {
            return string.Format("ASEH_Backend.App_Data.EmailTemplate.{0}.html", TemplateName);
        }

        public string LoadTemplate(string Path, IDictionary<string, string> Params)
        {
            var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(Path);

            using (var sr = new StreamReader(stream))
            {
                var Html = sr.ReadToEnd();
                foreach (var Item in Params)
                {
                    var key = string.Format("__{0}__", Item.Key);
                    Html = Html.Replace(key, Item.Value);
                }
                sr.Dispose();
                return Html;
            }

        }

        public void Send(List<string> Mails, string Subject, string Body)
        {
            var MMList = new List<MailMessage>();

            foreach (var Mail in Mails )
            {
                var MM = new MailMessage()
                {
                    From = new MailAddress(FromMail),
                    Subject = Subject,
                    Body = Body,
                    IsBodyHtml = true
                };
                MM.To.Add(new MailAddress(Mail));
                MMList.Add(MM);
            }

            using( var Smtp = new SmtpClient(SmtpHost, Port) { 
                Credentials = new NetworkCredential(Username, Password),
                EnableSsl = false,
            })
            {
                var Log = new EmailLog()
                {
                    CreatedAt = DateTime.Now,
                    Body = Body,
                    Subject = Subject,
                    ToMails = string.Join(",", Mails)
                };

                try
                {
                    foreach(var MM in MMList )
                    {
                        Smtp.Send(MM);
                    }
                } catch ( Exception err )
                {
                    Log.Error = err.Message;
                    Smtp.Dispose();
                }

                Db.EmailLog.Add(Log);
                Db.SaveChanges();
                
            }

        }
    }
}