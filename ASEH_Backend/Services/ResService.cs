﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace ASEH_Backend.Services
{
    public class ResService
    {
        public HttpResponseMessage CreateDownload(StreamContent StreamContent, string FileName)
        {

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = StreamContent
            };

            result.Content.Headers.ContentDisposition =
                new ContentDispositionHeaderValue("attachment")
                {
                    FileNameStar = FileName,
                    FileName = FileName
                };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/octet-stream");



            return result;
        }
    }
}