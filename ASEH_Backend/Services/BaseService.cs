﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models;
namespace ASEH_Backend.Services
{
    public class BaseService
    {
        public ASEHEntities Db = null;
        public BaseService()
        {
            Db = new ASEHEntities();
            Db.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
        }


        public string GetCurrentUrl()
        {
            var Url = HttpContext.Current.Request.Url;
            return string.Format("{0}://{1}{2}", Url.Port != 443 ? "http" : "https", Url.Host, (Url.Port != 80 && Url.Port != 443) ? ":" + Url.Port : "");
        }
    }
}