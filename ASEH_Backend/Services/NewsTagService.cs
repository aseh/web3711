﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models;

namespace ASEH_Backend.Services
{
    public class NewsTagService: BaseService
    {

        public List<NewsTag> List()
        {
            return Db.NewsTag.Where(x => x.DeletedAt == null).ToList();
        }
        public NewsTag Add(NewsTag data)
        {
            var isSame = Db.NewsTag.Any(x => x.DeletedAt == null && x.Title == data.Title);
            if ( isSame )
            {
                throw new Exception("標籤名稱重覆");
            }
            data.DeletedAt = null;
            data.CreatedAt = DateTime.Now;
            data.UpdatedAt = DateTime.Now;
            Db.NewsTag.Add(data);
            Db.SaveChanges();
            return data;
        }

        public NewsTag Edit(int Id, NewsTag data )
        {
            var dbData = Db.NewsTag.First(x => x.Id == Id);

            var isSame = Db.NewsTag.Any(x => x.DeletedAt == null && x.Title == data.Title && x.Id != Id);
            if (isSame)
            {
                throw new Exception("標籤名稱重覆");
            }

            dbData.UpdatedAt = DateTime.Now;
            dbData.Title = data.Title;
            Db.SaveChanges();
            return dbData;
        }

        public NewsTag Delete(int Id)
        {
            var dbData = Db.NewsTag.First(x => x.Id == Id);
            dbData.DeletedAt = DateTime.Now;
            Db.SaveChanges();
            return dbData;
        }
    }
}