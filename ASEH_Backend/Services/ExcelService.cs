﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ASEH_Backend.Services
{
    public class ExcelService
    {
        public MemoryStream CreateExcel(int RowCount, Dictionary<string, List<string>> Columns)
        {
            IWorkbook Workbook = new XSSFWorkbook();
            ISheet Sheet = Workbook.CreateSheet("Sheet1");

            var HeaderIndex = 0;
            var HeaderRow = Sheet.CreateRow(0);
            foreach (var Column in Columns)
            {
                HeaderRow.CreateCell(HeaderIndex).SetCellValue(Column.Key);
                HeaderIndex++;
            }

            for (var i = 1; i <= RowCount; i++)
            {
                var Row = Sheet.CreateRow(i);
                var j = 0;
                foreach (var Column in Columns)
                {
                    var Cell = Row.CreateCell(j);
                    Cell.SetCellValue(Column.Value[i - 1]);
                    j++;
                }
            }

            int ColumnIndex = 0;
            foreach (var Column in Columns)
            {
                Sheet.AutoSizeColumn(ColumnIndex);
                ColumnIndex++;
            }

            var Stream = new MemoryStream();

            Workbook.Write(Stream);

            return new MemoryStream(Stream.ToArray());

        }
    }
}