﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASEH_Backend.Services
{
    public class ScheduleService
    {
        public static string SendNews()
        {
            var newsService = new NewsService();
            newsService.SendByDate(DateTime.Now);
            return "OK";
        }
    }
}