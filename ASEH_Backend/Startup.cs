﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin;
using NSwag.AspNet.Owin;
using Owin;
using Hangfire;
using Hangfire.SqlServer;
using System.Collections.Generic;
using System.Diagnostics;
using ASEH_Backend.Services;

[assembly: OwinStartup(typeof(ASEH_Backend.Startup))]

namespace ASEH_Backend
{

    public class Startup
    {
        private IEnumerable<IDisposable> GetHangfireServers()
        {
            Hangfire.GlobalConfiguration.Configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseSqlServerStorage("Hangfire");

            yield return new BackgroundJobServer();
        }

        public void Configuration(IAppBuilder app)
        {
            // 如需如何設定應用程式的詳細資訊，請瀏覽 https://go.microsoft.com/fwlink/?LinkID=316888
            app.UseHangfireAspNet(GetHangfireServers);
            app.UseHangfireDashboard();

            var config = new HttpConfiguration();

            app.UseStaticFiles();
            app.UseSwaggerUi3(typeof(Startup).Assembly, settings =>
            {
                //針對RPC-Style WebAPI，指定路由包含Action名稱
                settings.GeneratorSettings.DefaultUrlTemplate =
                    "api/{controller}/{action?}";
                //可加入客製化調整邏輯
                settings.PostProcess = document =>
                {
                    document.Info.Title = "WebAPI 範例";
                };
            });
            app.UseWebApi(config);
            config.MapHttpAttributeRoutes();
            config.EnsureInitialized();

            
            RecurringJob.AddOrUpdate("每日六點送通知", () => ScheduleService.SendNews(), "0 0 22 * * *");
        }
    }
}
