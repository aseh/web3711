﻿using ASEH_Backend.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace ASEH_Backend.ApiControllers
{
    [RoutePrefix("api/files")]
    [Error]
    public class FileController : ApiController
    {
        [Route()]
        [HttpPost]
        public IHttpActionResult Upload()
        {
            var Form = HttpContext.Current.Request.Form;
            var Files = HttpContext.Current.Request.Files;

            if ( Files["file"] != null )
            {
                FileInfo fi = new FileInfo(Files["file"].FileName);
                string FilePath = string.Format("/Resource/Uploads/HtmlEditor/{0}{1}", Guid.NewGuid().ToString(), fi.Extension);
                Files["file"].SaveAs(string.Format("{0}/{1}", AppDomain.CurrentDomain.BaseDirectory, FilePath));
                var Url = HttpContext.Current.Request.Url;
                string ServerUrl = string.Format("{0}://{1}{2}", Url.Port != 443 ? "http" : "https", Url.Host, (Url.Port != 80 && Url.Port != 443) ? ":" + Url.Port : "");

                string ResPath = string.Format("{0}{1}", ServerUrl, FilePath);
                return Json(new ResultResponse<string>(ResPath));
            } else
            {
                throw new Exception("未上傳檔案");
            }
            
            


            
        }
    }
}
