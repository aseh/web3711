﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ASEH_Backend.Models;
using Models;
using ASEH_Backend.Services;

namespace ASEH_Backend.ApiControllers
{



    [RoutePrefix("api/news")]
    [Error]
    public class NewsController : ApiController
    {
        NewsService Service = new NewsService();

        [HttpGet]
        [Route()]
        public IHttpActionResult getNewsList(
            string keyword = "",
            DateTime? startAt = null,
            DateTime? endAt = null,
            NewsService.SubscribeStatus? status = null,
            int? tagId = null,
            int? page = null,
            int? pageSize = null
        )
        {
            return Json(Service.Search(keyword, tagId, status, startAt, endAt, page, pageSize));
        }

        [HttpPost]
        [Route()]
        public IHttpActionResult AddNews([FromBody] NewsBody data )
        {
            return Json(new ResultResponse<News>(Service.AddNews(data)));
        }

        [HttpPut]
        [Route("{id}")]
        public IHttpActionResult Edit(int id, [FromBody] NewsBody data)
        {
            return Json(new ResultResponse<News>(Service.UpdateNews(id, data)));
        }
        [HttpDelete]
        [Route("{id}")]
        public IHttpActionResult RemoveNews(int id)
        {
            return Json(new ResultResponse<News>(Service.DeleteNews(id)));
        }

        [HttpPost]
        [Route("{id}/send_tester")]
        public IHttpActionResult SendTester(long id)
        {
            Service.SendToTester(id);
            return Json(new ResultResponse<string>("OK"));
        }
        

        [HttpPost]
        [Route("send_by_date")]
        public IHttpActionResult SendByDate(DateTime date)
        {
            Service.SendByDate(date);
            return Json(new ResultResponse<string>("OK"));
        }
    }
}