﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ASEH_Backend.Models;
using Models;
using ASEH_Backend.Services;

namespace ASEH_Backend.ApiControllers
{
    [RoutePrefix("api/news_tag")]
    [Error]
    public class NewsTagController : ApiController
    {
        NewsTagService Service = new NewsTagService();

        [Route()]
        public IHttpActionResult GetList()
        {
            return Json(new ListResponse<NewsTag>(Service.List()));
        }
        [Route()]
        [HttpPost]
        public IHttpActionResult Add([FromBody] NewsTag data)
        {
            return Json(new ResultResponse<NewsTag>(Service.Add(data)));
        }

        [Route("{id}")]
        [HttpPut]
        public IHttpActionResult Update(int id, [FromBody] NewsTag data)
        {
            return Json(new ResultResponse<NewsTag>(Service.Edit(id, data)));
        }

        [Route("{id}")]
        [HttpDelete]
        public IHttpActionResult Del(int id)
        {
            return Json(new ResultResponse<NewsTag>(Service.Delete(id)));
        }

    }
}
