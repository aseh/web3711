﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ASEH_Backend.Models;
using Models;
using ASEH_Backend.Services;

namespace ASEH_Backend.ApiControllers
{
    [RoutePrefix("api/users")]
    [Error]
    public class UserController : ApiController
    {
        UserService Service = new UserService();


        [Route()]
        [HttpGet]
        public IHttpActionResult List(
            string keyword = "", 
            UserService.SubscribeStatus? status = null,
            int? page = null,
            int? pageSize = null,
            string exportType = null
        )
        {
  
            return Json(Service.Search(keyword, status, page, pageSize));
        }

        [Route("export")]
        [HttpGet]
        public HttpResponseMessage ExportList(
            string keyword = "",
            UserService.SubscribeStatus? status = null
        )
        {
            return Service.Export(keyword, status);
        }

        [Route("{id}")]
        [HttpGet]
        public IHttpActionResult Get(long id)
        {
            return Json(new ResultResponse<NewsUser>(Service.GetById(id)));
        }

        [Route()]
        [HttpPost]
        public IHttpActionResult Register([FromBody] NewsUser data )
        {
            return Json(new ResultResponse<NewsUser>(Service.Add(data)));
        }

        [HttpPost]
        [Route("check_mail")]
        public IHttpActionResult CheckEmail([FromForm] string email)
        {
            var data = Service.GetByEmail(email);
            return Json(new ResultResponse<string>(Service.GetToken(data)));
        }

        [HttpPost]
        [Route("reset_password")]
        public IHttpActionResult ResetPassword(
            [FromForm] string token,
            [FromForm] string password
        )
        {
            var data = Service.ValidToken(token);
            Service.ResetPassword(data.Id, password);
            return Json(new BaseResponse());
        }

        [HttpPatch]
        [Route("login")]
        public IHttpActionResult Login(
            [FromForm] string account,
            [FromForm] string password
        )
        {
            return Json(new ResultResponse<NewsUser>(Service.Login(account, password)));
        }
        
    }
}
