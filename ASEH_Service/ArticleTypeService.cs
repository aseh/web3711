﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;

namespace Service
{
    public class ArticleTypeService : IArticleTypeService
    {
        private IRepository<tbArticleType> repository = new GenericRepository<tbArticleType>();

        public IResult Create(tbArticleType instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbArticleType instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(string AttID)
        {
            IResult result = new Result(false);

            if (!this.IsExists(AttID))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(AttID);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(string AttID)
        {
            return this.repository.GetAll().Any(x => x.AttID == AttID);
        }

        public tbArticleType GetByID(string AttID)
        {
            return this.repository.Get(x => x.AttID == AttID);
        }

        public IEnumerable<tbArticleType> GetAll()
        {
            return this.repository.GetAll();
        }
    }
}
