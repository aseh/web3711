﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IArticleService
    {
        IResult Create(tbArticle instance);

        IResult Update(tbArticle instance);

        IResult Delete(long AtcID);

        bool IsExists(long AtcID);

        tbArticle GetByID(long AtcID);

        IEnumerable<tbArticle> GetAll();
    }
}
