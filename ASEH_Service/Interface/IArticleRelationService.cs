﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IArticleRelationService
    {
        IResult Create(tbArticleRelation instance);

        IResult Update(tbArticleRelation instance);

        IResult Delete(long ID);

        bool IsExists(long ID);

        tbArticleRelation GetByID(long ID);

        IEnumerable<tbArticleRelation> GetAll();
    }
}
