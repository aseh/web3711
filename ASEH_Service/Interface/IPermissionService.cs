﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;
using System.Data;

namespace Service.Interface
{
    public interface IPermissionService
    {
        IResult Create(tbPermission instance);

        IResult Update(tbPermission instance);

        IResult Delete(string PerID);

        bool IsExists(string PerID);

        tbPermission GetByID(string PerID);

        IEnumerable<tbPermission> GetAll();
    }
}
