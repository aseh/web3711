﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IFileService
    {
        IResult Create(tbFile instance);

        IResult Update(tbFile instance);

        IResult Delete(string FieID);

        bool IsExists(string FieID);

        tbFile GetByID(string FieID);

        IEnumerable<tbFile> GetAll();
    }
}
