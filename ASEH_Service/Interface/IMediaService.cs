﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IMediaService
    {
        IResult Create(tbMedia instance);

        IResult Update(tbMedia instance);

        IResult Delete(long MdaID);

        bool IsExists(long MdaID);

        tbMedia GetByID(long MdaID);

        IEnumerable<tbMedia> GetAll();

        IEnumerable<tbMedia> GetAllByMdtID(string MdtID);

        IEnumerable<tbMedia> GetAllByUseID(string UseID);
    }
}
