﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IPermRoleService
    {
        IResult Create(tbPermRole instance);

        IResult Update(tbPermRole instance);

        IResult Delete(long ID);

        bool IsExists(long ID);

        tbPermRole GetByID(long ID);

        tbPermRole GetByRolID(string RolID);

        tbPermRole GetByPerIDAndRolID(string PerID, string RolID);

        IEnumerable<tbPermRole> GetAll();

        IEnumerable<tbPermRole> GetAllByRolID(string RolID);
    }
}
