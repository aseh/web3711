﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IRecipientService
    {
        IResult Create(tbRecipient instance);

        IResult Update(tbRecipient instance);

        IResult Delete(string RcpID);

        bool IsExists(string RcpID);

        tbRecipient GetByID(string RcpID);

        IEnumerable<tbRecipient> GetAll();
    }
}
