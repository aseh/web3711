﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IReportQuestionnaire2Service
    {
        IResult Create(tbReportQuestionnaire2 instance);

        IResult Update(tbReportQuestionnaire2 instance);

        IResult Delete(long id);

        bool IsExists(long id);

        tbReportQuestionnaire2 GetByID(long id);

        IEnumerable<tbReportQuestionnaire2> GetAll();
    }
}
