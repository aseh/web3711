﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IQuestionnaireService
    {
        IResult Create(tbQuestionnaire instance);

        IResult Update(tbQuestionnaire instance);

        IResult Delete(long id);

        bool IsExists(long id);

        tbQuestionnaire GetByID(long id);

        IEnumerable<tbQuestionnaire> GetAll();
    }
}
