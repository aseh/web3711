﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IFileLogService
    {
        IResult Create(tbFileLog instance);

        IResult Update(tbFileLog instance);

        IResult Delete(long FigID);

        bool IsExists(long FigID);

        tbFileLog GetByID(long FigID);

        IEnumerable<tbFileLog> GetAll();
    }
}
