﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;
using System.Data;

namespace Service.Interface
{
    public interface IExceptionRecordService
    {
        IResult Create(tbExceptionRecord instance);

        IResult Update(tbExceptionRecord instance);

        IResult Delete(long ExrIndex);

        bool IsExists(long ExrIndex);

        tbExceptionRecord GetByID(long ExrIndex);

        IEnumerable<tbExceptionRecord> GetAll();
    }
}
