﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IFileTypeService
    {
        IResult Create(tbFileType instance);

        IResult Update(tbFileType instance);

        IResult Delete(string FitID);

        bool IsExists(string FitID);

        tbFileType GetByID(string FitID);

        IEnumerable<tbFileType> GetAll();
    }
}
