﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IReportQuestionnaireService
    {
        IResult Create(tbReportQuestionnaire instance);

        IResult Update(tbReportQuestionnaire instance);

        IResult Delete(long id);

        bool IsExists(long id);

        tbReportQuestionnaire GetByID(long id);

        IEnumerable<tbReportQuestionnaire> GetAll();
    }
}
