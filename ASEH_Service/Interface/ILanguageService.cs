﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface ILanguageService
    {
        IResult Create(tbLanguage instance);

        IResult Update(tbLanguage instance);

        IResult Delete(string LgeID);

        bool IsExists(string LgeID);

        tbLanguage GetByID(string LgeID);

        IEnumerable<tbLanguage> GetAll();
    }
}
