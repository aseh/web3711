﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IUsersService
    {
        IResult Create(tbUsers instance);

        IResult Update(tbUsers instance);

        IResult Delete(string UseID);

        bool IsExists(string UseID);

        tbUsers GetByID(string UseID);

        IEnumerable<tbUsers> GetAll();

        IEnumerable<tbUsers> GetAllByRolID(string RolID);

        //驗證帳密
        UserInfo Authen(string UseID, string UsePassword);
    }
}
