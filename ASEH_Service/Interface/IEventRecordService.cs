﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;
using System.Data;

namespace Service.Interface
{
    public interface IEventRecordService
    {
        IResult Create(tbEventRecord instance);

        IResult Update(tbEventRecord instance);

        IResult Delete(long EvrIndex);

        bool IsExists(long EvrIndex);

        tbEventRecord GetByID(long EvrIndex);

        IEnumerable<tbEventRecord> GetAll();
    }
}
