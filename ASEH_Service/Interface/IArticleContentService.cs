﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IArticleContentService
    {
        IResult Create(tbArticleContent instance);

        IResult Update(tbArticleContent instance);

        IResult Delete(long AtnID);

        bool IsExists(long AtnID);

        tbArticleContent GetByID(long AtnID);

        IEnumerable<tbArticleContent> GetAll();

        IEnumerable<tbArticleContent> GetAllByAtcIDAndLgeID(long AtcID, string LgeID);
    }
}
