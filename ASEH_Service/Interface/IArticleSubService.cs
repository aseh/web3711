﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IArticleSubService
    {
        IResult Create(tbArticleSub instance);

        IResult Update(tbArticleSub instance);

        IResult Delete(string SubID);

        bool IsExists(string SubID);

        tbArticleSub GetByID(string SubID);

        IEnumerable<tbArticleSub> GetAll();
    }
}
