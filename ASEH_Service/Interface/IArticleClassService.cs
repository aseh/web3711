﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IArticleClassService
    {
        IResult Create(tbArticleClass instance);

        IResult Update(tbArticleClass instance);

        IResult Delete(string AtsID);

        bool IsExists(string AtsID);

        tbArticleClass GetByID(string AtsID);

        IEnumerable<tbArticleClass> GetAll();
    }
}
