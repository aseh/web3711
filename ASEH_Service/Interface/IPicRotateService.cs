﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IPicRotateService
    {
        IResult Create(tbPicRotate instance);

        IResult Update(tbPicRotate instance);

        IResult Delete(long ProID);

        bool IsExists(long ProID);

        tbPicRotate GetByID(long ProID);

        IEnumerable<tbPicRotate> GetAll();

        IEnumerable<tbPicRotate> GetAllBySort();
    }
}
