﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IArticleTypeService
    {
        IResult Create(tbArticleType instance);

        IResult Update(tbArticleType instance);

        IResult Delete(string AttID);

        bool IsExists(string AttID);

        tbArticleType GetByID(string AttID);

        IEnumerable<tbArticleType> GetAll();
    }
}
