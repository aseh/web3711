﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IWebContentService
    {
        IResult Create(tbWebContent instance);

        IResult Update(tbWebContent instance);

        IResult Delete(string WctID);

        bool IsExists(string WctID);

        tbWebContent GetByID(string WctID);

        IEnumerable<tbWebContent> GetAll();

    }
}
