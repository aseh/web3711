﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IMediaTypeService
    {
        IResult Create(tbMediaType instance);

        IResult Update(tbMediaType instance);

        IResult Delete(string MdtID);

        bool IsExists(string MdtID);

        tbMediaType GetByID(string MdtID);

        IEnumerable<tbMediaType> GetAll();
    }
}
