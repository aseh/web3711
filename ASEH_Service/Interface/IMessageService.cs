﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IMessageService
    {
        IResult Create(tbMessage instance);

        IResult Update(tbMessage instance);

        IResult Delete(string MsgID);

        bool IsExists(string MsgID);

        tbMessage GetByID(string MsgID);

        IEnumerable<tbMessage> GetAll();
    }
}
