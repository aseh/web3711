﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Service.Misc;

namespace Service.Interface
{
    public interface IRolesService
    {
        IResult Create(tbRoles instance);

        IResult Update(tbRoles instance);

        IResult Delete(string RolID);

        bool IsExists(string RolID);

        tbRoles GetByID(string RolID);

        IEnumerable<tbRoles> GetAll();
    }
}
