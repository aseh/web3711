﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;

namespace Service
{
    public class MessageService : IMessageService
    {
        private IRepository<tbMessage> repository = new GenericRepository<tbMessage>();

        public IResult Create(tbMessage instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbMessage instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(string MsgID)
        {
            IResult result = new Result(false);

            if (!this.IsExists(MsgID))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(MsgID);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(string MsgID)
        {
            return this.repository.GetAll().Any(x => x.MsgID == MsgID);
        }

        public tbMessage GetByID(string MsgID)
        {
            return this.repository.Get(x => x.MsgID == MsgID);
        }

        public IEnumerable<tbMessage> GetAll()
        {
            return this.repository.GetAll();
        }
    }
}
