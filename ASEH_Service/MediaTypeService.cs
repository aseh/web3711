﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;

namespace Service
{
    public class MediaTypeService : IMediaTypeService
    {
        private IRepository<tbMediaType> repository = new GenericRepository<tbMediaType>();

        public IResult Create(tbMediaType instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbMediaType instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(string MdtID)
        {
            IResult result = new Result(false);

            if (!this.IsExists(MdtID))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(MdtID);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(string MdtID)
        {
            return this.repository.GetAll().Any(x => x.MdtID == MdtID);
        }

        public tbMediaType GetByID(string MdtID)
        {
            return this.repository.Get(x => x.MdtID == MdtID);
        }

        public IEnumerable<tbMediaType> GetAll()
        {
            return this.repository.GetAll();
        }
    }
}
