﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;

namespace Service
{
    public class FileTypeService : IFileTypeService
    {
        private IRepository<tbFileType> repository = new GenericRepository<tbFileType>();

        public IResult Create(tbFileType instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbFileType instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(string FitID)
        {
            IResult result = new Result(false);

            if (!this.IsExists(FitID))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(FitID);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(string FitID)
        {
            return this.repository.GetAll().Any(x => x.FitID == FitID);
        }

        public tbFileType GetByID(string FitID)
        {
            return this.repository.Get(x => x.FitID == FitID);
        }

        public IEnumerable<tbFileType> GetAll()
        {
            return this.repository.GetAll();
        }
    }
}
