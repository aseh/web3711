﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;

namespace Service
{
    public class ArticleClassService : IArticleClassService
    {
        private IRepository<tbArticleClass> repository = new GenericRepository<tbArticleClass>();

        public IResult Create(tbArticleClass instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbArticleClass instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(string AtsID)
        {
            IResult result = new Result(false);

            if (!this.IsExists(AtsID))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(AtsID);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(string AtsID)
        {
            return this.repository.GetAll().Any(x => x.AtsID == AtsID);
        }

        public tbArticleClass GetByID(string AtsID)
        {
            return this.repository.Get(x => x.AtsID == AtsID);
        }

        public IEnumerable<tbArticleClass> GetAll()
        {
            return this.repository.GetAll();
        }
    }
}
