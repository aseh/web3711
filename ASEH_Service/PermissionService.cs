﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;
using System.Data;

namespace Service
{
    public class PermissionService : IPermissionService
    {
        private IRepository<tbPermission> repository = new GenericRepository<tbPermission>();

        public IResult Create(tbPermission instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbPermission instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(string PerID)
        {
            IResult result = new Result(false);

            if (!this.IsExists(PerID))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(PerID);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(string PerID)
        {
            return this.repository.GetAll().Any(x => x.PerID == PerID);
        }

        public tbPermission GetByID(string PerID)
        {
            return this.repository.Get(x => x.PerID == PerID);
        }

        public IEnumerable<tbPermission> GetAll()
        {
            return this.repository.GetAll();
        }
    }
}
