﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;

namespace Service
{
    public class ArticleContentService : IArticleContentService
    {
        private IRepository<tbArticleContent> repository = new GenericRepository<tbArticleContent>();

        public IResult Create(tbArticleContent instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbArticleContent instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(long AtnID)
        {
            IResult result = new Result(false);

            if (!this.IsExists(AtnID))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(AtnID);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(long AtnID)
        {
            return this.repository.GetAll().Any(x => x.AtnID == AtnID);
        }

        public tbArticleContent GetByID(long AtnID)
        {
            return this.repository.Get(x => x.AtnID == AtnID);
        }

        public IEnumerable<tbArticleContent> GetAll()
        {
            return this.repository.GetAll();
        }

        public IEnumerable<tbArticleContent> GetAllByAtcIDAndLgeID(long AtcID, string LgeID)
        {
            var result = this.repository.GetAll();

            return result.Where(x => x.AtcID == AtcID && x.LgeID == LgeID).OrderBy(x => x.AtnSort);
        }
    }
}
