﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;

namespace Service
{
    public class PicRotateService : IPicRotateService
    {
        private IRepository<tbPicRotate> repository = new GenericRepository<tbPicRotate>();

        public IResult Create(tbPicRotate instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }
            
            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbPicRotate instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(long ProID)
        {
            IResult result = new Result(false);

            if (!this.IsExists(ProID))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(ProID);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(long ProID)
        {
            return this.repository.GetAll().Any(x => x.ProID == ProID);
        }

        public tbPicRotate GetByID(long ProID)
        {
            return this.repository.Get(x => x.ProID == ProID);
        }

        public IEnumerable<tbPicRotate> GetAll()
        {
            return this.repository.GetAll();
        }

        public IEnumerable<tbPicRotate> GetAllBySort()
        {
            var result = this.repository.GetAll();

            return result.OrderBy(x => x.ProSort);
        }
    }
}
