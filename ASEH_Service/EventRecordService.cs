﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;
using System.Data;

namespace Service
{
    public class EventRecordService : IEventRecordService
    {
        private IRepository<tbEventRecord> repository = new GenericRepository<tbEventRecord>();

        public IResult Create(tbEventRecord instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbEventRecord instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(long EvrIndex)
        {
            IResult result = new Result(false);

            if (!this.IsExists(EvrIndex))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(EvrIndex);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(long EvrIndex)
        {
            return this.repository.GetAll().Any(x => x.EvrIndex == EvrIndex);
        }

        public tbEventRecord GetByID(long EvrIndex)
        {
            return this.repository.Get(x => x.EvrIndex == EvrIndex);
        }

        public IEnumerable<tbEventRecord> GetAll()
        {
            return this.repository.GetAll();
        }
    }
}
