﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.BLL
{
    public partial class ASEHService
    {
        public ASEHService()
        {
            this.DbContext = new ASEHEntities();
        }

        public ASEHEntities DbContext { get; set; }
    }
}
