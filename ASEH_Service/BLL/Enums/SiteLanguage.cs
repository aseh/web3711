﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.BLL
{
    public enum SiteLanguage
    {
        zh_tw = 1,
        en,
    }

    public static class SiteLanVersionExtionstions
    {
        public static string GetKey(this SiteLanguage lang)
        {
            switch (lang)
            {
                case SiteLanguage.zh_tw:
                    return "zh-tw";
                case SiteLanguage.en:
                    return "en";
                default:
                    throw new NotImplementedException();
            }
        }

        public static string GetDbId(this SiteLanguage lang)
        {
            switch (lang)
            {
                case SiteLanguage.zh_tw:
                    return "CT";
                case SiteLanguage.en:
                    return "EN";
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
