﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Service.BLL;
using Models;

namespace Service.BLL
{
    public partial class ASEHService
    {
        QuestionnaireService Services = new QuestionnaireService();
        public tbQuestionnaire GetQuestionnaire(long id)
        {
            return this.DbContext.tbQuestionnaire.SingleOrDefault(m => m.id == id);
        }
        public IQueryable<tbQuestionnaire> GetQuestionnaireQuery(string type)
        {
            return this.DbContext.tbQuestionnaire.Where(m => m.type == type).OrderByDescending(m => m.create_date);
        }

        public void CreateQuestionnaire(tbQuestionnaire data)
        {
            Services.Create(data);
        }

        public void UpdateQuestionnaire(tbQuestionnaire data)
        {
            Services.Update(data);
        }
    }
}
