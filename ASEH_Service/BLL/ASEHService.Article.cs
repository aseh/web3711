﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Service.BLL;

namespace Service.BLL
{
    public partial class ASEHService
    {
        public tbArticle GetArticle(long id)
        {
            return this.DbContext.tbArticle.SingleOrDefault(m => m.AtcID == id);
        }
        public tbArticle GetArticle(string atcDesc)
        {
            return this.DbContext.tbArticle.SingleOrDefault(m => m.AtcDesc.Contains(atcDesc));
        }
        public tbArticle GetArticleSign()
        {
            var attID = "BT001";

            return this.DbContext.tbArticle.FirstOrDefault(m => m.AttID == attID);
        }
        public IQueryable<tbArticle> GetArticleQuery()
        {
            var attID = "AT001";

            return this.DbContext.tbArticle.Where(m => m.AtcOpenDate <= DateTime.Now &&
                                                       m.AttID == attID &&
                                                       m.AtcEnable == 1)
                                           .OrderByDescending(m => m.AtcOpenDate);
        }
    }
}
