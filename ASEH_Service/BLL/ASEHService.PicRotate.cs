﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Service.BLL;

namespace Service.BLL
{
    public partial class ASEHService
    {
        public tbPicRotate GetPicRotate(long id)
        {
            return this.DbContext.tbPicRotate.SingleOrDefault(m => m.ProID == id);
        }
        public IQueryable<tbPicRotate> GetPicRotateQuery()
        {
            return this.DbContext.tbPicRotate.OrderBy(m => m.ProSort);
        }
    }
}
