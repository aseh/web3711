﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Service.BLL;

namespace Service.BLL
{
    public partial class ASEHService
    {
        ReportQuestionnaireService _reportQueSvr = new ReportQuestionnaireService();
        ReportQuestionnaire2Service _reportQue2Svr = new ReportQuestionnaire2Service();

        public tbReportQuestionnaire GetReportQuestionnaire(long id)
        {
            return this.DbContext.tbReportQuestionnaire.SingleOrDefault(m => m.id == id);
        }
        public IQueryable<tbReportQuestionnaire> GetReportQuestionnaireQuery()
        {
            return this.DbContext.tbReportQuestionnaire.OrderByDescending(m => m.create_date);
        }

        public IQueryable<tbReportQuestionnaire2> GetReportQuestionnaireQuery2()
        {
            return this.DbContext.tbReportQuestionnaire2.OrderByDescending(m => m.create_date);
        }

        public void CreateReportQuestionnaire(tbReportQuestionnaire data)
        {
            _reportQueSvr.Create(data);
        }

        public void UpdateReportQuestionnaire(tbReportQuestionnaire data)
        {
            _reportQueSvr.Update(data);
        }

        public void CreateReportQuestionnaire2(tbReportQuestionnaire2 data)
        {
            _reportQue2Svr.Create(data);
        }
    }
}
