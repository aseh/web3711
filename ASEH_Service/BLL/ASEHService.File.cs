﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Service.BLL;

namespace Service.BLL
{
    public partial class ASEHService
    {
        FileService _fileSvr = new FileService();

        public tbFile GetFile(Guid id)
        {
            return this.DbContext.tbFile.SingleOrDefault(m => m.FieID == id.ToString());
        }
        public IQueryable<tbFile> GetFileQuery()
        {
            var fitID = "FIT001"; // SCR報告

            return this.DbContext.tbFile.Where(m => m.FitID == fitID)
                                        .OrderByDescending(m => m.CreateDate);
        }

        public void UpdateFile(tbFile data)
        {
            _fileSvr.Update(data);
        }
    }
}
