﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Service.BLL;

namespace Service.BLL
{
    public partial class ASEHService
    {
        FileLogService _fileLogSvr = new FileLogService();

        public void CreateFileLog(tbFileLog data)
        {
            _fileLogSvr.Create(data);
        }
    }
}
