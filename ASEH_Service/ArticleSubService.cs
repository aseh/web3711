﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;

namespace Service
{
    public class ArticleSubService : IArticleSubService
    {
        private IRepository<tbArticleSub> repository = new GenericRepository<tbArticleSub>();

        public IResult Create(tbArticleSub instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbArticleSub instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(string SubID)
        {
            IResult result = new Result(false);

            if (!this.IsExists(SubID))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(SubID);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(string SubID)
        {
            return this.repository.GetAll().Any(x => x.SubID == SubID);
        }

        public tbArticleSub GetByID(string SubID)
        {
            return this.repository.Get(x => x.SubID == SubID);
        }

        public IEnumerable<tbArticleSub> GetAll()
        {
            return this.repository.GetAll();
        }
    }
}
