﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;

namespace Service
{
    public class PermRoleService : IPermRoleService
    {
        private IRepository<tbPermRole> repository = new GenericRepository<tbPermRole>();

        public IResult Create(tbPermRole instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbPermRole instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(long ID)
        {
            IResult result = new Result(false);

            if (!this.IsExists(ID))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(ID);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(long ID)
        {
            return this.repository.GetAll().Any(x => x.ID == ID);
        }

        public tbPermRole GetByID(long ID)
        {
            return this.repository.Get(x => x.ID == ID);
        }

        public tbPermRole GetByRolID(string RolID)
        {
            return this.repository.Get(x => x.RolID == RolID);
        }

        public tbPermRole GetByPerIDAndRolID(string PerID, string RolID)
        {
            return this.repository.Get(x => x.PerID == PerID && x.RolID == RolID);
        }

        public IEnumerable<tbPermRole> GetAll()
        {
            return this.repository.GetAll();
        }

        public IEnumerable<tbPermRole> GetAllByRolID(string RolID)
        {
            var result = this.repository.GetAll();
            return result.Where(p => p.RolID == RolID);
        }
    }
}
