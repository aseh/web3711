﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;
using System.Data;

namespace Service
{
    public class ExceptionRecordService : IExceptionRecordService
    {
        private IRepository<tbExceptionRecord> repository = new GenericRepository<tbExceptionRecord>();

        public IResult Create(tbExceptionRecord instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbExceptionRecord instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(long ExrIndex)
        {
            IResult result = new Result(false);

            if (!this.IsExists(ExrIndex))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(ExrIndex);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(long ExrIndex)
        {
            return this.repository.GetAll().Any(x => x.ExrIndex == ExrIndex);
        }

        public tbExceptionRecord GetByID(long ExrIndex)
        {
            return this.repository.Get(x => x.ExrIndex == ExrIndex);
        }

        public IEnumerable<tbExceptionRecord> GetAll()
        {
            return this.repository.GetAll();
        }
    }
}
