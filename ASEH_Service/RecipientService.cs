﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;

namespace Service
{
    public class RecipientService : IRecipientService
    {
        private IRepository<tbRecipient> repository = new GenericRepository<tbRecipient>();

        public IResult Create(tbRecipient instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbRecipient instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(string RcpID)
        {
            IResult result = new Result(false);

            if (!this.IsExists(RcpID))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(RcpID);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(string RcpID)
        {
            return this.repository.GetAll().Any(x => x.RcpID == RcpID);
        }

        public tbRecipient GetByID(string RcpID)
        {
            return this.repository.Get(x => x.RcpID == RcpID);
        }

        public IEnumerable<tbRecipient> GetAll()
        {
            return this.repository.GetAll();
        }
    }
}
