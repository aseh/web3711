﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;

namespace Service
{
    public class ReportQuestionnaireService : IReportQuestionnaireService
    {
        private IRepository<tbReportQuestionnaire> repository = new GenericRepository<tbReportQuestionnaire>();

        public IResult Create(tbReportQuestionnaire instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbReportQuestionnaire instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(long id)
        {
            IResult result = new Result(false);

            if (!this.IsExists(id))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(id);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(long id)
        {
            return this.repository.GetAll().Any(x => x.id == id);
        }

        public tbReportQuestionnaire GetByID(long id)
        {
            return this.repository.Get(x => x.id == id);
        }

        public IEnumerable<tbReportQuestionnaire> GetAll()
        {
            return this.repository.GetAll();
        }

    }
}
