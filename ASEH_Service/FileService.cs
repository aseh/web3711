﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;

namespace Service
{
    public class FileService : IFileService
    {
        private IRepository<tbFile> repository = new GenericRepository<tbFile>();

        public IResult Create(tbFile instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbFile instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(string FieID)
        {
            IResult result = new Result(false);

            if (!this.IsExists(FieID))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(FieID);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(string FieID)
        {
            return this.repository.GetAll().Any(x => x.FieID == FieID);
        }

        public tbFile GetByID(string FieID)
        {
            return this.repository.Get(x => x.FieID == FieID);
        }

        public IEnumerable<tbFile> GetAll()
        {
            return this.repository.GetAll();
        }

    }
}
