﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;

namespace Service
{
    public class MediaService : IMediaService
    {
        private IRepository<tbMedia> repository = new GenericRepository<tbMedia>();

        public IResult Create(tbMedia instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbMedia instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(long MdaID)
        {
            IResult result = new Result(false);

            if (!this.IsExists(MdaID))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(MdaID);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(long MdaID)
        {
            return this.repository.GetAll().Any(x => x.MdaID == MdaID);
        }

        public tbMedia GetByID(long MdaID)
        {
            return this.repository.Get(x => x.MdaID == MdaID);
        }

        public IEnumerable<tbMedia> GetAll()
        {
            return this.repository.GetAll();
        }

        public IEnumerable<tbMedia> GetAllByMdtID(string MdtID)
        {
            var result = this.repository.GetAll();

            return result.Where(x => x.MdtID == MdtID).OrderByDescending(x => x.CreateDate);
        }

        public IEnumerable<tbMedia> GetAllByUseID(string UseID)
        {
            var result = this.repository.GetAll();

            return result.Where(x => x.UseID == UseID).OrderByDescending(x => x.CreateDate);
        }
    }
}
