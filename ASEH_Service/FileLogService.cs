﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;

namespace Service
{
    public class FileLogService : IFileLogService
    {
        private IRepository<tbFileLog> repository = new GenericRepository<tbFileLog>();

        public IResult Create(tbFileLog instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbFileLog instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(long FigID)
        {
            IResult result = new Result(false);

            if (!this.IsExists(FigID))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(FigID);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(long FigID)
        {
            return this.repository.GetAll().Any(x => x.FigID == FigID);
        }

        public tbFileLog GetByID(long FigID)
        {
            return this.repository.Get(x => x.FigID == FigID);
        }

        public IEnumerable<tbFileLog> GetAll()
        {
            return this.repository.GetAll();
        }

    }
}
