﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;

namespace Service
{
    public class LanguageService : ILanguageService
    {
        private IRepository<tbLanguage> repository = new GenericRepository<tbLanguage>();

        public IResult Create(tbLanguage instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbLanguage instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(string LgeID)
        {
            IResult result = new Result(false);

            if (!this.IsExists(LgeID))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(LgeID);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(string LgeID)
        {
            return this.repository.GetAll().Any(x => x.LgeID == LgeID);
        }

        public tbLanguage GetByID(string LgeID)
        {
            return this.repository.Get(x => x.LgeID == LgeID);
        }

        public IEnumerable<tbLanguage> GetAll()
        {
            return this.repository.GetAll();
        }
    }
}
