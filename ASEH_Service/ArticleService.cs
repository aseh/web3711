﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;

namespace Service
{
    public class ArticleService : IArticleService
    {
        private IRepository<tbArticle> repository = new GenericRepository<tbArticle>();

        public IResult Create(tbArticle instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbArticle instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(long AtcID)
        {
            IResult result = new Result(false);

            if (!this.IsExists(AtcID))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(AtcID);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(long AtcID)
        {
            return this.repository.GetAll().Any(x => x.AtcID == AtcID);
        }

        public tbArticle GetByID(long AtcID)
        {
            return this.repository.Get(x => x.AtcID == AtcID);
        }

        public IEnumerable<tbArticle> GetAll()
        {
            return this.repository.GetAll();
        }
    }
}
