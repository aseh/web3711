﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;

namespace Service
{
    public class WebContentService : IWebContentService
    {
        private IRepository<tbWebContent> repository = new GenericRepository<tbWebContent>();

        public IResult Create(tbWebContent instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbWebContent instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(string WctID)
        {
            IResult result = new Result(false);

            if (!this.IsExists(WctID))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(WctID);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(string WctID)
        {
            return this.repository.GetAll().Any(x => x.WctID == WctID);
        }

        public tbWebContent GetByID(string WctID)
        {
            return this.repository.Get(x => x.WctID == WctID);
        }

        public IEnumerable<tbWebContent> GetAll()
        {
            return this.repository.GetAll();
        }

    }
}

