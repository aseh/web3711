﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;

namespace Service
{
    public class UsersService : IUsersService
    {
        private Security newSecurity = new Security();
        private IRepository<tbUsers> repository = new GenericRepository<tbUsers>();

        public IResult Create(tbUsers instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbUsers instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(string UseID)
        {
            IResult result = new Result(false);

            if (!this.IsExists(UseID))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(UseID);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(string UseID)
        {
            return this.repository.GetAll().Any(x => x.UseID == UseID);
        }

        public tbUsers GetByID(string UseID)
        {
            return this.repository.Get(x => x.UseID == UseID);
        }

        public IEnumerable<tbUsers> GetAll()
        {
            return this.repository.GetAll();
        }

        public IEnumerable<tbUsers> GetAllByRolID(string RolID)
        {
            var result = this.repository.GetAll();
            
            return result.Where(x => x.RolID == RolID);

        }

        //驗證帳密
        public UserInfo Authen(string UseID, string UsePassword)
        {
            UserInfo clsUser = new UserInfo();
            if (AuthenUser(UseID, UsePassword) == true)
            {
                clsUser.UserID = UseID;
                clsUser.iControlPermission = 15;
                clsUser.iFormPermission = 15;
                clsUser.IsAuthen = true;
            }
            else
            {
                clsUser.UserID = UseID;
                clsUser.IsAuthen = false;
            }

            return clsUser;
        }

        //驗證帳密
        public tbUsers AuthenAndGetUser(string UseID, string UsePassword)
        {
            if (AuthenUser(UseID, UsePassword))
                return this.GetByID(UseID);
            else
                return null;
        }

        //驗證帳密
        protected bool AuthenUser(string UseID, string UsePassword)
        {
            bool result = false;

            string newPassword = newSecurity.Encrypt(UsePassword);

            var user = this.repository.Get(x => x.UseID == UseID && x.UsePassword == newPassword);

            if (user != null)
            {
                result = true;
            }

            return result;
        }
    }
}
