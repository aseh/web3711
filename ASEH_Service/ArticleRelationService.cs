﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;

namespace Service
{
    public class ArticleRelationService : IArticleRelationService
    {
        private IRepository<tbArticleRelation> repository = new GenericRepository<tbArticleRelation>();

        public IResult Create(tbArticleRelation instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbArticleRelation instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(long ID)
        {
            IResult result = new Result(false);

            if (!this.IsExists(ID))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(ID);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(long ID)
        {
            return this.repository.GetAll().Any(x => x.ID == ID);
        }

        public tbArticleRelation GetByID(long ID)
        {
            return this.repository.Get(x => x.ID == ID);
        }

        public IEnumerable<tbArticleRelation> GetAll()
        {
            return this.repository.GetAll();
        }
    }
}
