﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Interface;
using Models.Repository;
using Service.Misc;
using Service.Interface;

namespace Service
{
    public class RolesService : IRolesService
    {
        private IRepository<tbRoles> repository = new GenericRepository<tbRoles>();

        public IResult Create(tbRoles instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Create(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Update(tbRoles instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException();
            }

            IResult result = new Result(false);
            try
            {
                this.repository.Update(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public IResult Delete(string RolID)
        {
            IResult result = new Result(false);

            if (!this.IsExists(RolID))
            {
                result.Message = "找不到資料";
            }

            try
            {
                var instance = this.GetByID(RolID);
                this.repository.Delete(instance);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            return result;
        }

        public bool IsExists(string RolID)
        {
            return this.repository.GetAll().Any(x => x.RolID == RolID);
        }

        public tbRoles GetByID(string RolID)
        {
            return this.repository.Get(x => x.RolID == RolID);
        }

        public IEnumerable<tbRoles> GetAll()
        {
            return this.repository.GetAll();
        }
    }
}
