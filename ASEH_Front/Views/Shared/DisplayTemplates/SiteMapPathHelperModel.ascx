﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl`1[[MvcSiteMapProvider.Web.Html.Models.SiteMapPathHelperModel,MvcSiteMapProvider]]" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>
<%@ Import Namespace="MvcSiteMapProvider.Web.Html.Models" %>

<ol id="breadcrumbs" class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
    <% foreach (var node in Model)
       { %>
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <% if (node == Model.First())
           { %>
        <a itemprop="item" href="<%= (ViewContext.RouteData.Values["controller"].ToString().ToLower() == "ch") ? Url.Action("Index", "Ch") : Url.Action("Index", "En") %>">
            <i class="icon-home"></i>
            <span itemprop="name"><%= (ViewContext.RouteData.Values["controller"].ToString().ToLower() == "ch") ? "首頁" : "Home" %></span>
        </a>
        <meta itemprop="position" content="1" />
        <% }
           else
           { %>
        <a itemprop="item" href="<%= node.Url %>">
            <span itemprop="name"><%= node.Title %></span>
        </a>
        <meta itemprop="position" content="2" />
        <% } %>
    </li>
    <% } %>
</ol>
