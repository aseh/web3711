﻿using Service.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using LowercaseRoutesMVC;

namespace ASEH_Front
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.LowercaseUrls = true;
            string[] allowHtmlActions = { 
                                            "Csr_Business_Conduct_Ethics",
                                            "Csr_Conflict_Minerals_Compliance",
                                            "Csr_Corporate_Sustainability_Policy",
                                            "Csr_Human_Rights_Management",
                                            "Csr_Supplier_Coc",
                                            "Csr_Sustainability_Strategy",
                                            "Stakeholder_Communication"
                                        };
            string[] allowDefaultActions = { 
                                                "About",
                                                "Contact",
                                                "Csr",
                                                "Csr_Download",
                                                "Csr_Report",
                                                "Csr_Report_Questionnaire",
                                                "Csr_Report_Questionnaire2",
                                                "Csr_Stakeholder_Engagement",
                                                "Csr_Sustainability_Governance",
                                                "Csr_Video",
                                                "Download",
                                                "Index",
                                                "Press_Room",
                                                "Press_Room_Detail",
                                                "Term_Of_Use",
                                                "Climate_Change_Questionnaire",
                                                "SiteMap",
                                                "Search",
                                                "Milestones"
                                            };

            routes.MapRoute(
                name: "Html",
                url: "{controller}/{action}.html/{id}",
                defaults: new { controller = "En", action = "Index", id = UrlParameter.Optional },
                constraints: new { action = string.Join("|", allowHtmlActions) }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}",
                defaults: new { controller = "En", action = "Index" }
                //constraints: new { action = string.Join("|", allowDefaultActions) }
            );

            routes.MapRoute(
                name: "PressRoom",
                url: "{controller}/Press_Room_Detail/{id}",
                defaults: new { controller = "En", action = "Press_Room_Detail", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "CsrReport",
                url: "{controller}/Csr_Report/{id}",
                defaults: new { controller = "En", action = "Csr_Report", id = UrlParameter.Optional }
            );
        }
    }
}
