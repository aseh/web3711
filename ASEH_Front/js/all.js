//IE old version tip when ie version < 9
var userAgent = window.navigator.userAgent;
if (userAgent.indexOf("MSIE 7.0") > 0 || userAgent.indexOf("MSIE 8.0") > 0 || userAgent.indexOf("MSIE 9.0") > 0) {
    setTimeout(
        function () {
            window.location.href = "/browser.html";
        }, 0);
}

//Footer SiteMap
function footerMatchHeight() {

    var sitemap = $('.sitemap>li');
    if ($(window).innerWidth() >= 768) {
        sitemap.matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });
    } else {
        sitemap.matchHeight({ remove: true });
    }
}

function footerhide() {
    var $arrow = $('.sitemap').find('.toggle').children('.arrow')
    if ($(window).innerWidth() >= 768) {
        $arrow.hide(0);
    } else {
        $arrow.show(0);
    }
}

function footerToogle() {
    var $arrow = $('.sitemap').find('.toggle').children('.arrow');
    $arrow.on('click touch', function (event) {
        event.preventDefault();
        var $this = $(this).parent('.toggle');
        $this.toggleClass('open');
        $this.next('ul').stop(true, true).slideToggle('fast');
    });
}

//Main menu
function menuToggle() {
    if ($('.hamburger').is('.is-active')) {
        if ($(window).innerWidth() >= 1025) {
            $('.nav-pc').stop(true, true).fadeIn(0)
        } else {
            // $('.nav-mobile').stop(true,true).fadeIn(0)
        }
    } else {
        if ($(window).innerWidth() >= 1025) {
            $('.nav-pc').stop(true, true).fadeOut(0)
        } else {
            // $('.nav-mobile').stop(true,true).fadeOut(0)
        }
    }
}

function nav_mobile_build() {
    var $nav = $('.nav-mobile .main'),
        $naxt = '<i class="icon-arrow-right to-next"></i>',
        $back_f = '<li><a href="javascript:;" class="go-back"><i class="icon-arrow-left"></i>';
    $back_b = '</a></li>';
    var $class_hidden = 'is-hidden';

    $nav.find('.has-next').each(function (index, el) {
        var $this = $(this);
        var backText = $this.children('a').text();
        var backText_parent = "";
        if ($this.parents('.has-next').length > 0) {
            var num = $this.parents('.has-next').length;
            for (i = 1; i <= num; i++) {
                var backup = backText_parent;
                backText_parent = $this.parents('.has-next').eq(i - 1).children('a').text() + " \/ " + backup;
            }
        }
        $this.children('a').append($naxt);
        $this.children('ul').addClass($class_hidden).prepend($back_f + backText_parent + backText + $back_b);
    });
    nav_mobile()
}

function nav_mobile() {
    $('.to-next').click(function (event) {
        $(this).parent().addClass('is-active');
        $(this).parent().next().removeClass('is-hidden');
        $(this).parent().parent().parent().addClass('move-out');
        return false
    });
    $('.go-back').click(function (event) {
        $(this).parent().parent().addClass('is-hidden');
        $(this).parent().parent().siblings('.is-active').removeClass('is-active');
        $(this).parent().parent().parent().parent().removeClass('move-out');
    });
}

//lockscroll
//http://stackoverflow.com/questions/4770025/how-to-disable-scrolling-temporarily
// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = { 37: 1, 38: 1, 39: 1, 40: 1 };
function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
}
function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}
function disableScroll() {
    if (window.addEventListener) // older FF
        window.addEventListener('DOMMouseScroll', preventDefault, false);
    window.onwheel = preventDefault; // modern standard
    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
    // window.ontouchmove  = preventDefault; // mobile
    document.onkeydown = preventDefaultForScrollKeys;
}
function enableScroll() {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    // window.ontouchmove = null;
    document.onkeydown = null;
}

function fixBannerFitH1Height() {
    $('.page-banner-s.banner-fit h1').height($('.page-banner-s.banner-fit').height());
}
function fixPlantslistDivHeight() {
    if ($(window).innerWidth() >= 1025) {
        $('.plants-list .content-lg .col-md-5 .list').outerHeight($('.plants-list .content-lg .col-md-7 .list').outerHeight());
    } else {
        $('.plants-list .content-lg .col-md-5 .list').outerHeight('');
    }
}
function init_masonry() {
    // init Masonry
    if ($('.timeline_div').length > 0) {
        var $container = $('.timeline_div');
        var i = 0;
        // layout Masonry after each image loads
        $container.imagesLoaded().progress(function () {
            $container.masonry({
                itemSelector: '.item',
                columnWidth: '.item',
                gutter: '.timeline'
            });

        }).done(function (instance) {
            //setTimeout(fix_timeline_year_pos,1500);
        });
        $container.on('layoutComplete',
            function (event, laidOutItems) {

                i++;

                if (i == $('.timeline_div .item').length) {
                    console.log(i);
                    setTimeout(fix_timeline_year_pos, 1000);
                }


            }
        );

        $(window).on('resize', function (event) {
            setTimeout(fix_timeline_year_pos, 2000);
        });
        function fix_timeline_year_pos() {


            $('.timeline_div .item').each(function () {
                $(this).removeClass('right');
                if ($(this).css('left') != '0px') {
                    $(this).addClass('right');
                }
            });
            $('.corner.big.before').attr('style', 'top:5px');
        }
    }
}

$(function () {
    //go top
    var $gotop = $(".gotop");
    if ($('body').scrollTop() === 0) {
        $gotop.hide();
    }
    $gotop.on("click touchstart", function () {
        $('html, body').animate({
            scrollTop: 0
        }, 400, "swing");
    }).focus(function () {
        $(this).blur();
    });

    // not supported CSS3 column-count
    if (!Modernizr.csscolumns) {
        $.getScript("/js/vendor/jquery.columnlist.min.js")
            .done(function () {
                $('.nav-pc .tab-content ul').columnlist({
                    size: 3,
                    'class': 'column-list',
                });
            });
    }
    fixBannerFitH1Height();
    fixPlantslistDivHeight();
    init_masonry();
    //Main Menu Click
    var scrollLock = false;
    function mainMenuClick() {

        var maskname = "stop-scroll";
        if (!$('.' + maskname).length) {
            $('header').after('<div class="' + maskname + '"></div>')
        } else {
            $('.' + maskname).remove();
        }

        $('.hamburger').toggleClass('is-active');
        $('html').toggleClass('nav-open');

        menuToggle();

        scrollLock = !scrollLock;
        scrollLock ? disableScroll() : enableScroll()
    }
    //hamburger
    $('.hamburger').on('click', function (event) {
        //event.preventDefault();
        //麵包選單
        if ($('#breadcrumbs-pc').length > 0 && $(window).innerWidth() > 1024) {
            $('#breadcrumbs-pc').toggle();

            $('#dropdown-view').removeClass('open')
            $('#breadcrumbs-view').find('li.active').removeClass('active');
        }

        mainMenuClick()
    });
    $(document).on('click touchstart', '.stop-scroll', function (event) {
        event.preventDefault();
        //麵包選單
        if ($('#breadcrumbs-pc').length > 0 && $(window).innerWidth() > 1024) {
            $('#breadcrumbs-pc').toggle();
        }

        mainMenuClick()
    });
    nav_mobile_build()

    //PC Navigation Link
    $('.nav > li > a')
        .on('mouseenter ', function (event) {
            $(this).tab('show');
        })
        .on('click', function (event) {
            event.preventDefault();
            var href = $(this).attr('data-href');
            window.location.href = href;
        });

    footerMatchHeight();
    footerhide();
    footerToogle();

    //page breadcrumbs
    var $breadbrumbs = $('#breadcrumbs').find('li'),
        $breadcrumbsLast = $breadbrumbs.eq($breadbrumbs.length - 1);
    $breadcrumbsLast.on('click touchstart', function (event) {
        event.preventDefault();
        $(this).blur();
    });


    //Breadcrumbs PC View
    if ($('#breadcrumbs-pc').length > 0) {
        //顯示breadcrumbs
        var $item = $('#breadcrumbs-pc');
        $('#dropdown-view').after($item);

        var $breadcrumbsDropdownView = $('#dropdown-view');
        var $breadcrumbsView = $('#breadcrumbs-view');
        $breadcrumbsView.on('mouseenter', '.glyphicon', function (event) {
            event.preventDefault();
            var $this = $(this),
                $parent = $this.parent(),
                $left = $this.offset().left - 50,
                $list = $this.next('ul').clone();

            $parent.siblings().removeClass('active');
            $parent.addClass('active');

            $list.css('marginLeft', $left)
            $breadcrumbsDropdownView.empty().append($list).addClass('open');
        });

        $breadcrumbsDropdownView.on('mouseleave', function (event) {
            $breadcrumbsDropdownView.removeClass('open')
            $breadcrumbsView.find('li.active').removeClass('active');
        });
    }

});

$(window)
    .on('load', function (event) {
        //Load Remove
        $('#page-loader').fadeOut('400', function () {
            $(this).remove();
        });
    }).on('scroll', function (event) {
        //Gosticky - PC
        var scrollTop = $(document).scrollTop();
        var header = $('header'),
            content = $('#wrapper');
        if (scrollTop > 0 && $(window).innerWidth() >= 1025) {
            //Start Scroll
            $('.header-sample').addClass('gosticky');
            header.addClass('gosticky');
            $('.gotop').stop(true, true).show(0);
        } else if (scrollTop <= 0 && $(window).innerWidth() >= 1025) {
            //ScrollTop
            $('.header-sample').removeClass('gosticky');
            header.removeClass('gosticky');
            $('.gotop').stop(true, true).show(0);
        }
    }).on('resize', function (event) {
        event.preventDefault();
        footerMatchHeight();
        footerhide();
        menuToggle();
        fixBannerFitH1Height();
        fixPlantslistDivHeight();

    });
$(document)
    .on('scrollstart scrollstop', function (event) {
        //Gosticky - mobile
        var scrollTop = $(document).scrollTop();
        var header = $('header'),
            content = $('#wrapper');
        if (scrollTop >= 0 && $(window).innerWidth() < 1025) {
            header.addClass('gosticky');
        } else if (scrollTop < 0 && $(window).innerWidth() < 1025) {
            header.removeClass('gosticky');
        }
    });

$(document).ready(function () {
    $('.dropdown-menu li .en').on('click', function () {
        // en website
        var url = window.location.href.toLowerCase();
        var hostname = window.location.hostname;
        var pathname = window.location.pathname.toLowerCase();

        if (url.indexOf('/ch/') != -1) {
            var new_url = url.replace("/ch/", "/en/");
            console.log(new_url);
            window.location.href = new_url;
        }
        else if (pathname == '/ch') {
            var new_url = url.replace("/ch", "/en");
            console.log(new_url);
            window.location.href = new_url;
        }
        else if (pathname == '/') {
            var new_url = url + 'en/';
            console.log(new_url);
            window.location.href = new_url;
        }
    });
    $('.dropdown-menu li .ch').on('click', function () {
        var url = window.location.href.toLowerCase();
        var hostname = window.location.hostname;
        var pathname = window.location.pathname.toLowerCase();

        if (url.indexOf('/en/') != -1) {
            var new_url = url.replace("/en/", "/ch/");
            console.log(new_url);
            window.location.href = new_url;
        }
        else if (pathname == '/en') {
            var new_url = url.replace("/en", "/ch");
            console.log(new_url);
            window.location.href = new_url;
        }
    });
});

$(document).ready(function () {
    //Set All Link Active
    function applyElements(eles, text) {
        eles.each(function (idx) {
            var eleHref = $(eles[idx]).attr('href')
            var eleTitle = $(eles[idx]).attr('data-title')
            if (eleHref == text && eleHref !== '') {
                console.log('href', eles[idx])
                $(eles[idx]).addClass('active')
            } else if (eleTitle === text && eleTitle !== '' && eleHref === undefined) {
                console.log('title', eles[idx])
                $(eles[idx]).addClass('active')
            }
        })
    }
    function addLinkActive(href) {
        applyElements($('a'), href)
        applyElements($('div'), href)
    }

    var pathname = window.location.pathname
    addLinkActive(pathname)
    var breadcrumbs = $('#breadcrumbs')
    if (breadcrumbs.length > 0) {
        var links = $('#breadcrumbs a')
        if (links.length > 0) {
            links.each(function (idx) {
                addLinkActive($(links[idx]).attr('href'))
                addLinkActive($(links[idx]).text())
            })
        }
    }


    var currentScrollTop = 0
    $(window).on('scroll', function () {
        var header = $('.ASEHeader')[0]
        var scrollY = window.pageYOffset
        if (scrollY > currentScrollTop + 10 && scrollY > 60) {
            currentScrollTop = scrollY
            $(header).addClass('scrollHide');
        } else if (scrollY < currentScrollTop - 10) {
            currentScrollTop = scrollY
            $(header).removeClass('scrollHide');
        }

        //var bread = $(breadcrumbs)[0]
        //var rect = bread.parentElement.getBoundingClientRect()

        //if (rect.y < 60) {
        //  $(bread).addClass('sticky')
        //} else {
        //  $(bread).removeClass('sticky')
        //}
    })
})