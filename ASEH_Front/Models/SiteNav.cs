﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASEH_Front.Models
{
    public class SiteNav
    {
        public SiteNav()
        {

        }

        public string Title { get; set; }
        public List<SiteNav> Children { get; set; }
        public string Url { get; set; }
        public int No { get; set; }
    }
}