﻿using Models;
using Service.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASEH_Front.Models.Download
{
    public class DownloadViewModel
    {
        public DownloadViewModel()
        {
            
        }

        public List<tbFile> FileList { get; set; }
    }
}