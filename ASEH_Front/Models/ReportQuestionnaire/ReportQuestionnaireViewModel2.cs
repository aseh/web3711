﻿using Models;
using Service.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASEH_Front.Models.ReportQuestionnaire
{   
    public class Text
    {
        
        public string Ch { get; set; }
        public string En { get; set;  }

        public Text(string Ch, string En )
        {
            this.Ch = Ch;
            this.En = En;
        }

        public string getText(string Lang)
        {
            if ( Lang == "Ch")
            {
                return this.Ch;
            } else
            {
                return this.En;
            }
        }
    }
    public class ReportStakeholder: Text
    {
        public string Key { get; set; }
        public string Ch { get; set; }
        public string En { get; set; }

        public ReportStakeholder(string Key, string Ch, string En): base(Ch, En)
        {
            this.Key = Key;
            this.Ch = Ch;
            this.En = En;
        }

    }
    public class ReportTopic
    {   
        public string Key { get; set; }
        public Text Issue { get; set; }
        public Text Content { get; set;  }
        
        public ReportTopic(string Key, string IssueCh, string IssueEn, string Ch, string En)
        {
            this.Key = Key;
            this.Issue = new Text(IssueCh, IssueEn);
            this.Content = new Text(Ch, En);
        }
    }

    public class ReportSection: Text
    {
        public string Key { get; set; }
        public string Ch { get; set; }
        public string En { get; set;  }

        public bool isYesOrNo { get; set; }
        public List<ReportTopic> TopicList { get; set; }

        public List<ReportStakeholder> StackholderList { get; set; }

        public ReportSection(string Key, string Ch, string En ): base(Ch, En)
        {
            this.Key = Key;
            this.Ch = Ch;
            this.En = En;
            this.isYesOrNo = true;
        }
        public ReportSection (string Ch, string En, List<ReportTopic> TopicList ) : base(Ch, En)
        {
            this.Ch = Ch;
            this.En = En;
            this.TopicList = TopicList;
        }

        public ReportSection (string Ch, string En, List<ReportStakeholder> StackholderList) : base(Ch, En)
        {
            this.Ch = Ch;
            this.En = En;
            this.StackholderList = StackholderList;
        }

    }

    public class ReportOption: Text
    {
        public string Key { get; set; }
        public string Ch { get; set; }
        public string En { get; set; }

        public ReportOption(string Key, string Ch, string En): base(Ch, En)
        {
            this.Key = Key;
            this.Ch = Ch;
            this.En = En;
        }
    }
    public class ReportQuestionnaireViewModel2
    {
        public ReportQuestionnaireViewModel2()
        {

        }

        public ReportQuestionnaireViewModel2(string Lang)
        {
            this.Lang = Lang;
        }

        public string stakeholder { get; set; }

        public string do1 { get; set; }
        public string do2 { get; set; }
        public string q1 { get; set; }
        public string q2 { get; set; }
        public string q3 { get; set; }
        public string q4 { get; set; }
        public string q5 { get; set; }
        public string q6 { get; set; }
        public string q7 { get; set; }
        public string q8 { get; set; }
        public string q9 { get; set; }
        public string q10 { get; set; }
        public string q11 { get; set; }
        public string q12 { get; set; }
        public string q13 { get; set; }
        public string q14 { get; set; }
        public string q15 { get; set; }
        public string q16 { get; set; }
        public string q17 { get; set; }
        public string q18 { get; set; }
        public string q19 { get; set; }
        public string q20 { get; set; }
        public string q21 { get; set; }
        public string q22 { get; set; }
        public string q23 { get; set; }
        public string additional { get; set; }
        public string other { get; set; }
        public string FieId { get; set; }

        public string Lang { get; set; }
        public List<ReportStakeholder> getStakeholders()
        {
            List<ReportStakeholder> list = new List<ReportStakeholder>();
            list.Add(new ReportStakeholder("Shareholder/Investor", "股東/投資人", "Shareholder/Investor"));
            list.Add(new ReportStakeholder("Employee", "員工", "Employee"));
            list.Add(new ReportStakeholder("Customer", "客戶", "Customer"));
            list.Add(new ReportStakeholder("Supplier/Contractor", "供應商/承攬商", "Supplier/Contractor"));
            list.Add(new ReportStakeholder("Government", "政府", "Government"));
            list.Add(new ReportStakeholder("NGOs/NPOs", "非政府組織/非營利組織", "NGOs/NPOs"));
            list.Add(new ReportStakeholder("Community Resident", "社區居民", "Community Resident"));
            list.Add(new ReportStakeholder("Media", "媒體", "Media"));
            list.Add(new ReportStakeholder("Industry Unions/Association", "產業公會/協會", "Industry Unions/Association"));
            list.Add(new ReportStakeholder("Other", "其他(請描述)", "Other, please indicate"));
            return list;
        }

        public List<ReportSection> getSections()
        {
            List<ReportSection> sections = new List<ReportSection>();

            List<ReportTopic> section1Topics = new List<ReportTopic>();
            List<ReportTopic> section2Topics = new List<ReportTopic>();
            List<ReportTopic> section3Topics = new List<ReportTopic>();

            section1Topics.Add(new ReportTopic("q1", "法令遵循", "Regulatory Compliance", "遵循所有適用法律，透過宣導與教育訓練促進合法合規意識", "Ensure compliance with all applicable laws; promote awareness in compliance with laws and regulations"));
            section1Topics.Add(new ReportTopic("q2", "商業道德", "Business Ethics", "頒布行為與道德守則，並且設置舉報系統，避免違犯反貪腐/反競爭行為", "Publish codes of conduct and codes of ethics and establishreporting channels; anti-corruption; anti-competitivebehavior;information security (including protection of customerproprietary information)"));
            section1Topics.Add(new ReportTopic("q3", "客戶關係管理", "Customer Relationship Management", "進行客戶滿意度調查與申訴機制，維持客戶服務品質", "Conduct customer satisfaction survey and maintain grievance system to maintain customer service quality"));
            section1Topics.Add(new ReportTopic("q4", "永續供應鏈", "Sustainable Supply Chain", "將永續整合至採購政策，推動供應商環境/社會績效評估與稽核，避免採購衝突礦產", "Formulate sustainable procurement policy; conduct suppliers’ sustainability risk assessment/ audit and avoid purchasing “conflict minerals”"));
            //section1Topics.Add(new ReportTopic("q5", "創新與研發", "Innovation and R&D", "新製程與新產品研發，同時在研發過程考量環境與社會創新，強化產品品質與智慧財產權的取得", "New process/new product development; factor in environmental and social impacts in research and development (R&D); enhance product quality and  technology/intellectual property acquisition"));
            section1Topics.Add(new ReportTopic("q18", "創新管理與永續製造", "Innovation Management and Sustainable Manufacturing", "新製程與新產品研發在研發過程同時考量環境與社會創新，從材料、採購、設計、生產與物流包裝等階段，提高能源效率、降低原物料使用、降低危害物質使用、提供永續製造服務，強化產品品質與智慧財產權的取得", "New process/new product development; factor inenvironmental and social impacts in research and development(R&D); integrate sustainable practices into all stagesof themanufacturing process, including material usage, design,procurement, production and packing to reduce theimpactson environment; enhance product quality andtechnology/intellectual property acquisition"));
            section1Topics.Add(new ReportTopic("q19", "資訊安全管理", "Information Security Management", "成立資安管理組織，取得國際資訊安全標準ISO 27001 認證，導入NIST CSF 成熟度評估機制，定期進行資訊系統的災難復原演練，客戶資訊保密", "Launched the Information Security Management Committee,obtaining the ISO 27001 certification, implementingNIST CSFmaturity assessment tool, and conducting annual disasterrecovery drill from major crisis events of informationsystem,protection of customer proprietary information"));

            section2Topics.Add(new ReportTopic("q6", "水資源管理", "Water Resource Management", "進行水源用水分析與監控，提高製程用水效率(中水回收廠)，積極減少廢水排放與參與投資人水揭露(CDP Water)倡議", "Analyze and monitor the source of water usage; improve the efficiency of processed water (Reclaimed Water Recycling Plant); reduce wastewater discharge and continue participation in CDP water use disclosure"));
            section2Topics.Add(new ReportTopic("q7", "氣候變遷", "Climate Change", "採取「減緩」與「調適」兩大策略因應氣候變遷，並且從產品、製程與供應鏈進行碳管理", "Climate change mitigation and adaptation; implement carbon management in product development, and in the manufacturing and supply chain process"));
            section2Topics.Add(new ReportTopic("q8", "能源管理", "Energy Management", "從製程與設備提高能資源使用效率，導入智慧電網，並且研擬再生能源策略", "Enhance efficiency of energy utilization in the manufacturing process and equipment; promote the building of smart grid and renewable energy strategy"));
            section2Topics.Add(new ReportTopic("q9", "廢棄物與循環再生", "Waste and Circular", "進行廢棄物減量與回收，讓產品、零組件及原物料可循環再生，展現永續資源的價值", "Reduce waste and recycle; aim to turn products, components and raw materials into reusable resources and create a circular system for sustainable development"));
            //section2Topics.Add(new ReportTopic("q10", "永續製造", "Sustainable Manufacturing", "從材料、採購、設計、生產與物流包裝等階段，提高能源效率、降低原物料使用、降低危害物質使用、提供永續製造服務", "Integrate sustainable practices into all stages of the manufacturing process, including material usage, design, procurement, production and packing to reduce the impacts on environment"));
            section2Topics.Add(new ReportTopic("q11", "空氣污染防制", "Air Pollution Prevention", "針對揮發性有機化合物(VOCs)、硫氧化物SOx、氮氧化物NOx及粒狀污染物，採用不同防治設備進行處理，符合法規標準。", "Various prevention and control equipment are used to treat the VOCs, SOx, NOx, and particulate matter in accordance with environmental legislation."));

            section3Topics.Add(new ReportTopic("q12", "職業安全衛生", "Occupation Health and Safety", "導入職業安全衛生管理系統，預防職業傷害，並且進行防災演練，攜手承攬商落實安全管理", "Introduce Occupation Health and Safety (OHS) management system to prevent accidents; introduce disaster response mechanisms;  work with contractors to ensure safety management mechanisms"));
            section3Topics.Add(new ReportTopic("q13", "人才吸引及留任", "Talent Attraction and Retention", "提供具有競爭力薪資福利，推動員工投入度調查，瞭解員工需求與想法，進行員工溝通，吸引與留任人才", "Ensure employees’ base pay remain competitive; conduct “employee engagement survey” to understand employee needs and opinions to attract and retain outstanding employees"));
            section3Topics.Add(new ReportTopic("q14", "人力發展", "Talent Development", "每年訂定年度培訓課程計劃，同時進行內部分享，強化員工職涯能力，同時有助於經營目標 ", "Employee training & education; internal sharing; enhance employees’ continual progress to improve enterprise competitiveness"));
            section3Topics.Add(new ReportTopic("q15", "人權", "Human Rights", "針對隱私權、個資、童工、強迫性勞動、歧視、集會自由、平等/公平等議題，進行人權風險評估與減緩措施，建立一個包容性職場", "Implement human rights risk assessment and risk mitigation in relation to privacy, personal information, child labor, forced or compulsory labor, discrimination, freedom of association, freedom/ equality; establish an inclusive workplace"));
            section3Topics.Add(new ReportTopic("q16", "多元與包容", "Diversity and inclusion", "建立員工多元化(性別、國籍、年齡、種族、弱勢)的職場，吸引不同的全球人才，強化組織競爭力", "Establish workplace diversity; attract global talent to enhance organization competitiveness"));
            section3Topics.Add(new ReportTopic("q17", "社會參與", "Social Involvement", "投入環保公益、社區營造與產學教育，並且對外支持永續倡議，發揮社會的正面影響力", "Support meaningful environmental causes, community engagement and industry-academia collaboration; support sustainability advocacy to extend a positive social influence "));

            /*section1Topics.Add(new ReportTopic("", "", "", "", ""));*/

            sections.Add(new ReportSection("身為利害關係人，您的身份是：", "Please indicate your stakeholder classification: ", this.getStakeholders()));
            sections.Add(new ReportSection("do1", "是否知道「日月光投控」每年發行企業社會責任報告書?", "Are you aware that ASE publishes the CSR report annually?"));
            sections.Add(new ReportSection("do2", "是否閱讀過「日月光投控」的企業社會責任報告書?", "Have you ever read ASE’s CSR report?"));
            sections.Add(new ReportSection(
                "請針對下列「日月光投控」的「經濟議題」，表示您的關注程度",
                "Please tick the box indicating the degree of concern on Economic sustainability issues",
                section1Topics
            ));

            sections.Add(new ReportSection("請針對下列「日月光投控」的「環境議題」，表示您的關注程度", "Please tick the box indicating the degree of concern on Environmental sustainability issues ", section2Topics));
            sections.Add(new ReportSection("請針對下列「日月光投控」的「社會議題」，表示您的關注程度", "Please tick the box indicating the degree of concern on Social sustainability issues ", section3Topics));

            return sections;
        }

        public Text getTitle()
        {
            return new Text("日月光投控企業社會責任報告書永續議題調查問卷", "ASE Corporate Sustainability Report - Stakeholder Concerns Questionnaire");
        }

        public Text getSummer()
        {
            return new Text("日月光投控每年持續進行企業社會責任報告書的編撰與出版，揭露我們對於社會責任的各種作法與永續發展的績效。為使揭露的資訊內容能夠更符合社會大眾的需求，在此誠摯地邀請您提供寶貴的意見與建議，協助我們瞭解您所關注的議題，做為資訊揭露的重要參考依據。本調查採不記名方式，所得資料僅作為日月光分析外界關注議題使用，不作為其他用途。", "ASE Technology Holding’s annual Corporate Sustainability Report documents the company’s sustainability issues and performance in key areas including stakeholder concerns. The purpose of this questionnaire is to collect stakeholders’ feedback and suggestions for analyzing, and addressing important issues of concern and present the results in the report. We sincerely request your support to complete this questionnaire. \nThis survey is anonymous and your data will not be used for any other purpose except as herein stated. We like to thank you in advance for your time and effort.");
        }

        public Text getTableHeader1()
        {
            return new Text("議題類別", "Sustainability Issues");
        }
        public Text getTableHeader2()
        {
            return new Text("內容陳述為日月光投控的做法", "The content describes our practice");
        }



        public Text getTableHeader3()
        {
            return new Text("關注程度", "Degree of Concern");
        }

        public List<ReportOption> getOptions()
        {
            List<ReportOption> options = new List<ReportOption>();
            options.Add(new ReportOption("VERY HIGH", "非常關注", "Very High"));
            options.Add(new ReportOption("HIGH", "關注", "High"));
            options.Add(new ReportOption("SLIGHT", "稍微關注", "Slight"));
            options.Add(new ReportOption("NONE", "無", "None"));

            return options;
        }

        public Text getLast()
        {
            return new Text("除上述永續議題，其他欲建議我們新增的議題，以及想給予我們其他的寶貴意見，竭誠歡迎。", "Any other comments and/or suggestions");
        }

        public Text getValid()
        {
            return new Text("驗證碼", "Verification");
        }

        public Text getSubmitButton() {
            return new Text("送出", "Sent");
        }

        public Text getCancelButton()
        {
            return new Text("清除", "Reset");
        }

      

    }
}