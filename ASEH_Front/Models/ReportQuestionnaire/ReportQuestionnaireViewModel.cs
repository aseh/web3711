﻿using Models;
using Service.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASEH_Front.Models.ReportQuestionnaire
{
    public class ReportQuestionnaireViewModel
    {
        public ReportQuestionnaireViewModel()
        {
            
        }

        public string stakeholder { get; set; }
        public string q1 { get; set; }
        public string q2 { get; set; }
        public string q3 { get; set; }
        public string q4 { get; set; }
        public string q5 { get; set; }
        public string q6 { get; set; }
        public string q7 { get; set; }
        public string q8 { get; set; }
        public string q9 { get; set; }
        public string q10 { get; set; }
        public string q11 { get; set; }
        public string q12 { get; set; }
        public string q13 { get; set; }
        public string q14 { get; set; }
        public string q15 { get; set; }
        public string q16 { get; set; }
        public string q17 { get; set; }
        public string q18 { get; set; }
        public string q19 { get; set; }
        public string q20 { get; set; }
        public string q21 { get; set; }
        public string q22 { get; set; }
        public string q23 { get; set; }
        public string additional { get; set; }
        public string other { get; set; }
        public string FieId { get; set; }



    }
}