﻿using Models;
using Service.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASEH_Front.Models.Article
{
    public class ArticleViewModel
    {
        public ArticleViewModel()
        {
            
        }

        public List<tbArticle> ArticleList { get; set; }
        public List<tbPicRotate> PicRotateList { get; set; }
        public tbPicRotate PicRotate { get; set; }
    }
}