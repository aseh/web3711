﻿using Models;
using Service.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASEH_Front.Models.Article
{
    public class ArticleDetailViewModel
    {
        public tbArticle Article { get; set; }
        public tbArticle ArticleSigh { get; set; }
    }
}