﻿
using ASEH_Backend.Services;
using ASEH_Front.Models;
using ASEH_Front.Models.Article;
using ASEH_Front.Models.Common.Quest;
using ASEH_Front.Models.Download;
using ASEH_Front.Models.ReportQuestionnaire;
using Models;
using Newtonsoft.Json;
using Service.BLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using web.ViewModels.GoogleSearch;

namespace ASEH_Front.Controllers
{
    //[RequireHttps]
    [OutputCache(NoStore = true, Duration = 0)]
    public class EnController : BaseController
    {

        UserService userService = new UserService();

        public EnController()
        {
            this.lang = "en";
        }

        public ActionResult News_Login()
        {
            this.Init();
            ViewBag.CountryList = Db.Country.ToList();
            try
            {
                validToken();
                return RedirectToAction("News_Profile");
            }
            catch (Exception)
            {

            }

            return View(new NewsUser()
            {
                Country = ViewBag.CountryList[0].Title
            });
        }

        [HttpPost]
        public ActionResult News_Login(NewsUser data)
        {
            this.Init();
            ViewBag.CountryList = Db.Country.ToList();
            var serv = new UserService();
            try
            {
                var user = serv.Login(data.Account, data.Password);
                var token = serv.GetToken(user, 180);
                Session["NewsUserToken"] = token;
                TempData["Message"] = "Login Success";
                return RedirectToAction("News_Profile");
            }
            catch (Exception err)
            {
                ViewBag.Message = "Login Failed";
                ViewBag.IsError = true;
            }

            return View(new NewsUser()
            {
                Country = ViewBag.CountryList[0].Title
            });
        }


        public ActionResult News_Register()
        {
            this.Init();
            ViewBag.CountryList = Db.Country.ToList();

            return View("News_Login", new NewsUser()
            {
                Country = ViewBag.CountryList[0].Title
            });
        }

        [HttpPost]
        public ActionResult News_Register(NewsUser data)
        {

            this.Init();
            ViewBag.CountryList = Db.Country.ToList();

            var ConfirmPassword = HttpContext.Request.Form.Get("ConfirmPassword");
            var serv = new UserService();
            try
            {
                if (Request.Cookies["CAPTCHA"].Value != this.Request.Form.Get("VerifyCode"))
                {
                    throw new Exception("Sorry, Verification code error");
                }
                if (ConfirmPassword != data.Password)
                {
                    throw new Exception("Sorry, new and confirmed passwords are inconsistent.");
                }
                data.Email = data.Account;
                try
                {
                    serv.Add(data);
                } catch ( Exception )
                {
                    throw new Exception("Sorry, Email Is Duplicate");
                }
                TempData["Message"] = "Register Success";
                return RedirectToAction("News_Login");
            }
            catch (Exception err)
            {
                ViewBag.IsError = true;
                ViewBag.Message = err.Message;
            }

            return View("News_Login", data);
        }

        public ActionResult News_Logout()
        {
            this.Init();
            this.Logout();

            return RedirectToAction("News_Login");
        }

        public ActionResult News_Edit_Profile()
        {
            this.Init();
            try
            {
                var sessionData = validToken();
                var dbData = userService.GetById(sessionData.Id);

                ViewBag.CountryList = Db.Country.ToList();
                return View(dbData);
            }
            catch (Exception)
            {
                return RedirectToAction("News_Login");
            }

        }



        [HttpPost]
        public ActionResult News_Edit_Profile(NewsUser data)
        {
            this.Init();


            try
            {
                var sessionUser = validToken();
                try
                {
                    var serv = new UserService();
                    serv.Update(sessionUser.Id, data);
                    TempData["Message"] = "Change Profile Success";
                    return RedirectToAction("News_Profile");
                }
                catch (Exception err)
                {
                    ViewBag.CountryList = Db.Country.ToList();
                    ViewBag.Message = "Sorry, Email Is Duplicate";
                    ViewBag.IsError = true;
                    return View(sessionUser);
                }
            }
            catch (Exception)
            {
                return RedirectToAction("News_Login");
            }

        }

        public ActionResult News_Edit_Password()
        {
            this.Init();
            return View();
        }

        [HttpPost]
        public ActionResult News_Edit_Password(FormCollection form)
        {
            string Password = form.Get("Password");
            string ConfirmPassword = form.Get("ConfirmPassword");
            try
            {
                var serv = new UserService();
                var sessionUser = validToken();

                if (Password != ConfirmPassword)
                {
                    ViewBag.Message = "Sorry, new and confirmed passwords are inconsistent.";
                    ViewBag.IsError = true;
                }
                else
                {
                    userService.ResetPassword(sessionUser.Id, Password);
                    TempData["Message"] = "Change Password Success";
                    return RedirectToAction("News_Profile");
                }
                this.Init();
                return View();
            }
            catch (Exception)
            {
                return RedirectToAction("News_Login");
            }

        }

        public ActionResult News_Forget(string token)
        {
            this.Init();
            if (token != null)
            {
                try
                {
                    var dbData = userService.ValidToken(token);
                    ViewBag.Token = token;
                }
                catch (Exception err)
                {
                    ViewBag.Message = "Change Password Expired";
                    ViewBag.IsError = true;
                }
            }

            return View();
        }

        [HttpPost]
        public ActionResult News_Forget(FormCollection form)
        {
            var Token = form.Get("Token");
            if (Token == null)
            {
                // CheckEmail
                var Email = form.Get("Email");
                try
                {
                    var dbData = userService.GetByEmail(Email);

                    //return RedirectToAction("News_Forget", new
                    //{
                    //    token = userService.GetToken(dbData, 30)
                    //});
                    userService.SendForgetEmail(dbData.Id);
                    TempData["Message"] = "Email Verify Success, Please Check Email";
                    return RedirectToAction("News_Login");
                }
                catch (Exception err)
                {
                    ViewBag.Message = "The Email Not Register";
                    ViewBag.IsError = true;
                }

            }
            else
            {
                //Update Password
                var Password = form.Get("Password");
                var ConfirmPassword = form.Get("ConfirmPassword");
                if (Password != ConfirmPassword)
                {
                    ViewBag.Message = "Sorry, new and confirmed passwords are inconsistent.";
                    ViewBag.IsError = true;
                }
                else
                {
                    try
                    {
                        var tokenData = userService.ValidToken(Token);
                        userService.ResetPassword(tokenData.Id, Password);
                        TempData["Message"] = "Change Password Success, Please Use New Password Login";
                        return RedirectToAction("News_Login");
                    }
                    catch (Exception)
                    {
                        ViewBag.Message = "Change Password Expired";
                        ViewBag.IsError = true;
                        return RedirectToAction("News_Forget");
                    }
                }

            }

            this.Init();
            return View();
        }


        
        public ActionResult News_Profile()
        {
            this.Init();
            try
            {
                validToken();
            }
            catch (Exception)
            {
                return RedirectToAction("News_Login");
            }

            return View();
        }

        public ActionResult Milestones()
        {
            this.Init();
            return this.View();
        }

        public ActionResult Search(string q = "", int page = 1)
        {
            this.Init();
            string searchUrl = String.Format("https://www.googleapis.com/customsearch/v1?key=AIzaSyCoIG9dTAyeNTxVWaAPZ2leVdt8f3MZm0c&cx=017526043270526596107:s1vtx3mblil&q={0}&start={1}", q, ((page - 1) * 10) + 1);
            WebRequest req = HttpWebRequest.Create(searchUrl);

            req.ContentType = "application/json";


            GoogleSearchResult result;
            string body;
            // 取得回應資料
            using (HttpWebResponse response = req.GetResponse() as HttpWebResponse)
            {
                using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    body = sr.ReadToEnd();
                    result = GoogleSearchResult.FromJson(body);
                }
            }

            ViewBag.SearchQuery = q;
            ViewBag.SearchPage = page;
            ViewBag.SearchBody = body;


            ViewBag.lang = "en";
            return View("../Search/index", result);
        }
        public ActionResult SiteMap()
        {
            
            this.Init();
            return View();
        }
        public ActionResult Index()
        {
            this.Init();
            var model = new ArticleViewModel();
            var query = _serv.GetArticleQuery();

            model.ArticleList = query.Take(3).ToList();

            var picRotagtes = _serv.GetPicRotateQuery().Where(m => m.ProEnable == 1);

            if (picRotagtes.Count() > 0)
            {
                model.PicRotateList = picRotagtes.ToList();
            }

            return View(model);
        }

        public ActionResult Download()
        {
            this.Init();
            return View();
        }

        public ActionResult About()
        {
            this.Init();
            return View();
        }

        public ActionResult Contact()
        {
            this.Init();
            return View();
        }

        public ActionResult Csr()
        {
            this.Init();
            return View();
        }

        public ActionResult Csr_Business_Conduct_Ethics()
        {
            this.Init();
            return View();
        }

        //public ActionResult Csr_Conflict_Minerals_Compliance()
        //{
        //    this.Init();
        //    return View();
        //}

        public ActionResult Csr_Corporate_Sustainability_Policy()
        {
            this.Init();
            return View();
        }

        public ActionResult Csr_Human_Rights_Management()
        {
            this.Init();
            return View();
        }

        public ActionResult Stakeholder_Communication()
        {
            this.Init();
            return View();
        }

        public ActionResult Climate_Change_Questionnaire()
        {
            this.Init();
            string JsonString = System.IO.File.ReadAllText(Server.MapPath(String.Format("~/App_Data/quest2.json")));
            QuestBody QuestBody = JsonConvert.DeserializeObject<QuestBody>(JsonString);
            QuestBody.Lang = "En";
            QuestBody.Form = new FormCollection();
            ViewBag.Title = "Questionnaire on climate change and water security impacts";
            return View("Questionnaire", QuestBody);
        }

        [HttpPost]
        public ActionResult Climate_Change_Questionnaire(FormCollection Form)
        {
            this.Init();
            string JsonString = System.IO.File.ReadAllText(Server.MapPath(String.Format("~/App_Data/quest2.json")));
            QuestBody QuestBody = JsonConvert.DeserializeObject<QuestBody>(JsonString);
            QuestBody.Lang = "En";
            QuestBody.Form = Form;
            if (string.IsNullOrEmpty(Form["stakeholder"]))
            {
                ViewData["stakeholder"] = "Please choose your relationship with ASE";

                return View("Questionnaire", QuestBody);
            }

            if (Request.Cookies["CAPTCHA"].Value != Form["vcode"])
            {
                ViewData["captcha"] = "Verification code error";

                return View("Questionnaire", QuestBody);
            }
            var table = new tbQuestionnaire();

            var list = new Dictionary<string, string>();
            foreach (string key in Form.Keys)
            {
                list.Add(key, Form[key]);
            }

            table.form = JsonConvert.SerializeObject(list);
            table.ip = Request.UserHostAddress;
            table.create_date = DateTime.Now;
            table.update_date = DateTime.Now;
            table.type = "CLIMATE";

            _serv.CreateQuestionnaire(table);
            ViewBag.Title = "Questionnaire on climate change and water security impacts";
            ViewData["msg"] = "Thanks for your valuable comments.";
            return View("Questionnaire", QuestBody);
        }
        public ActionResult Csr_Report_Questionnaire()
        {
            this.Init();
            var model = new ReportQuestionnaireViewModel2("En");
            var fieId = Request.QueryString["id"];

            model.FieId = fieId;
            model.Lang = "En";

            return View(model);
            
        }

        [HttpPost]
        public ActionResult Csr_Report_Questionnaire(FormCollection post, ReportQuestionnaireViewModel2 model)
        {
            this.Init();
            if (string.IsNullOrEmpty(post["stakeholder"]))
            {
                ViewData["stakeholder"] = "Please choose your relationship with ASE";
                model.Lang = "En";
                ViewData.Model = model;

                return View("Csr_Report_Questionnaire");
            }

            if (Request.Cookies["CAPTCHA"].Value != post["vcode"])
            {
                ViewData["captcha"] = "Verification code error";
                model.Lang = "En";
                ViewData.Model = model;

                return View("Csr_Report_Questionnaire");
            }

            var data = new tbReportQuestionnaire2();

            data.stakeholder = post["stakeholder"];
            data.do1 = post["do1"];
            data.do2 = post["do2"];
            data.q1 = post["q1"];
            data.q2 = post["q2"];
            data.q3 = post["q3"];
            data.q4 = post["q4"];
            data.q5 = post["q5"];
            data.q6 = post["q6"];
            data.q7 = post["q7"];
            data.q8 = post["q8"];
            data.q9 = post["q9"];
            data.q10 = post["q10"];
            data.q11 = post["q11"];
            data.q12 = post["q12"];
            data.q13 = post["q13"];
            data.q14 = post["q14"];
            data.q15 = post["q15"];
            data.q16 = post["q16"];
            data.q17 = post["q17"];

            data.additional = post["additional"];
            data.ip = Request.UserHostAddress;
            data.create_date = DateTime.Now;
            data.update_date = DateTime.Now;
            data.other = post["other"];

            _serv.CreateReportQuestionnaire2(data);

            #region Add FileLog

            var fieid = post["fieid"];

            if (!string.IsNullOrEmpty(fieid))
            {
                var fileLog = new tbFileLog();

                fileLog.FIeID = fieid;
                fileLog.FigDesc = "Ch/Questionnaire2";
                fileLog.CreateTime = DateTime.Now;

                _serv.CreateFileLog(fileLog);
            }

            #endregion

            ViewData.Model = new ReportQuestionnaireViewModel2("En");


            ViewData["msg"] = "Thanks for your valuable comments.";

            return View("Csr_Report_Questionnaire");
        }

        public ActionResult Csr_Report_Questionnaire_Back()
        {
            this.Init();
            var model = new ReportQuestionnaireViewModel();
            var fieId = Request.QueryString["id"];

            model.FieId = fieId;

            return View(model);
        }

        [HttpPost]
        public ActionResult Csr_Report_Questionnaire_Back(FormCollection post, ReportQuestionnaireViewModel model)
        {
            this.Init();
            if (string.IsNullOrEmpty(post["stakeholder"]))
            {
                ViewData["stakeholder"] = "Please choose your relationship with ASE";

                ViewData.Model = model;

                return View("Csr_Report_Questionnaire");
            }

            if (Request.Cookies["CAPTCHA"].Value != post["vcode"])
            {
                ViewData["captcha"] = "Verification code error";

                ViewData.Model = model;

                return View("Csr_Report_Questionnaire");
            }

            var data = new tbReportQuestionnaire();

            data.stakeholder = post["stakeholder"];
            data.q1 = post["Corporate"];
            data.q2 = post["Ethics"];
            data.q3 = post["Regulatory"];
            data.q4 = post["Stakeholders"];
            data.q5 = post["Strategy"];
            data.q6 = post["Customer"];
            data.q7 = post["Taxation"];
            data.q8 = post["Supply"];
            data.q9 = post["Innovation"];
            data.q10 = post["Risk"];
            data.q11 = post["Climate"];
            data.q12 = post["Waste"];
            data.q13 = post["Water"];
            data.q14 = post["Energy"];
            data.q15 = post["Green"];
            data.q16 = post["Training"];
            data.q17 = post["Equality"];
            data.q18 = post["Human"];
            data.q19 = post["Labor"];
            data.q20 = post["Health"];
            data.q21 = post["Community"];
            data.q22 = post["Charity"];
            data.q23 = post["Initiatives"];
            data.additional = post["additional"];
            data.ip = Request.UserHostAddress;
            data.create_date = DateTime.Now;
            data.update_date = DateTime.Now;
            data.other = post["other"];

            _serv.CreateReportQuestionnaire(data);

            #region Add FileLog

            var fieid = post["fieid"];

            if (!string.IsNullOrEmpty(fieid))
            {
                var fileLog = new tbFileLog();

                fileLog.FIeID = fieid;
                fileLog.FigDesc = "En/Questionnaire";
                fileLog.CreateTime = DateTime.Now;

                _serv.CreateFileLog(fileLog);
            }

            #endregion

            ViewData.Model = new ReportQuestionnaireViewModel();

            ViewData["msg"] = "Thanks for your valuable comments.";

            return View("Csr_Report_Questionnaire");
        }

        public ActionResult Csr_Stakeholder_Engagement()
        {
            this.Init();
            return View();
        }

        //public ActionResult Csr_Supplier_Coc()
        //{
        //    this.Init();
        //    return View();
        //}

        public ActionResult Csr_Sustainability_Governance()
        {
            this.Init();
            return View();
        }

        public ActionResult Csr_Sustainability_Strategy()
        {
            this.Init();
            return View();
        }

        public ActionResult Csr_Download()
        {
            this.Init();
            return View();
        }

        public ActionResult Csr_Report()
        {
            this.Init();
            var model = new DownloadViewModel();
            var query = _serv.GetFileQuery();

            model.FileList = query.ToList();

            return View(model);
        }

        [HttpPost]
        public void Csr_Report(Guid id)
        {
            this.Init();
            var file = _serv.GetFile(id);

            if (file != null)
            {
                file.FieDownload = file.FieDownload + 1;

                _serv.UpdateFile(file);

                var fileLog = new tbFileLog();

                fileLog.FIeID = file.FieID;
                fileLog.FigDesc = "En/Download";
                fileLog.CreateTime = DateTime.Now;

                _serv.CreateFileLog(fileLog);

                //string filePath = Server.MapPath(string.Format("~/uploads/{0}/{1}", file.FieID, file.FieFilePath_EN));

                //if (System.IO.File.Exists(filePath))
                //{
                //    string fileNameOrExtension = file.FieFilePath_EN;
                //    string mimeType = "application/unknown";
                //    string ext = (fileNameOrExtension.Contains(".")) ? System.IO.Path.GetExtension(fileNameOrExtension).ToLower() : "." + fileNameOrExtension;

                //    Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);

                //    if (regKey != null && regKey.GetValue("Content Type") != null) mimeType = regKey.GetValue("Content Type").ToString();

                //    file.FieDownload = file.FieDownload + 1;

                //    _serv.UpdateFile(file);

                //    var fileLog = new tbFileLog();

                //    fileLog.FIeID = file.FieID;
                //    fileLog.FigDesc = "Download";
                //    fileLog.CreateTime = DateTime.Now;

                //    _serv.CreateFileLog(fileLog);

                //    return File(filePath, mimeType, file.FieFilePath_EN);
                //}
            }

            //return View();
        }

        public ActionResult Csr_Video()
        {
            this.Init();
            return View();
        }

        public ActionResult Term_Of_Use()
        {
            this.Init();
            return View();
        }

        public ActionResult Press_Room()
        {
            this.Init();
            var model = new ArticleViewModel();
            var query = _serv.GetArticleQuery();

            model.ArticleList = query.ToList();

            return View(model);
        }

        [Route("PressRoomDetail")]
        public ActionResult Press_Room_Detail(string id)
        {
            this.Init();
            var model = new ArticleDetailViewModel();

            id = id.Replace("-", " ");

            model.Article = _serv.GetArticle(id);

            //20200722 Fix: 若還未到發佈日期則直接轉址到列表頁
            if (model.Article == null || model.Article.AtcEnable == 0 || model.Article.AtcOpenDate > DateTime.Now)
            {
                return RedirectToAction("Press_Room");
            }


            model.ArticleSigh = _serv.GetArticleSign();

            

            SetSEO(model.Article.AtcName_EN, model.Article.SEODesc_EN, model.Article.OgImage);

            return View(model);
        }
        public ActionResult Ip_Management()
        {
            this.Init();
            return View();
        }
    }
}