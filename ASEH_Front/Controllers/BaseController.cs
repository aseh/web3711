﻿using ASEH_Front.Models;
using Newtonsoft.Json;
using Service.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Models;
using ASEH_Backend.Services;


namespace ASEH_Front.Controllers
{
    public class BaseController : Controller
    {
        public ASEHService _serv = new ASEHService();
        public ASEHEntities Db = new ASEHEntities();
        public string lang;
        public void Init()
        {
            string JsonString = System.IO.File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + (this.lang == "ch" ? "/App_Data/3711ch.json" : "/App_Data/3711en.json"));
            List<SiteNav> SiteNavList = JsonConvert.DeserializeObject<List<SiteNav>>(JsonString);
            ViewBag.site_nav_list = SiteNavList;
            ViewBag.SiteOgImage = "http://www.aseglobal.com/pic/content/ch/images/og.png";
            ViewBag.SiteUrl = Request.Url;
            if ( lang == "ch")
            {
                ViewBag.SiteName = "日月光投控";
                ViewBag.SiteDesc = "日月光投控為全球領先半導體封裝與測試製造服務公司，日月光與矽品共組日月光投資控股公司為全球領先半導體封裝與測試製造服務公司，提供客戶微型化、高效能與高整合的技術服務。";
            } else
            {
                ViewBag.SiteName = "ASE Technology Holding";
                ViewBag.SiteDesc = "ASE Technology Holding Co., Ltd. is jointly established by the combination of Advanced Semiconductor Engineering, Inc. and Siliconware Precision Industries Co., Ltd. ASE Technology Holding is the world's leading provider of independent semiconductor manufacturing services in assembly and test.";
            }
            ViewBag.Lang = lang;

            if ( TempData["Message"] != null )
            {
                ViewBag.Message = TempData["Message"];
                TempData.Remove("Message");
            }
        }

        public void SetSEO(string SiteName, string SiteDesc, string OgImage) 
        {
            ViewBag.SiteName = SiteName;
            ViewBag.SiteDesc = SiteDesc;
            ViewBag.SiteOgImage = new Uri(Request.Url, OgImage);
        }

        public void SetUserToken(NewsUser Data) 
        {
            UserService service = new UserService();
            var token = service.GetToken(Data);
            Session["NewsUserToken"] = token;
        }

        public NewsUser validToken()
        {
            string token = (string)Session["NewsUserToken"];
            if (token == null )
            {
                throw new Exception("尚未登入");
            }
            UserService service = new UserService();
            return service.ValidToken(token);
        }

        public void Logout ()
        {
            Session.Remove("NewsUserToken");
        }
    }
}