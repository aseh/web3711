﻿using ASEH_Front.Models.Article;
using ASEH_Front.Models.Download;
using ASEH_Front.Models.ReportQuestionnaire;
using Models;
using Service.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASEH_Front.Controllers
{
    //[RequireHttps]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return Redirect("/en");
        }
    }
}