<!--#include file="admin_news_chk.asp"-->
<!--#include file="I_Chk_Null.asp"-->
<%
	autoid=request("autoid")
	search=request("search")
	page=request("page")
	if autoid="" then
		session("message") = "編號錯誤!"
		response.redirect "admin_partner.asp?chk_kind_recno=" & chk_kind_recno & "&search=" & search & "&page=" & page
	end if

	sqlst="select * from partner where autoid=" & autoid
	set rs=conn.execute(sqlst)
	if rs.eof then
		rs.close
		set rs=nothing
		conn.close
		set conn=nothing
		response.redirect "admin_partner.asp?page=" & page
	else
			autoid	= Chk_null(rs("autoid"))
			upt_time	= Chk_null(rs("upt_time"))
			upt_als		= Chk_null(rs("upt_als"))
			company_title		= Chk_null(rs("company_title"))
			company_phone  	= Chk_null(rs("company_phone"))
			username		= Chk_null(rs("username"))
			job_title		= Chk_null(rs("job_title"))
			phone_ext	= Chk_null(rs("phone_ext"))			
			cellphone	= Chk_null(rs("cellphone"))			
			email	= Chk_null(rs("email"))
			add_time	= Chk_null(rs("add_time"))
			company_fax	= Chk_null(rs("company_fax"))
			company_fax	= Chk_null(rs("company_fax"))
			company_add	= Chk_null(rs("company_add"))
			sex	= Chk_null(rs("sex"))
			if sex="先生" then
				sex1="checked"
				sex2=""
			else
				sex2="checked"
				sex1=""
			end if
			atype	= Chk_null(rs("type"))
			if instr(atype,"Manufacturers") > 0 then type1="checked"
			if instr(atype,"Agents") > 0 then type2="checked"
			if instr(atype,"Agency") > 0 then type3="checked"
			
			unit	= Chk_null(rs("unit"))
			business	= Chk_null(rs("business"))
			factory_number	= Chk_null(rs("factory_number"))
			factory_add	= Chk_null(rs("factory_add"))
			brand	= Chk_null(rs("brand"))
			uninumber	= Chk_null(rs("uninumber"))
			money1	= Chk_null(rs("money1"))
			money2	= Chk_null(rs("money2"))
			customer	= Chk_null(rs("customer"))
			supplier	= Chk_null(rs("supplier"))
			equipment	= Chk_null(rs("equipment"))
			apply	= Chk_null(rs("apply"))
			function_way	= Chk_null(rs("function_way"))
			company_url	= Chk_null(rs("company_url"))
			
			president	= Chk_null(rs("president"))
			founded_date	= Chk_null(rs("founded_date"))
			factory_tel	= Chk_null(rs("factory_tel"))
			employee_number	= Chk_null(rs("employee_number"))
			iso_certification	= Chk_null(rs("iso_certification"))
			introduction	= Chk_null(rs("introduction"))
	end if
	rs.close
	set rs=nothing
	conn.close
	set conn=nothing
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>
$(document).ready(function (){
    $(document).on('change', '.type', function() {
    	//console.log($(this).prop('checked'));
    	var Manufacturers=$('#Manufacturers').prop('checked');
    	var Agents=$('#Agents').prop('checked');
    	var Agency=$('#Agency').prop('checked');
    	if(!Manufacturers && !Agents && !Agency){
				$('.Agents').show();
				$('.Manufacturers').show();
				$('.Manufacturers input:text').attr('disabled',false);
				$('.Agents input:text').attr('disabled',false);
    	}else{
				$('.Agents').hide();
				$('.Manufacturers').hide();
				$('.Manufacturers input:text').attr('disabled',true);
				$('.Agents input:text').attr('disabled',true);
			}

    	if(Manufacturers){
				$('.Manufacturers').show();
				$('.Manufacturers input:text').attr('disabled',false);
			}
			if(Agents){
				$('.Agents').show();
				$('.Agents input:text').attr('disabled',false);
			}
    }); 
    $(".type").trigger("change");
    
    $(document).on('change', '#email', function() {
    	var cemail=$(this).val();
			var chk_emails = ["126.com","128.com","139.com","163.com","163.net","17173.com","188.com","189.cn","21cn.com","263.net","aliyun.com","aol.com","chinaren.com","citiz.net","cntv.cn","eyou.com","facebookmail.com","foxmail.com","getpocket.com","giga.net.tw","gmail.com","hotmail.com","icloud.com","kimo.com","line.naver.jp","livemail.tw","mac.com","mail2000.com.tw","mailbox.evernote.com","me.com","hinet.net","url.com.tw","msn.com","netease.com","outlook.com","pchome.com.tw","qq.com","renren.com","rongbiz.com","seed.net.tw","sina.cn","sina.com","singnet.com.sg","sohu.com","sohu.net","so-net.net.tw","tom.com","totalbb.net.tw","wo.com.cn","xinhuanet.com","yahoo.com","yahoo.com.tw","yeah.net","ymail.com"]; 
			chk=false;
			for	(index = 0; index < chk_emails.length; index++) {
	      if($(this).val().toLowerCase().indexOf(chk_emails[index])>= 1){
					chk=chk_emails[index];
				}
			}
			if(chk){
				alert('請填寫官方E-mail帳號，ASE不接受私人信箱-'+chk);
				document.form1.email.focus();
			}
    }); 
    $(document).on('change', '.Agents', function() {
    	if(!$('input[name="type"]:checked').length){
				alert('請先選取公司屬性');
    	}
    }); 
    $(document).on('change', '.Manufacturers', function() {
    	if(!$('input[name="type"]:checked').length){
				alert('請先選取公司屬性');
    	}
    }); 
}); 
</script>

<body>
<center>
	<%
	Set fs = CreateObject("Scripting.FileSystemObject")
		if fs.FileExists(Session("ADMIN_HTML_PATH") & "top.htm") then		
			Set input1 = fs.OpenTextFile(Session("ADMIN_HTML_PATH") & "top.htm",1,False,True)
			do while not input1.AtEndOfStream
				txt1 = input1.Readline			
				txt1=replace(txt1,"$home_url",session("HOME_URL"))	
				txt1=replace(txt1,"$member_url",session("MEMBER_URL"))	
				response.write txt1 & chr(10)
			loop	
		end if
	set fs=nothing
%>
<div class="services">
 [<a href='admin_partner.asp'>回上頁</a>]
<h2>公司通訊及資料</h2>
<form id="form1" name="form1" method="post" action="admin_partner_chgm.asp">
<h6>
<table width="735" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="355" valign="top">
      <table width="350" border="0" cellpadding="0" cellspacing="0" class="tableForm1">
        <tr>
          <td height="25" colspan="3"><span class="content_bold">公司通訊</span></td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td width="110" align="center" bgcolor="#CCCCCC">公司抬頭*</td>
          <td width="1" align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td width="179" style="padding:5px">
            <label for="textfield3"></label>
            <input name="company_title" type="text" id="textfield3" required size="25" value="<%=company_title%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td align="center" bgcolor="#CCCCCC">公司電話*</td>
          <td width="1" align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="phone" name="company_phone" id="textfield2" required size="25" value="<%=company_phone%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td align="center" bgcolor="#CCCCCC">公司傳真*</td>
          <td width="1" align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="company_fax" id="textfield3" required size="25" value="<%=company_fax%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td align="center" bgcolor="#CCCCCC">公司網址*</td>
          <td width="1" align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="url" name="company_url" id="textfield4" required size="25" value="<%=company_url%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td align="center" bgcolor="#CCCCCC">公司地址*</td>
          <td width="1" align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="company_add" id="textfield5" required size="25" value="<%=company_add%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
      </table>
      <br />
      <table width="350" border="0" cellpadding="0" cellspacing="0" class="tableForm1">
        <tr>
          <td height="25" colspan="3"><span class="content_bold">申請人資料</span></td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td width="110" align="center" bgcolor="#CCCCCC">性別*</td>
          <td width="1" align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td width="179" style="padding:5px">
            <input type="radio" name="sex" required value="先生" <%=sex1%> />
            先生
            <input type="radio" name="sex" required value="女士" <%=sex2%> />
            女士 </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td align="center" bgcolor="#CCCCCC">姓名*</td>
          <td width="1" align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="username" id="textfield7" required size="25" value="<%=username%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td align="center" bgcolor="#CCCCCC">部門*</td>
          <td width="1" align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="unit" id="textfield8" required size="25	" value="<%=unit%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td align="center" bgcolor="#CCCCCC">職稱*</td>
          <td width="1" align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="job_title" id="textfield9" required size="25	" value="<%=job_title%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td align="center" bgcolor="#CCCCCC">分機號碼*</td>
          <td width="1" align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="phone_ext" id="textfield10" required size="25" value="<%=phone_ext%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td align="center" bgcolor="#CCCCCC">手機號碼*</td>
          <td width="1" align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="cellphone" id="textfield11" required size="25" value="<%=cellphone%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td align="center" bgcolor="#CCCCCC"> 電子郵件信箱*</td>
          <td width="1" align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="email" name="email" id="email" required size="25" value="<%=email%>" />
          </td>
        </tr>
        <tr>
          <td align="center" bgcolor="#CCCCCC">&nbsp;</td>
          <td width="1" align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">請填寫官方E-mail帳號，<br />
            ASE不接受私人信箱</td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
    </table>
    </td>
    <td width="350" valign="top">
      <table width="350" border="0" cellpadding="0" cellspacing="0" class="tableForm1">
        <tr>
          <td height="25" colspan="3"><span class="content_bold">公司資料</span></td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td align="center" bgcolor="#CCCCCC">公司屬性*</td>
          <td width="1" align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="checkbox" name="type" class="type" id="Manufacturers" value="Manufacturers" <%=type1%> />
            製造商
            <input type="checkbox" name="type" class="type" id="Agents" value="Agents" <%=type2%> />
            代理商
            <input type="checkbox" name="type" class="type" id="Agency" value="Agency" <%=type3%> />
            仲介商</td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td align="center" bgcolor="#CCCCCC">營業項目*</td>
          <td width="1" align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="business" id="textfield6" required size="25" value="<%=business%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr class="Manufacturers">
          <td align="center" bgcolor="#CCCCCC">工廠登記證號*</td>
          <td width="1" align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="factory_number" id="textfield15" required size="25" value="<%=factory_number%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr class="Manufacturers">
          <td align="center" bgcolor="#CCCCCC">工廠地址*</td>
          <td width="1" align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="factory_add" id="textfield17" required size="25" value="<%=factory_add%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr class="Agents">
          <td align="center" bgcolor="#CCCCCC">代理品牌*</td>
          <td width="1" align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="brand" id="textfield19" required size="25" value="<%=brand%>" />
          </td>
        </tr>
        <tr class="Manufacturers">
          <td align="center" bgcolor="#CCCCCC">工廠電話</td>
          <td align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="factory_tel" id="textfield25"  size="25" value="<%=factory_tel%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td align="center" bgcolor="#CCCCCC">統一編號*</td>
          <td align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="uninumber" id="textfield19" required size="25" value="<%=uninumber%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td align="center" bgcolor="#CCCCCC">資本額*</td>
          <td align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="money1" id="textfield19" required size="25" value="<%=money1%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td align="center" bgcolor="#CCCCCC">營業額*</td>
          <td align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="money2" id="textfield19" required size="25" value="<%=money2%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr class="Manufacturers">
          <td align="center" bgcolor="#CCCCCC">負責人*</td>
          <td align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="president" id="textfield25"  size="25" value="<%=president%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
       <tr class="Manufacturers">
          <td align="center" bgcolor="#CCCCCC">成立日期*</td>
          <td align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="founded_date" id="textfield25"  size="25" value="<%=founded_date%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr class="Manufacturers">
          <td align="center" bgcolor="#CCCCCC">員工總數*</td>
          <td align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="employee_number" id="textfield25"  size="25" value="<%=employee_number%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td align="center" bgcolor="#CCCCCC">主要往來客戶*</td>
          <td align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="customer" id="textfield29" required size="25" value="<%=customer%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
         <tr class="Manufacturers">
          <td align="center" bgcolor="#CCCCCC">ISO認證</td>
          <td align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="iso_certification" id="textfield25"  size="25" value="<%=iso_certification%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr class="Manufacturers Agents">
          <td align="center" bgcolor="#CCCCCC">主要往來供應商*</td>
          <td align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="supplier" id="textfield27" required size="25" value="<%=supplier%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr class="Manufacturers">
          <td align="center" bgcolor="#CCCCCC">主要生產設備名稱</td>
          <td align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="equipment" id="textfield25"  size="25" value="<%=equipment%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr class="Manufacturers Agents">
          <td align="center" bgcolor="#CCCCCC">主要應用範圍<br />
            (晶圓/封裝/測試)</td>
          <td align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="apply" id="textfield13"  size="25" value="<%=apply%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr class="Manufacturers Agents">
          <td align="center" bgcolor="#CCCCCC">產品功能/用途</td>
          <td align="center" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
          <td style="padding:5px">
            <input type="text" name="function_way" id="textfield31"  size="25" value="<%=function_way%>" />
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        <tr class="Manufacturers">
          <td colspan="3" align="center" style="padding:5px">公司簡介*<br>
      			<textarea name="introduction" maxlength="200" cols="55" rows="5" required id="textfield20"><%=introduction%></textarea>
          </td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#999999"><img src="../Images/csr_c/speace.gif" width="1" height="1" /></td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center"><span style="padding:5px">
      <input type="submit" name="button2" id="button2" value="送出" />
      <input type="hidden" name="flg" value="chg" />
      <input type="hidden" name="autoid" value="<%=autoid%>" />
      <input type="hidden" name="page" value="<%=page%>" />
      <input type="hidden" name="search" value="<%=search%>" />
    </span></td>
    </tr>
</table>
</h6>
</form>
</div>
</center>
</body>
</html>
<%
if session("message") <> "" then
	response.write "<script language='javascript'>"
	response.write "	setTimeout(function() {alert ('" & session("message") & "');},1250);"
	response.write "</script>"
	session("message") = ""
end if
%>
<script language="javascript">
<!--
document.write('<img src=../static.asp?area='+escape("加入ASE供應夥伴-修改(<%=autoid%>)")+'>');
-->
</script>