<!--#include file="admin_news_chk.asp"-->
<!--#include file="I_Chk_Null.asp"-->
<%
    autoid=request("autoid")
    search=request("search")
    page=request("page")
    if autoid="" then
        session("message") = "編號錯誤！"
        response.redirect "admin_partner.asp?search=" & search & "&page=" & page
    end if
    if Session("news")<>"2" then
        session("message") = "很抱歉，您無修改資料權限！"
        response.redirect "admin_partner.asp?search=" & search & "&page=" & page
    end if
    sqlst="select * from partner where autoid=" & autoid
    set rs=conn.execute(sqlst)
    if rs.eof then
        rs.close
        set rs=nothing
        conn.close
        set conn=nothing
        session("message") = "查無資料！"
        response.redirect "admin_partner.asp?page=" & page
    else
      autoid  = Chk_null(rs("autoid"))
      rid  = Chk_null(rs("id"))
            company_title       = Chk_null(rs("company_title"))
            company_phone   = Chk_null(rs("company_phone"))
            username        = Chk_null(rs("username"))
            job_title       = Chk_null(rs("job_title"))
            phone_ext   = Chk_null(rs("phone_ext"))
      if job_title <>"" or phone_ext <> "" then
        term=job_title&" 年又 "&phone_ext&" 月"
      else
        term=""
      end if
            cellphone   = Chk_null(rs("cellphone"))
            email   = Chk_null(rs("email"))
            company_fax = Chk_null(rs("company_fax"))
            company_add = Chk_null(rs("company_add"))
            'sex    = Chk_null(rs("sex"))
            'if sex="先生" then
            '   sex1="checked"
            '   sex2=""
            'else
            '   sex2="checked"
            '   sex1=""
            'end if
            atype   = Chk_null(rs("type"))
            if instr(atype,"Manufacturers") > 0 then type1="checked"
            if instr(atype,"Agents") > 0 then type2="checked"
            if instr(atype,"Agency") > 0 then type3="checked"

      business  = Chk_null(rs("business"))
      'response.write business
      'response.end
            unit    = Chk_null(rs("unit"))
            factory_number  = Chk_null(rs("factory_number"))
            factory_add = Chk_null(rs("factory_add"))
            brand   = Chk_null(rs("brand"))
            uninumber   = Chk_null(rs("uninumber"))
            money1  = Chk_null(rs("money1"))
            money2  = Chk_null(rs("money2"))
            customer    = Chk_null(rs("customer"))
            supplier    = Chk_null(rs("supplier"))
            equipment   = Chk_null(rs("equipment"))
            apply   = Chk_null(rs("apply"))
            function_way    = Chk_null(rs("function_way"))
            company_url = Chk_null(rs("company_url"))
      upt_time  = Chk_null(rs("upt_time"))
      upt_als   = Chk_null(rs("upt_als"))
      add_time  = Chk_null(rs("add_time"))

    file1   = Chk_null(rs("file1"))
      file2 = Chk_null(rs("file2"))
      file3 = Chk_null(rs("file3"))
      file4 = Chk_null(rs("file4"))
    aid=right("00000"&autoid,6)
    end if
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>日月光集團 – 商業行為與道德遵循舉報系統</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
<script src="js/respond.min.js"></script>
<![endif]-->
<script>
$(document).ready(function(){
    $('body').on('click', '.more', function(e){
        //var iuser_count = parseInt($("#iuser_count").val());
        var iuser_count = $(".iuser").length;
        var count = iuser_count + 1;
        $("#iuser_count").val(count);
        if(iuser_count==0)$(this).remove();
        var last=$(this).parent().parent().last().find('.iuser');
        //console.log(last);
        last.html('<i class="glyphicon glyphicon-minus"></i>');
        last.removeClass('more');
        last.addClass('x');
        last.removeClass('btn-info');
        last.addClass('btn-danger');
        $("#iuser_area").append('<div class="input-group">\n\
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>\n\
                                <input type="text" class="form-control" name="iuser_name" placeholder="姓名"/>\n\
                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>\n\
                                <input type="text" class="form-control" name="iuser_unit" placeholder="部門"/>\n\
                                <span class="input-group-addon"><i class="glyphicon glyphicon-pawn"></i></span>\n\
                                <input type="text" class="form-control" name="iuser_jobtitle" placeholder="職稱" />\n\
                                <span class="input-group-btn data-up">\n\
                                <button type="button" class="btn btn-default btn-info more iuser" id="more'+count+'" data-dir="up"><i class="glyphicon glyphicon-plus"></i></button></span></div>');

    });
    $('body').on('click', '.x', function(e){
        var iuser_count = parseInt($("#iuser_count").val())-1;
        //console.log(iuser_count);
        $("#iuser_count").val(iuser_count);
        //console.log($("#iuser_count").val());
        $(this).parent().parent().remove();
    });
    $('body').on('change', '.upload', function(e){
        //console.log($(this).val());
        $('#'+$(this).attr('name')).val($(this).val());
    });
    $('body').on('change', '.others', function(e){
        if($(this).val()=='其他' || $(this).val()=='Others'){
            $('#'+$(this).attr('name')+'_others').attr('disabled',false);
            $('#'+$(this).attr('name')+'_others').attr('required',true);
        }else{
            $('#'+$(this).attr('name')+'_others').attr('required',false);
            $('#'+$(this).attr('name')+'_others').attr('disabled',true);
            $('#'+$(this).attr('name')+'_others').val('');
        }
    });
    $('#rid').val('<%=rid%>');
    $('#company_fax').val('<%=company_fax%>');
    $('#company_url').val('<%=company_url%>');
    $('#job_title').val('<%=job_title%>');
    $('#phone_ext').val('<%=phone_ext%>');
    $('#cellphone').val('<%=cellphone%>');
    //$('input:radio[name="type"]').filter('[value="<%=atype%>"]').attr('checked', true);
    $('input:radio[name="type"]').filter('[value="<%=atype%>"]').click();
    $('#rid').trigger('change');
    $('#company_fax').trigger('change');
    $('#company_url').trigger('change');
    $('#cellphone').trigger('change');
});
</script>
<style>
.fileUpload {
    position: relative;
    overflow: hidden;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
</style>
</head>
<body>
<div class="header">
    <div class="container">
        <div class="col-md-5 logo">
            <a href="index.html"><img src="image/ase_logo.png" width="180" height="56"></a>
        </div>
        <div class="col-md-7 menubar">
            <ul class="nav nav-pills" role="tablist">
                <li> <a href="admin_partner.asp" title="舉報系統">舉報系統</a></li>
                <li> <a href="admin_user_pchg.asp" title="修改密碼">修改密碼</a></li>
                <li> <a href="admin_user.asp" title="權限異動">權限異動</a></li>
                <li> <a href="admin_data.asp" title="系統參數">信件通知管理</a></li>
                <li> <a href="admin_logout.asp" title="管理登出">管理登出</a></li>
            </ul>
        </div>
    </div>
</div>
    <!-- end:header-top -->
    <div class="container">
    <h3 style="padding:20px 0">案件編號：<%=aid%></h3>
    <h4 style="color:#CC0000">舉報時間：<%=add_time%>
      <div style="display:inline;float: right;"><a href="admin_partner_detail.asp?autoid=<%=autoid%>" class="btn btn-success">回詳細資料頁</a>&nbsp;&nbsp;&nbsp;
      <div style="display:inline;float: right;"><button onclick="location.href='admin_partner.asp';" class="btn btn-info">回列表頁</button>
    </h4>
        <div class="co">
        	<h5 style="padding:40px 0 10px 0">本系統為舉報日月光人員違反道德與誠信行為之系統，在這裡所提交的任何訊息，將全部予以保密並直接遞交至日月光管理高層。如您要反應的內容為緊急事件，敬請直接聯絡日月光各權責部門，謝謝。
舉報日月光人員違反道德與誠信之行為，請提供以下訊息：〈<span style="color:#CC0000">*：必須填寫</span>〉</h5>
<form method="post" action="ch_editm.asp" enctype="multipart/form-data">
<input type="hidden" name="autoid" id="autoid" value="<%=autoid%>">
<input type="hidden" name="flg" value="update">
                <div class="row">
                    <div class="col-md-12">
                        <div class="well well-sm">
                            <form>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">您與日月光的關係：<span style="color:#CC0000">*</span></label>
                                        <div class="input-group">
                                        <select name="id" id="rid" required class="form-control others">
                                            <option value="" selected>請選擇</option>
<%if brand="english" then%>
                                            <option value="Customers">Customers</option>
                                            <option value="Employees">Employees</option>
                                            <option value="Shareholders">Shareholders</option>
                                            <option value="Suppliers/Contractor">Suppliers/Contractor</option>
                                            <option value="Community">Community</option>
                                            <option value="Government">Government</option>
                                            <option value="Industry Unions/Associations">Industry Unions/Associations</option>
                                            <option value="NGOs">NGOs</option>
                                            <option value="Media">Media</option>
                                            <option value="Others">Others</option>
<%else%>
                                            <option value="客戶">客戶</option>
                                            <option value="員工">員工</option>
                                            <option value="股東">股東</option>
                                            <option value="供應商/承攬商">供應商/承攬商</option>
                                            <option value="社區">社區</option>
                                            <option value="政府">政府</option>
                                            <option value="產業工會/協會">產業工會/協會</option>
                                            <option value="非政府組織">非政府組織</option>
                                            <option value="媒體">媒體</option>
                                            <option value="其他">其他</option>
<%end if%>
                                        </select>
                                            <span class="input-group-addon"></span>
                                          <input type="text" class="form-control" name="id_others" id="id_others" placeholder="其他" value="<%=supplier%>" disabled />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="cols-sm-2 control-label">您的姓名與聯絡方式：〈您所提供的所有訊息均受保密〉<span style="color:#CC0000">*</span></label>
                                        <div class="cols-sm-6">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                                <input type="text" class="form-control" name="username" required placeholder="您的姓名" value="<%=username%>"/>
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
                                                <input type="text" class="form-control" name="company_title" required id="company_title" placeholder="公司名稱" value="<%=company_title%>"/>
												<span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                                <input type="text" class="form-control" name="company_phone" required placeholder="電話號碼" value="<%=company_phone%>"/>
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                                <input type="email" class="form-control" name="email" required placeholder="電子郵件" value="<%=email%>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">請選擇涉及本案的日月光廠區：<span style="color:#CC0000">*</span></label>
                                        <div class="input-group">
                                            <select name="company_fax" id="company_fax" required class="form-control others">
                                                <option value="" selected>請選擇</option>
                                                <option value="ASE Kaohsiung">日月光高雄廠</option>
                                                <option value="ASE Chungli">日月光中壢廠</option>
                                                <option value="ASE Shanghai - Assembly & Test">日月光上海封裝廠</option>
                                                <option value="ASE Shanghai - Material">日月光上海材料廠</option>
                                                <option value="ASE Kunshan">日月光昆山廠</option>
                                                <option value="ASE Suzhou">日月光蘇州廠</option>
                                                <option value="ASE Weihai">日月光威海廠</option>
                                                <option value="ASE Wuxi">日月光無錫廠</option>
                                                <option value="ASE Korea">日月光韓國廠</option>
                                                <option value="ASE Japan">日月光日本廠</option>
                                                <option value="ASE Malaysia">日月光馬來西亞廠</option>
                                                <option value="ASE Singapore">日月光新加坡廠</option>
                                                <option value="ISE Labs">ISE Labs</option>
                                                <option value="USI">USI</option>
                                                <option value="Others">其他</option>
                                            </select>
                                            <span class="input-group-addon"></span>
                                          <input type="text" class="form-control" name="company_fax_others" id="company_fax_others" value="<%=equipment%>" required placeholder="其他" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="control-label">請列出本案所涉及之日月光人員：<span style="color:#CC0000">*</span></label>
                                        <div class="cols-sm-6" id="iuser_area">
<%
  sqlst="select * from iuser where autoid_fk=" & autoid&" order by iuser_sn"
    set rs = Server.CreateObject("ADODB.Recordset")
    rs.open sqlst,conn,1,4
  if not rs.eof then
    i=0
    Do while not rs.eof
        if i=rs.recordcount-1 then
            cssclass1= "more iuser"
            cssclass2= "plus"
            btn_color= "info "
        else
            cssclass1= "x iuser"
            cssclass2= "minus"
            btn_color= "danger "
        end if
%>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input type="text" class="form-control" required name="iuser_name" placeholder="姓名" value="<%=rs("iuser_name")%>"/>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                <input type="text" class="form-control" required name="iuser_unit" placeholder="部門" value="<%=rs("iuser_unit")%>"/>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-pawn"></i></span>
                                <input type="text" class="form-control" required name="iuser_jobtitle" placeholder="職稱" value="<%=rs("iuser_jobtitle")%>"/>
                                <span class="input-group-btn data-up">
                                <%if i>0 then%>
                                <button class="btn btn-default btn-<%=btn_color%> <%=cssclass1%>" id="more<%=i%>" data-dir="up"><i class="glyphicon glyphicon-<%=cssclass2%>"></i></button>
                                <%end if%></span>
                            </div>
<%  rs.movenext
    i=i+1
    Loop
  end if
  rs.close
  set rs=nothing
  conn.close
  set conn=nothing
%>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label for="name" class="control-label">您舉報的議題：<span style="color:#CC0000">*</span></label>
                                        <div class="input-group">
                                    <select name="company_url" id="company_url" required class="form-control others">
                                        <option value="" selected>請選擇</option>
<%if brand="english" then%>
                                        <option value="Corruption">Corruption</option>
                                        <option value="Conflicts of Interest">Conflicts of Interest</option>
                                        <option value="Insider Trading">Insider Trading</option>
                                        <option value="Fair Competition and Antitrust">Fair Competition and Antitrust</option>
                                        <option value="Secret Divulgence">Secret Divulgence</option>
                                        <option value="Others">Others</option>
<%else%>
                                        <option value="舞弊">舞弊</option>
                                        <option value="利益衝突">利益衝突</option>
                                        <option value="內線交易">內線交易</option>
                                        <option value="公平競爭">公平競爭</option>
                                        <option value="洩漏商業機密">洩漏商業機密</option>
                                        <option value="其他">其他</option>
<%end if%>
                                    </select>
                                            <span class="input-group-addon"></span>
                                          <input type="text" class="form-control" name="company_url_others" id="company_url_others" value="<%=apply%>" required placeholder="其他" disabled>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="name" class="control-label">請說明本案發生的地點？</label>
                                      <input type="text" class="form-control" name="company_add" value="<%=company_add%>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">請說明本案發生的具體或大致時間？</label>
                                     	<input type="text" class="form-control" name="unit" value="<%=unit%>" placeholder="例如：2012年3月28日 星期一；約一個月前等⋯">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">就您所知，您認為這個問題已持續多久？</label>
                                        <div class="input-group">
                                            <select name="job_title" id="job_title" class="form-control">
                                                <option value="" selected>請選擇</option>
                                                <option value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                            </select>
                                            <span class="input-group-addon">年</span>
                                            <select name="phone_ext" id="phone_ext" class="form-control">
                                                <option value="" selected>請選擇</option>
                                                <option value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                            </select>
                                            <span class="input-group-addon">月</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">您是如何得知此違規行為？<span style="color:#CC0000">*</span></label>
                                        <div class="input-group">
                                        <select name="cellphone" id="cellphone" required class="form-control others">
                                            <option value="" selected>請選擇</option>
<%if brand="english" then%>
                                            <option value="Personal Experience">Personal experience</option>
                                            <option value="Experiences sharing from colleagues, relatives, and friends">Experiences sharing from colleagues, relatives, and friends</option>
                                            <option value="Hearsay">Hearsay</option>
                                            <option value="Inference">Inference</option>
                                            <option value="Internet">Internet</option>
                                            <option value="Others">Others</option>
<%else%>
                                            <option value="親身經歷">親身經歷</option>
                                            <option value="同事及親友的經歷">同事及親友的經歷</option>
                                            <option value="傳聞">傳聞</option>
                                            <option value="推測">推測</option>
                                            <option value="網路">網路</option>
                                            <option value="其他">其他</option>
<%end if%>
                                        </select>
                                            <span class="input-group-addon"></span>
                                          <input type="text" class="form-control" name="cellphone_others" id="cellphone_others" value="<%=function_way%>" required placeholder="其他" disabled>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="control-label">就您所知，日月光管理階層是否知道此違規行為？</label>
                                        <div class="cols-sm-6">
                                            <div class="btn-group" data-toggle="buttons">
<%if brand="english" then%>
                                                <label class="btn btn-default">
                                                    <input type="radio" name="type" value="yes">Yes
                                                </label>
                                                <label class="btn btn-default">
                                                    <input type="radio" name="type" value="no">No
                                                </label>
                                                <label class="btn btn-default">
                                                    <input type="radio" name="type" value="Do Not Know">Do Not Know
                                                </label>
<%else%>
                                                <label class="btn btn-default">
                                                    <input type="radio" name="type" value="是">是
                                                </label>
                                                <label class="btn btn-default">
                                                    <input type="radio" name="type" value="否">否
                                                </label>
                                                <label class="btn btn-default">
                                                    <input type="radio" name="type" value="不確定">不確定
                                                </label>
<%end if%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="name" class="control-label">請詳細描述本案違反道德與誠信行為之內容或事件經過：<span style="color:#CC0000">*</span></label>
                                        <div class="cols-sm-6">
                                          <textarea name="business" required rows="7" class="form-control"><%=business%></textarea>
                                      </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="control-label">就您所知，該人員是否有隱瞞其違反道德與誠信行為的方法，其方法為何？</label>
                                        <div class="cols-sm-6">
                                          <textarea name="factory_number" rows="7" class="form-control"><%=factory_number%></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">請提供關於本案的所有細節或任何有助調查的相關資訊：</label>
                                        <textarea name="factory_add" rows="7" class="form-control"><%=factory_add%></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="control-label">若您有與本案相關或任何有助調查的文件或檔案，亦請上傳：</label>
                                        <div class="cols-sm-6">
                                            <div class="input-group" style="padding-bottom:5px">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
                                                <input type="text" class="form-control" id="file1" value="<%=file1%>">
                                                <span class="input-group-btn data-up">
                                                <div class="fileUpload btn btn-default btn-info" data-dir="up"><input type="file" class="upload" name="file1">選擇檔案</div></span>
                                            </div>
                                            <div class="input-group" style="padding-bottom:5px">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
                                                <input type="text" class="form-control" id="file2" value="<%=file2%>">
                                                <span class="input-group-btn data-up">
                                                <div class="fileUpload btn btn-default btn-info" data-dir="up"><input type="file" class="upload" name="file2">選擇檔案</div></span>
                                            </div>
                                            <div class="input-group" style="padding-bottom:5px">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
                                                <input type="text" class="form-control" id="file3" value="<%=file3%>">
                                                <span class="input-group-btn data-up">
                                                <div class="fileUpload btn btn-default btn-info" data-dir="up"><input type="file" class="upload" name="file3">選擇檔案</div></span>
                                           </div>
                                            <div class="input-group" style="padding-bottom:5px">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
                                                <input type="text" class="form-control" id="file4" value="<%=file4%>">
                                                <span class="input-group-btn data-up">
                                                <div class="fileUpload btn btn-default btn-info" data-dir="up"><input type="file" class="upload" name="file4">選擇檔案</div></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                		<label for="name" class="control-label">請輸入圖片中的驗證碼：<span style="color:#CC0000">*</span></label>
                                        <input type="text" name="validcode" style="width:120px;display:inline;" maxlength="4" class="form-control" placeholder="四位數字" />
 <a href="javascript:void(0)" title="點我可更新驗證碼" onclick="RefreshImage('imgCaptcha')"><img id="imgCaptcha" src="../vcode.asp" /></a>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary pull-left" id="btnContactUs">提交</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
<footer>
    <div id="footer">
      <div class="container">
            <div class="row">
              <div class="col-md-6 col-md-offset-3 text-center">
                <h6 style="color:#aaaaaa">© 2017 ASE Group. All rights reserved.</h6>
              </div>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
<script language="javascript">
<!--
    function RefreshImage(valImageId) {
        var objImage = document.images[valImageId];
        if (objImage == undefined) {
            return;
        }
        var now = new Date();
        objImage.src = objImage.src.split('?')[0] + '?x=' + now.toUTCString();
    }
    document.write('<img src=../static.asp?area='+escape("修改舉報資料")+'&autoid=<%=autoid%>>');
-->
</script>
<%
if session("message") <> "" then
    response.write "<script language='javascript'>"
    response.write "    setTimeout(function() {alert ('" & session("message") & "');},1250);"
    response.write "RefreshImage('imgCaptcha');"
    response.write "</script>"
    session("message") = ""
end if
%>
