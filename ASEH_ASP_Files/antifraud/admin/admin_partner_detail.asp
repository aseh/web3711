<!--#include file="admin_news_chk.asp"-->
<!--#include file="I_Chk_Null.asp"-->
<%
	autoid=request("autoid")
	search=request("search")
	page=request("page")
	if autoid="" then
		session("message") = "編號錯誤!"
		response.redirect "admin_partner.asp?chk_kind_recno=" & chk_kind_recno & "&search=" & search & "&page=" & page
	end if

	sqlst="select * from partner where autoid=" & autoid
	set rs=conn.execute(sqlst)
	if rs.eof then
		rs.close
		set rs=nothing
		conn.close
		set conn=nothing
		response.redirect "admin_partner.asp?page=" & page
	else
      autoid  = Chk_null(rs("autoid"))
      rid  = Chk_null(rs("id"))
			company_title		= Chk_null(rs("company_title"))
			company_phone  	= Chk_null(rs("company_phone"))
			username		= Chk_null(rs("username"))
			job_title		= Chk_null(rs("job_title"))
			phone_ext	= Chk_null(rs("phone_ext"))
      if job_title <>"" or phone_ext <> "" then
        term=job_title&" 年又 "&phone_ext&" 月"
      else
        term=""
      end if
			cellphone	= Chk_null(rs("cellphone"))
			email	= Chk_null(rs("email"))
			company_fax	= Chk_null(rs("company_fax"))
			company_fax	= Chk_null(rs("company_fax"))
			company_add	= Chk_null(rs("company_add"))
			'sex	= Chk_null(rs("sex"))
			'if sex="先生" then
			'	sex1="checked"
			'	sex2=""
			'else
			'	sex2="checked"
			'	sex1=""
			'end if
			atype	= Chk_null(rs("type"))
			if instr(atype,"Manufacturers") > 0 then type1="checked"
			if instr(atype,"Agents") > 0 then type2="checked"
			if instr(atype,"Agency") > 0 then type3="checked"

      business  = Chk_null(rs("business"))
      'response.write business
      'response.end
			unit	= Chk_null(rs("unit"))
			factory_number	= Chk_null(rs("factory_number"))
			factory_add	= Chk_null(rs("factory_add"))
			brand	= Chk_null(rs("brand"))
			uninumber	= Chk_null(rs("uninumber"))
			money1	= Chk_null(rs("money1"))
			money2	= Chk_null(rs("money2"))
			customer	= Chk_null(rs("customer"))
			supplier	= Chk_null(rs("supplier"))
			equipment	= Chk_null(rs("equipment"))
			apply	= Chk_null(rs("apply"))
			function_way	= Chk_null(rs("function_way"))
			company_url	= Chk_null(rs("company_url"))
      upt_time  = Chk_null(rs("upt_time"))
      upt_als   = Chk_null(rs("upt_als"))
      add_time  = Chk_null(rs("add_time"))

			file1	= Chk_null(rs("file1"))
      file2 = Chk_null(rs("file2"))
      file3 = Chk_null(rs("file3"))
      file4 = Chk_null(rs("file4"))

	end if
  aid=right("00000"&autoid,6)
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>商業行為與道德遵循舉報系統</title>
<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/28e7751dbec/integration/bootstrap/3/dataTables.bootstrap.css">
<link rel="stylesheet" href="css/style.css">

<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10-dev/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/28e7751dbec/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>
<body>
<div class="header">
    <div class="container">
        <div class="col-md-5 logo">
            <a href="admin_partner.asp"><img src="image/ase_logo.png" width="180" height="56"></a>
        </div>
        <div class="col-md-7 menubar">
            <ul class="nav nav-pills" role="tablist">
                <li> <a href="admin_partner.asp" title="舉報系統">舉報系統</a></li>
                <li> <a href="admin_user_pchg.asp" title="修改密碼">修改密碼</a></li>
                <li> <a href="admin_user.asp" title="權限異動">權限異動</a></li>
                <li> <a href="admin_logout.asp" title="管理登出">管理登出</a></li>
            </ul>
        </div>
    </div>
</div>

    <!-- end:header-top -->
    <div class="container">
    <h3 style="padding:20px 0">案件編號：<%=aid%></h3>
    <h4 style="color:#CC0000">舉報時間：<%=add_time%>
    <%if Session("news")="2" then%>
      <div style="display:inline;float: right;">
    <%end if%>
      <div style="display:inline;float: right;"><button onclick="location.href='admin_partner.asp';" class="btn btn-info">回列表頁</button>
    </h4>
    <table width="100%" class="table table-hover">
      <thead>
        <tr>
          <th>與日月光的關係</th>
          <th>舉報人聯絡資料</th>
          <th>涉案廠區</th>
          <th>涉案人員</th>
          <th>舉報議題</th>
          <th>案發地點</th>
          <th>案發時間</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><%=rid%><br><%=supplier%></td>
          <td><%=username%><br>
          <%=company_title%><br>
          <%=company_phone%><br>
          <%=email%>
          </td>
          <td><%=company_fax%><br><%=equipment%></td>
          <td>
<%
  sqlst="select * from iuser where autoid_fk=" & autoid&" order by iuser_sn"
  set rs=conn.execute(sqlst)
  if not rs.eof then
    Do while not rs.eof
      response.write rs("iuser_name")&"<br>"
      response.write rs("iuser_manufacturingsites")&"<br>"
      response.write rs("iuser_unit")&"<br>"
      response.write rs("iuser_jobtitle")&"<br>"
      rs.movenext
    Loop
  end if
  rs.close
  set rs=nothing
  conn.close
  set conn=nothing
%>
          </td>
          <td><%=company_url%><br><%=apply%></td>
          <td><%=company_add%></td>
          <td><%=unit%></td>
        </tr>
      </tbody>
    </table>
    <table width="80%" align="center" class="table table-hover">
      <thead>
      <thead>
        <tr>
          <th width="15%">案發經過</th>
          <th>違規行為</th>
          <th>管理階層是否知情</th>
          <th>問題持續時間</th>
          <th width="15%">違反行為的方法</th>
          <th width="18%">案發細節與證據</th>
          <th>相關文件或檔案</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><%=business%></td>
          <td><%=cellphone%><br><%=function_way%></td>
          <td><%=atype%></td>
          <td><%=term%></td>
          <td><%=factory_number%></td>
          <td><%=factory_add%></td>
          <td>
          <%if file1 <> "" then %>
          <a target="_blank" href="<%=Session("UPLOAD_URL")&file1%>"><%=file1%></a><br>
          <%end if%>
          <%if file2 <> "" then %>
          <a target="_blank" href="<%=Session("UPLOAD_URL")&file2%>"><%=file2%></a><br>
          <%end if%>
          <%if file3 <> "" then %>
          <a target="_blank" href="<%=Session("UPLOAD_URL")&file3%>"><%=file3%></a><br>
          <%end if%>
          <%if file4 <> "" then %>
          <a target="_blank" href="<%=Session("UPLOAD_URL")&file4%>"><%=file4%></a><br>
          <%end if%>
          </td>
        </tr>
      </tbody>
    </table>
    <p>&nbsp;</p>
    </div>

    <footer>
        <div id="footer">
          <div class="container">
                <div class="row">
                  <div class="col-md-6 col-md-offset-3 text-center">
                    <p>© 2017 ASE Group. All rights reserved.</p>
                  </div>
                </div>
            </div>
        </div>
    </footer>



</div>
</div>
</body>
</html>
<%
if session("message") <> "" then
	response.write "<script language='javascript'>"
	response.write "	setTimeout(function() {alert ('" & session("message") & "');},1250);"
	response.write "</script>"
	session("message") = ""
end if
%>
<script language="javascript">
<!--
document.write('<img src=../static.asp?area='+escape("詳細資料(<%=aid%>)")+'&autoid=<%=autoid%>>');
-->
</script>