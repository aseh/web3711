﻿<!--#include file="admin_user_chk.asp"-->
<!--#include file="admin_chk.asp"-->
<!--#include file="I_Chk_Null.asp"-->
<%
	search= request("search")
	if len(search) > 0 then
		csql	= "select * from admin" 	&_
					" where admin_recno like '%" & search & "%'" &_
					" or upt_als like '%" & search & "%'" &_
					" or upt_time like '%" & search & "%'" &_
					" or admin like '%" & search & "%'" &_
					" or news like '%" & search & "%'" &_
					" or member like '%" & search & "%'" &_
					" or product like '%" & search & "%'" &_
					" or orderform like '%" & search & "%'" &_
					" or auction like '%" & search & "%'" &_
					" or service like '%" & search & "%'" &_
					" or discuss like '%" & search & "%'" &_
					" or upload like '%" & search & "%'" &_
					" or floor like '%" & search & "%'" &_					
					" or billboard like '%" & search & "%'" &_
					" or people like '%" & search & "%'" &_
					" or static like '%" & search & "%'" &_
					" order by admin_recno"
	else
		csql	="select * from admin order by admin_recno"
	end if

	'response.write csql
	'response.end
	set rs = server.createobject("ADODB.Recordset")
	rs.open csql,conn,1,4
	page="1"
	if request("page")<>"" then
		page=request("page")
	end if
	if not isnumeric(page) then
		page="1"
	end if
	rs.PageSize = 10
    	if cint(page) > rs.pagecount then
       		page = "1"
    	end if
	Set fs = CreateObject("Scripting.FileSystemObject")
	if fs.FileExists(Session("ADMIN_HTML_PATH") & "admin_user.htm") then
		Set input = fs.OpenTextFile(Session("ADMIN_HTML_PATH") & "admin_user.htm",1,False,True)
		do while not input.AtEndOfStream
			txt = input.Readline
			if inStr(txt,"<!--user_lista-->")>0 then
				if not rs.eof then
					txt=replace(txt,"<!--user_lista-->","")
					if Session("ADMIN") = "2" then
						txt = right(txt,len(txt)-instr(txt,"<!--access-->")-13+1)
					else
						txt = left(txt,instr(txt,"<!--access-->")-1)
					end if
					rs.absolutepage = page
				      	i=1
	    				do while i<=rs.pagesize and not rs.eof
						upt_time 	= Chk_Null(rs("upt_time"))
						upt_als 	= Chk_Null(rs("upt_als"))
						admin_recno 	= Chk_Null(rs("admin_recno"))
						password	= Chk_Null(rs("password"))
						admin 		= Chk_Null(rs("admin"))
						news 		= Chk_Null(rs("news"))
						member		= Chk_Null(rs("member"))
						product		= Chk_Null(rs("product"))
						orderform	= Chk_Null(rs("orderform"))
						auction		= Chk_Null(rs("auction"))
						service		= Chk_Null(rs("service"))
						discuss		= Chk_Null(rs("discuss"))
						faq		= Chk_Null(rs("faq"))
						upload		= Chk_Null(rs("upload"))
						card		= Chk_Null(rs("card"))
						floor		= Chk_Null(rs("floor"))						
						billboard	= Chk_Null(rs("billboard"))
						cstatic		= Chk_Null(rs("static"))
						people		= Chk_Null(rs("people"))
						txt1 = txt
						txt1 = replace(txt1,"$upt_time",upt_time)
						txt1 = replace(txt1,"$upt_als",upt_als)
						txt1 = replace(txt1,"$admin_recno",admin_recno)					
						txt1 = replace(txt1,"$password",password)
						txt1 = replace(txt1,"$admin",showadmin(admin))
						txt1 = replace(txt1,"$news",showadmin(news))
						txt1 = replace(txt1,"$member",member)
						txt1 = replace(txt1,"$product",product)
						txt1 = replace(txt1,"$orderform",orderform)
						txt1 = replace(txt1,"$auction",auction)
						txt1 = replace(txt1,"$service",showadmin(service))
						txt1 = replace(txt1,"$discuss",discuss)
						txt1 = replace(txt1,"$faq",faq)
						txt1 = replace(txt1,"$card",card)
						txt1 = replace(txt1,"$upload",upload)
						txt1 = replace(txt1,"$floor",floor)						
						txt1 = replace(txt1,"$billboard",showadmin(billboard))
						txt1 = replace(txt1,"$static",showadmin(cstatic))						
						txt1 = replace(txt1,"$people",people)						
						txt1 = replace(txt1,"$page",page)
						txt1 = replace(txt1,"$search",search)
						response.write txt1 & chr(10)	
						i=i+1
	    					rs.movenext
	    				loop
	    			end if
	    		elseif instr(txt,"<!--access-->") > 0 then
				if Session("ADMIN") = "2" then
					txt = replace(txt,"<!--access-->","")
					response.write txt
				end if	    			
	    		elseif instr(txt,"<!--page-->")>0 then '頁碼
				response.write "<p><span class=font>頁次:<font size=3>"
				if cint(page)>10 then
					response.write " <a href=admin_user.asp?search=" & server.urlencode(search) & "&page=" & cstr(int((cint(page)-1)/10)*10) & ">" & cstr(int((cint(page)-1)/10)*10) & "頁之前</a>"
				end if   
				i = int((cint(page)-1)/10)*10+1
				do while i <= rs.pagecount and i <= (int((cint(page)-1)/10)+1)*10
					if cstr(i)<>page then
						response.write "  <a href=admin_user.asp?search=" & server.urlencode(search) & "&page=" & cstr(i) & ">" & cstr(i) & "</a>"
					else
						response.write " <b>" & cstr(i) & "</b>"
					end if
					i=i+1
				loop
				if i <= rs.pagecount then
					response.write " <a href=admin_user.asp?search=" & server.urlencode(search) & "&page=" & cstr(i) & ">" & cstr(i) & "頁之後</a>"
				end if 	
				response.write "</font></span>"
			elseif instr(txt,"<!--message-->")>0 then
				if session("message") <> "" then				
					response.write "<script language='javascript'>"
					response.write "	alert ('" & session("message") & "')"
					response.write "</script>"
					session("message") = ""
				end if				
			elseif instr(txt,"<!--top-->")>0 then
				if fs.FileExists(Session("ADMIN_HTML_PATH") & "top.htm") then		
					Set input1 = fs.OpenTextFile(Session("ADMIN_HTML_PATH") & "top.htm",1,False,True)
					do while not input1.AtEndOfStream
						txt1 = input1.Readline			
						txt1=replace(txt1,"$home_url",session("HOME_URL"))	
						txt1=replace(txt1,"$member_url",session("MEMBER_URL"))	
						response.write txt1 & chr(10)
					loop	
				end if		
			elseif instr(txt,"<!--copyright-->")>0 then
				if fs.FileExists(Session("HTML_PATH") & "copyright.htm") then		
					Set input1 = fs.OpenTextFile(Session("HTML_PATH") & "copyright.htm",1,False,True)
					do while not input1.AtEndOfStream
						txt1 = input1.Readline			
						txt1=replace(txt1,"$home_url",session("HOME_URL"))	
						txt1=replace(txt1,"$member_url",session("MEMBER_URL"))	
						response.write txt1 & chr(10)
					loop	
				end if		
			else	
				txt = replace(txt,"$page",page)
				txt = replace(txt,"$search",search)
				response.write txt & chr(10)
			end if
		loop
	else
		response.write "畫面檔不存在!"
	end if
	rs.close
	conn.close
	function showadmin(atype)
		if atype <> "" then
			select case atype
				case 1
					showadmin="讀"
				case 2
					showadmin="寫"
				case 0
					showadmin="無"
				case else
					showadmin="無"
			end select			
		end if
	end function
%>