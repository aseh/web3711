﻿<!--#include file="admin_user_chk.asp"-->
<!--#include file="I_Chk_Null.asp"-->
<%
	page = request("page")
	search = request("search")
	if request("admin_recno")="" then
		session("message") = "未指定代號"
		response.redirect "admin_user.asp?page=" & page & "&search=" & search
	end if
	admin_recno = request("admin_recno")
			      		
	sqlst="select * from admin where admin_recno='" & admin_recno & "'"      		
	set rs=conn.execute(sqlst)
	if rs.eof then
		rs.close
		set rs=nothing
		conn.close
		set conn=nothing
		session("message") = "無此代號"
		response.redirect "admin_user.asp?page=" & page & "&search=" & search
	else
		upt_time 	= Chk_Null(rs("upt_time"))
		upt_als 	= Chk_Null(rs("upt_als"))
		admin_recno 	= Chk_Null(rs("admin_recno"))
		admin_name	= Chk_Null(rs("name"))
		password	= Chk_Null(rs("password"))
		Set DesCrypt = New Cls_DES
		password = DesCrypt.DES(password,deskey,1)
		Set DesCrypt = Nothing
		admin 		= Chk_Null(rs("admin"))
		news 		= Chk_Null(rs("news"))
		member		= Chk_Null(rs("member"))
		product		= Chk_Null(rs("product"))
		orderform	= Chk_Null(rs("orderform"))
		auction		= Chk_Null(rs("auction"))
		service		= Chk_Null(rs("service"))
		discuss		= Chk_Null(rs("discuss"))
		faq		= Chk_Null(rs("faq"))
		upload		= Chk_Null(rs("upload"))
		floor		= Chk_Null(rs("floor"))
		billboard	= Chk_Null(rs("billboard"))
		cstatic		= Chk_Null(rs("static"))
		card		= Chk_Null(rs("card"))
		people		= Chk_Null(rs("people"))
		area		= Chk_Null(rs("area"))
		admin_email		= Chk_Null(rs("admin_email"))
	end if
	rs.close
	set rs=nothing
	Set fs = CreateObject("Scripting.FileSystemObject")
	if fs.FileExists(Session("ADMIN_HTML_PATH") & "admin_user_chg.htm") then
		Set input = fs.OpenTextFile(Session("ADMIN_HTML_PATH") & "admin_user_chg.htm",1,False,True)
		do while not input.AtEndOfStream
			txt = input.Readline
			if inStr(txt,"<!--country_list-->")>0 then
				cSql="select countryname from country where countryname <> '' order by countryname"
				set rs1=conn.execute(cSql)
				do while not rs1.eof
					response.write "<option "
					if rs1("countryname") = area then
						response.write "selected "
					end if
					response.write " value=" & rs1("countryname") & ">" & trim(rs1("countryname")) & "</option>"
					rs1.movenext
				loop			
			elseif instr(txt,"<!--message-->")>0 then
				if session("message") <> "" then				
					response.write "<script language='javascript'>"
					response.write "	alert ('" & session("message") & "')"
					response.write "</script>"
					session("message") = ""
				end if				
			elseif instr(txt,"<!--top-->")>0 then
				if fs.FileExists(Session("ADMIN_HTML_PATH") & "top.htm") then		
					Set input1 = fs.OpenTextFile(Session("ADMIN_HTML_PATH") & "top.htm",1,False,True)
					do while not input1.AtEndOfStream
						txt1 = input1.Readline			
						txt1=replace(txt1,"$home_url",session("HOME_URL"))	
						txt1=replace(txt1,"$member_url",session("MEMBER_URL"))	
						response.write txt1 & chr(10)
					loop	
				end if		
			elseif instr(txt,"<!--copyright-->")>0 then
				if fs.FileExists(Session("HTML_PATH") & "copyright.htm") then		
					Set input1 = fs.OpenTextFile(Session("HTML_PATH") & "copyright.htm",1,False,True)
					do while not input1.AtEndOfStream
						txt1 = input1.Readline			
						txt1=replace(txt1,"$home_url",session("HOME_URL"))	
						txt1=replace(txt1,"$member_url",session("MEMBER_URL"))	
						response.write txt1 & chr(10)
					loop	
				end if		
			else	
				txt = replace(txt,"$page",page)
				txt = replace(txt,"$search",search)
				txt = replace(txt,"$admin_name",admin_name)
				txt = replace(txt,"$password",password)
				txt = replace(txt,"$admin_email",admin_email)
				txt = replace(txt,"$admin_recno",admin_recno)
				txt = replace(txt,"$upt_time",upt_time)				
				txt = replace(txt,"$upt_als",upt_als)
				if admin = "1" then
					txt = replace(txt,"$admin_rcheck","checked")
					txt = replace(txt,"$admin_wcheck","")
					txt = replace(txt,"$admin_nocheck","")
				elseif admin="2" then
					txt = replace(txt,"$admin_rcheck","")
					txt = replace(txt,"$admin_wcheck","checked")
					txt = replace(txt,"$admin_nocheck","")
				else
					txt = replace(txt,"$admin_rcheck","")
					txt = replace(txt,"$admin_wcheck","")
					txt = replace(txt,"$admin_nocheck","checked")
				end if
				if news = "1" then
					txt = replace(txt,"$news_rcheck","checked")
					txt = replace(txt,"$news_wcheck","")
					txt = replace(txt,"$news_nocheck","")
				elseif news="2" then
					txt = replace(txt,"$news_rcheck","")
					txt = replace(txt,"$news_wcheck","checked")
					txt = replace(txt,"$news_nocheck","")
				else
					txt = replace(txt,"$news_rcheck","")
					txt = replace(txt,"$news_wcheck","")
					txt = replace(txt,"$news_nocheck","checked")
				end if
				if member = "1" then
					txt = replace(txt,"$member_rcheck","checked")
					txt = replace(txt,"$member_wcheck","")
					txt = replace(txt,"$member_nocheck","")
					txt = replace(txt,"$member_oocheck","")
				elseif member="2" then
					txt = replace(txt,"$member_rcheck","")
					txt = replace(txt,"$member_wcheck","checked")
					txt = replace(txt,"$member_nocheck","")
					txt = replace(txt,"$member_oocheck","")
				elseif member="3" then
					txt = replace(txt,"$member_rcheck","")
					txt = replace(txt,"$member_wcheck","")
					txt = replace(txt,"$member_nocheck","")
					txt = replace(txt,"$member_oocheck","checked")
				else
					txt = replace(txt,"$member_rcheck","")
					txt = replace(txt,"$member_wcheck","")
					txt = replace(txt,"$member_nocheck","checked")
					txt = replace(txt,"$member_oocheck","")
				end if
				if product = "1" then
					txt = replace(txt,"$product_rcheck","checked")
					txt = replace(txt,"$product_wcheck","")
					txt = replace(txt,"$product_nocheck","")
				elseif product="2" then
					txt = replace(txt,"$product_rcheck","")
					txt = replace(txt,"$product_wcheck","checked")
					txt = replace(txt,"$product_nocheck","")
				else
					txt = replace(txt,"$product_rcheck","")
					txt = replace(txt,"$product_wcheck","")
					txt = replace(txt,"$product_nocheck","checked")
				end if
				if orderform = "1" then
					txt = replace(txt,"$orderform_rcheck","checked")
					txt = replace(txt,"$orderform_wcheck","")
					txt = replace(txt,"$orderform_nocheck","")
				elseif orderform="2" then
					txt = replace(txt,"$orderform_rcheck","")
					txt = replace(txt,"$orderform_wcheck","checked")
					txt = replace(txt,"$orderform_nocheck","")
				else
					txt = replace(txt,"$orderform_rcheck","")
					txt = replace(txt,"$orderform_wcheck","")
					txt = replace(txt,"$orderform_nocheck","checked")
				end if
				if auction = "1" then
					txt = replace(txt,"$auction_rcheck","checked")
					txt = replace(txt,"$auction_wcheck","")
					txt = replace(txt,"$auction_nocheck","")
				elseif auction="2" then
					txt = replace(txt,"$auction_rcheck","")
					txt = replace(txt,"$auction_wcheck","checked")
					txt = replace(txt,"$auction_nocheck","")
				else
					txt = replace(txt,"$auction_rcheck","")
					txt = replace(txt,"$auction_wcheck","")
					txt = replace(txt,"$auction_nocheck","checked")
				end if
				if service = "1" then
					txt = replace(txt,"$service_rcheck","checked")
					txt = replace(txt,"$service_wcheck","")
					txt = replace(txt,"$service_nocheck","")
				elseif service="2" then
					txt = replace(txt,"$service_rcheck","")
					txt = replace(txt,"$service_wcheck","checked")
					txt = replace(txt,"$service_nocheck","")
				else
					txt = replace(txt,"$service_rcheck","")
					txt = replace(txt,"$service_wcheck","")
					txt = replace(txt,"$service_nocheck","checked")
				end if
				if upload = "1" then
					txt = replace(txt,"$upload_rcheck","checked")
					txt = replace(txt,"$upload_wcheck","")
					txt = replace(txt,"$upload_nocheck","")
					txt = replace(txt,"$upload_oocheck","")
					txt = replace(txt,"$upload_owocheck","")
				elseif upload="2" then
					txt = replace(txt,"$upload_rcheck","")
					txt = replace(txt,"$upload_wcheck","checked")
					txt = replace(txt,"$upload_nocheck","")
					txt = replace(txt,"$upload_oocheck","")
					txt = replace(txt,"$upload_owocheck","")
				
				elseif upload="3" then
					txt = replace(txt,"$upload_rcheck","")
					txt = replace(txt,"$upload_wcheck","")
					txt = replace(txt,"$upload_nocheck","")
					txt = replace(txt,"$upload_oocheck","checked")
					txt = replace(txt,"$upload_owocheck","")
				
				elseif upload="4" then
					txt = replace(txt,"$upload_rcheck","")
					txt = replace(txt,"$upload_wcheck","")
					txt = replace(txt,"$upload_nocheck","")
					txt = replace(txt,"$upload_oocheck","")
					txt = replace(txt,"$upload_owocheck","checked")
				
				else
					txt = replace(txt,"$upload_rcheck","")
					txt = replace(txt,"$upload_wcheck","")
					txt = replace(txt,"$upload_nocheck","checked")
					txt = replace(txt,"$upload_oocheck","")
					txt = replace(txt,"$upload_owocheck","")
				end if
				
				if floor = "1" then
					txt = replace(txt,"$floor_rcheck","checked")
					txt = replace(txt,"$floor_wcheck","")
					txt = replace(txt,"$floor_nocheck","")
				elseif floor="2" then
					txt = replace(txt,"$floor_rcheck","")
					txt = replace(txt,"$floor_wcheck","checked")
					txt = replace(txt,"$floor_nocheck","")
				else
					txt = replace(txt,"$floor_rcheck","")
					txt = replace(txt,"$floor_wcheck","")
					txt = replace(txt,"$floor_nocheck","checked")
				end if
				if billboard = "1" then
					txt = replace(txt,"$billboard_rcheck","checked")
					txt = replace(txt,"$billboard_wcheck","")
					txt = replace(txt,"$billboard_nocheck","")
				elseif billboard="2" then
					txt = replace(txt,"$billboard_rcheck","")
					txt = replace(txt,"$billboard_wcheck","checked")
					txt = replace(txt,"$billboard_nocheck","")
				else
					txt = replace(txt,"$billboard_rcheck","")
					txt = replace(txt,"$billboard_wcheck","")
					txt = replace(txt,"$billboard_nocheck","checked")
				end if
				if card = "1" then
					txt = replace(txt,"$card_rcheck","checked")
					txt = replace(txt,"$card_wcheck","")
					txt = replace(txt,"$card_nocheck","")
				elseif card="2" then
					txt = replace(txt,"$card_rcheck","")
					txt = replace(txt,"$card_wcheck","checked")
					txt = replace(txt,"$card_nocheck","")
				else
					txt = replace(txt,"$card_rcheck","")
					txt = replace(txt,"$card_wcheck","")
					txt = replace(txt,"$card_nocheck","checked")
				end if
				if cstatic = "1" then
					txt = replace(txt,"$static_rcheck","checked")
					txt = replace(txt,"$static_wcheck","")
					txt = replace(txt,"$static_nocheck","")
				elseif cstatic="2" then
					txt = replace(txt,"$static_rcheck","")
					txt = replace(txt,"$static_wcheck","checked")
					txt = replace(txt,"$static_nocheck","")
				else
					txt = replace(txt,"$static_rcheck","")
					txt = replace(txt,"$static_wcheck","")
					txt = replace(txt,"$static_nocheck","checked")
				end if
				if poeple = "1" then
					txt = replace(txt,"$people_rcheck","checked")
					txt = replace(txt,"$people_wcheck","")
					txt = replace(txt,"$people_nocheck","")
				elseif people="2" then
					txt = replace(txt,"$people_rcheck","")
					txt = replace(txt,"$people_wcheck","checked")
					txt = replace(txt,"$people_nocheck","")
				else
					txt = replace(txt,"$people_rcheck","")
					txt = replace(txt,"$people_wcheck","")
					txt = replace(txt,"$people_nocheck","checked")
				end if
								
				response.write txt & chr(10)
			end if
		loop
	else
		response.write "畫面檔不存在!"
	end if
	conn.close
	set conn=nothing

%>