<!--#include file="admin_news_chk.asp"-->
<!--#include file="../UpLoad_Class.asp"-->
<!--#include file="../I_SendMail_at.asp"-->
<%
    if Session("news")<>"2" then
        session("message") = "很抱歉，您無修改資料權限！"
        response.redirect "admin_partner.asp?search=" & search & "&page=" & page
    end if
dim upload
set upload = new AnUpLoad
upload.Exe = "jpg|bmp|jpeg|gif|png|pdf|txt|doc|xls|docx|xlsx"
upload.MaxSize = 8 * 1024 * 1024 '8M
upload.SingleSize = 2 * 1024 * 1024 '2M
upload.Charset="utf-8"
upload.GetData()
'response.write upload.forms("flg")
'response.end
'session("agree")="yes"
if upload.ErrorID>0 then
	response.write "<script>alert('"&upload.Description&"');history.go(-1);</Script>"
	response.end
else
	validcode = upload.forms("validcode")
	if Session("GetCode") <> validcode then
		session("message") = "驗證碼輸入錯誤～請重新確認是否輸入正確！"
		response.write "<script>alert('"&session("message")&"');history.go(-1);</Script>"
		session("message") = ""
		response.end
	end if
	if (upload.forms("flg")="update") then
		if upload.forms("autoid")="" then
			session("message") = "編號空白！"
		end if
		if upload.forms("company_title")="" then
			session("message") = "您與日月光的關係沒有選擇！"
		end if
		if upload.forms("username")="" then
			session("message") = "您的姓名空白！"
		end if
		if upload.forms("company_title")="" then
			session("message") = "公司名稱空白！"
		end if
		if upload.forms("company_phone")="" then
			session("message") = "電話號碼空白！"
		end if
		if upload.forms("email")="" then
			session("message") = "電子郵件空白！"
		end if
		if upload.forms("company_fax")="" then
			session("message") = "涉及本案的日月光廠區沒有選擇！"
		end if
		if upload.forms("iuser_name")="" then
			session("message") = "本案所涉及之日月光人員姓名空白！"
		end if
		if upload.forms("iuser_unit")="" then
			session("message") = "本案所涉及之日月光人員部門空白！"
		end if
		if upload.forms("iuser_jobtitle")="" then
			session("message") = "本案所涉及之日月光人員職稱空白！"
		end if
		if upload.forms("company_url")="" then
			session("message") = "您舉報的議題沒有選擇！"
		end if
		if upload.forms("cellphone")="" then
			session("message") = "您是如何得知此違規行為沒有選擇！"
		end if
		if upload.forms("business")="" then
			session("message") = "本案違反道德與誠信行為之內容或事件經過空白！"
		end if

		if session("message") = "" then
			autoid			=upload.forms("autoid")
			aid=right("00000"&autoid,6)
			id				=upload.forms("id")
			company_title	=upload.forms("company_title")
			company_phone	=upload.forms("company_phone")
			company_fax		=upload.forms("company_fax")
			company_url		=upload.forms("company_url")
			company_add		=upload.forms("company_add")
			sex				=upload.forms("sex")
			username		=upload.forms("username")
			unit			=upload.forms("unit")
			job_title		=upload.forms("job_title")
			phone_ext		=upload.forms("phone_ext")
			cellphone		=upload.forms("cellphone")
			email			=upload.forms("email")
			atype			=upload.forms("type")
			business		=upload.forms("business")
			factory_number	=upload.forms("factory_number")
			factory_add		=upload.forms("factory_add")
			brand			=upload.forms("brand")
			uninumber		=upload.forms("uninumber")
			money1			=upload.forms("money1")
			money2			=upload.forms("money2")
			customer		=upload.forms("customer")
			iuser_name		=upload.forms("iuser_name")
			iuser_unit		=upload.forms("iuser_unit")
			iuser_jobtitle	=upload.forms("iuser_jobtitle")

			'其他選項
			supplier		=upload.forms("id_others")
			equipment		=upload.forms("company_fax_others")
			apply			=upload.forms("company_url_others")
			function_way	=upload.forms("cellphone_others")

			'csql = "select company_title from partner where company_title='" & company_title & "'"
			'set rs=conn.execute(csql)
			'if not rs.eof then
			'	session("message") = "公司抬頭重覆！"
			'else
				n=now()
				now_time= year(n) & "/" & month(n) & "/" & day(n) & " " & hour(n) & ":" & minute(n) & ":" & second(n)
				csql	=	"update partner " &_
								" set upt_time = '" & now_time & "'," &_
								"upt_als='" & Session("USER") & "'," &_
								"company_title='" & company_title & "'," &_
								"company_phone='" & company_phone & "'," &_
								"company_fax='" & company_fax & "'," &_
							    "company_url = '" & company_url & "'," &_
								"sex='" & sex & "'," &_
								"company_add='" & company_add & "'," &_
								"username='" & username & "'," &_
								"unit='" & unit & "'," &_
								"job_title='" & job_title & "'," &_
								"phone_ext='" & phone_ext & "'," &_
								"cellphone='" & cellphone & "'," &_
								"email='" & email & "'," &_
								"type='" & atype & "'," &_
								"business='" & business & "'," &_
								"factory_number='" & factory_number & "'," &_
								"factory_add='" & factory_add & "'," &_
								"brand='" & brand & "'," &_
								"uninumber='" & uninumber & "'," &_
								"money1='" & money1 & "'," &_
								"money2='" & money2 & "'," &_
								"customer='" & customer & "'," &_
								"supplier='" & supplier & "'," &_
								"equipment='" & equipment & "'," &_
								"apply='" & apply & "'," &_
								"president='" & president & "'," &_
								"founded_date='" & founded_date & "'," &_
								"factory_tel='" & factory_tel & "'," &_
								"employee_number='" & employee_number & "'," &_
								"function_way='" & function_way & "'," &_
								"introduction='" & introduction & "'," &_
								"id='" & id & "'" &_
							" where autoid=" & autoid
				'response.write csql
				conn.execute(csql)
				conn.execute("delete from iuser where autoid_fk="&autoid)
				iuser_name_array=split(iuser_name,",")
				iuser_unit_array=split(iuser_unit,",")
				iuser_jobtitle_array=split(iuser_jobtitle,",")
				For i=LBound(iuser_name_array) to UBound(iuser_name_array)
					csql="insert into iuser (autoid_fk,iuser_name,iuser_unit,iuser_jobtitle) values ('" & autoid & "','" & iuser_name_array(i) & "','" & iuser_unit_array(i) & "','" & iuser_jobtitle_array(i) & "')"
					'Response.Write csql
					conn.execute(csql)
				Next
				savepath="../ufiles"
				set file1 = upload.files("file1")
				if file1.isfile then
					file1.UserSetName=autoid&"_"&year(n)&month(n)&day(n)&"_1"
					result = file1.saveToFile(savepath,-1,true)
					file1name=file1.filename
					if result then
						csql="update partner " &_
										" set file1 = '" & file1.filename & "' where autoid=" & autoid
						conn.execute(csql)
						'response.Write "文件'" & file.LocalName & "'上传成功，保存位置'" & server.MapPath(savepath & "/" & file.filename) & "',文件大小" & file.size & "字节"
					else
						'response.Write file.Exception
						'response.end
					end if
				else
					file1name=""
				end if
				set file2 = upload.files("file2")
				if file2.isfile then
					file2.UserSetName=autoid&"_"&year(n)&month(n)&day(n)&"_2"
					result = file2.saveToFile(savepath,-1,true)
					file2name=file2.filename
					if result then
						csql="update partner " &_
										" set file2 = '" & file2.filename & "' where autoid=" & autoid
						conn.execute(csql)
					end if
				else
					file2name=""
				end if
				set file3 = upload.files("file3")
				if file3.isfile then
					file3.UserSetName=autoid&"_"&year(n)&month(n)&day(n)&"_3"
					result = file3.saveToFile(savepath,-1,true)
					file3name=file3.filename
					if result then
						csql="update partner " &_
										" set file3 = '" & file3.filename & "' where autoid=" & autoid
						conn.execute(csql)
					end if
				else
					file3name=""
				end if
				set file4 = upload.files("file4")
				if file4.isfile then
					file4.UserSetName=autoid&"_"&year(n)&month(n)&day(n)&"_4"
					result = file4.saveToFile(savepath,-1,true)
					file4name=file4.filename
					if result then
						csql="update partner " &_
										" set file4 = '" & file4.filename & "' where autoid=" & autoid
						conn.execute(csql)
					end if
				else
					file4name=""
				end if

				conn.close
				set conn=nothing
				session("message") = "案件編號： "&aid&" 修改成功！"
				response.redirect "admin_partner_detail.asp?autoid="&autoid
			'end if
		else
			response.write "<script>alert('"&session("message")&"');history.go(-1);</Script>"
			session("message") = ""
			response.end
		end if
	end if
	'response.redirect "ch.asp"
end if
Function ReadTextFile(filePath,CharSet)
       dim stm
       set stm=Server.CreateObject("adodb.stream")
       stm.Type=1
       stm.Mode=3
       stm.Open
       stm.LoadFromFile filePath
       stm.Position=0
       stm.Type=2
       stm.Charset=CharSet
       ReadTextFile = stm.ReadText
       stm.Close
       set stm=nothing
End Function
%>