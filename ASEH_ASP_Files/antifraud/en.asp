<!--#include file="odbc.htw"-->
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<link rel="alternate" hreflang="en" href="https://www.aseglobal.com/antifraud/en.asp" />
<link rel="alternate" hreflang="zh-tw" href="https://www.aseglobal.com/antifraud/ch.asp" />
<meta name="description" content="ASE Technology Holding Code of Conduct Compliance Reporting System" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>ASE Technology Holding | Code of Conduct Compliance Reporting System</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/ase-box.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
<script src="js/respond.min.js"></script>
<![endif]-->
<script>
$(document).ready(function(){
    $("#mainContentAgreement").click(function(){
      if (this.checked) {
        $("#fileReport").removeAttr('disabled');
      } else {
        $("#fileReport").attr("disabled", true);
      }
    });
    $("#fileReport").click(function(){
        $(".to").fadeOut("slow");
        $(".co").fadeIn("slow");
    });
<% 'session("agree")="yes"
if session("agree")="yes" then%>
    $("#fileReport").removeAttr('disabled');
    $("#mainContentAgreement").attr("checked", true);
    $("#fileReport").click();
<%end if%>
    $('body').on('click', '.more', function(e){
        //var iuser_count = parseInt($("#iuser_count").val());
        var iuser_count = $(".iuser").length;
        var count = iuser_count + 1;
        $("#iuser_count").val(count);
        if(iuser_count==0)$(this).remove();
        var last=$(this).parent().parent().last().find('.iuser');
        //console.log(last);
        last.html('<i class="glyphicon glyphicon-minus"></i>');
        last.removeClass('more');
        last.addClass('x');
        last.removeClass('btn-info');
        last.addClass('btn-danger');
        $("#iuser_area").append('<div class="input-group">\n\
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>\n\
                                <input type="text" class="form-control" name="iuser_name" placeholder="Name"/>\n\
                                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>\n\
                                <input type="text" class="form-control" required name="iuser_manufacturingsites" placeholder="Manufacturing Sites"/>\n\
                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>\n\
                                <input type="text" class="form-control" name="iuser_unit" placeholder="Department"/>\n\
                                <span class="input-group-addon"><i class="glyphicon glyphicon-pawn"></i></span>\n\
                                <input type="text" class="form-control" name="iuser_jobtitle" placeholder="Title" />\n\
                                <span class="input-group-btn data-up">\n\
                                <button type="button" class="btn btn-default btn-info more iuser" id="more'+count+'" data-dir="up"><i class="glyphicon glyphicon-plus"></i></button></span></div>');

    });
    $('body').on('click', '.x', function(e){
        var iuser_count = parseInt($("#iuser_count").val())-1;
        //console.log(iuser_count);
        $("#iuser_count").val(iuser_count);
        //console.log($("#iuser_count").val());
        $(this).parent().parent().remove();
    });
    $('body').on('change', '.upload', function(e){
        console.log($(this).val());
        $('#'+$(this).attr('name')).val($(this).val());
    });
    $('body').on('change', '.others', function(e){
        if($(this).val()=='其他' || $(this).val()=='Others'){
            $('#'+$(this).attr('name')+'_others').attr('disabled',false);
            $('#'+$(this).attr('name')+'_others').attr('required',true);
        }else{
            $('#'+$(this).attr('name')+'_others').attr('required',false);
            $('#'+$(this).attr('name')+'_others').attr('disabled',true);
            $('#'+$(this).attr('name')+'_others').val('');
        }
    });
});
</script>
</head>
<body>
    <header id="fh5co-header-section">
        <div class="container">
            <div class="nav-header">
            	<div class="col-md-2"></div>
            	<div class="col-md-2">
       		    	<img src="images/ASE_logo_en.png" width="185">
                </div>
           	  	<div class="col-md-6" style="padding-top:10px">
              		<h3>Code of Conduct Compliance Reporting System</h3>
                </div>
            	<div class="col-md-2">
                    <div class="ASEHeader">
                        <div class="ASEHeader__Lang">
                            <a href="en.asp" class="active">English</a>
                            <a href="ch.asp">中文</a>
                        </div>
                    </div>
                </div>
          </div>
        </div>
    </header>
    <!-- end:header-top -->
    <div class="container">
        <div class="to">
            <h5 style="padding:40px 0 20px 0">One of the most important assets of ASE is our integrity, professionalism and just reputation. We strive to create an honest and responsible culture. Any corruption and fraud will not be allowed. Please contact us if you find any violation, suspected violation or any conduct that could result in a violation of ASE's ethical standards by an ASE’s member.</h5>
            <h4>Reminder:</h4>
            <h5>
            <ol style="padding-left:20px;">
                <li>ASE may use your personal information, including name, telephone number and email address, to facilitate investigation or to contact you. If necessary, ASE may provide your personal information  to third parties that are appointed by ASE to assist in the investigation. Information regarding the process of your personal information you will find in ASE's <a href="/content/en/ase_privacy.html" target="_blank"><b>Privacy Policy</b></a>.</li>
                <li>Unless otherwise provided by laws, ASE, including the third parties appointed by ASE to assist in related investigation, will maintain the confidentiality of your personal information and protect your identity and privacy to the fullest extent permitted by law.</li>
                <li>You may not act maliciously or knowingly and willfully make a false statement. You shall assume liability for the allegations that prove to have been made maliciously or knowingly to be false.</li>
                <li>You have right to make a report through this system anonymously. However, it is difficult to initiate or continue the investigation or to prove the existence of a violation if you do not provide specific evidence, information or documentations in your report. If ASE is unable to prove the existence of a violation and you are out of ASE&rsquo;s reach, the investigation will be closed and ASE will only keep the corresponding reporting for reference purpose.</li>
                <li>To promptly act to investigate and/or resolve the issue, please provide as much detailed information and documents as possible. Please note that if the information or documents are insufficient, the investigation may be hampered.</li>
                <li>You shall maintain the confidentiality of any or part of information of any communication between you and ASE. Unless otherwise required by law, you shall not file a lawsuit for the information of any communication between you and ASE, nor file a lawsuit by using information of any communication between you and ASE.</li>
            </ol></h5>
            <h5>
            <input type="checkbox" name="checkbox" id="mainContentAgreement">
             I have read and understood the above statement, and agreed to ASE's collection, processing and use of my personal information in accordance with the purposes as stated above.<br>
              <br>
          <button type="button" class="btn btn-info btn-sm" disabled="disabled" id="fileReport">File a new Report</button></h5>
            <noscript><h3 class="text-danger">Please turn on Javascript function.</h5></noscript>
		</div>
        <div class="co" style="display:none;">
        	<h5 style="padding:40px 0 10px 0">Please note that this System is designed for reporting the violation of ASE's ethical standards by an ASE’s members. All information submitted in this report will be maintained confidential and delivered to ASE management only. Please directly contact corresponding departments for emergency reporting that requires immediate attentions and verbal communication.<br>
        	  <br>
          Please provide information as follows: (<span style="color:#CC0000">*: Required fields</span>)</h5>
<form method="post" action="enm.asp" enctype="multipart/form-data">
<input type="hidden" name="iuser_count" id="iuser_count" value="1">
<input type="hidden" name="flg" value="add">
                <div class="row">
                    <div class="col-md-12">
                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">What is your relationship to ASE？<span style="color:#CC0000">*</span></label>
                                      <div class="input-group">
                                        <select name="id" required class="form-control others">
                                            <option value="" selected>Choose One</option>
                                            <option value="Customers">Customers</option>
                                            <option value="Employees">Employees</option>
                                            <option value="Shareholders">Shareholders</option>
                                            <option value="Suppliers/Contractor">Suppliers/Contractor</option>
                                            <option value="Community">Community</option>
                                            <option value="Government">Government</option>
                                            <option value="Industry Unions/Associations">Industry Unions/Associations</option>
                                            <option value="NGOs">NGOs</option>
                                            <option value="Media">Media</option>
                                            <option value="Others">Others</option>
                                        </select>
											<span class="input-group-addon"></span>
                                          <input type="text" class="form-control" name="id_others" id="id_others" size="2" disabled />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="cols-sm-2 control-label">Please provide your name and contact information:<br>
                                        (Please note your information will be remain anonymous and confidential during the entire reporting/investigating process.)</label>
                                        <div class="cols-sm-6">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                                <input type="text" class="form-control" name="username" placeholder="Your Name"/>
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
                                                <input type="text" class="form-control" name="company_title" placeholder="Company"/>
												<span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                                <input type="text" class="form-control" name="company_phone" placeholder="Phone" />
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                                <input type="email" class="form-control" name="email" placeholder="E-mail" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">The company of ASE Group involving:<span style="color:#CC0000">*</span></label>
                                      <div class="input-group">
                                        <select name="company_fax" required class="form-control others">
                                            <option value="" selected>Choose One</option>
                                            <option value="ASE Industrial Holding Co., Ltd">ASE Industrial Holding Co., Ltd</option>
                                            <option value="Advanced Semiconductor Engineering, Inc. and its subsidiaries">Advanced Semiconductor Engineering, Inc. and its subsidiaries</option>
                                            <option value="Siliconware Precision Industries Co., Ltd. and its subsidiaries">Siliconware Precision Industries Co., Ltd. and its subsidiaries</option>
                                            <option value="Universal Scientific Industrial (Shanghai) Co., Ltd. and its subsidiaries">Universal Scientific Industrial (Shanghai) Co., Ltd. and its subsidiaries</option>
                                            <option value="Shanghai Ding Hui Real Estate Development Co., Ltd. and its subsidiaries">Shanghai Ding Hui Real Estate Development Co., Ltd. and its subsidiaries</option>
                                            <option value="Others">Others</option>
                                        </select>
											<span class="input-group-addon"></span>
                                          <input type="text" class="form-control" name="company_fax_others" id="company_fax_others" required size="2" disabled />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="control-label">Please identify the manufacturing sites and individual(s) engaged in this behavior:<span style="color:#CC0000">*</span></label>
                                        <div class="cols-sm-6" id="iuser_area">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                                <input type="text" class="form-control" required name="iuser_name" placeholder="Name"/>
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                                <input type="text" class="form-control" required name="iuser_manufacturingsites" placeholder="Manufacturing Sites"/>
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                                <input type="text" class="form-control" required name="iuser_unit" placeholder="Department"/>
												<span class="input-group-addon"><i class="glyphicon glyphicon-pawn"></i></span>
                                                <input type="text" class="form-control" required name="iuser_jobtitle" placeholder="Title" />
                                                <span class="input-group-btn data-up">
                                                <button class="btn btn-default btn-info more" id="more0" data-dir="up"><i class="glyphicon glyphicon-plus"></i></button></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="name" class="control-label">The issue you are reporting about:<span style="color:#CC0000">*</span></label>
                                      <div class="input-group">
                                        <select name="company_url" required class="form-control others">
                                            <option value="" selected>Choose One</option>
                                            <option value="Corruption">Corruption</option>
                                            <option value="Conflicts of Interest">Conflicts of Interest</option>
                                            <option value="Insider Trading">Insider Trading</option>
                                            <option value="Fair Competition and Antitrust">Fair Competition and Antitrust</option>
                                            <option value="Secret Divulgence">Secret Divulgence</option>
                                            <option value="Privacy or Personal Information Protection">Privacy or Personal Information Protection</option>
                                            <option value="Others">Others</option>
                                        </select>
											<span class="input-group-addon"></span>
                                          <input type="text" class="form-control" name="company_url_others" id="company_url_others" required size="2" disabled />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="name" class="control-label">Where did this incident or violation occur?</label>
                                      <input type="text" class="form-control" name="company_add" id="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Please provide the specific or approximate time of this incident?</label>
                                     	<input type="text" class="form-control" name="unit" id="" placeholder="Example: Monday, March 28, 2012; Approximately a month ago...">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">How long do you think this problem has persisted?</label>
                                        <div class="input-group">
                                            <select name="job_title" class="form-control">
                                                <option value="" selected>Choose One</option>
                                                <option value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                            </select>
                                            <span class="input-group-addon">Year(s)</span>
                                            <select name="phone_ext" class="form-control">
                                                <option value="" selected>Choose One</option>
                                                <option value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                            </select>
                                            <span class="input-group-addon">Month(s)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">How did you become aware of this violation?<span style="color:#CC0000">*</span></label>
                                      <div class="input-group">
                                        <select name="cellphone" required class="form-control others">
                                            <option value="" selected>Choose One</option>
                                            <option value="Personal Experience">Personal experience</option>
                                            <option value="Experiences sharing from colleagues, relatives, and friends">Experiences sharing from colleagues, relatives, and friends</option>
                                            <option value="Hearsay">Hearsay</option>
                                            <option value="Inference">Inference</option>
                                            <option value="Internet">Internet</option>
                                            <option value="Others">Others</option>
                                        </select>
											<span class="input-group-addon"></span>
                                          <input type="text" class="form-control" name="cellphone_others" id="cellphone_others" required size="2" disabled />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="control-label">Is management level aware of this behavior?</label>
                                        <div class="cols-sm-6">
                                            <div class="btn-group" data-toggle="buttons">
                                                <label class="btn btn-default">
                                                    <input type="radio" name="type" value="yes">Yes
                                                </label>
                                                <label class="btn btn-default">
                                                    <input type="radio" name="type" value="no">No
                                                </label>
                                                <label class="btn btn-default">
                                                    <input type="radio" name="type" value="Do Not Know">Do Not Know
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="name" class="control-label">Please describe the course of the event and details of this incident:<span style="color:#CC0000">*</span></label>
                                        <div class="cols-sm-6">
                                          <textarea name="business" rows="7" class="form-control"></textarea>
                                      </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="control-label">As far as your knowledge, is there any individual who took measures to conceal this violation? If yes, please describe the steps the individual(s) took to conceal this violation:</label>
                                        <div class="cols-sm-6">
                                          <textarea name="factory_number" rows="7" class="form-control" ></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Please provide any information regarding the alleged violation that could be useful in the investigation and resolving this situation:</label>
                                        <textarea name="factory_add" rows="7" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="control-label">If you have any supporting evidence, please upload:</label>
                                        <div class="cols-sm-6">
                                            <div class="input-group" style="padding-bottom:5px">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
                                                <input type="text" class="form-control" id="file1">
                                                <span class="input-group-btn data-up">
                                                <div class="fileUpload btn btn-default btn-info" data-dir="up"><input type="file" class="upload" name="file1">Select File</div></span>
                                            </div>
                                            <div class="input-group" style="padding-bottom:5px">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
                                                <input type="text" class="form-control" id="file2">
                                                <span class="input-group-btn data-up">
                                                <div class="fileUpload btn btn-default btn-info" data-dir="up"><input type="file" class="upload" name="file2">Select File</div></span>
                                            </div>
                                            <div class="input-group" style="padding-bottom:5px">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
                                                <input type="text" class="form-control" id="file3">
                                                <span class="input-group-btn data-up">
                                                <div class="fileUpload btn btn-default btn-info" data-dir="up"><input type="file" class="upload" name="file3">Select File</div></span>
                                            </div>
                                            <div class="input-group" style="padding-bottom:5px">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
                                                <input type="text" class="form-control" id="file4">
                                                <span class="input-group-btn data-up">
                                                <div class="fileUpload btn btn-default btn-info" data-dir="up"><input type="file" class="upload" name="file4">Select File</div></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                		<label for="name" class="control-label">Please enter the code exactly as you see it in the image:</label>
<input type="text" name="validcode" style="width:120px;display:inline;" maxlength="4" class="form-control" placeholder="4 Numbers" />
 <a href="javascript:void(0)" title="Click to refresh" onclick="RefreshImage('imgCaptcha')"><img id="imgCaptcha" src="vcode.asp" /></a>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary pull-left" id="btnContactUs">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>

<footer>
    <div id="footer">
      <div class="container">
            <div class="row">
              <div class="col-md-6 col-md-offset-3 text-center">
                <h6 style="color:#aaaaaa">© 2017 ASE Group. All rights reserved.</h6>
              </div>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
<script language="javascript">
<!--
    function RefreshImage(valImageId) {
        var objImage = document.images[valImageId];
        if (objImage == undefined) {
            return;
        }
        var now = new Date();
        objImage.src = objImage.src.split('?')[0] + '?x=' + now.toUTCString();
    }
    document.write('<img src=static.asp?area='+escape("Code of Conduct Compliance Reporting System")+'>');
-->
</script>
<%
if session("message") <> "" then
    response.write "<script language='javascript'>"
    response.write "    setTimeout(function() {alert ('" & session("message") & "');},1250);"
    response.write "RefreshImage('imgCaptcha');"
    response.write "</script>"
    session("message") = ""
end if
%>
