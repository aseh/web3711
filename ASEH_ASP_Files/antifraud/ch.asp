<!--#include file="odbc.htw"-->
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<link rel="alternate" hreflang="en" href="https://www.aseglobal.com/antifraud/en.asp" />
<link rel="alternate" hreflang="zh-tw" href="https://www.aseglobal.com/antifraud/ch.asp" />
<meta name="description" content="日月光投控商業行為與道德遵循舉報系統" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>日月光投控 | 商業行為與道德遵循舉報系統</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/ase-box.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
<script src="js/respond.min.js"></script>
<![endif]-->
<script>
$(document).ready(function(){
    $("#mainContentAgreement").click(function(){
	  if (this.checked) {
		$("#fileReport").removeAttr('disabled');
	  } else {
		$("#fileReport").attr("disabled", true);
	  }
	});
    $("#fileReport").click(function(){
        $(".to").fadeOut("slow");
        $(".co").fadeIn("slow");
    });
<% 'session("agree")="yes"
if session("agree")="yes" then%>
    $("#fileReport").removeAttr('disabled');
    $("#mainContentAgreement").attr("checked", true);
    $("#fileReport").click();
<%end if%>
    $('body').on('click', '.more', function(e){
        //var iuser_count = parseInt($("#iuser_count").val());
        var iuser_count = $(".iuser").length;
        var count = iuser_count + 1;
        $("#iuser_count").val(count);
        if(iuser_count==0)$(this).remove();
        var last=$(this).parent().parent().last().find('.iuser');
        //console.log(last);
        last.html('<i class="glyphicon glyphicon-minus"></i>');
        last.removeClass('more');
        last.addClass('x');
        last.removeClass('btn-info');
        last.addClass('btn-danger');
        $("#iuser_area").append('<div class="input-group">\n\
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>\n\
                                <input type="text" class="form-control" name="iuser_name" placeholder="姓名"/>\n\
                                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>\n\
                                <input type="text" class="form-control" required name="iuser_manufacturingsites" placeholder="廠區"/>\n\
                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>\n\
                                <input type="text" class="form-control" name="iuser_unit" placeholder="部門"/>\n\
                                <span class="input-group-addon"><i class="glyphicon glyphicon-pawn"></i></span>\n\
                                <input type="text" class="form-control" name="iuser_jobtitle" placeholder="職稱" />\n\
                                <span class="input-group-btn data-up">\n\
                                <button type="button" class="btn btn-default btn-info more iuser" id="more'+count+'" data-dir="up"><i class="glyphicon glyphicon-plus"></i></button></span></div>');

    });
    $('body').on('click', '.x', function(e){
        var iuser_count = parseInt($("#iuser_count").val())-1;
        //console.log(iuser_count);
        $("#iuser_count").val(iuser_count);
        //console.log($("#iuser_count").val());
        $(this).parent().parent().remove();
    });
    $('body').on('change', '.upload', function(e){
        console.log($(this).val());
        $('#'+$(this).attr('name')).val($(this).val());
    });
    $('body').on('change', '.others', function(e){
        if($(this).val()=='其他' || $(this).val()=='Others'){
            $('#'+$(this).attr('name')+'_others').attr('disabled',false);
            $('#'+$(this).attr('name')+'_others').attr('required',true);
        }else{
            $('#'+$(this).attr('name')+'_others').attr('required',false);
            $('#'+$(this).attr('name')+'_others').attr('disabled',true);
            $('#'+$(this).attr('name')+'_others').val('');
        }
    });
});
</script>
</head>
<body>
    <header id="fh5co-header-section">
        <div class="container">
            <div class="nav-header">
            	<div class="col-md-2"></div>
            	<div class="col-md-2">
       		    	<img src="images/ASE_logo.png?1" width="160">
                </div>
           	  	<div class="col-md-5" style="padding-top:10px">
              		<h3>商業行為與道德遵循舉報系統</h3>
                </div>
            	<div class="col-md-3">
                    <div class="ASEHeader">
                        <div class="ASEHeader__Lang">
                            <a href="en.asp">English</a>
                            <a href="ch.asp" class="active">中文</a>
                        </div>
                    </div>
                </div>
          </div>
        </div>
    </header>
    <!-- end:header-top -->
    <div class="container">
        <div class="to">
            <h5 style="padding:40px 0 20px 0">日月光最重要的資產之一，就是我們正直、專業及公正的名聲。我們努力培養誠實與負責任的文化，絕不允許任何貪污舞弊的行為。如果您發現日月光人員有任何違反、疑似違反或可能導致違反日月光道德規範的行為時，敬請向我們提出舉報。</h5>
            <h4>舉報須知：</h4>
            <h5>
            <ol style="padding-left:20px;">
                <li>您所提供之個人資料，如含有姓名、電話、地址及電子郵件等資訊，日月光公司將為調查舉報事項與聯繫用途而於舉報調查期間處理利用之；調查若有必要，日月光公司得傳遞您必要之個人資訊予日月光委託協助調查舉報事項之第三方協助單位。有關您個人資料處理的相關規定，請參閱日月光<a href="/content/ch/ase_privacy.html" target="_blank"><b>隱私權政策</b></a>。</li>
                <li>除法律另有規定外，日月光（包含受日月光委託調查之第三方）對於您所提供的個人資料將予以保密，並依法採取適當保護措施保護您的個人資料及隱私。</li>
                <li>您不得明知不實而故意捏造事實進行舉報，倘被證明是出於惡意或有故意捏造虛偽陳述之情事，您必須自負相關法律責任。</li>
                <li>您可以匿名提出舉報。但匿名舉報如未同時提出具體事證供日月光調查，則可能有難以調查或事證不足的疑慮。如案件事證不足且無法聯繫舉報人，日月光僅能將案件存參結案。</li>
                <li>為能盡早調查與解決問題，請您務必提供相關具體資訊與文件，若相關資訊與文件不足時，日月光將難以進行調查。</li>
                <li>如日月光為舉報案件與您聯繫，您必須對該聯繫內容完全保密，除非經法律要求，您不得對該聯繫內容提起訴訟，或以該聯繫內容作為訴訟資料。</li>
            </ol></h5>
            <h5>
            <input type="checkbox" name="checkbox" id="mainContentAgreement"> 本人已詳閱且充分瞭解上述說明，並同意日月光於符合上述說明目的之範圍內蒐集、處理及利用本人之個人資料。<br>
              <br>
            <button type="button" class="btn btn-info btn-sm" disabled="disabled" id="fileReport">請按此進行舉報</button></h5>
            <noscript><h3 class="text-danger">請先開啟 Javascript 功能。</h3></noscript>
		</div>
        <div class="co" style="display:none;">
        	<h5 style="padding:40px 0 10px 0">本系統為舉報日月光人員違反道德與誠信行為之系統，在這裡所提交的任何訊息，將全部予以保密並直接遞交至日月光管理高層。如您要反應的內容為緊急事件，敬請直接聯絡日月光各權責部門，謝謝。
舉報日月光人員違反道德與誠信之行為，請提供以下訊息：〈<span style="color:#CC0000">*：必須填寫</span>〉</h5>
<form method="post" action="chm.asp" enctype="multipart/form-data">
<input type="hidden" name="iuser_count" id="iuser_count" value="1">
<input type="hidden" name="flg" value="add">
                <div class="row">
                    <div class="col-md-12">
                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">您與日月光的關係：<span style="color:#CC0000">*</span></label>
                                      <div class="input-group">
                                        <select name="id" required class="form-control others">
                                            <option value="" selected>請選擇</option>
                                            <option value="客戶">客戶</option>
                                            <option value="員工">員工</option>
                                            <option value="股東">股東</option>
                                            <option value="供應商/承攬商">供應商/承攬商　　　　　　　　</option>
                                            <option value="社區">社區</option>
                                            <option value="政府">政府</option>
                                            <option value="產業工會/協會">產業工會/協會</option>
                                            <option value="非政府組織">非政府組織</option>
                                            <option value="媒體">媒體</option>
                                            <option value="其他">其他</option>
                                        </select>
											<span class="input-group-addon"></span>
                                          <input type="text" class="form-control" name="id_others" id="id_others" size="2" disabled />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="cols-sm-2 control-label">您的姓名與聯絡方式：〈您所提供的所有訊息均受保密〉</label>
                                        <div class="cols-sm-6">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                                <input type="text" class="form-control" name="username" placeholder="您的姓名"/>
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
                                                <input type="text" class="form-control" name="company_title" id="company_title"  placeholder="公司名稱"/>
												<span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                                <input type="text" class="form-control" name="company_phone" placeholder="電話號碼" />
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                                <input type="email" class="form-control" name="email" placeholder="電子郵件" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">請選擇涉及本案的日月光集團公司：<span style="color:#CC0000">*</span></label>
                                      <div class="input-group">
                                        <select name="company_fax" required class="form-control others">
                                            <option value="" selected>請選擇</option>
                                            <option value="ASE Industrial Holding Co., Ltd">日月光投資控股股份有限公司</option>
                                            <option value="Advanced Semiconductor Engineering, Inc. and its subsidiaries">日月光半導體製造股份有限公司及其子公司</option>
                                            <option value="Siliconware Precision Industries Co., Ltd. and its subsidiaries">矽品精密工業股份有限公司及其子公司</option>
                                            <option value="Universal Scientific Industrial (Shanghai) Co., Ltd. and its subsidiaries">環旭電子股份有限公司及其子公司</option>
                                            <option value="Shanghai Ding Hui Real Estate Development Co., Ltd. and its subsidiaries">上海鼎匯房地產開發有限公司及其子公司</option>
                                            <option value="Others">其他</option>
                                          </select>
											<span class="input-group-addon"></span>
                                          <input type="text" class="form-control" name="company_fax_others" id="company_fax_others" required size="2" disabled />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="control-label">請列出本案所涉及之日月光人員：<span style="color:#CC0000">*</span></label>
                                        <div class="cols-sm-6" id="iuser_area">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                                <input type="text" class="form-control" required name="iuser_name" placeholder="姓名"/>
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                                <input type="text" class="form-control" required name="iuser_manufacturingsites" placeholder="廠區"/>
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                                <input type="text" class="form-control" required name="iuser_unit" placeholder="部門"/>
												<span class="input-group-addon"><i class="glyphicon glyphicon-pawn"></i></span>
                                                <input type="text" class="form-control" required name="iuser_jobtitle" placeholder="職稱" />
                                                <span class="input-group-btn data-up">
                                                <button class="btn btn-default btn-info more" id="more0" data-dir="up"><i class="glyphicon glyphicon-plus"></i></button></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="name" class="control-label">您舉報的議題：<span style="color:#CC0000">*</span></label>
                                      <div class="input-group">
                                            <select name="company_url" required class="form-control others">
                                                <option value="" selected>請選擇</option>
                                                <option value="舞弊">舞弊</option>
                                                <option value="利益衝突">利益衝突</option>
                                                <option value="內線交易">內線交易</option>
                                                <option value="公平競爭">公平競爭</option>
                                                <option value="洩漏商業機密">洩漏商業機密</option>
                                                <option value="隱私或個資保護">隱私或個資保護</option>
                                                <option value="其他">其他</option>
                                            </select>
											<span class="input-group-addon"></span>
                                          <input type="text" class="form-control" name="company_url_others" id="company_url_others" required size="2" disabled />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="name" class="control-label">請說明本案發生的地點？</label>
                                      <input type="text" class="form-control" name="company_add" id="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">請說明本案發生的具體或大致時間？</label>
                                     	<input type="text" class="form-control" name="unit" id="" placeholder="例如：2012年3月28日 星期一；約一個月前等⋯">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">就您所知，您認為這個問題已持續多久？</label>
                                        <div class="input-group">
                                            <select name="job_title" class="form-control">
                                                <option value="" selected>請選擇</option>
                                                <option value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                            </select>
                                            <span class="input-group-addon">年</span>
                                            <select name="phone_ext" class="form-control">
                                                <option value="" selected>請選擇</option>
                                                <option value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                            </select>
                                            <span class="input-group-addon">月</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">您是如何得知此違規行為？<span style="color:#CC0000">*</span></label>
                                      <div class="input-group">
                                        <select name="cellphone" required class="form-control others">
                                            <option value="" selected>請選擇</option>
                                            <option value="親身經歷">親身經歷</option>
                                            <option value="同事及親友的經歷">同事及親友的經歷　　　　　　　　</option>
                                            <option value="傳聞">傳聞</option>
                                            <option value="推測">推測</option>
                                            <option value="網路">網路</option>
                                            <option value="其他">其他</option>
                                        </select>
											<span class="input-group-addon"></span>
                                          <input type="text" class="form-control" name="cellphone_others" id="cellphone_others" required size="2" disabled />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="control-label">就您所知，日月光管理階層是否知道此違規行為？</label>
                                        <div class="cols-sm-6">
                                            <div class="btn-group" data-toggle="buttons">
                                                <label class="btn btn-default"><input type="radio" name="type" value="是">是</label>
                                                <label class="btn btn-default"><input type="radio" name="type" value="否">否</label>
                                                <label class="btn btn-default"><input type="radio" name="type" value="不確定">不確定</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="name" class="control-label">請詳細描述本案違反道德與誠信行為之內容或事件經過：<span style="color:#CC0000">*</span></label>
                                        <div class="cols-sm-6">
                                          <textarea name="business" required rows="7" class="form-control"></textarea>
                                      </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="control-label">就您所知，該人員是否有隱瞞其違反道德與誠信行為的方法，其方法為何？</label>
                                        <div class="cols-sm-6">
                                          <textarea name="factory_number" rows="7" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">請提供關於本案的所有細節或任何有助調查的相關資訊：</label>
                                        <textarea name="factory_add" rows="7" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="control-label">若您有與本案相關或任何有助調查的文件或檔案，亦請上傳：</label>
                                        <div class="cols-sm-6">
                                            <div class="input-group" style="padding-bottom:5px">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
                                                <input type="text" class="form-control" id="file1">
                                                <span class="input-group-btn data-up">
                                                <div class="fileUpload btn btn-default btn-info" data-dir="up"><input type="file" class="upload" name="file1">選擇檔案</div></span>
                                            </div>
                                            <div class="input-group" style="padding-bottom:5px">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
                                                <input type="text" class="form-control" id="file2">
                                                <span class="input-group-btn data-up">
                                                <div class="fileUpload btn btn-default btn-info" data-dir="up"><input type="file" class="upload" name="file2">選擇檔案</div></span>
                                            </div>
                                            <div class="input-group" style="padding-bottom:5px">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
                                                <input type="text" class="form-control"  id="file3">
                                                <span class="input-group-btn data-up">
                                                <div class="fileUpload btn btn-default btn-info" data-dir="up"><input type="file" class="upload" name="file3">選擇檔案</div></span>
                                           </div>
                                            <div class="input-group" style="padding-bottom:5px">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
                                                <input type="text" class="form-control" id="file4">
                                                <span class="input-group-btn data-up">
                                                <div class="fileUpload btn btn-default btn-info" data-dir="up"><input type="file" class="upload" name="file4">選擇檔案</div></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                		<label for="name" class="control-label">請輸入圖片中的驗證碼：<span style="color:#CC0000">*</span></label>
                                        <input type="text" name="validcode" style="width:120px;display:inline;" maxlength="4" class="form-control" placeholder="四位數字" />
 <a href="javascript:void(0)" title="點我可更新驗證碼" onclick="RefreshImage('imgCaptcha')"><img id="imgCaptcha" src="vcode.asp" /></a>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary pull-left" id="btnContactUs">提交</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
<footer>
    <div id="footer">
      <div class="container">
            <div class="row">
              <div class="col-md-6 col-md-offset-3 text-center">
                <h6 style="color:#aaaaaa">© 2017 ASE Group. All rights reserved.</h6>
              </div>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
<script language="javascript">
<!--
    function RefreshImage(valImageId) {
        var objImage = document.images[valImageId];
        if (objImage == undefined) {
            return;
        }
        var now = new Date();
        objImage.src = objImage.src.split('?')[0] + '?x=' + now.toUTCString();
    }
    document.write('<img src=static.asp?area='+escape("商業行為與道德遵循舉報系統")+'>');
-->
</script>
<%
if session("message") <> "" then
    response.write "<script language='javascript'>"
    response.write "    setTimeout(function() {alert ('" & session("message") & "');},1250);"
    response.write "RefreshImage('imgCaptcha');"
    response.write "</script>"
    session("message") = ""
end if
%>
